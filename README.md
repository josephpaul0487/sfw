sfw
===
_Simple framework for android_

	You can see the logs if "debuggingEnabled" is "true" in AppConfig annotation . ie,  any errors in this frame work


How to configure
----------------
    1.  Create a class which extends Application
    2.  Annotate Application class with AppConfig - ** see below
    3.  Override onCreate method in Application class
    4.  Write following code in Application class onCreate method
	
        Sfw.setup(this);
	


AppConfig
---------

1.  Follow below steps to use built in database helper
-
        * Create a class witch extends SfwDatabase
        * Annotate it with DbConfig

            ** You should provide dbName, entity class names and version

        * Override onCreateDb,onDbCreated,onUpgradeDb and isLoggedIn to provide your custom codes

        * Set initializeDatabase to true in appconfig annotation  __default is true__
        * Set the above class as databaseClassType in the appconfig annotation   __mandatory__


2.  Follow below steps to use retrofit web api helper
-
        * Set initializeApiHelper to true     __default is true__
        * Set retrofitBaseUrl                 __mandatory__
        * You can override retrofitConnectionTimeout, retrofitReadTimeout and retrofitWriteTimeout
        * You can create a class which extends SfwApiHelper to override and provide your custom codes

            ** Create a class which extends SfwApiHelper
            ** Write the following code to get the instance   ---  It is a static method. So it is not necessary to use the same method name
	    	
                public static YourCalssName getInstance() {
                    return (YourCalssName) SfwApiHelper.getInstance();
                }
		
            ** You can override the following functions to use your own codes

                1. callSuccess
                2. callError
                3. handleResponse
                4. setDbValuesOnSuccess
                5. addMoreKeyValueForParams
                6. onApiCompleted
                7. addCustomHeaders
                8. onApiError

            ** You can create a new class which extends SfwClient to use Your own Client class in Retrofit

                *** Set retrofitClientClassType to your class name in Appconfig annotation


    3. You can use Preferences class to use shared prefference helper

         * You should extend the Preferences class to use your own codes

            ** Set preferenceClassType to your class name in Appconfig annotation


    4. setVmPolicy          ----  default is true

    5. debuggingEnabled     ----  default is false

    6. bindViewsIncludedPkgsStartsWith

        * You can provide any external  package names  that using SFW's @BindView or @OnClick annotation

        



SqlDatabase
-----------

	Use annotationProcessor in gradle to generate classes to set the tablenames to  fields
	The classname syntax is      className + "_tbl_SFW"

	@DbEntity(tables={"agent","customer"},useRetrofitAnnotationForColumnName=true)
	-
		1. You should annotatate classes by using @DbEntity
		2. You can use multiple tablenames for one class
		3. if useRetrofitAnnotationForColumnName is "true", you can use Retrofit's @SerializedName(value="column name") annotation for column name
		4. Entity class must have an empty constructor

	@DbPrimary
	-
		1. To set a coulmn as primary
		2. Only "long" type field is supported
		3. Should contain a primary key
		4. Multiple primary keys not supported.
		5. Will check for primary key in the entity class first. If not found it will check superclasses. 
		6. The first primary key will use to set primary key. All others will ignore.

	@DbExclude
	-
		1. You can ignore a field by using this annotation

	@DbAnnotation(columnName = "name",columnType = ColumnTypes.VARCHAR,length = 100)
	-   
		1. You can annotate a field by using this annotation 
		2. columnType : Only used to set the String dbType (TEXT or VARCHAR)
		3. length : You can set length to INTEGER and VARCHAR coulmn types
		4. The field name will set as coulmn name if you are not annotate the field with this or @SerializedName annotation OR "columnName" is empty

	@DbConfig(entities = {TestModel.class},dbName = "Test",debuggingEnabled = true,version = 28) This annotation is mandatory if you want to extend SqlDatabase
	-

You can see the logs if "debuggingEnabled" is "true" in this class or in AppConfig annotation . ie, Create | Update Database | any errors
-

 	AppDb extends SqlDatabase {

	  	@param db This method will called from {@link Sqlite#onCreate(SQLiteDatabase)}
		NOTE : don't call {@link #getInstance()} from this method. {@link SfwDatabase#getDb()} will null at this time 
		You can override and perform your own actions 
 
      	protected void onCreateDb(SQLiteDatabase db) {}


      	@param db This method will called from {@link Sqlite#onUpgrade(SQLiteDatabase, int, int)}}
     	NOTE : don't call {@link #getInstance()} from this method. {@link SfwDatabase#getDb()} will null at this time
	 	You can override and perform your own actions 
  
     	protected void onUpgradeDb(SQLiteDatabase db, int oldVersion, int newVersion) {}
	

     	@param ctx
     	This will call after creating {@link SQLiteDatabase} instance 
     	Now you can use {@link #getInstance()} or {@link #getDb()} method
    
    	protected void onDbCreated(@NonNull Context ctx) {}
	}



build.gradle
=-----------

root gradle
-----------

	allprojects {
	    repositories {
	        google()
	        jcenter()
	        maven { url "https://jitpack.io" }
	        maven {url "https://dl.bintray.com/josephpaul0487/JPTF"}
	    }

	}
app gradle
----------

	android {
		...
		 compileOptions {
	        sourceCompatibility JavaVersion.VERSION_1_8
	        targetCompatibility JavaVersion.VERSION_1_8
	    }
	}

	dependencies {
		implementation 'com.squareup.retrofit2:converter-gson:latest-version' 
		implementation 'jpt.sfw:jptf:latest-version'
		implementation 'com.hbb20:ccp:latest-version'
	}


PROGUARD
=-------

	-keep class jpt.sfw.** { *; }
	-keepattributes *Annotation*
	-keep interface jpt.sfw.** { *; }
	-keep enum jpt.sfw.** { *; }

	-keep class YOUR PACKAGE.** { *; }
	-keep interface YOUR PACKAGE.** { *; }
	-keep enum YOUR PACKAGE.** { *; }

