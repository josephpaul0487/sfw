package jpt.sfw.database;

import java.util.Objects;

public class DbTypeModel {
    String classFieldName;
    String columnName;
    int index;

    public DbTypeModel() {
    }

    public DbTypeModel(String classFieldName, String columnName,int index) {
        this.classFieldName = classFieldName;
        this.columnName = columnName;
        this.index=index;
    }

    public String getClassFieldName() {
        return classFieldName;
    }

    public DbTypeModel setClassFieldName(String classFieldName) {
        this.classFieldName = classFieldName;
        return this;
    }

    public String getColumnName() {
        return columnName;
    }

    public DbTypeModel setColumnName(String columnName) {
        this.columnName = columnName;
        return this;
    }

    public int getIndex() {
        return index;
    }

    public DbTypeModel setIndex(int index) {
        this.index = index;
        return this;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        DbTypeModel that = (DbTypeModel) o;
        return Objects.equals(classFieldName, that.classFieldName);
    }

    @Override
    public int hashCode() {
        return Objects.hash(classFieldName);
    }

    @Override
    public String toString() {
        return "DbTypeModel{" +
                "classFieldName='" + classFieldName + '\'' +
                ", columnName='" + columnName + '\'' +
                ", index=" + index +
                '}';
    }
}
