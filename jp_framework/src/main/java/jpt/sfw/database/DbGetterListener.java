package jpt.sfw.database;

import java.util.List;

import androidx.annotation.IntRange;
import androidx.annotation.NonNull;

public interface DbGetterListener<T> {
    void onComplete(@NonNull List<T> data, @NonNull String table, @NonNull Class<T> classType, @IntRange(from = 0) int listCount);
}
