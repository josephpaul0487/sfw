package jpt.sfw.database;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;

public class DbRemoverModel<T> {
    @NonNull
    Class<T> cls;
    @Nullable
    String tableName;
    @Nullable String selection;
    @Nullable String[] selectionArgs;
    @Nullable DbRemoverListener<T> remover;
    boolean useThread;

    public DbRemoverModel(@NonNull Class<T> cls) {
        this.cls = cls;
    }

    public DbRemoverModel(@NonNull Class<T> cls, @Nullable String tableName) {
        this.cls = cls;
        this.tableName = tableName;
    }

    public DbRemoverModel(@NonNull Class<T> cls, @Nullable String tableName, @Nullable DbRemoverListener<T> remover) {
        this.cls = cls;
        this.tableName = tableName;
        this.remover = remover;
    }


    @Nullable
    public String getTableName() {
        return tableName;
    }

    public DbRemoverModel<T> setTableName(@Nullable String tableName) {
        this.tableName = tableName;
        return this;
    }

    @Nullable
    public String getSelection() {
        return selection;
    }

    public DbRemoverModel<T> setSelection(@Nullable String selection) {
        this.selection = selection;
        return this;
    }

    @Nullable
    public String[] getSelectionArgs() {
        return selectionArgs;
    }

    public DbRemoverModel<T> setSelectionArgs(@Nullable String[] selectionArgs) {
        this.selectionArgs = selectionArgs;
        return this;
    }

    @Nullable
    public DbRemoverListener<T> getRemover() {
        return remover;
    }

    public DbRemoverModel<T> setRemover(@Nullable DbRemoverListener<T> remover) {
        this.remover = remover;
        return this;
    }

    public boolean isUseThread() {
        return useThread;
    }

    public DbRemoverModel<T> setUseThread(boolean useThread) {
        this.useThread = useThread;
        return this;
    }
}
