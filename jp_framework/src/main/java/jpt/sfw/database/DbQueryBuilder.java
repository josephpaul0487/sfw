package jpt.sfw.database;

import java.util.ArrayList;
import java.util.List;

import androidx.annotation.NonNull;

public class DbQueryBuilder {

    private final StringBuilder builder=new StringBuilder();
    private int groupStartCount;
    private boolean buildCalled;
    private final List<String> args=new ArrayList<>();


    public DbQueryBuilder where() {
        if(builder.length()>0)
            builder.append(" WHERE ");
        return this;
    }

    public DbQueryBuilder and() {
        if(builder.length()>0)
            builder.append(" AND ");
        return this;
    }

    public DbQueryBuilder or() {
        if(builder.length()>0)
            builder.append(" OR ");
        return this;
    }

    public DbQueryBuilder groupStart() {
        builder.append("(");
        groupStartCount++;
        return this;
    }

    public DbQueryBuilder groupEnd() {
        if(builder.length()>0)
            builder.append(")");
        groupStartCount--;
        return this;
    }

    public DbQueryBuilder like(@NonNull String column,@NonNull String expression) {
        builder.append(column).append(" LIKE ?");
        args.add(expression);
        return this;
    }

    public DbQueryBuilder notLike(@NonNull String column,@NonNull String expression) {
        builder.append(column).append(" NOT LIKE ?");
        args.add(expression);
        return this;
    }

    public DbQueryBuilder equals(@NonNull String column,double value) {
        return equals(column,String.valueOf(value));
    }

    public DbQueryBuilder equals(@NonNull String column,long value) {
        return equals(column,String.valueOf(value));
    }



    public DbQueryBuilder equals(@NonNull String column,@NonNull String value) {
        builder.append(column).append("=?");
        args.add(value);
        return this;
    }

    public DbQueryBuilder equalsBlank(@NonNull String column) {
        builder.append(column).append("=''");
        return this;
    }

    public DbQueryBuilder notEquals(@NonNull String column,@NonNull String value) {
        builder.append(column).append("!='").append(value).append("'");
        return this;
    }

    public DbQueryBuilder notEqualsBlank(@NonNull String column) {
        builder.append(column).append("!=''");
        return this;
    }

    public DbQueryBuilder greaterThan(@NonNull String column,@NonNull String value) {
        builder.append(column).append(">?");
        args.add(value);
        return this;
    }

    public DbQueryBuilder lessThan(@NonNull String column,@NonNull String value) {
        builder.append(column).append("<");
        args.add(value);
        return this;
    }

    public DbQueryBuilder greaterThanOrEqual(@NonNull String column,@NonNull String value) {
        builder.append(column).append(">=");
        args.add(value);
        return this;
    }

    public DbQueryBuilder lessThanOrEqual(@NonNull String column,@NonNull String value) {
        builder.append(column).append("<=");
        args.add(value);
        return this;
    }

    public DbQueryBuilder in(@NonNull String expression) {
        builder.append(" IN(?) ");
        args.add(expression);
        return this;
    }
    public DbQueryBuilder notIn(@NonNull String expression) {
        builder.append(" NOT IN()");
        args.add(expression);
        return this;
    }

    public DbQueryBuilder isNull(@NonNull String column) {
        builder.append(" IS NULL ").append(column).append(" ");
        return this;
    }

    public DbQueryBuilder isNotNull(@NonNull String column) {
        builder.append(" IS NOT NULL ").append(column).append(" ");
        return this;
    }

    public DbQueryBuilder between(@NonNull String value1,@NonNull String value2) {
        builder.append(" BETWEEN ? AND ? ");
        args.add(value1);
        args.add(value2);
        return this;
    }

    public DbQueryBuilder notBetween(@NonNull String value1,@NonNull String value2) {
        builder.append(" NOT BETWEEN ? AND ? ");
        args.add(value1);
        args.add(value2);
        return this;
    }


   public DbQueryBuilder build() {
        if(groupStartCount!=0)
            throw new RuntimeException("Group started but not ended");
        buildCalled=true;
        return  this;
   }

   public String getSelectionString() {
        if(!buildCalled) {
            build();
        }
//       if(!buildCalled)
//           throw new RuntimeException("You should call build() before calling this method");
        return toString();
   }

   public String [] selectionArguments() {
       if(!buildCalled) {
           build();
       }
//       if(!buildCalled)
//           throw new RuntimeException("You should call build() before calling this method");
        return args.toArray(new String[0]);
   }

    @Override
    public String toString() {
        return builder.toString();
    }
}
