package jpt.sfw.database;

import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.os.Bundle;

import jpt.sfw.R;
import jpt.sfw.SfwActivity;
import jpt.sfw.annotations.DbAnnotation;
import jpt.sfw.annotations.DbConfig;
import jpt.sfw.app.Sfw;
import jpt.sfw.annotations.AppConfig;
import jpt.sfw.models.ImageModel;

import java.lang.reflect.Constructor;
import java.util.List;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;

public class SfwDatabase {
    protected static SfwDatabase database;
    private DbHelper dbHelper;
    private DbConfig annotation;
    private static boolean initializeDatabase;

    public static SfwDatabase init(@NonNull Context ctx, @NonNull AppConfig config) {
        Class<? extends SfwDatabase> cls=config.databaseClassType();
        if (database == null) {
            try {
                initializeDatabase=config.initializeDatabase();
                if(initializeDatabase) {
                    Constructor<? extends SfwDatabase> constructor;
                    try {
                        constructor= cls.getDeclaredConstructor(Context.class);
                    } catch (Exception ignored) {
                        constructor= cls.getConstructor(Context.class);
                    }

                    constructor.setAccessible(true);
                    database = constructor.newInstance(ctx);
                    database.dbHelper.setIsDebuggingEnabledFromAppConfig(config.debuggingEnabled());
                    DbHelper.constructorError = null;
                }
            } catch (Throwable e) {
                if(DbHelper.constructorError==null)
                throw new IllegalAccessError("Can't initialize this class : " + cls.getName() + ".  Class should contain a public  constructor with one parameter (Context) . Should not be an abstract class. ");
                else
                    throw new IllegalAccessError(DbHelper.constructorError);
            }
        }
        return database;
    }

    public static SfwDatabase getInstance() {
        if (initializeDatabase && database == null)
            throw new IllegalAccessError("You should initialize this class using init(Context ctx,Class<? extends SfwDatabase> cls) method from your application class");
        return database;
    }

    public void destroy() {
        try {
            getDb().releaseReference();
            dbHelper=null;
            database=null;
            annotation=null;
        } catch (Exception ignored){}
    }


    /**
     * No need to call from your application
     * It will call from {@link SfwActivity#onCreate(Bundle)}
     * This method is to check the table info's stored in memory
     */
    public void recheck() {
        if(dbHelper!=null)
        dbHelper.recheck();
    }

    protected SfwDatabase(Context ctx) {
        try {
            annotation = Sfw.getAnnotation(DbConfig.class,getClass());
            if (annotation == null && !getClass().equals(SfwDatabase.class)) {
                DbHelper.setConstructorError("You should annotate " + getClass().getName() + " class by using jpt.sfw.annotations.DbConfig");
            }
            dbHelper = new DbHelper(annotation);
            dbHelper.setDb(new Sqlite(ctx).getWritableDatabase());
            dbHelper.recheck();
            onDbCreated(ctx);
        } catch (Throwable t){
            DbHelper.setConstructorError(t.getMessage());
        }

    }

    protected SQLiteDatabase getDb() {
        return dbHelper.getDb();
    }



    public boolean isIsDebuggingEnabled() {
        return dbHelper.isIsDebuggingEnabled();
    }

    public void setIsDebuggingEnabled(boolean isDebuggingEnabled) {
        dbHelper.setIsDebuggingEnabled(isDebuggingEnabled);
    }

    /**
     * @param key Will clear the particular data which is associated with this key from the database and memory
     */
    public void clear(@Nullable Class cls, @Nullable String key) {
        dbHelper.clear(cls, key);
    }

    /**
     * Will clear all data from the memory.
     * And will clear all data from database except the keys are stored in {@link DbHelper#keysShouldKeepInDb}
     *
     * @param fullClear true -> will clear all data from database without checking {@link DbHelper#keysShouldKeepInDb}
     */
    public void clearAll(boolean fullClear) {
        dbHelper.clearAll(fullClear);
    }

    public void setInMemoryKeys(List<String> inMemoryKeys) {
        dbHelper.setInMemoryKeys(inMemoryKeys);
    }

    public void setKeysShouldKeep(List<String> keysShouldKeep) {
        dbHelper.setKeysShouldKeepInDb(keysShouldKeep);
    }


    public void addCommonValues(@NonNull String key, String title, String content) {
        dbHelper.addCommonValues(key, title, content);
    }

    public CommonTableModel getCommonModel(String key) {
        return dbHelper.getCommonModel(key);
    }

    public boolean hasKeyInMemory(@NonNull String key) {
        return dbHelper.hasKeyInMemory(key);
    }

    public boolean hasKeyInDb(@NonNull String key) {
        return dbHelper.hasKeyInDb(key);
    }

    public boolean hasKey(@NonNull String key) {
        return hasKeyInMemory(key) || hasKeyInDb(key);
    }

    protected void executeSql(String sql) {
        dbHelper.executeSql(sql);
    }

    public <T> void addObserver( @NonNull Class fromClass,@NonNull Class<T> classType, boolean fetchDataNow,@NonNull DatabaseHelperInterface<T> observer) {
        addObserver(fromClass,classType,null,fetchDataNow,observer);
    }

    public <T> void addObserver( @NonNull Class fromClass,@NonNull Class<T> classType,@Nullable String tableName, boolean fetchDataNow,@NonNull DatabaseHelperInterface<T> observer) {
        dbHelper.addObserver(observer,fromClass,classType,tableName,fetchDataNow);
    }

    public <T> boolean isObserverActive(@NonNull Class fromClass, @NonNull Class<T> classType, @Nullable String tableName) {
        return dbHelper.isObserverActive(fromClass,classType,tableName);
    }

    public void clearObservers(@NonNull Class outerClass) {
dbHelper.clearObservers(outerClass);
    }

    public void clearAllObservers() {
        dbHelper.clearAllObservers();
    }

    public <T> void setData(int key,final @Nullable List<T> list) {
        dbHelper.setData( "" + key,list);
    }

    /**
     * @param key "list" will set to Common database or inMemory if   ' T does not have a {@link DbAnnotation} or key does not belong to T tables '
     */
    public <T> void setData( @NonNull String key,final @Nullable List<T> list) {
        dbHelper.setData( key,list);
    }

    /**
     * @param key "list" will set to Common database or inMemory if   ' T does not have a {@link DbAnnotation} or key does not belong to T tables '
     */
    public <T> void addData( @NonNull String key,final @Nullable List<T> list) {
        dbHelper.addData( key,list);
    }

    /**
     * @param key  "data" will set to Common database or inMemory if   ' T does not have a {@link DbAnnotation} or key does not belong to T tables '
     */
    public <T> void setSingleData(final @Nullable T data, @Nullable String key) {
        dbHelper.setSingleData(data, key);
    }

    public <T> List<T> getData(int key, @NonNull Class<T> type) {
        return getData("" + key, type);
    }

    public <T> List<T> getData(@NonNull String key, @NonNull Class<T> type) {
        return dbHelper.getData(key, type);
    }

    public <T> T getSingleData(int dbKey, @NonNull Class<T> type) {
        return getSingleData("" + dbKey, type);
    }

    public <T> T getSingleData(String dbKey, @NonNull Class<T> type) {
        return dbHelper.getSingleData(dbKey, type);
    }

    public <T> T put(@Nullable T data) {
        return put(new DbPutterModel<>(data,null,false));
    }

    public <T> T put(@Nullable T data,String tableName) {
        return put(new DbPutterModel<>(data,tableName,false));
    }

    public <T> T put(@Nullable T data,String tableName,boolean useThread) {
        return put(new DbPutterModel<>(data,tableName,useThread));
    }

    public <T> T put(@Nullable T data,String tableName,boolean useThread,DbPutterListener<T> listener) {
        return put(new DbPutterModel<>(data,tableName,useThread).setDbPutterListener(listener));
    }

    public <T> T put(@NonNull DbPutterModel<T> putter) {
        return dbHelper.put(putter);
    }

    public <T> T putAll(@Nullable List<T> data,String tableName) {
        return put(new DbPutterModel<>(tableName,false,data));
    }

    public <T> T putAll(@Nullable List<T> data,String tableName,boolean useThread) {
        return put(new DbPutterModel<>(tableName,useThread,data));
    }

    public <T> T putAll(@Nullable List<T> data,String tableName,boolean useThread,DbPutterListener<T> listener) {
        return put(new DbPutterModel<>(tableName,useThread,data).setDbPutterListener(listener));
    }

    public <T> int putAll(@NonNull DbPutterModel<T> putter) {
        return dbHelper.putAll(putter);
    }

    public  <T> List<T> getAll(String table, Cursor query, Class<T> cls, DbQueriesMainModel mainModel, @Nullable DbGetterListener<T> dbGetterListener) {
        return dbHelper.getAll(table, query, cls, mainModel, dbGetterListener);
    }

    public <T> List<T> getAll(@NonNull Class<T> cls) {
        return getAll(new DbGetterModel<>(cls));
    }

    public <T> List<T> getAll(@NonNull Class<T> cls,@Nullable DbQueryBuilder conditions) {
        if(conditions==null) {
            return getAll(cls);
        }
        conditions.build();
        return getAll(new DbGetterModel<>(cls).setCondition(conditions.getSelectionString()).setConditionArgs(conditions.selectionArguments()));
    }

    public <T> List<T> getAll(@NonNull Class<T> cls,String table) {
        return getAll(new DbGetterModel<>(cls,table));
    }

    public <T> List<T> getAll(@NonNull Class<T> cls,String table,@Nullable DbQueryBuilder conditions) {
        if(conditions==null) {
            return getAll(cls,table);
        }
        conditions.build();
        return getAll(new DbGetterModel<>(cls,table).setCondition(conditions.getSelectionString()).setConditionArgs(conditions.selectionArguments()));
    }

    public <T> List<T> getAll(@NonNull Class<T> cls,String table,DbGetterListener<T> listener) {
        return getAll(new DbGetterModel<>(cls,table,listener));
    }

    public <T> List<T> getAll(@NonNull Class<T> cls,String table,DbGetterListener<T> listener,@Nullable DbQueryBuilder conditions) {
        if(conditions==null) {
            return getAll(cls,table,listener);
        }
        conditions.build();
        return getAll(new DbGetterModel<>(cls,table,listener).setCondition(conditions.getSelectionString()).setConditionArgs(conditions.selectionArguments()));
    }

    public <T> List<T> getAll(@NonNull DbGetterModel<T> model) {
        return dbHelper.getAll(model);
    }

    public <T> List<T> getAll(@NonNull DbGetterModel<T> model,@Nullable DbQueryBuilder conditions) {
        if(conditions!=null) {
            conditions.build();
            model.setCondition(conditions.getSelectionString()).setConditionArgs(conditions.selectionArguments());
        }

        return dbHelper.getAll(model);
    }

    public <T> T getSingleData(@NonNull Class<T> cls) {
        return getSingleData(new DbGetterModel<>(cls));
    }

    public <T> T getSingleData(@NonNull Class<T> cls,@Nullable String tableName) {
        return getSingleData(new DbGetterModel<>(cls,tableName));
    }

    public <T> T getSingleData(@NonNull Class<T> cls,@Nullable String tableName,@NonNull DbQueryBuilder conditions) {
        conditions.build();
        return getSingleData(new DbGetterModel<>(cls,tableName).setCondition(conditions.getSelectionString()).setConditionArgs(conditions.selectionArguments()));
    }

    public <T> T getSingleData(@NonNull DbGetterModel<T> model) {
        return dbHelper.getSingleData(model);
    }

    public <T> T getDataByPrimaryField(@NonNull Class<T> cls, @Nullable String tableName,@NonNull String primaryFieldValue) {
        return dbHelper.getDataByPrimaryField(cls, tableName, primaryFieldValue);
    }

    public <T> int remove(@NonNull Class<T> cls,@NonNull DbQueryBuilder condition) {
        condition.build();
        return dbHelper.removeAll(new DbRemoverModel<>(cls).setSelection(condition.getSelectionString()).setSelectionArgs(condition.selectionArguments()));
    }

    public <T> int remove(@NonNull Class<T> cls,@Nullable String tableName,@NonNull DbQueryBuilder condition) {
        condition.build();
        return dbHelper.removeAll(new DbRemoverModel<>(cls,tableName).setSelection(condition.getSelectionString()).setSelectionArgs(condition.selectionArguments()));
    }


    public <T> int remove(@NonNull T model) {
        return dbHelper.remove(model,null);
    }

    public <T> int remove(@NonNull T model,@Nullable String tableName) {
        return dbHelper.remove(model,tableName);
    }


    public <T> int remove(@NonNull List<T> models) {
        return dbHelper.removeAll(models,null);
    }

    public <T> int remove(@NonNull List<T> models,@Nullable String tableName) {
        return dbHelper.removeAll(models,tableName);
    }

    public <T> int remove(@NonNull Class<T> cls) {
        return removeAll(new DbRemoverModel<>(cls));
    }

    public <T> int remove(@NonNull Class<T> cls,@Nullable String tableName) {
        return removeAll(new DbRemoverModel<>(cls).setTableName(tableName));
    }

    public <T> int removeAll(@NonNull DbRemoverModel<T> model) {
        return dbHelper.removeAll(model);
    }


    public <T> void removeAll(@NonNull Class[] cls, @Nullable DbRemoverListener<T> dbRemoverListener, boolean useThread) {
        dbHelper.removeAll(cls, dbRemoverListener, useThread);
    }

    public <T> void removeAllExcept(@Nullable List<Class> cls, @Nullable DbRemoverListener<T> remover, boolean useThread) {
        dbHelper.removeAllExcept(cls, remover, useThread);
    }

    //AUTO START
    public boolean isAutoStartChecked() {
        return dbHelper.isAutoStartChecked();
    }

    public void setAutoStartChecked(boolean checked) {
        dbHelper.setAutoStartChecked(checked);
    }


    public void setItemsForGallery(List<ImageModel> list, int catId) {
        dbHelper.setItemsForGallery(list, catId);
    }

    public List<ImageModel> getItemsForGallery(int catId) {
        return dbHelper.getItemsForGallery(catId);
    }

    public boolean isLoggedIn() {
        return true;
    }

    public boolean isDebuggingEnabled() {
        return dbHelper.isIsDebuggingEnabled();
    }

    public SfwDatabase setDebuggingEnabled(boolean debuggingEnabled) {
        dbHelper.setIsDebuggingEnabled(debuggingEnabled);
        return this;
    }

    /**
     * @param db This method will called from {@link Sqlite#onCreate(SQLiteDatabase)}
     * NOTE : don't call {@link #getInstance()} from this method. {@link SfwDatabase#getDb()} will null at this time
     *           use db instead
     */
    protected void onCreateDb(SQLiteDatabase db) {

    }

    /**
     * @param db This method will called from {@link Sqlite#onUpgrade(SQLiteDatabase, int, int)}}
     * NOTE : don't call {@link #getInstance()} from this method. {@link SfwDatabase#getDb()} will null at this time
     *           use db instead
     */
    protected void onUpgradeDb(SQLiteDatabase db, int oldVersion, int newVersion) {

    }

    /**
     * @param ctx
     * This will call after creating {@link SQLiteDatabase} instance
     * Now you can use {@link #getInstance()} or {@link #getDb()} method
     */
    protected void onDbCreated(@NonNull Context ctx) {
    }


    private class Sqlite extends SQLiteOpenHelper {


        Sqlite(Context context) {
            super(context, (annotation == null || annotation.dbName().isEmpty()) ? context.getString(R.string.app_name) + ".db" : annotation.dbName(), null, annotation == null ? 1 : annotation.version());
        }

        @Override
        public void onCreate(SQLiteDatabase db) {
            dbHelper.onCreate(db);
            onCreateDb(db);
        }

        @Override
        public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
            dbHelper.onUpgrade(db);
            onUpgradeDb(db, oldVersion, newVersion);
        }
    }




}
