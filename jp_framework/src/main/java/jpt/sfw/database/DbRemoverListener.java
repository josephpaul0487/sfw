package jpt.sfw.database;

import java.util.HashMap;

import androidx.annotation.NonNull;

public interface DbRemoverListener<T> {
    /**
     * @param tables ->  keys  -> Class types     values -> Table names and removedItem count
     */
    void onComplete(@NonNull HashMap<Class, HashMap<String,Integer>> tables,int totalRemovedItemCount);
}
