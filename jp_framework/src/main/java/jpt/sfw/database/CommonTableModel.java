package jpt.sfw.database;

import java.util.Objects;

public class CommonTableModel {
    private int  id;
    private String key;
    private String title;
    private String content;
    private String updatedTime;

    public CommonTableModel(int id, String key, String title, String content, String updatedTime) {
        this.id = id;
        this.key = key;
        this.title = title;
        this.content = content;
        this.updatedTime = updatedTime;
    }

    public int getId() {
        return id;
    }

    public CommonTableModel setId(int id) {
        this.id = id;
        return this;
    }

    public String getKey() {
        return key;
    }

    public CommonTableModel setKey(String key) {
        this.key = key;
        return this;
    }

    public String getTitle() {
        return title;
    }

    public CommonTableModel setTitle(String title) {
        this.title = title;
        return this;
    }

    public String getContent() {
        return content;
    }

    public CommonTableModel setContent(String content) {
        this.content = content;
        return this;
    }

    public String getUpdatedTime() {
        return updatedTime==null?"":updatedTime;
    }

    public CommonTableModel setUpdatedTime(String updatedTime) {
        this.updatedTime = updatedTime;
        return this;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        CommonTableModel that = (CommonTableModel) o;
        return Objects.equals(key, that.key);
    }

    @Override
    public int hashCode() {
        return Objects.hash(key);
    }
}
