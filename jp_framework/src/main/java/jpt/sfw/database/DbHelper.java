package jpt.sfw.database;

import android.content.ContentValues;
import android.database.Cursor;
import android.database.SQLException;
import android.database.sqlite.SQLiteDatabase;
import android.os.Handler;
import android.os.Looper;
import android.text.TextUtils;
import android.util.Log;

import com.google.gson.Gson;
import com.google.gson.annotations.SerializedName;
import com.google.gson.reflect.TypeToken;

import java.lang.ref.SoftReference;
import java.lang.reflect.Constructor;
import java.lang.reflect.Field;
import java.lang.reflect.ParameterizedType;
import java.lang.reflect.Type;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Locale;
import java.util.Map;
import java.util.Set;
import java.util.regex.Pattern;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import jpt.sfw.SfwActivity;
import jpt.sfw.enums.ColumnTypes;
import jpt.sfw.annotations.DbAnnotation;
import jpt.sfw.annotations.DbConfig;
import jpt.sfw.annotations.DbEntity;
import jpt.sfw.annotations.DbExclude;
import jpt.sfw.annotations.DbPrimary;
import jpt.sfw.app.Sfw;
import jpt.sfw.enums.InsertConflictAlgorithm;
import jpt.sfw.fragments.SfwFragment;
import jpt.sfw.annotations.AppConfig;
import jpt.sfw.models.ImageModel;

public class DbHelper {
    private SQLiteDatabase db;
    private DbConfig annotation;
    private Gson gson = new Gson();
    private boolean isDebuggingEnabled;
    /**
     * To get the actual error if #SqlDataBase. constructor.newInstance() method will throw only one error. so we cannot get the actul error if the error occurs while constructor.newInstance() call
     */
    static String constructorError;

    private List<String> inMemoryKeys = Collections.emptyList();
    private List<String> keysShouldKeepInDb = Collections.emptyList();
    private HashMap<String, DbQueriesMainModel> dbQueriesMainModels = new HashMap<>();
    private final HashMap<String, Object> contents = new HashMap<>();
    private final HashMap<String, HashMap<String, List<DatabaseHelperInterface>>> observers = new HashMap<>();
    private final List<String> observersClassNames = new ArrayList<>();

    static final String TAG = "SqlDataBase";
    //    private static final Object OBSERVER_LOCK = new Object();
    private static final String TBL_COMMON = "tbl_common";
    private static final String TBL_DB_CLASS_QUERIES = "tbl_class_queries";

    private static final String CLM_ID = "clm_id";
    private static final String CLM_KEY = "clm_key";
    private static final String CLM_TITLE = "clm_title";
    private static final String CLM_CONTENT = "clm_content";
    private static final String CLM_TABLE = "clm_table";
    private static final String CLM_UPDATED_TIME = "clm_updated_time";

    private static final int KEY_GALLERY = 1;
    private static final int KEY_AUTO_START = 2;

    private static final Pattern nameValidator = Pattern.compile("[a-zA-Z_]*");


    public static boolean isSystemField(Field f) {
        return f != null && (f.getName().equals("serialVersionUID") || f.getName().equals("shadow$_monitor_") || f.getName().equals("shadow$_klass_") || f.getName().equals("$change") || f.getName().equals("$shadow"));
    }

    private static boolean isFieldValidForColumn(@Nullable Field f, @Nullable String tableName) {
        if (f == null)
            return false;
        if (!isSystemField(f)) {
            DbExclude exclude = f.getAnnotation(DbExclude.class);
            if (exclude != null && !TextUtils.isEmpty(tableName)) {
                List<String> tables = Arrays.asList(f.getAnnotation(DbExclude.class).tables());
                return !tables.contains(tableName);
            }
            return true;
        }
        return false;
    }

    private static boolean isAllTablesExcludedForField(@Nullable Field f, @Nullable String[] entityTables) {
        if (f == null)
            return true;
        DbExclude exclude = f.getAnnotation(DbExclude.class);
        if (exclude != null) {
            List<String> tables = Arrays.asList(f.getAnnotation(DbExclude.class).tables());
            if ((tables.size() == 1 && tables.get(0).toLowerCase().equals("all")))
                return true;
            boolean allTablesExcluded = true;
            for (String table : entityTables) {
                if (!tables.contains(table)) {
                    allTablesExcluded = false;
                    break;
                }
            }
            return allTablesExcluded;
        }
        return false;
    }

    private static void isValidName(String name, boolean isColumnName) {
        if (TextUtils.isEmpty(name) || !nameValidator.matcher(name).find() || name.length() > 50) {
            String error = (isColumnName ? "Column " : "Table ") + "names should not be empty and must only contains alphabets and _ character..  Ang length should be less than 51 characters   found NAME = " + name + "  length = " + name.length();
            setConstructorError(error);
        }
    }

    DbHelper(DbConfig annotation) {
        this.annotation = annotation;
        if (annotation != null)
            this.isDebuggingEnabled = annotation.debuggingEnabled();
    }


    DbHelper setDb(SQLiteDatabase db) {
        this.db = db;
        return this;
    }

    String getConstructorError() {
        return constructorError;
    }

    static void setConstructorError(@Nullable String constructorError) {
        DbHelper.constructorError = constructorError;
        if (constructorError != null)
            throw new IllegalAccessError(constructorError);
    }

    private void log(String message, Throwable t) {
            Sfw.log(TAG, message, t,true);
    }

    SQLiteDatabase getDb() {
        return db;
    }


    void recheck() {
        if (db != null)
            if (dbQueriesMainModels.size() < 1 && annotation != null) {
                for (Class cls : annotation.entities()) {
                    try {
                        DbEntity annotation = Sfw.getAnnotation(DbEntity.class, cls);
                        if (annotation == null)
                            setConstructorError("" + cls.getName() + " should annotated with " + DbEntity.class.getName() + ".");
                        for (String table : annotation.tables())
                            dbQueriesMainModels.put(cls.getName() + "_" + table, getQueryMainModel(cls, table));
                    } catch (Exception ignored) {
                    }
                }
            }
    }

    private <T> boolean isAnEntityClass(@Nullable Class<T> cls, @Nullable String tableName) {
        return Sfw.getAnnotation(DbEntity.class, cls) != null && getTableName(cls, tableName, false) != null;
    }


    public static Type getListTypeToken(Field f) {
        try {
            if (!List.class.equals(f.getType()))
                return f.getType();
            ParameterizedType listType = (ParameterizedType) f.getGenericType();
            Class<?> parameterType = (Class<?>) listType.getActualTypeArguments()[0];
            return TypeToken.getParameterized(List.class, parameterType).getType();
        } catch (Exception ignored) {
        }
        return List.class;
    }


    boolean isIsDebuggingEnabled() {
        return isDebuggingEnabled;
    }

    void setIsDebuggingEnabled(boolean isDebuggingEnabled) {
        this.isDebuggingEnabled = isDebuggingEnabled;
    }

    /**
     * This method will not set {@link #isDebuggingEnabled} value if any class overriding {@link SfwDatabase} class
     */
    void setIsDebuggingEnabledFromAppConfig(boolean isDebuggingEnabled) {
        if (annotation == null)
            this.isDebuggingEnabled = isDebuggingEnabled;
    }

    private void closeCursor(Cursor c) {
        try {
            c.close();
        } catch (Exception ignored) {
        }
    }

    private void sleep() {
        try {
            Thread.sleep(1);
        } catch (Exception ignored) {
        }
    }

    public static List<Field> getAllFields(Class cls) {
        return getAllFields(cls, false, null, null, null);
    }


    /**
     * @param cls            The class to get the details
     * @param checkDbExclude (true & (fields==null || fields.isEmpty)) -> Will check the {@link DbExclude} is present or not
     * @param tableName      To check whether the field is excluded from the columns. @see {@link #isFieldValidForColumn(Field, String)}
     * @param tables         To check whether the field is excluded from the columns. @see {@link #isAllTablesExcludedForField(Field, String[])}
     * @param fieldsToReturn if not empty it will only returning the fields. and ite will ignore "tableName", "tables" and "checkDbExclude" params
     * @return
     */
    public static List<Field> getAllFields(Class cls, boolean checkDbExclude, @Nullable String tableName, @Nullable String[] tables, @Nullable List<String> fieldsToReturn) {
        return getAllFields(cls, new ArrayList<>(), checkDbExclude, tableName, tables, fieldsToReturn);
    }

    /**
     * @param cls            The class to get the details
     * @param list           All fields will add to this list
     * @param checkDbExclude (true & (fields==null || fields.isEmpty)) -> Will check the {@link DbExclude} is present or not
     * @param tableName      To check whether the field is excluded from the columns. @see {@link #isFieldValidForColumn(Field, String)}
     * @param tables         To check whether the field is excluded from the columns. @see {@link #isAllTablesExcludedForField(Field, String[])}
     * @param fieldsToReturn if not empty it will only returning the fields. and ite will ignore "tableName", "tables" and "checkDbExclude" params
     * @return
     */
    private static List<Field> getAllFields(Class cls, @NonNull ArrayList<Field> list, boolean checkDbExclude, @Nullable String tableName, @Nullable String[] tables, @Nullable List<String> fieldsToReturn) {
        if (cls == null)
            return list;
        boolean fieldsToReturnEmpty = fieldsToReturn == null || fieldsToReturn.isEmpty();
        Field[] fields = cls.getDeclaredFields();
        for (Field f : fields) {
            if (!fieldsToReturnEmpty) {
                if (fieldsToReturn.contains(f.getName()))
                    list.add(f);
                continue;
            }

            if (!isSystemField(f) && (!checkDbExclude || (isFieldValidForColumn(f, tableName) && !isAllTablesExcludedForField(f, tables))))
                list.add(f);
        }
        if (cls.getSuperclass() != null && !cls.getSuperclass().equals(Object.class)) {
            getAllFields(cls.getSuperclass(), list, checkDbExclude, tableName, tables, fieldsToReturn);
        }
        return list;
    }

    public static Field getField(Class cls, String fieldName) {

        try {
            Field f = cls.getDeclaredField(fieldName);
            f.setAccessible(true);
            return f;
        } catch (Exception ignored) {
            if (cls.getSuperclass() != null)
                return getField(cls.getSuperclass(), fieldName);
        }
        return null;
    }

    /**
     * @param cls pass null if you want to user {@link #TBL_COMMON}
     * @param key Will clear the particular data which is associated with this key from the database and memory
     */
    public <T> void clear(@Nullable Class<T> cls, @Nullable String key) {
        try {
            if (isAnEntityClass(cls, key)) {
                removeAll(new DbRemoverModel<>(cls));
                return;
            }
            if (!TextUtils.isEmpty(key)) {
                boolean isFromMemory = inMemoryKeys.contains(key);
                if (!isFromMemory)
                    db.delete(TBL_COMMON, CLM_KEY + "=?", new String[]{key});
                else
                    contents.remove(key);
                notifyObservers(cls, key, 0, true, true, !isFromMemory, isFromMemory, new ArrayList<>());
            }
        } catch (Exception ignored) {
        }
    }

    /**
     * Will clear all data from the memory.
     * And will clear all data from database except the keys are stored in {@link #keysShouldKeepInDb}
     *
     * @param fullClear true -> will clear all data from database without checking {@link #keysShouldKeepInDb}
     *                  <p>
     *                  NOTE : This will not delete the tables created using {@link DbEntity}
     *                  You should use {@link #removeAll(Class[], DbRemoverListener, boolean)} to remove the tables created using {@link DbEntity}
     **/
    void clearAll(boolean fullClear) {
        try {
            Set<String> keySet = contents.keySet();
            contents.clear();
            for (String key : keySet) {
                notifyObservers(null, key, 0, true, true, false, true, new ArrayList<>());
            }
            StringBuilder builder = new StringBuilder();
            String[] args = new String[keysShouldKeepInDb.size()];
            if (fullClear)
                args = new String[0];
            for (int i = args.length; i > 0; i--) {
                builder.append(builder.length() > 0 ? " AND " : "").append(CLM_KEY).append("!=?");
                args[i - 1] = "" + keysShouldKeepInDb.get(i - 1);
            }
            Cursor c = db.query(TBL_COMMON, new String[]{CLM_KEY}, builder.toString(), args, null, null, null);
            while (c.moveToNext())
                notifyObservers(null, c.getString(0), 0, true, true, true, false, new ArrayList<>());
            closeCursor(c);
            db.delete(TBL_COMMON, builder.toString(), args);
        } catch (Exception e) {
            log("clearAll", e);
        }
    }

    void setInMemoryKeys(List<String> inMemoryKeys) {
        this.inMemoryKeys = inMemoryKeys == null ? Collections.emptyList() : new ArrayList<>(inMemoryKeys);
    }

    void setKeysShouldKeepInDb(List<String> keysShouldKeepInDb) {
        this.keysShouldKeepInDb = keysShouldKeepInDb == null ? Collections.emptyList() : new ArrayList<>(keysShouldKeepInDb);
    }

    private <T> List<T> deSerialize(String content, Type type) {
        try {
            if (!TextUtils.isEmpty(content)) {
                List<T> list = gson.fromJson(content, type);
                if (list != null)
                    return list;
            }
        } catch (Throwable ignored) {
        }
        return new ArrayList<>();
    }

    private <T> T deSerializeSingle(String content, Class<T> type) {
        try {
            if (!TextUtils.isEmpty(content))
                return gson.fromJson(content, type);
        } catch (Throwable ignored) {
        }
        return null;
    }

    private <T> T deSerializeSingle(String content, Type type) {
        try {
            if (!TextUtils.isEmpty(content))
                return gson.fromJson(content, type);
        } catch (Throwable ignored) {
        }
        return null;
    }

    private <T> String serialize(Type type, List<T> list) {
        try {
            if (list != null)
                return gson.toJson(list, type);
        } catch (Throwable ignored) {
        }
        return "";
    }

    private <T> String serializeSingle(T model) {
        try {
            return gson.toJson(model, model.getClass());
        } catch (Throwable ignored) {
        }
        return "";
    }

    private <T> String serializeSingle(T model, Type type) {
        try {
            return gson.toJson(model, type);
        } catch (Throwable ignored) {
        }
        return "";
    }

    @NonNull
    private <T> List<T> getDataFromContent(@NonNull String key) {
        try {
            Object data = contents.get(key);
            if (data != null)
                return (ArrayList<T>) data;
        } catch (Exception e) {
            log("getDataFromContent", e);
        }
        return new ArrayList<>();
    }

    void addCommonValues(String key, String title, String content) {
        ContentValues cv = new ContentValues();
        cv.put(CLM_KEY, key);
        cv.put(CLM_TITLE, title);
        cv.put(CLM_CONTENT, content);
        SimpleDateFormat dateFormat = new SimpleDateFormat(
                "yyyy-MM-dd HH:mm:ss", Locale.getDefault());
        cv.put(CLM_UPDATED_TIME, dateFormat.format(new Date()));
        if (db.update(TBL_COMMON, cv, CLM_KEY + "=?", new String[]{key}) < 1) {
            db.insert(TBL_COMMON, null, cv);
        }
    }

    public CommonTableModel getCommonModel(String key) {
        CommonTableModel model = new CommonTableModel(-1, "", "", "", "");
        try {
            Cursor c = db.query(TBL_COMMON, new String[]{CLM_ID, CLM_TITLE, CLM_CONTENT, CLM_UPDATED_TIME},
                    CLM_KEY + "=?", new String[]{key}, null, null, null, "1");
            if (c.moveToFirst()) {
                model = new CommonTableModel(c.getInt(0), key, c.getString(1), c.getString(2), c.getString(3));
            }
            closeCursor(c);

        } catch (Exception ignored) {
        }
        return model;
    }

    boolean hasKeyInMemory(String key) {
        return contents.containsKey(key);//!getCommonModel(key).getContent().isEmpty();
    }

    boolean hasKeyInDb(String key) {
        return !getCommonModel(key).getContent().isEmpty();
    }

    void executeSql(String sql) {
        try {
            db.execSQL(sql);
        } catch (Exception ignored) {
        }
    }


    /**
     * @param tableName You can provide KEY for inMemoryData and Common table data
     * @param fromClass The outer class of the observer to destroy reference when the outer class get destroyed. @see {@link #clearObservers(Class)}
     */
    public <T> void addObserver(@NonNull DatabaseHelperInterface<T> observer, @NonNull Class fromClass, @NonNull Class<T> classType, @Nullable String tableName, boolean fetchDataNow) {
//        synchronized (OBSERVER_LOCK) {
        String table = getTableName(classType, tableName);
        HashMap<String, List<DatabaseHelperInterface>> map = observers.get(table);

        if (map == null) {
            map = new HashMap<>();
            List<DatabaseHelperInterface> set = new ArrayList<>();
            set.add(observer);
            map.put(fromClass.getName(), set);
        } else {
            List<DatabaseHelperInterface> set = map.get(fromClass.getName());
            if (set == null) {
                set = new ArrayList<>();
            }
            set.add(observer);
            map.put(fromClass.getName(), set);
        }
        observers.put(table, map);
        if (!observersClassNames.contains(fromClass.getName()))
            observersClassNames.add(fromClass.getName());
        SoftReference<DatabaseHelperInterface> softReference = new SoftReference<>(observer);
        if (fetchDataNow) {
            getAll(new DbGetterModel<>(classType, tableName, (data, table1, classType1, listCount) -> {
                try {
                    DatabaseHelperInterface helperInterface = softReference.get();
                    if ( helperInterface != null) {
                        helperInterface.onDbContentChanged(classType1, table1, listCount, false, true, false, false, data);
                    }
                } catch (Exception e) {
                    log("addObserver", e);
                }

            }));
        }
//        }
    }

    public <T> boolean isObserverActive(@NonNull Class fromClass, @NonNull Class<T> classType, @Nullable String tableName) {
        String table = getTableName(classType, tableName,false);
        if (table == null) {
            return false;
        }
        HashMap<String, List<DatabaseHelperInterface>> map = observers.get(table);
        if (map == null) {
            return false;
        }
        return map.containsKey(fromClass.getName());
    }

    /**
     * To clear all references to the class to avoid memory leaks.
     * You can call this method manually
     * This will called from the framework classes : {@link SfwActivity#onDestroy()} and {@link SfwFragment#onDestroy()}
     */
    public void clearObservers(@NonNull Class outerClass) {
        String key = outerClass.getName();
        if (!observersClassNames.contains(key))
            return;
        new Thread(() -> {
//            synchronized (OBSERVER_LOCK) {
            try {
                Set<String> tables = observers.keySet();
                List<String> tablesToRemove = new ArrayList<>();
                for (String table : tables) {
                    HashMap<String, List<DatabaseHelperInterface>> map = observers.get(table);
                    if (map != null) {
                        map.remove(key);
                        if (map.size() > 0)
                            observers.put(table, map);
                        else
                            tablesToRemove.add(table);
                    } else {
                        tablesToRemove.add(table);
                    }
                    sleep();
                }
                for (String table : tablesToRemove)
                    observers.remove(table);
            } catch (Exception e) {
                log("clearObservers", e);
            }
//            }
        }).start();
    }

    public void clearAllObservers() {
        observersClassNames.clear();
        observers.clear();
    }

    /**
     * Should call this from main thread
     */
    private <T> void notifyObservers(@Nullable Class<T> cls, @NonNull String key, int objectCount, boolean isThisRemovedItems, boolean isThisAFullList, boolean isFromCommonTable, boolean isFromStaticMemory, @NonNull List<T> content) {
        HashMap<String, List<DatabaseHelperInterface>> map = observers.get(key);
        if (map != null) {
            try {
                for (Map.Entry<String, List<DatabaseHelperInterface>> s : map.entrySet()) {
                    for (DatabaseHelperInterface ref : s.getValue()) {
                        if (ref != null) {
                            try {
                                ref.onDbContentChanged(cls, key, objectCount, isThisRemovedItems, isThisAFullList, isFromCommonTable, isFromStaticMemory, content);
                            } catch (Exception e) {
                                log("notifyObservers", e);
                            }
                        } else {
                            log("notifyObservers ::: Observer is null ... Class : "+s.getKey(),null);
                        }
                    }
                }
            } catch (Exception e) {
                log("notifyObservers", e);
            }
        }
    }


    /**
     * @param key "list" will set to Common database or inMemory if   ' T does not have a {@link DbAnnotation} or key does not belong to T tables '
     */
    <T> void setData(@NonNull String key, final @Nullable List<T> list) {
        try {
            if (list == null || list.isEmpty()) {
                contents.remove(key);
                clear(null, key);
                return;
            }

            Class cls = list.get(0).getClass();
            if (isAnEntityClass(cls, key)) {
                removeAll(new DbRemoverModel<>(cls, key));
                putAll(new DbPutterModel<>(key, false, list));
                return;
            }

            final Type type = new TypeToken<List<T>>() {
            }.getType();
            //  new Thread(() -> {
            if (list == null) {
                clear(null, key);
            } else {
                if (inMemoryKeys.contains("" + key))
                    contents.put(key, list);
                else
                    addCommonValues(key, key, serialize(type, list));
            }
            //  }).start();
        } catch (Exception e) {
            log("setData", e);
        }

    }

    /**
     * @param key "list" will set to Common database or inMemory if   ' T does not have a {@link DbAnnotation} or key does not belong to T tables '
     */
    <T> void addData(@NonNull String key, final @Nullable List<T> list) {
        try {
            if (list == null || list.isEmpty()) {
                return;
            }

            if (isAnEntityClass(list.get(0).getClass(), key)) {
                putAll(new DbPutterModel<>(key, false, list));
                return;
            }
            final Type type = new TypeToken<List<T>>() {
            }.getType();
            //  new Thread(() -> {
            if (list != null) {
                if (inMemoryKeys.contains(key)) {
                    List<T> currentList = (List<T>) contents.get(key);
                    if (currentList == null)
                        currentList = new ArrayList<>();
                    currentList.addAll(list);
                    contents.put(key, currentList);
                    notifyObservers(null, key, currentList.size(), false, false, false, true, currentList);
                } else {
                    List<T> currentList = deSerialize(getCommonModel(key).getContent(), TypeToken.getParameterized(List.class, type).getType());
                    currentList.addAll(list);
                    addCommonValues(key, key, serialize(type, currentList));
                    notifyObservers(null, key, currentList.size(), false, false, true, false, currentList);
                }
            }
            //  }).start();
        } catch (Exception e) {
            log("addData", e);
        }

    }

    /**
     * @param key "data" will set to Common database or inMemory if   ' T does not have a {@link DbEntity} or key does not belong to T tables '
     */
    <T> void setSingleData(final @Nullable T data, @Nullable String key) {
        try {
            if (data != null && isAnEntityClass(data.getClass(), key)) {
                removeAll(new DbRemoverModel<>(data.getClass(), key));
                put(new DbPutterModel<>(data, key, false));
                return;
            }
            //  new Thread(() -> {
            if (data == null && !TextUtils.isEmpty(key)) {
                contents.remove(key);
                clear(null, key);
            } else if (data != null) {
                final Type type = data.getClass();
                if (inMemoryKeys.contains(key)) {
                    contents.put(key, data);
                    notifyObservers(null, key, 1, false, true, false, true, Collections.singletonList(data));
                } else {
                    addCommonValues(key, key, serializeSingle(data, type));
                    notifyObservers(null, key, 1, false, true, true, false, Collections.singletonList(data));
                }
            }
            //  }).start();
        } catch (Exception e) {
            log("setData", e);
        }

    }


    <T> List<T> getData(@NonNull String key, @NonNull Class<T> type) {
        if (isAnEntityClass(type, key)) {
            return getAll(new DbGetterModel<>(type, key));
        }
        if (inMemoryKeys.contains(key))
            return getDataFromContent(key);
        try {
            return deSerialize(getCommonModel(key).getContent(), TypeToken.getParameterized(List.class, type).getType());
        } catch (Exception ignored) {
        }
        return new ArrayList<>();
    }

    <T> T getSingleData(String dbKey, @NonNull Class<T> type) {
        try {
            if (isAnEntityClass(type, dbKey)) {
                return getSingleData(new DbGetterModel<>(type, dbKey));
            }
            if (inMemoryKeys.contains(dbKey)) {
                return (T) contents.get(dbKey);
            }
            return deSerializeSingle(getCommonModel(dbKey).getContent(), type);
        } catch (Exception e) {
            log("getSingleData", e);
        }
        return null;
    }

    <T> T put(@NonNull DbPutterModel<T> putter) {
        try {
            if (putter.models != null && putter.model == null) {
                putAll(putter);
                return null;
            }
            boolean isUsingMainThread = Looper.getMainLooper().getThread() == Thread.currentThread();
            if (!isUsingMainThread || (putter.dbPutterListener == null && !putter.useThread))
                return putInThread(putter);
            else
                new Thread(() -> {
                    putInThread(putter);
                }).start();
        } catch (Exception e) {
            log("put", e);
        }
        return putter.model;
    }

    private <T> T putInThread(@NonNull DbPutterModel<T> putter) {
        Class cls = null;
        String table = null;
        boolean isInserted = false;
        boolean isUpdated = false;
        SoftReference<DbPutterListener> listener = putter.dbPutterListener != null ? new SoftReference<>(putter.dbPutterListener) : null;
        T model = putter.model;
        if (model != null) {

            try {
                cls = putter.model.getClass();
                table = getTableName(model.getClass(), putter.tableName);
                DbQueriesMainModel mainModel = getQueryMainModel(cls, table);

                if (mainModel != null) {
                    int[] count = putModel(model, putter.conflictAlgorithm, table, cls, mainModel, getAllFields(cls, true, table, null, mainModel.getFields()),
                            mainModel.getDbFields(), true);
                    isInserted = count[0] > 0;
                    isUpdated = count[1] > 0;
                } else
                    log("PUT class=" + model.getClass().getName() + "   Table details not found.", null);
            } catch (Exception e) {
                log("PUT class=" + model.getClass().getName(), e);
            }
        }
        notifyAfterPut(isInserted ? 1 : 0, isUpdated ? 1 : 0, table, cls, listener, isInserted ? Collections.singletonList(model) : new ArrayList<>(), isUpdated ? Collections.singletonList(model) : new ArrayList<>());
        return model;
    }

    <T> int putAll(@NonNull DbPutterModel<T> model) {
        try {
            if (model.models == null && model.model != null) {
                put(model);
                return 0;
            }

            boolean isUsingMainThread = Looper.getMainLooper().getThread() == Thread.currentThread();
            if (!isUsingMainThread || (model.dbPutterListener == null && !model.useThread))
                return putAllInThread(model);
            else
                new Thread(() -> {
                    putAllInThread(model);
                }).start();
        } catch (Exception e) {
            log("putAll", e);
        }
        return 0;
    }

    private <T> int putAllInThread(@NonNull DbPutterModel<T> putter) {
        int newCount = 0, updateCount = 0;
        String table = null;
        Class cls = null;
        SoftReference<DbPutterListener> listener = putter.dbPutterListener != null ? new SoftReference<>(putter.dbPutterListener) : null;
        boolean isUsingMainThread = Looper.getMainLooper().getThread() == Thread.currentThread();
        List<T> insertedItems = new ArrayList<>();
        List<T> updatedItems = new ArrayList<>();
        if (putter.models != null && putter.models.size() > 0) {
            try {
                cls = putter.models.get(0).getClass();
                table = getTableName(cls, putter.tableName);
                DbQueriesMainModel mainModel = getQueryMainModel(cls, table);
                if (mainModel != null) {
                    List<DbTypeModel> types = mainModel.getDbFields();
                    List<Field> fields = getAllFields(cls, true, table, null, mainModel.getFields());
                    for (T model : putter.models) {
                        int[] count = putModel(model, putter.conflictAlgorithm, table, cls, mainModel, fields, types, true);
                        newCount += count[0];
                        updateCount += count[1];
                        if (count[0] > 0)
                            insertedItems.add(model);
                        else if (count[1] > 0)
                            updatedItems.add(model);
                        if (!isUsingMainThread)
                            sleep();
                    }
                }
            } catch (Exception e) {
                log("putAll", e);
            }
        }
        notifyAfterPut(newCount, updateCount, table, cls, listener, insertedItems, updatedItems);
        return newCount;
    }

    private <T> void notifyAfterPut(int newCount, int updateCount, @Nullable String table, @Nullable Class<T> cls, @Nullable SoftReference<DbPutterListener> listener, @NonNull List<T> insertedItems, @NonNull List<T> updatedItems) {
        try {
            if (TextUtils.isEmpty(table))
                return;
            new Handler(Looper.getMainLooper()).post(() -> {
                try {
                    if (listener != null ) {
                        DbPutterListener<T> l = listener.get();
                        if(l!=null) {
                            l.onComplete(table, cls, newCount, updateCount, insertedItems, updatedItems);
                        }
                    }

                    if (updatedItems.size() > 0 || insertedItems.size() > 0) {
                        insertedItems.addAll(updatedItems);
                        notifyObservers(cls, table, updateCount + newCount, false, false, false, false, insertedItems);
                    }
                } catch (Exception e) {
                    log("putAll on Result", e);
                }
            });

        } catch (Exception e) {
            log("putAll on Result", e);
        }
    }

    private <T> int[] putModel(@NonNull T model, InsertConflictAlgorithm conflictAlgorithm, @NonNull String table, @NonNull Class<T> cls, @NonNull DbQueriesMainModel mainModel, @NonNull List<Field> fields, @NonNull List<DbTypeModel> types, boolean needToSetPrimaryValue) {
        int[] count = {0, 0};
        try {
            ContentValues cv = new ContentValues();
            String primaryKey = null;
            Field primaryField = null;// This will always null if needToSetPrimaryValue=false OR mainModel#primaryFieldType==String.class (String primary field id not auto increment)
            for (DbTypeModel dbTypeModel : types) {
                Field f = fields.get(dbTypeModel.getIndex());//getField(model.getClass(), dbTypeModel.getClassFieldName());
                try {
                    assert f != null;
                    f.setAccessible(true);
                    String column = dbTypeModel.getColumnName();

                    if (column != null) {
                        Type type = f.getType();
                        if (type.equals(long.class)) {
                            cv.put(column, f.getLong(model));
                            if (mainModel.getPrimaryField().equals(column)) {
                                primaryKey = String.valueOf(f.getLong(model));
                                if (needToSetPrimaryValue)
                                    primaryField = f;
                            }
                        } else if (type.equals(int.class)) {
                            cv.put(column, f.getInt(model));
                        } else if (type.equals(boolean.class)) {
                            cv.put(column, f.getBoolean(model) ? 1 : 0);
                        } else if (type.equals(String.class)) {
                            cv.put(column, (String) f.get(model));
                            if (mainModel.getPrimaryField().equals(column)) {
                                primaryKey = (String) f.get(model);
//                                if (needToSetPrimaryValue)
//                                    primaryField = f;
                            }
                        } else if (type.equals(float.class)) {
                            cv.put(column, f.getFloat(model));
                        } else if (type.equals(double.class)) {
                            cv.put(column, f.getDouble(model));
                        } else {
                            Object fieldValue = f.get(model);
                            if (fieldValue != null)
                                cv.put(column, gson.toJson(fieldValue, getListTypeToken(f)));
                        }
                    }

                } catch (Exception e) {
                    log("PUT ALL class=" + cls.getName() + " : Field = " + f.getName() + "  field Type = " + f.getType().getName(), e);
                }
            }

            if (cv.size() > 0) {
                long insertedId=-1;
                if (!TextUtils.isEmpty(primaryKey) && (!TextUtils.isDigitsOnly(primaryKey) || Long.parseLong(primaryKey) > 0)) {
                    Cursor haveData = db.query(table, new String[]{mainModel.getPrimaryField()}, mainModel.getPrimaryField() + "=?", new String[]{primaryKey}, null, null, null, "1");
                    int cursorCount = haveData.getCount();
                    closeCursor(haveData);
                    if (cursorCount > 0) {
                        if (conflictAlgorithm == InsertConflictAlgorithm.REPLACE && db.update(table, cv, mainModel.getPrimaryField() + "=?", new String[]{primaryKey}) > 0) {
                            count[1] = 1;
                        }
                    } else {
                        if ((insertedId=db.insert(table, null, cv)) > 0) {
                            count[0] = 1;
                        }
                    }
                } else {
                    if (mainModel.getPrimaryFieldType().equals(long.class.getName()))
                        cv.remove(mainModel.getPrimaryField());

                    if ((insertedId=db.insert(table, null, cv)) > 0) {
                        count[0] = 1;
                    }
                }

                //Only autoincrement long value to be set
                try {
                    if (needToSetPrimaryValue && insertedId>0 && primaryField != null && mainModel.getPrimaryFieldType().equals(long.class.getName()) && Long.parseLong(primaryKey) < 1 && (count[0] > 0 || count[1] > 0)) {
                        primaryField.set( model,insertedId);
                    }
                } catch (Exception e) {
                    log("putAll : Set primary field", e);
                }
            }
        } catch (Exception e) {
            log("putAll", e);
        }

        return count;
    }


    <T> List<T> getAll(String table, Cursor query, Class<T> cls, DbQueriesMainModel mainModel, @Nullable DbGetterListener<T> dbGetterListener) {
        boolean isUsingMainThread = Looper.getMainLooper().getThread() == Thread.currentThread();
        SoftReference<DbGetterListener> listener = dbGetterListener != null ? new SoftReference<>(dbGetterListener) : null;
        List<T> list = new ArrayList<>();
        try {
            List<Field> fields = getAllFields(cls, true, table, null, mainModel.getFields());

            while (query.moveToNext()) {
                T model = cls.newInstance();
                for (DbTypeModel dbTypeModel : mainModel.getDbFields()) {

                    Field f = fields.get(dbTypeModel.getIndex());//getField(cls, dbTypeModel.getClassFieldName());
                    try {
                        assert f != null;
                        f.setAccessible(true);
                        String column = dbTypeModel.getColumnName();
                        if (column != null) {
                            Type type = f.getType();
                            int index = query.getColumnIndex(column);
                            if (type.equals(long.class)) {
                                f.set(model, query.getLong(index));
                            } else if (type.equals(int.class)) {
                                f.set(model, query.getInt(index));
                            } else if (type.equals(boolean.class)) {
                                f.set(model, query.getInt(index) == 1);
                            } else if (type.equals(String.class)) {
                                f.set(model, query.getString(index));
                            } else if (type.equals(float.class)) {
                                f.set(model, query.getFloat(index));
                            } else if (type.equals(double.class)) {
                                f.set(model, query.getDouble(index));
                            } else {
                                String content = query.getString(index);
                                if (content != null) {
                                    f.set(model, gson.fromJson(content, getListTypeToken(f)));
                                }
                            }
                        }

                    } catch (Exception e) {
                        log("getAllByLimit Set Field : field = " + f.getName(), e);
                    }
                }
                list.add(model);
                if (!isUsingMainThread)
                    sleep();
            }
            closeCursor(query);
        } catch (Exception e) {
            log("getAll", e);
        }

        if (listener != null) {
            try {
                DbGetterListener<T> dbGetterListener1 = listener.get();
                new Handler(Looper.getMainLooper()).post(() -> {
                    try {
                        dbGetterListener1.onComplete(list, table, cls, list.size());
                    } catch (Exception e) {
                        log("getAll on Result", e);
                    }
                });

            } catch (Exception e) {
                log("getAll on Result", e);
            }
        }

        return list;
    }

    <T> List<T> getAll(@NonNull DbGetterModel<T> model) {
        try {
            boolean isUsingMainThread = Looper.getMainLooper().getThread() == Thread.currentThread();

            if (!isUsingMainThread || model.dbGetterListener == null)
                return getAllinThread(model);
            else
                new Thread(() -> {
                    getAllinThread(model);
                }).start();
        } catch (Exception e) {
            log("getAllByLimit", e);
        }
        return new ArrayList<>();
    }

    private <T> List<T> getAllinThread(@NonNull DbGetterModel<T> model) {
        try {
            String table = getTableName(model.cls, model.tableName);
            DbQueriesMainModel mainModel = getQueryMainModel(model.cls, table);
            if (mainModel != null) {
                Cursor query = db.query(model.distinct, table, mainModel.getDbColumns(), model.condition, model.conditionArgs, model.groupBy, model.having, model.orderBy == null ? mainModel.getPrimaryField() : model.orderBy, model.limit);
                return getAll(table, query, model.cls, mainModel, model.dbGetterListener);
            } else
                log("getAllByLimit : Your class " + model.cls.getName() + " does not found in database", null);
        } catch (Exception e) {
            log("getAllByLimit", e);
        }
        return new ArrayList<>();
    }

    <T> T getSingleData(@NonNull DbGetterModel<T> model) {
        List<T> list = getAll(model.setLimit("1"));
        if (list.isEmpty())
            return null;
        return list.get(0);
    }


    <T> T getDataByPrimaryField(@NonNull Class<T> cls, @Nullable String tableName, @NonNull String primaryFieldValue) {
        DbQueriesMainModel mainModel = getQueryMainModel(cls, tableName);
        return getSingleData(DbGetterModel.getModelByCondition(cls, tableName, mainModel.getPrimaryField() + "=?", new String[]{String.valueOf(primaryFieldValue)}, "1", null));
    }

    public <T> int remove(@NonNull T model, @Nullable String tableName) {
        if (model != null)
            return removeAll(Collections.singletonList(model), tableName);
        return 0;
    }

    public <T> int removeAll(@NonNull List<T> models, @Nullable String tableName) {
        if (models != null && models.size() > 0 && models.get(0) != null) {
            DbQueriesMainModel mainModel = getQueryMainModel(models.get(0).getClass(), tableName);
            Field f = getField(models.get(0).getClass(), mainModel.getPrimaryClassField());
            boolean isLong = mainModel.getPrimaryFieldType().equals(long.class.getName());
            String columnName=mainModel.getPrimaryField();
            StringBuilder selection = new StringBuilder();
            List<String> args=new ArrayList<>();
            for (T model : models) {
                try {
                    args.add(""+(isLong?f.getLong(model):f.get(model)));
                    if(selection.length()>0)
                        selection.append(" OR ");
                    selection.append(columnName).append("=?");
                } catch (Exception ignored) {
                }
            }
            if(args.size()>0) {
                DbRemoverModel removerModel=new DbRemoverModel(models.get(0).getClass(),tableName).setSelection(selection.toString()).setSelectionArgs(args.toArray(new String[0]));
                return removeAll(removerModel);
            }
        }
        return 0;
    }


    <T> int removeAll(@NonNull DbRemoverModel<T> model) {
        try {
            boolean isUsingMainThread = Looper.getMainLooper().getThread() == Thread.currentThread();
            if (!isUsingMainThread || (model.remover == null && !model.useThread))
                return removeAllinThread(model);
            else
                new Thread(() -> {
                    removeAllinThread(model);
                }).start();
        } catch (Exception e) {
            log("getAllByLimit", e);
        }
        return 0;
    }

    /**
     * @param model We cannot fetch all the other items from the table after delete by selection  -> because we don't know the exact selection condition.
     *              So You should fetch the balance data using code like {@link #getAll(DbGetterModel)}
     */
    private <T> int removeAllinThread(@NonNull DbRemoverModel<T> model) {
        try {
            SoftReference<DbRemoverListener> listener = model.remover != null ? new SoftReference<>(model.remover) : null;
            String table = getTableName(model.cls, model.tableName);
            if (table != null) {
                //As per the doc, To remove all rows and get a count pass "1" as the whereClause.
                int count = db.delete(table, model.selection == null ? "1" : model.selection, model.selection == null ? null : model.selectionArgs);
//                if (listener != null && listener.get() != null) {
                new Handler(Looper.getMainLooper()).post(() -> {
                    try {
                        if (listener != null ) {
                            DbRemoverListener<T> removerListener = listener.get();
                            HashMap<Class, HashMap<String, Integer>> result = new HashMap<>();
                            HashMap<String, Integer> tables = new HashMap<>();
                            tables.put(model.tableName, count);
                            result.put(model.cls, tables);
                            removerListener.onComplete(result, count);
                        }
                        if (count > 0) {
                            if (model.selection == null)
                                notifyObservers(model.cls, table, 0, false, true, false, false, new ArrayList<>());
                            else
                                notifyObservers(model.cls, table, 0, true, false, false, false, new ArrayList<>());
                        }
                    } catch (Exception e) {
                        log("removeAll on Result", e);
                    }
                });
//                }
                return count;
            }
        } catch (Exception e) {
            log("removeAll  class : " + model.cls.getName(), e);
        }
        return 0;
    }

    public <T> void removeAll(@NonNull Class[] cls, @Nullable DbRemoverListener<T> dbRemoverListener, boolean useThread) {
        try {
            boolean isUsingMainThread = Looper.getMainLooper().getThread() == Thread.currentThread();
            if (!isUsingMainThread || (dbRemoverListener == null && !useThread))
                removeAllinThread(cls, dbRemoverListener);
            else
                new Thread(() -> {
                    removeAllinThread(cls, dbRemoverListener);
                }).start();
        } catch (Exception e) {
            log("getAllByLimit", e);
        }
    }

    private <T> void removeAllinThread(@NonNull Class[] cls, @Nullable DbRemoverListener<T> remover) {
        HashMap<Class, HashMap<String, Integer>> result = new HashMap<>();
        int totalCount = 0;
        for (Class c : cls) {
            if (c == null)
                continue;
            DbEntity annotation = Sfw.getAnnotation(DbEntity.class, c);
            String[] tables = annotation == null ? new String[]{c.getSimpleName()} : annotation.tables();
            HashMap<String, Integer> tableResult = new HashMap<>();
            for (String table : tables) {
                try {
                    if (!TextUtils.isEmpty(table)) {
                        int count = db.delete(table, null, null);
                        totalCount += count;
                        tableResult.put(table, count);
                    } else {
                        tableResult.put(table, 0);
                    }
                } catch (Exception e) {
                    log("removeAll  class : " + c.getName() + "  table : " + table, e);
                }
            }
            result.put(c, tableResult);
        }
        try {
            if (remover != null) {
                int count = totalCount;
                new Handler(Looper.getMainLooper()).post(() -> {
                    try {
                        remover.onComplete(result, count);
                    } catch (Exception e) {
                        log("removeAll on Result", e);
                    }
                });
            }
        } catch (Exception e) {
            log("removeAll  ", e);
        }
    }

    <T> void removeAllExcept(@Nullable List<Class> cls, @Nullable DbRemoverListener<T> remover, boolean useThread) {
        if (annotation != null) {
            Class[] classes = annotation.entities();
            List<Class> removeList = new ArrayList<>();
            if (cls != null)
                for (Class c : classes) {
                    if (!cls.contains(c)) {
                        removeList.add(c);
                    }
                }
            removeAll(removeList.toArray(new Class[removeList.size()]), remover, useThread);
        }
    }

    private String getTableName(Class cls, String tableName) {
        return getTableName(cls, tableName, true);
    }

    private String getTableName(@Nullable Class cls, @Nullable String tableName, boolean throwException) {
        if (cls == null) {
            if (throwException)
                throw new IllegalAccessError("Class should not be null");
            return null;
        }
        DbEntity annotation = Sfw.getAnnotation(DbEntity.class, cls);
        if (annotation == null) {
            if (throwException)
                throw new IllegalAccessError("" + cls.getName() + " should annotated with " + DbEntity.class.getName() + ".");
            return null;
        }
        if (!TextUtils.isEmpty(tableName)) {
            for (String table : annotation.tables())
                if (table.equals(tableName))
                    return tableName;
            if (throwException)
                throw new IllegalAccessError("" + cls.getName() + " does not have a table named " + tableName + ".");
            return null;
        }
        String table;
        if (tableName == null) {
            table = annotation.tables().length == 0 ? cls.getSimpleName() : annotation.tables()[0];
        } else {
            table = tableName;
        }
        return table;
    }

    private DbQueriesMainModel getQueryMainModel(@NonNull Class cls, @Nullable String tableName) {
        DbQueriesMainModel mainModel = dbQueriesMainModels.get(cls.getName() + "_" + getTableName(cls, tableName));
        if (mainModel != null)
            return mainModel;
        Cursor c = db.query(TBL_DB_CLASS_QUERIES, new String[]{CLM_CONTENT}, CLM_KEY + "=?", new String[]{cls.getName() + "_" + tableName}, null, null, null);
        if (c.moveToFirst()) {
            mainModel = gson.fromJson(c.getString(0), DbQueriesMainModel.class);
        }
        closeCursor(c);
        return mainModel;
    }


    //AUTO START
    boolean isAutoStartChecked() {
        return "true".equals(getCommonModel("" + KEY_AUTO_START).getContent());
    }

    void setAutoStartChecked(boolean checked) {
        addCommonValues("" + KEY_AUTO_START, "autostart", "" + checked);
    }


    void setItemsForGallery(List<ImageModel> list, int catId) {
        if (list == null)
            return;
        addCommonValues(KEY_GALLERY + "_" + catId, "gallery", serialize(new TypeToken<ArrayList<ImageModel>>() {
        }.getType(), list));
    }

    List<ImageModel> getItemsForGallery(int catId) {
        return deSerialize(getCommonModel(KEY_GALLERY + "_" + catId).getContent(), new TypeToken<ArrayList<ImageModel>>() {
        }.getType());
    }

    public boolean isLoggedIn() {
        return true;
    }

    void onCreate(SQLiteDatabase db) {
        try {

            db.execSQL("CREATE TABLE IF NOT EXISTS " + TBL_COMMON + "(" +
                    CLM_ID + " INTEGER PRIMARY KEY AUTOINCREMENT," + CLM_KEY + " VARCHAR(100)," +
                    CLM_TITLE + " VARCHAR(150)," +
                    CLM_CONTENT + " TEXT," + CLM_UPDATED_TIME + " DATETIME)");

            db.execSQL("CREATE TABLE IF NOT EXISTS " + TBL_DB_CLASS_QUERIES + "(" + CLM_KEY + " VARCHAR(200) PRIMARY KEY," + CLM_CONTENT + " TEXT," + CLM_TABLE + " VARCHAR(100))");

            createOrUpdateTable(false, db);

        } catch (Exception e) {
            log("DbOnCreate", e);
        }
    }

    private void createOrUpdateTable(boolean isForUpgrade, SQLiteDatabase db) {
        if (annotation != null) {
            HashMap<String, Class> allTableNames = new HashMap<>();
            for (Class cls : annotation.entities()) {
                DbEntity classAnnotation = Sfw.getAnnotation(DbEntity.class, cls);

                if (classAnnotation == null) {
                    setConstructorError("" + cls.getName() + " should annotated with " + DbEntity.class.getName() + ".");
                }
                String[] tableNames = classAnnotation.tables();
                if (tableNames.length == 0)
                    tableNames = new String[]{cls.getSimpleName()};

                List<String> tablesToDelete = Arrays.asList(classAnnotation.clearTablesOnUpgrade());
                if (tablesToDelete.size() == 1 && tablesToDelete.get(0).toLowerCase().equals("all"))
                    tablesToDelete = Arrays.asList(tableNames);
//                createTable(db, cls, classAnnotation, tableNames);
                for (String table : tableNames) {
                    String lowercase = table.toLowerCase();
                    if (allTableNames.containsKey(lowercase))
                        setConstructorError("Table name (" + table + ") already used in the class " + allTableNames.get(table).getName());

                    if (TextUtils.isEmpty(lowercase) || lowercase.startsWith("sfw_") || lowercase.contains(",")) {
                        setConstructorError("Table name should not be empty OR starts with \"sfw_\" OR contain \",\"].  " + cls.getName() + " should rename the table " + table + ".");
                    }
                    if (lowercase.equals("tbl_class_queries") || lowercase.equals("tbl_common") || lowercase.equals("all"))
                        setConstructorError("This table name is a reserved keyword :  table = " + table);
                    isValidName(table, false);
                    allTableNames.put(lowercase, cls);
                    if (isForUpgrade && tablesToDelete.contains(table)) {
                        try {
                            db.delete(table, null, null);
                        } catch (Exception ignored) {
                        }
                    }
                    String upgradeOrCreate = isForUpgrade ? "Upgrade" : "Create";

                    boolean havePrimaryField = false;
                    StringBuilder builder = new StringBuilder();
                    List<Field> fields = getAllFields(cls, true, table, tableNames, null);
                    List<DbTypeModel> dbFields = new ArrayList<>();
                    List<String> classFields = new ArrayList<>();

                    String primaryDbName = null, primaryFieldName = null, primaryFieldType = null;
                    List<String> columnNames = new ArrayList<>();

                    boolean useRetrofitAnnotationForColumns = classAnnotation.useRetrofitAnnotationForColumnName();
                    for (int i = 0, j = fields.size(); i < j; i++) {
                        Field f = fields.get(i);

                        try {
//                            if (Modifier.isFinal(f.getModifiers())) {
//                                log("Create table for class " + cls.getName() + ". Final fields are not allowed.  Ignoring field \" " + f.getName() + " \"", null);
//                                continue;
//                            }
                            Class type = f.getType();
                            DbAnnotation a = f.getAnnotation(DbAnnotation.class);
                            String columnName = f.getName();
                            int length = a == null ? 0 : a.length();
                            if (a != null && !a.columnName().trim().isEmpty())
                                columnName = a.columnName().trim();
                            else if (useRetrofitAnnotationForColumns) {
                                SerializedName serializedName = f.getAnnotation(SerializedName.class);
                                if (serializedName != null && !serializedName.value().trim().isEmpty())
                                    columnName = serializedName.value().trim();
                            }
                            isValidName(columnName, false);
                            if (columnNames.contains(columnName)) {
                                setConstructorError(upgradeOrCreate + " table for class " + cls.getName() + ". Column name already used. column name : " + columnName + ". Field \" " + f.getName() + " \"");
                            }
                            if (builder.length() > 0)
                                builder.append(",");
                            columnNames.add(columnName);
                            builder.append(columnName);
                            ColumnTypes columnType = null;

                            if (a != null) {
                                columnType = a.columnType();
                            }
                            DbPrimary primary = f.getAnnotation(DbPrimary.class);
                            boolean isPrimaryField = false;
                            boolean setPrimaryField = false;

                            if (!havePrimaryField && primary != null) {
                                List<String> primaryTables = primary == null ? new ArrayList<>() : Arrays.asList(primary.tables());
                                if (primaryTables.size() > 0) {
                                    if (primaryTables.get(0).toLowerCase().equals("all") || primaryTables.contains(table)) {
                                        setPrimaryField = true;
                                    }
                                }
                            }

                            if (setPrimaryField) {
//                                if (primary != null) {
//                                    if (havePrimaryField)
//                                        throw new IllegalAccessError("Class should not contain more than one primary key --" + f.getName() + "-- (" + cls.getName() + ").");
                                if (!type.equals(long.class) && !type.equals(String.class)) {
                                    setConstructorError("Primary key should be a long or String type --" + f.getName() + "-- (" + cls.getName() + ").");
                                }
                                isPrimaryField = true;
                                havePrimaryField = true;
                                primaryDbName = columnName;
                                primaryFieldName = f.getName();
                                primaryFieldType = f.getType().getName();
//                                builder.append(" INTEGER PRIMARY KEY AUTOINCREMENT");
                            }
                            if (type.equals(long.class)) {
                                if (isPrimaryField)
                                    builder.append(" INTEGER PRIMARY KEY AUTOINCREMENT");
                                else
                                    builder.append(" INTEGER");
                            } else if (type.equals(int.class)) {
                                builder.append(" INTEGER (").append(length == 0 ? 10 : length).append(")");
                            } else if (type.equals(boolean.class)) {
                                builder.append(" INTEGER (1) DEFAULT 0");
                            } else if (type.equals(String.class)) {
                                if (columnType == null || columnType == ColumnTypes.VARCHAR)
                                    builder.append(" VARCHAR(").append(length == 0 ? 200 : length).append(")");
                                else
                                    builder.append(" TEXT");
                                if (isPrimaryField)
                                    builder.append(" PRIMARY KEY");
                            } else if (type.equals(float.class) || type.equals(double.class)) {
                                builder.append(" FLOAT");
                            } else {
                                builder.append(" TEXT");
                            }

                            dbFields.add(new DbTypeModel(f.getName(), columnName, i));
                            classFields.add(f.getName());
                        } catch (Exception e) {
                            log(upgradeOrCreate + "  DB :  class=" + cls.getName(), e);
                        }

                    }

                    if (builder.length() > 0) {
                        if (!havePrimaryField) {
                            setConstructorError("You should annotate a field using @DbPrimary. If you are already annotated, please check the table names provided.  @DbPrimary)tables={\"all\"} ");
                        }
//                        if (isForUpgrade) {
                        try {
                            Cursor c = db.query(TBL_DB_CLASS_QUERIES, new String[]{CLM_CONTENT}, CLM_KEY + "=?", new String[]{cls.getName() + "_" + table}, null, null, null, null);

                            if (c.moveToFirst()) {
                                DbQueriesMainModel mainModel = gson.fromJson(c.getString(0), DbQueriesMainModel.class);
                                closeCursor(c);
                                String[] dbColumns = mainModel.getDbColumns();

                                //Conditions -> field name change , column name change , field change (ie, assign a column name to other field), field add or remove , add or remove @DbExclude annotation
                                if (!Arrays.equals(dbColumns, columnNames.toArray(new String[0])) || !mainModel.getDbFields().equals(dbFields) || !mainModel.getPrimaryField().equals(primaryDbName) || !mainModel.getPrimaryClassField().equals(primaryFieldName) || !mainModel.getPrimaryFieldType().equals(primaryFieldType)) {
                                    if (!isForUpgrade)
                                        setConstructorError("The entity or table integrity changed. You should upgrade your db version to alter the table  : table = " + table + "  class = " + cls.getName());

                                    List<String> backupColumns = new ArrayList<>();
                                    for (String clm : dbColumns) {
                                        if (columnNames.contains(clm)) {
                                            backupColumns.add(clm);
                                        }
                                    }
                                    String columns = TextUtils.join(",", backupColumns);
                                    if (!columns.isEmpty()) {
                                        boolean dataBackedup = false;
                                        String backupTable = "sfw_" + table;

                                        db.execSQL("CREATE TEMPORARY TABLE " + backupTable + " (" + columns + ")");
                                        try {
                                            db.execSQL("INSERT INTO " + backupTable + " (" + columns + ")  SELECT " + columns + " FROM " + table);
                                            db.execSQL("DROP TABLE " + table);
                                            dataBackedup = true;
                                        } catch (Exception ignored) {
                                        }
                                        if (isDebuggingEnabled)
                                            Log.e(upgradeOrCreate + " Sql", "CREATE TABLE IF NOT EXISTS " + table + " (" + builder.toString() + ")");
                                        db.execSQL("CREATE TABLE IF NOT EXISTS " + table + " (" + builder.toString() + ")");
                                        if (dataBackedup) {
                                            try {
                                                db.execSQL("INSERT INTO " + table + " (" + columns + ") SELECT " + columns + " FROM " + backupTable);
                                            } catch (Exception e) {
                                                if (isDebuggingEnabled)
                                                    Log.e(" RESTORING", "Data restoring failed. Table = " + table + " CLASS = " + cls.getName(), e);
                                            }
                                        }
                                        db.execSQL("DROP TABLE " + backupTable);

                                    }
                                }
                            } else {
                                closeCursor(c);
                                if (isDebuggingEnabled)
                                    Log.e(upgradeOrCreate + " Sql", "CREATE TABLE IF NOT EXISTS " + table + " (" + builder.toString() + ")");
                                db.execSQL("CREATE TABLE IF NOT EXISTS " + table + " (" + builder.toString() + ")");
                            }
                        } catch (Exception e) {
                            setConstructorError(e.getLocalizedMessage());
                        }
                        ContentValues cv = new ContentValues();
                        cv.put(CLM_KEY, cls.getName() + "_" + table);
                        DbQueriesMainModel mainModel = new DbQueriesMainModel(columnNames.toArray(new String[0]), dbFields, primaryDbName, primaryFieldName, primaryFieldType, classFields);
                        cv.put(CLM_CONTENT, gson.toJson(mainModel));
                        cv.put(CLM_TABLE, table);
                        db.replace(TBL_DB_CLASS_QUERIES, null, cv);

                    } else {
                        setConstructorError("The DbEntity class -> " + cls.getName() + " does not have any columns.");
                    }

                }
            }
            if (isForUpgrade) {
                //Remove old tables that is not in the new tables [tableNames]
                Cursor c = db.query(TBL_DB_CLASS_QUERIES, new String[]{CLM_TABLE}, null, null, null, null, null);
                while (c.moveToNext()) {
                    String table = c.getString(0).toLowerCase();
                    if (!allTableNames.containsKey(table)) {
                        try {
                            db.delete(c.getString(0), null, null);
                        } catch (Exception ignored) {
                        }
                    }
                }
                closeCursor(c);
            }
        }
    }


    void onUpgrade(SQLiteDatabase db) {
        try {
            createOrUpdateTable(true, db);
        } catch (Exception e) {
            log("DbOnUpgrade", e);
        }
    }
}
