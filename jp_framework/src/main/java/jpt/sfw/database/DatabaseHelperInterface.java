package jpt.sfw.database;

import java.util.List;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;

public interface DatabaseHelperInterface<T> {
    void  onDbContentChanged(@Nullable Class<T> cls, @NonNull String key, int objectCount,boolean isThisRemovedItems,boolean isThisAFullList, boolean isFromCommonTable, boolean isFromStaticMemory, @NonNull List<T> content);
}
