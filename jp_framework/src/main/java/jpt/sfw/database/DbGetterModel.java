package jpt.sfw.database;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;

public class DbGetterModel<T> {
    @NonNull
    Class<T> cls;
    @Nullable
    String tableName;
    String condition;
    String[] conditionArgs;
    String limit;
    String orderBy;
    String groupBy;
    String having;
    boolean distinct;
    @Nullable
    DbGetterListener<T> dbGetterListener;


    public DbGetterModel(@NonNull Class<T> cls) {
        this.cls = cls;
    }

    public DbGetterModel(@NonNull Class<T> cls, @Nullable String tableName) {
        this.cls = cls;
        this.tableName = tableName;
    }

    public DbGetterModel(@NonNull Class<T> cls, @Nullable DbGetterListener<T> dbGetterListener) {
        this.cls = cls;
        this.dbGetterListener = dbGetterListener;
    }

    public DbGetterModel(@NonNull Class<T> cls, @Nullable String tableName, @Nullable DbGetterListener<T> dbGetterListener) {
        this.cls = cls;
        this.tableName = tableName;
        this.dbGetterListener = dbGetterListener;
    }

    public  static  <T> DbGetterModel<T> getModelByCondition(@NonNull Class<T> cls, @Nullable String tableName, String condition, String[] conditionArgs, String limit, @Nullable DbGetterListener<T> dbGetterListener) {
        return new DbGetterModel<>(cls,tableName,dbGetterListener).setCondition(condition).setConditionArgs(conditionArgs).setLimit(limit);
    }

    public  static  <T> DbGetterModel<T> getModelByLimit(@NonNull Class<T> cls, @Nullable String tableName, String limit, @Nullable DbGetterListener<T> dbGetterListener) {
        return new DbGetterModel<>(cls,tableName,dbGetterListener).setLimit(limit);
    }

    public  static  <T> DbGetterModel<T> getModelOrderBy(@NonNull Class<T> cls, @Nullable String tableName, String orderBy) {
        return new DbGetterModel<>(cls,tableName).setOrderBy(orderBy);
    }

    @Nullable
    public String getTableName() {
        return tableName;
    }

    public DbGetterModel<T> setTableName(@Nullable String tableName) {
        this.tableName = tableName;
        return this;
    }

    public String getCondition() {
        return condition;
    }

    public DbGetterModel<T> setCondition(String condition) {
        this.condition = condition;
        return this;
    }

    public String[] getConditionArgs() {
        return conditionArgs;
    }

    public DbGetterModel<T> setConditionArgs(String[] conditionArgs) {
        this.conditionArgs = conditionArgs;
        return this;
    }

    public String getLimit() {
        return limit;
    }

    public DbGetterModel<T> setLimit(String limit) {
        this.limit = limit;
        return this;
    }

    public String getOrderBy() {
        return orderBy;
    }

    public DbGetterModel<T> setOrderBy(String orderBy) {
        this.orderBy = orderBy;
        return this;
    }

    public String getGroupBy() {
        return groupBy;
    }

    public DbGetterModel<T> setGroupBy(String groupBy) {
        this.groupBy = groupBy;
        return this;
    }

    public String getHaving() {
        return having;
    }

    public DbGetterModel<T> setHaving(String having) {
        this.having = having;
        return this;
    }

    public boolean isDistinct() {
        return distinct;
    }

    public DbGetterModel<T> setDistinct(boolean distinct) {
        this.distinct = distinct;
        return this;
    }

    @Nullable
    public DbGetterListener<T> getDbGetterListener() {
        return dbGetterListener;
    }

    public DbGetterModel<T> setDbGetterListener(@Nullable DbGetterListener<T> dbGetterListener) {
        this.dbGetterListener = dbGetterListener;
        return this;
    }
}
