package jpt.sfw.database;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

public class DbQueriesMainModel {
    private List<DbTypeModel> dbFields;
    private String primaryField;
    private String primaryClassField;
    private String [] dbColumns;
    private String primaryFieldType;
    private List<String> fields;

    public DbQueriesMainModel() {
    }

    public DbQueriesMainModel(String [] dbColumns,List<DbTypeModel> dbFields, String primaryField, String primaryClassField,String primaryFieldType,List<String> fields) {
        this.dbFields = dbFields;
        this.primaryField = primaryField;
        this.primaryClassField = primaryClassField;
        this.dbColumns=dbColumns;
        this.primaryFieldType=primaryFieldType;
        this.fields=fields==null?new ArrayList<>():fields;
    }


    public List<DbTypeModel> getDbFields() {
        return dbFields;
    }

    public DbQueriesMainModel setDbFields(List<DbTypeModel> dbFields) {
        this.dbFields = dbFields;
        return this;
    }

    public String getPrimaryField() {
        return primaryField;
    }

    public DbQueriesMainModel setPrimaryField(String primaryField) {
        this.primaryField = primaryField;
        return this;
    }

    public String getPrimaryClassField() {
        return primaryClassField;
    }

    public DbQueriesMainModel setPrimaryClassField(String primaryClassField) {
        this.primaryClassField = primaryClassField;
        return this;
    }

    public String[] getDbColumns() {
        return dbColumns;
    }

    public DbQueriesMainModel setDbColumns(String[] dbColumns) {
        this.dbColumns = dbColumns;
        return this;
    }

    public String getPrimaryFieldType() {
        return primaryFieldType;
    }

    public DbQueriesMainModel setPrimaryFieldType(String primaryFieldType) {
        this.primaryFieldType = primaryFieldType;
        return this;
    }

    public List<String> getFields() {
        return fields;
    }

    public DbQueriesMainModel setFields(List<String> fields) {
        this.fields = fields;
        return this;
    }

    @Override
    public String toString() {
        return "DbQueriesMainModel{" +
                "dbFields=" + dbFields.toString() +
                ", primaryField='" + primaryField + '\'' +
                ", primaryClassField='" + primaryClassField + '\'' +
                ", dbColumns=" + Arrays.toString(dbColumns) +
                '}';
    }
}
