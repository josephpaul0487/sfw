package jpt.sfw.database;

import java.util.List;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;

public interface DbPutterListener<T> {
    /**
     * @param table                 To which table
     * @param classType             To which entity
     * @param newItemCount          The newly added items count
     * @param updatedItemCount      The updated rows count
     * @param updatedItems          Will return the updated  models
     * @param insertedItems         Will return the inserted models
     */
    void onComplete(@Nullable String table, @Nullable Class<T> classType, int newItemCount, int updatedItemCount, @NonNull List<T> updatedItems,@NonNull List<T> insertedItems);
}
