package jpt.sfw.database;

import android.database.sqlite.SQLiteDatabase;

import java.util.List;

import androidx.annotation.Nullable;
import jpt.sfw.enums.InsertConflictAlgorithm;

public class DbPutterModel<T> {
    @Nullable
    T model;
    @Nullable String tableName;
    boolean useThread;
    @Nullable DbPutterListener dbPutterListener;
    @Nullable
    List<T> models;
    DbQueryBuilder conditions;

    /**
     * for insert conflict resolver
     */
    InsertConflictAlgorithm conflictAlgorithm= InsertConflictAlgorithm.REPLACE;


    /**
     * to put single model
     */
    public DbPutterModel(@Nullable T model, @Nullable String tableName, boolean useThread) {
        this.model = model;
        this.tableName = tableName;
        this.useThread = useThread;
    }

    /**
     * to put single model
     */
    public DbPutterModel(@Nullable T model, @Nullable String tableName, boolean useThread, @Nullable DbPutterListener dbPutterListener, InsertConflictAlgorithm conflictAlgorithm) {
        this.model = model;
        this.tableName = tableName;
        this.useThread = useThread;
        this.dbPutterListener = dbPutterListener;
        this.conflictAlgorithm = conflictAlgorithm;
    }

    /**
     * to put single model
     */
    public DbPutterModel(@Nullable T model) {
        this.model = model;
    }

    /**
     * to put a list
     */
    public DbPutterModel(@Nullable List<T> models) {
        this.models = models;
    }

    public DbPutterModel(@Nullable String tableName, boolean useThread, @Nullable List<T> models) {
        this.tableName = tableName;
        this.useThread = useThread;
        this.models = models;
    }

    public static <T> DbPutterModel<T> putAll(@Nullable String tableName, @Nullable DbPutterListener dbPutterListener, boolean useThread, @Nullable List<T> models) {
        return new DbPutterModel<>(tableName,useThread,models).setDbPutterListener(dbPutterListener);
    }

    public static <T> DbPutterModel<T> putAll(@Nullable String tableName, @Nullable DbPutterListener dbPutterListener, boolean useThread, @Nullable List<T> models, InsertConflictAlgorithm conflictAlgorithm) {
        return new DbPutterModel<>(tableName,useThread,models).setDbPutterListener(dbPutterListener).setConflictAlgorithm(conflictAlgorithm);
    }


    @Nullable
    public T getModel() {
        return model;
    }

    public DbPutterModel<T> setModel(@Nullable T model) {
        this.model = model;
        return this;
    }

    @Nullable
    public String getTableName() {
        return tableName;
    }

    public DbPutterModel<T> setTableName(@Nullable String tableName) {
        this.tableName = tableName;
        return this;
    }

    public boolean isUseThread() {
        return useThread;
    }

    public DbPutterModel<T> setUseThread(boolean useThread) {
        this.useThread = useThread;
        return this;
    }

    @Nullable
    public DbPutterListener getDbPutterListener() {
        return dbPutterListener;
    }

    public DbPutterModel<T> setDbPutterListener(@Nullable DbPutterListener dbPutterListener) {
        this.dbPutterListener = dbPutterListener;
        return this;
    }

    @Nullable
    public List<T> getModels() {
        return models;
    }

    public DbPutterModel<T> setModels(@Nullable List<T> models) {
        this.models = models;
        return this;
    }

    public InsertConflictAlgorithm getConflictAlgorithm() {
        return conflictAlgorithm;
    }

    public DbPutterModel<T> setConflictAlgorithm(InsertConflictAlgorithm conflictAlgorithm) {
        this.conflictAlgorithm = conflictAlgorithm;
        return this;
    }

    public DbQueryBuilder getConditions() {
        return conditions;
    }

    public DbPutterModel<T> setConditions(DbQueryBuilder conditions) {
        this.conditions = conditions;
        return this;
    }
}
