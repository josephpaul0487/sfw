package jpt.sfw;

import android.content.Intent;
import android.content.IntentFilter;
import android.os.Bundle;
import android.view.View;

import jpt.sfw.app.AlertConfig;
import jpt.sfw.app.BackStackHolder;
import jpt.sfw.app.FragmentConfig;
import jpt.sfw.app.ProgressConfig;
import jpt.sfw.database.SfwDatabase;
import jpt.sfw.app.ToastConfig;
import jpt.sfw.appui.AlertDialog;
import jpt.sfw.appui.ProgressDialog;
import jpt.sfw.interfaces.SfwActivityHelperInterface;
import jpt.sfw.interfaces.SfwActivityInterface;
import jpt.sfw.webservice.ApiHelperListener;
import jpt.sfw.interfaces.OnActivityListener;
import jpt.sfw.interfaces.OnAlertListener;

import androidx.annotation.CallSuper;
import androidx.annotation.DrawableRes;
import androidx.annotation.IdRes;
import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.annotation.StringRes;
import androidx.appcompat.app.AppCompatActivity;
import androidx.fragment.app.Fragment;

//import jpt.sfw.fragments.SfwFragment;

/**
 * Using  1-3  permission constants
 */
public abstract class SfwActivity<D extends SfwDatabase> extends AppCompatActivity implements SfwActivityInterface, SfwActivityHelperInterface, ApiHelperListener, OnAlertListener {

    private SfwActivityHelper sfwActivityHelper;
    public D db = (D) SfwDatabase.getInstance();

    @Override
    public final void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (db != null)
            db.recheck();
        sfwActivityHelper = new SfwActivityHelper(this);
        sfwActivityHelper.onCreate(savedInstanceState);
    }

    /**
     * Should override this method to use programmatically generated view as content view (ie, not from the layout resource)
     */
    @Override
    public View getContentView() {
        return null;
    }

    @Override
    public void setStatusBarColor(int statusBarColor) {
        sfwActivityHelper.setStatusBarColor(statusBarColor);
    }

    /**
     * @return true  if no need to call {@link #callHome()}
     */
    public abstract boolean init(@Nullable Bundle savedInstanceState);

    @CallSuper
    @Override
    public void initListeners() {
        sfwActivityHelper.initListeners();
    }

    public final void registerReceiver() {
        sfwActivityHelper.registerReceiver();
    }

    /**
     * Should implement this method of child activities to check drawer status -> opened or not
     * eg:   [HomeActivity.onBackPressed]
     */
    @Override
    public void onBackPressed() {
        sfwActivityHelper.onBackPressed();
    }

    @CallSuper
    @Override
    public void onDestroy() {
        try {
            if (db != null)
                db.clearObservers(getClass());
            sfwActivityHelper.onDestroy();
            super.onDestroy();
            if (sfwActivityHelper.isCallGConExit())
                System.gc();
        } catch (Exception ignored) {
        }
    }

    @CallSuper
    @Override
    public void onLowMemory() {
        super.onLowMemory();
        sfwActivityHelper.onLowMemory();
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        sfwActivityHelper.onActivityResult(requestCode, resultCode, data);
        super.onActivityResult(requestCode, resultCode, data);
    }


    @Override
    public void onRequestPermissionsResult(int requestCode, String[] permissions, int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        sfwActivityHelper.onRequestPermissionsResult(requestCode, permissions, grantResults);
    }

    @CallSuper
    @Override
    protected void onResumeFragments() {
        super.onResumeFragments();
        sfwActivityHelper.onResumeFragments();
    }

    public final void addOnActivityListener(OnActivityListener onActivityListener) {
        sfwActivityHelper.addOnActivityListener(onActivityListener);
    }

    public final void removeOnActivityListener(OnActivityListener onActivityListener) {
        sfwActivityHelper.removeOnActivityListener(onActivityListener);
    }

    public final Fragment getCurrentFragment() {
        return sfwActivityHelper.getCurrentFragment();
    }

    public final Fragment getCurrentFragment(@IdRes int containerId) {
        return sfwActivityHelper.getCurrentFragment(containerId);
    }


    @Override
    public final void showProgress(ProgressConfig config) {
        sfwActivityHelper.showProgress(config, false);
    }

    public final void showProgress(ProgressConfig config, boolean changeMsgIfShowing) {
        sfwActivityHelper.showProgress(config, changeMsgIfShowing);
    }

    @NonNull
    public ProgressDialog createProgressDialog() {
        return new ProgressDialog(this);
    }


    @CallSuper
    @Override
    public void dismissProgress() {
        sfwActivityHelper.dismissProgress();
    }

    public ProgressDialog getProgressDialog() {
        return sfwActivityHelper.getProgressDialog();
    }

    @Override
    public final void showMessage(@NonNull AlertConfig config) {
        sfwActivityHelper.showMessage(config);
    }

    @Override
    @NonNull
    public AlertDialog createAlertDialog() {
        return new AlertDialog(this);
    }

    public AlertDialog getAlertDialog() {
        return sfwActivityHelper.getAlertDialog();
    }

    public void showInternetErrMsg(boolean dismissProgressDialog, int requestCode, boolean setRetry, @Nullable OnAlertListener listener) {
        showInternetErrMsg(true, requestCode, setRetry, /*setRetry ?*/ R.drawable.ic_close /*: 0*/, listener);
    }

    public void showInternetErrMsg(boolean dismissProgressDialog, int requestCode, boolean setRetry, @DrawableRes int negativeImage, @Nullable OnAlertListener listener) {
        showInternetErrMsg(true, requestCode, setRetry ? R.drawable.ic_refresh : R.drawable.ic_accept, negativeImage, listener);
    }

    public void showInternetErrMsg(boolean dismissProgressDialog, int requestCode, @DrawableRes int positiveImage, @DrawableRes int negativeImage, @Nullable OnAlertListener listener) {
        sfwActivityHelper.showInternetErrMsg(dismissProgressDialog, requestCode, positiveImage, negativeImage, listener);
    }


    public void toast(@StringRes int stringId) {
        sfwActivityHelper.toast(stringId);
    }

    public void toast(CharSequence msg) {
        sfwActivityHelper.toast(msg);
    }

    @Override
    public void toast(ToastConfig msg) {
        sfwActivityHelper.toast(msg);
    }

    public final FragmentConfig createConfig(@NonNull Fragment fragment, CharSequence pageTitle, Object data) {
        return sfwActivityHelper.createConfig(fragment, pageTitle, data);
    }

    public final boolean changeFragment(@NonNull Fragment fragment, CharSequence pageTitle, Object data) {
        return sfwActivityHelper.changeFragment(fragment, pageTitle, data);
    }

    public final boolean changeFragment(@NonNull Fragment fragment, CharSequence pageTitle, Object data, boolean needDrawer) {
        return sfwActivityHelper.changeFragment(fragment, pageTitle, data, needDrawer);
    }

    public boolean addTagAndChangeFragment(@NonNull Fragment fragment, @NonNull FragmentConfig configWithoutTag) {
        return sfwActivityHelper.addTagAndChangeFragment(fragment, configWithoutTag);
    }


    /**
     * @param fragment new fragment
     * @Param config   Configuration
     */

    @Override
    public final boolean changeFragment(@NonNull Fragment fragment, @NonNull FragmentConfig config) {
        return sfwActivityHelper.changeFragment(fragment, config);
    }


    public void onClick(View v, int viewId) {

    }


    public boolean broadcastMessageReceived(Intent intent) {
        return false;
    }

    public void onLoginDetailsChanged(boolean forceLogout) {
    }


    /**
     * * should call this from {@link Fragment#onViewCreated(View, Bundle)}, if you are not setting any animation on fragment changing
     * OR call this from {@link Fragment#onCreateAnimator(int, boolean, int)} OR {@link Fragment#onCreateAnimation(int, boolean, int)}
     */
    @CallSuper
    public void afterFragmentCreated(@NonNull Fragment f) {
        sfwActivityHelper.afterFragmentCreated(f);

    }


    /**
     * Clear all fragments from the memory except the given fragment tag
     */
    public final boolean clearBackStack(String fragmentTagToBeKeep) {
        return sfwActivityHelper.clearBackStack(fragmentTagToBeKeep);
    }

    @Override
    public boolean clearBackStack(int upto) {
        return sfwActivityHelper.clearBackStack(upto);
    }

    /**
     * will clear the given count of backstacks from the top
     */
    @Override
    public boolean clearTopBackStacks(int count) {
        return sfwActivityHelper.clearTopBackStacks(count);
    }

    /**
     * will clear the given count of backstacks from the bottom
     */
    @Override
    public boolean clearBottomBackStacks(int count) {
        return sfwActivityHelper.clearBottomBackStacks(count);
    }

    /**
     * will clear the  backstacks upto (includeThis is false) or just below the fragmentTagUpTo index  (if includeThis is true)
     */
    @Override
    public boolean clearBackStackUpTo(String fragmentTagUpTo, boolean includeThis) {
        return sfwActivityHelper.clearBackStackUpTo(fragmentTagUpTo, includeThis);
    }

    /**
     * will clear the given count of backstacks from the top
     */
    @Override
    public boolean clearBackStacks(int startIndex, int count, boolean topToBottom) {
        return sfwActivityHelper.clearBackStacks(startIndex, count, topToBottom);
    }

    /**
     * Clearing the given fragment tag from backStack
     */
    public final boolean clearSingleFragment(String fragmentTagToBeCleared, boolean forceRemoveTop) {
        return sfwActivityHelper.clearSingleFragment(fragmentTagToBeCleared, forceRemoveTop);
    }

    public final View getFragmentViewFromBackStack(String tag) {
        return sfwActivityHelper.getFragmentViewFromBackStack(tag);
    }

    @Override
    public Fragment getFragment(String tag) {
        return sfwActivityHelper.getFragment(tag);
    }

    public final FragmentConfig getFragmentConfig(String tag) {
        return sfwActivityHelper.getFragmentConfig(tag);
    }

    public final FragmentConfig getFragmentConfig(Fragment fragment) {
        return sfwActivityHelper.getFragmentConfig(fragment);
    }

    public final void setViewToBackStack(View view, String tag) {
        sfwActivityHelper.setViewToBackStack(view, tag);
    }

    public final void setDataToBackStack(Object data, String tag) {
        sfwActivityHelper.setDataToBackStack(data, tag);
    }

    @Override
    public BackStackHolder.FragmentContentHolder getTopBackStackHolder(String fragmentTag) {
        return sfwActivityHelper.getTopBackStackHolder(fragmentTag);
    }

    @Override
    public BackStackHolder.FragmentContentHolder getBottomBackStackHolder(String fragmentTag) {
        return sfwActivityHelper.getBottomBackStackHolder(fragmentTag);
    }

    @Override
    public boolean isTagExistsInBackStack(String fragmentTag) {
        return sfwActivityHelper.isTagExistsInBackStack(fragmentTag);
    }

    @Override
    public boolean isBackStackTop(String fragmentTagToCheck, String fragmentToCompare) {
        return sfwActivityHelper.isBackStackTop(fragmentTagToCheck, fragmentToCompare);
    }

    @Override
    public int getBackStackIndex(String fragmentTag) {
        return sfwActivityHelper.getBackStackIndex(fragmentTag);
    }

    @Override
    public void onRecreateCalled() {
        sfwActivityHelper.onRecreateCalled();
    }



    @Override
    public boolean changeFragmentToLastHolder() {
        return sfwActivityHelper.changeFragmentToLastHolder();
    }

    public final int getBackStackCount() {
        return sfwActivityHelper.getBackStackCount();
    }

    /**
     * To check current fragment is home or not (to check whether this activity should finished or not)
     * {@link #onBackPressed()}
     *
     * @param fragment
     * @return
     */
    public abstract boolean isHome(@Nullable Fragment fragment);

    /**
     * It will call on {@link #onBackPressed()}
     */

    public abstract boolean callHome();


    public void onInternetConnectionChange(boolean isNetworkAvailable) {
    }

    public void addBroadcastReceiverActions(IntentFilter filter) {
    }

    public boolean isFragmentAnimationUsed() {
        return sfwActivityHelper.isFragmentAnimationUsed();
    }

    @Override
    public String getCurrentFragmentAsString() {
        return sfwActivityHelper.getCurrentFragmentAsString();
    }

    @Override
    public void recreate() {
        sfwActivityHelper.onRecreateCalled();
        super.recreate();
    }
}
