package jpt.sfw.annotations;

import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

@Retention(RetentionPolicy.RUNTIME)
@Target(ElementType.TYPE)
public @interface DbConfig {
    Class [] entities();
    int version() default 1;
    String dbName() default "";
    boolean debuggingEnabled() default false;
}
