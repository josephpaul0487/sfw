package jpt.sfw.annotations;

import android.app.Activity;
import android.view.View;

import jpt.sfw.SfwActivity;
import jpt.sfw.R;

import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

import androidx.annotation.ColorRes;
import androidx.annotation.LayoutRes;
import androidx.annotation.StringRes;

@Retention(RetentionPolicy.RUNTIME)
@Target(ElementType.TYPE)
public @interface SfwActivityAnnotation {
    /**
     * @return   true ->  Turn the activity into a full screen
     */
    boolean isFullScreen() default false;

    /**
     * @return  true -> If you are using your custom view, not the layout resource
     *          true -> {@link SfwActivity#getContentView()} will call to set the content view to the activity instead of {@link android.app.Activity#setContentView(int)}
     *                  You should override {@link SfwActivity#getContentView()} method in your {@link android.app.Activity} to provide your custom view
     */
    boolean usingCustomViewAsContent()  default false;

    /**
     * @return  Layout resource id to set the content view of the {@link android.app.Activity}
     * If you use your custom view, return 0
     */
    @LayoutRes int contentViewLayout() default 0;

    /**
     * Use this method in library projects to provide layout resource id to set the content view of the {@link android.app.Activity}
     * Inconstant values not supporting in annotations while using in android library project
     */
    String contentViewLayoutAsString() default "";

    /**
     * @return  true -> You'll see a info before you leave the {@link android.app.Activity}
     *    To provide info -> Use {@link #backPressInfoResId()} for normal projects OR use {@link #backPressInfoResIdAsString()} for library projects
     *    Default info string is {@link R.string#info_press_back}
     */
    boolean isNeedToShowInfoOnBackPress() default false;

    /**
     * @return  Use this method in non-library projects to provide onBackPressedInfo   eg : R.string.info_press_back
     */
    @StringRes int backPressInfoResId() default 0;
    /** Use this method in library projects to provide onBackPressedInfo
     * eg :  Use   @SfwActivityAnnotation(backPressInfoResIdAsString = "info_press_back")   for R.string.info_press_back
     * Inconstant values not supporting in annotations while using in android library project
     */
    String backPressInfoResIdAsString() default "info_press_back";

    /**
     * @return Provide an array of {@link android.view.View} ids to avoid keyboard hiding while on click on the view (If you are using @{@link OnClick} annotation to bind the view)
     */
    int [] avoidKeyboardHideOnClick() default {};

    /**
     * To use custom animations in the fragment transaction... You can use either {@link android.animation.Animator} or {@link android.view.animation.Animation} ids
     * @return   "4" values to use {@link androidx.fragment.app.FragmentTransaction#setCustomAnimations(int, int, int, int)}
     *        OR "2" values to use {@link androidx.fragment.app.FragmentTransaction#setCustomAnimations(int, int)}
     *        OR {} for No Animation
     *  default animations used for the fragment transaction are
     *          R.animator.slide_fragment_horizontal_right_in, R.animator.slide_fragment_horizontal_right_out, R.animator.slide_fragment_horizontal_left_in, R.animator.slide_fragment_horizontal_left_out
     */
    int [] animations() default {0,0,0,0};

    /**
     * @return  true -> {@link System#gc()} will call {@link Activity#onDestroy()}
     */
    boolean callGConExit() default true;

    /**
     * Use this method in normal projects to provide statusBarColor
     * @return  default will be {@link R.color#statusBarColor}
     */
    @ColorRes int statusBarColor() default 0;

    /**
     * Use this method in library projects to provide statusBarColor
     * Inconstant values not supporting in annotations while using in android library project
     */
    String statusBarColorAsString() default "statusBarColor";

    /**
     * @return if true, the {@link SfwActivity#onClick(View, int)} } will throw a {@link RuntimeException} if any exception occurred.
     *      if false, will debug the exception if {@link jpt.sfw.app.Sfw#isDebuggingEnabled} is true
     */
    boolean throwOnClickException() default false;

}
