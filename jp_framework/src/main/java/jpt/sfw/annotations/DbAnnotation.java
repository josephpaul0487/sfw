package jpt.sfw.annotations;

import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

import jpt.sfw.enums.ColumnTypes;

@Retention(RetentionPolicy.RUNTIME)
@Target(ElementType.FIELD)
public @interface DbAnnotation {
    String columnName() default "";
    ColumnTypes columnType() default ColumnTypes.VARCHAR;
    int length() default 0;
}
