package jpt.sfw.annotations;

import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

import jpt.sfw.app.Preferences;
import jpt.sfw.database.SfwDatabase;
import jpt.sfw.webservice.SfwApiHelper;
import jpt.sfw.webservice.SfwClient;
import okhttp3.Interceptor;

/**
 * You should annotate a class using this Annotation and pass in to
 */
@Retention(RetentionPolicy.RUNTIME)
@Target(ElementType.TYPE)
public @interface AppConfig {

    boolean initializeDatabase() default true;
    boolean initializeApiHelper() default true;
    Class<? extends SfwApiHelper> apiHelperClassType() default SfwApiHelper.class;
    Class<? extends SfwClient> retrofitClientClassType() default SfwClient.class;
    Class<? extends SfwDatabase> databaseClassType() default SfwDatabase.class;
    Class<? extends Preferences> preferenceClassType() default Preferences.class;
    String retrofitBaseUrl();
    boolean setVmPolicy() default true;
    boolean debuggingEnabled() default false;

    String [] bindViewsIncludedPkgsStartsWith() default {};

    long retrofitConnectionTimeout() default 0;
    long retrofitReadTimeout() default 0;
    long retrofitWriteTimeout() default 0;




}
