package jpt.sfw.annotations;

import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.Target;

import static java.lang.annotation.RetentionPolicy.RUNTIME;

/**
 * Set a view as optional if the {@link BindView} OR {@link OnClick} used.
 **/
@Target(ElementType.FIELD)
@Retention(RUNTIME)
public @interface Nullable {
}
