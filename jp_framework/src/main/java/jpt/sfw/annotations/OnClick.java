package jpt.sfw.annotations;

import android.view.View;

import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.Target;

import static java.lang.annotation.RetentionPolicy.RUNTIME;

/**
 * Can replace {@link BindView} annotaion
 * Bind the view for each ID specified.
 * Add common {@link View.OnClickListener} to the specified view
 **/
@Target({ElementType.FIELD,ElementType.TYPE})
@Retention(RUNTIME)
public @interface OnClick  {
    /** View ID to which the filed to be bound. */
    int value() default View.NO_ID;

    /** Use this argument in library projects to bind the views
     * eg :  Use   @BindView(idName = "mainLayout")   for R.id.mainLayout
     * Inconstant values not supporting in annotations while using in android library project
     */
    String idName() default "";

    /** View IDs to which the filed to be bound. */
    int [] values() default {};
}
