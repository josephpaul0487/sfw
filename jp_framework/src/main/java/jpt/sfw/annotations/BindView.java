package jpt.sfw.annotations;

import android.view.View;

import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.Target;

import static java.lang.annotation.RetentionPolicy.RUNTIME;

/**
 * Bind the view for each ID specified.
 **/
@Target(ElementType.FIELD)
@Retention(RUNTIME)
public @interface BindView {
    /** View ID to which the filed to be bound. */
    int  value() default View.NO_ID;
    /** View Arrays to bound
     * eg: TextView [] views*/
    int  values() default View.NO_ID;

    /** Use this argument in library projects to bind the views
     * eg :  Use   @BindView(idName = "mainLayout")   for R.id.mainLayout
     * Inconstant values not supporting in annotations while using in android library project
     */
    String idName() default "";
}
