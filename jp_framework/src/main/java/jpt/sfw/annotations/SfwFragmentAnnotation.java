package jpt.sfw.annotations;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

import androidx.annotation.LayoutRes;
import jpt.sfw.fragments.SfwFragment;

@Retention(RetentionPolicy.RUNTIME)
@Target(ElementType.TYPE)
public @interface SfwFragmentAnnotation {
    /**
     *
     * @return   false -> Ff you have large layouts or any issue with the back stack
     */
    boolean keepViewInBackStackMemory() default true;
    /**
     * @return Provide an array of {@link android.view.View} ids to avoid keyboard hiding while on click on the view (If you are using @{@link OnClick} annotation to bind the view)
     */
    int [] avoidKeyboardHideOnClick() default {};

    /**
     * To set {@link SfwFragment#onClickListener} to a view.
     * Will throw an exception if view not found
     * Use this method to set up the {@link SfwFragment#onClickListener} without initialize a view
     * @return  @{@link android.view.View} ids
     */
    int [] onClickViewIds() default {};

    /**
     * @return  Layout resource id to set the content view of the {@link androidx.fragment.app.Fragment}
     * If you use your custom view, return 0
     * If this method return 0, {@link SfwFragment#createView(LayoutInflater, ViewGroup, Bundle)} will be call to set the view
     * To create a custom view use {@link SfwFragment#createView(LayoutInflater, ViewGroup, Bundle)}
     */
    @LayoutRes int contentViewLayout() default 0;

    /**
     * Use this method in library projects to provide layout resource id to set the content view of the {@link androidx.fragment.app.Fragment}
     * Inconstant values not supporting in annotations while using in android library project
     */
    String contentViewLayoutAsString() default "";

    /**
     * @return if true, the {@link SfwFragment#onClick(View, int)} will throw a {@link RuntimeException} if any exception occurred.
     *      if false, will debug the exception if {@link jpt.sfw.app.Sfw#isDebuggingEnabled} is true
     */
    boolean throwOnClickException() default false;


    /**
     *
     * @return true -> will set the minimum height of the view as it's parent viewgroup's (To whick viewgroup the fragment is loaded) height
     */
    boolean setMinimumHeightAsParentHeight() default true;
}
