package jpt.sfw.fragments;

import android.content.Intent;
import android.content.IntentFilter;
import android.os.Bundle;
import android.view.View;


import androidx.annotation.CallSuper;
import androidx.annotation.Nullable;
import jpt.sfw.R;
import jpt.sfw.database.SfwDatabase;

public abstract class ChildFragment<T extends SfwDatabase> extends SfwFragment<T> {
    protected int position;

    @CallSuper
    @Override
    public void init(View rootView, Object data, boolean isViewCreatedFromBackStack, @Nullable Bundle savedInstanceState) {
        if(getArguments()!=null && getArguments().containsKey("position"))
        position = getArguments().getInt("position");
    }

    public abstract void onContentChanged();

    public abstract void onSwipeRefreshCalled();

    @Override
    public IntentFilter setBroadcastReceiverActions() {
        IntentFilter filter = new IntentFilter();
        filter.addAction(getString(R.string.intent_action_login_details_change));
        filter.addAction(getString(R.string.intent_action_content_changed));
        filter.addAction(getString(R.string.intent_action_swipe_refresh));
        return filter;
    }


    @Override
    public boolean broadcastMessageReceived(Intent intent) {
        try {
            if (intent.getAction().equals(getString(R.string.intent_action_swipe_refresh))) {
                if (position == intent.getIntExtra(getString(R.string.intent_extra_position), -1))
                    onSwipeRefreshCalled();
            } else if (intent.getAction().equals(getString(R.string.intent_action_login_details_change))) {
                onLoginDetailsChanged(true);
            } else {
                onContentChanged();
            }
        } catch (Exception ignored) {
        }
        return true;
    }
}
