package jpt.sfw.fragments;

import android.animation.Animator;
import android.animation.AnimatorInflater;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.graphics.Typeface;
import android.os.Bundle;
import android.os.Handler;
import android.text.Editable;
import android.text.InputFilter;
import android.text.TextUtils;
import android.util.Log;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.view.inputmethod.EditorInfo;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;

import com.google.android.material.textfield.TextInputLayout;
import com.hbb20.CountryCodePicker;

import jpt.sfw.database.SfwDatabase;
import jpt.sfw.interfaces.SfwActivityInterface;
import jpt.sfw.BuildConfig;
import jpt.sfw.R;
import jpt.sfw.app.AlertConfig;
import jpt.sfw.app.Sfw;
import jpt.sfw.app.FragmentConfig;
import jpt.sfw.app.ProgressConfig;
import jpt.sfw.app.Utilities;
import jpt.sfw.app.Validation;
import jpt.sfw.appui.SfwButtonLayout;
import jpt.sfw.webservice.ApiHelperListener;
import jpt.sfw.annotations.SfwFragmentAnnotation;
import jpt.sfw.annotations.BindView;
import jpt.sfw.annotations.Nullable;
import jpt.sfw.interfaces.OnActivityListener;
import jpt.sfw.interfaces.OnAlertListener;
import jpt.sfw.annotations.OnClick;
import jpt.sfw.ui.SfwEditText;
import jpt.sfw.ui.SfwTextInputLayout;
import jpt.sfw.ui.SfwCommonTextWatcher;
import jpt.sfw.webservice.SfwApiHelper;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import androidx.annotation.CallSuper;
import androidx.annotation.FontRes;
import androidx.annotation.IdRes;
import androidx.annotation.IntRange;
import androidx.annotation.NonNull;
import androidx.annotation.StringRes;
import androidx.core.content.res.ResourcesCompat;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentTransaction;
import androidx.localbroadcastmanager.content.LocalBroadcastManager;
import androidx.recyclerview.widget.GridLayoutManager;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import androidx.recyclerview.widget.StaggeredGridLayoutManager;

import io.michaelrocks.libphonenumber.android.PhoneNumberUtil;
import io.michaelrocks.libphonenumber.android.Phonenumber;

/**
 * Using ' 1 ' for alert contant
 */
//Please add these methods if any fragment not extending this class
public abstract class SfwFragment<D extends SfwDatabase> extends Fragment implements OnActivityListener, OnAlertListener, ApiHelperListener {


    protected final View getHolderView() {
        return getBaseHelper() != null ? getBaseHelper().getFragmentViewFromBackStack(getHolderTag()) : null;
    }

    @Override
    public String getHolderTag() {
        return getClass().getName();
    }

    @Override
    public final boolean isAfterCreatedFragmentCalled() {
        return isAfterCreatedFragmentCalled;
    }

    protected final String getStringRes(@StringRes int resId) {
        try {
            return getString(resId);
        } catch (Exception ignored) {
        }
        return "";
    }

    protected final SfwActivityInterface getBaseHelper() {
        try {
            return (SfwActivityInterface) getActivity();
        } catch (Exception ignored) {
        }
        return null;
    }

    protected final FragmentConfig createConfig(@NonNull Fragment fragment, CharSequence pageTitle, Object data) {
        return new FragmentConfig(pageTitle, fragment instanceof OnActivityListener ? ((OnActivityListener) fragment).getHolderTag() : fragment.getClass().getName(), data);
    }


    public boolean changeFragment(@NonNull Fragment fragment, @NonNull FragmentConfig config) {
        if (getBaseHelper() != null) {
            return getBaseHelper().changeFragment(fragment, config);
        }
        return false;
    }

    protected boolean addTagAndChangeFragment(@NonNull Fragment fragment, @NonNull FragmentConfig configWithoutTag) {
        return getBaseHelper().addTagAndChangeFragment(fragment, configWithoutTag);
    }

    protected boolean changeFragment(@NonNull Fragment fragment, @NonNull CharSequence pageTitle, @androidx.annotation.Nullable Object data) {
        return changeFragment(fragment, createConfig(fragment, pageTitle, data));
    }

    protected boolean changeFragment(@NonNull Fragment fragment, @NonNull CharSequence pageTitle, @androidx.annotation.Nullable Object data, @NonNull String tagToRemove) {
        return changeFragment(fragment, createConfig(fragment, pageTitle, data).setTagToRemove(tagToRemove));
    }

    protected final void changeFragment(@NonNull Fragment fragment, CharSequence pageTitle, Object data, boolean needDrawer) {
        changeFragment(fragment, createConfig(fragment, pageTitle, data).setNeedDrawer(needDrawer));
    }


    @Override
    public final View onCreateView(@NonNull LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View v = null;
        SfwFragmentAnnotation annotation = Sfw.getAnnotation(SfwFragmentAnnotation.class, getClass());
        if (annotation != null) {
            throwViewClickException = annotation.throwOnClickException();
            keepViewInBackStackMemory = annotation.keepViewInBackStackMemory();
            avoidKeyboardHideOnClick.clear();
            int[] avoidIds = annotation.avoidKeyboardHideOnClick();
            if (avoidIds.length > 0) {
                for (int id : avoidIds)
                    avoidKeyboardHideOnClick.add(id);
            }
        }
        isAnimationCompleted = isAfterCreatedFragmentCalled = isInitCalled = isViewCreated = false;
        if (getParentFragment() == null)
            v = getHolderView();
        isViewCreatedFromBackStack = v != null;
        if (!isViewCreatedFromBackStack) {
            if (annotation == null || annotation.contentViewLayout() == 0) {
                int layoutId = Sfw.getIdFromString(annotation == null ? "" : annotation.contentViewLayoutAsString(), R.layout.class, 0);
                if (layoutId == 0)
                    v = createView(inflater, container, savedInstanceState);
                else
                    v = inflater.inflate(layoutId, container, false);
            } else
                v = inflater.inflate(annotation.contentViewLayout(), container, false);
        }
        if (v == null)
            throw new IllegalAccessError("View must not be null. Please annotate " + this + " fragment class using " + SfwFragmentAnnotation.class + " and provide contentViewLayout() [layout id] OR create a custom view by overriding createView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) method in " + this + " fragment.");
        return v;
    }

    @CallSuper
    @Override
    public final void onViewCreated(@NonNull View view, Bundle savedInstanceState) {


        IntentFilter filter = setBroadcastReceiverActions();
        if (filter != null) {
            mReceiver = new Receiver();
            getActivity().registerReceiver(mReceiver, filter);
        }

        onClickListener = this::clickOnView;

        SfwFragmentAnnotation annotation = Sfw.getAnnotation(SfwFragmentAnnotation.class, getClass());
        int[] onClickIds = annotation == null ? new int[0] : annotation.onClickViewIds();
        for (int id : onClickIds) {
            View v = view.findViewById(id);
            if (v == null)
                throw new RuntimeException("No view found for id " + id + " (" + getClass().getName() + ") .");
            v.setOnClickListener(onClickListener);
        }

        if (getBaseHelper() != null) {
            isFromBackStack = getBaseHelper().getFragmentConfig(getHolderTag()) != null;
            getBaseHelper().addOnActivityListener(this);
            if (getParentFragment() == null && keepViewInBackStackMemory) {
                getBaseHelper().setViewToBackStack(view, getHolderTag());
            }
        }


        view.setOnClickListener(onClickListener);
        if (view.findViewById(R.id.layoutHideKeyboard) != null) {
            view.findViewById(R.id.layoutHideKeyboard).setOnClickListener(onClickListener);
        }
        Sfw.bindViews(this, view, onClickListener);
        phoneNumberUtil = Sfw.getPhoneNumberUtil(view.getContext().getApplicationContext());
        setMobileWatcher(layoutMobile, 0);
//        onLoginDetailsChanged(false);

        try {
            if ((isAnimationCompleted || !getBaseHelper().isFragmentAnimationUsed()) && !isAfterCreatedFragmentCalled && getParentFragment() == null) {
                isAfterCreatedFragmentCalled = true;
                getBaseHelper().afterFragmentCreated(SfwFragment.this);
            }
            isInitCalled = true;

        } catch (Exception e) {
            Sfw.log(TAG, "onViewCreated : " + getClass().getName(), e, true);
        }
        isViewCreated = true;

        super.onViewCreated(view, savedInstanceState);
        if (annotation != null && annotation.setMinimumHeightAsParentHeight()) {
            try {
                FragmentConfig config = getBaseHelper().getFragmentConfig(getHolderTag());
                if (config != null) {
                    View parent = getActivity().findViewById(config.getToId());
                    if (parent.getHeight() > 0) {
                        view.setMinimumHeight(parent.getHeight());
                    } else {
                        new Handler().postDelayed(() -> {
                            try {
                                if (parent.getHeight() > 0) {
                                    view.setMinimumHeight(parent.getHeight());
                                }
                            } catch (Exception e) {
                                Sfw.log(TAG, "Set View minimum height : " + getClass().getName(), e, true);
                            }

                        }, 400);
                    }
                }
            } catch (Exception e) {
                Sfw.log(TAG, "Set View minimum height : " + getClass().getName(), e, true);
            }
        }
        onCreatedView(view, savedInstanceState);

    }

    @Override
    public final void onViewStateRestored(@androidx.annotation.Nullable Bundle savedInstanceState) {
        super.onViewStateRestored(savedInstanceState);
        try {
            FragmentConfig config = getBaseHelper().getFragmentConfig(getHolderTag());
            init(getView(), config == null ? null : config.getData(), isViewCreatedFromBackStack, savedInstanceState);
        } catch (Exception e) {
            Sfw.log(TAG, "onViewStateRestored : " + getClass().getName(), e, true);
        }
    }

    @CallSuper
    @Override
    public void onDestroyView() {
        super.onDestroyView();
        try {
            SfwApiHelper.removeListener(this);
            getBaseHelper().removeOnActivityListener(this);
        } catch (Exception ignored) {
        }
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        try {
            if (db != null)
                db.clearObservers(getClass());
            if (mReceiver != null) {
                getActivity().unregisterReceiver(mReceiver);
            }
        } catch (Exception ignored) {
        }
    }

    @Override
    public final Animator onCreateAnimator(int transit, boolean enter, int nextAnim) {
        try {
            Animator animator = AnimatorInflater.loadAnimator(getActivity(), nextAnim);
            if (enter)
                animator.addListener(
                        new Animator.AnimatorListener() {
                            @Override
                            public void onAnimationStart(Animator animation) {

                            }

                            @Override
                            public void onAnimationEnd(Animator animation) {
                                try {
                                    if (isInitCalled && !isAfterCreatedFragmentCalled && getParentFragment() == null) {
                                        isAfterCreatedFragmentCalled = true;
                                        getBaseHelper().afterFragmentCreated(SfwFragment.this);
                                    }
                                    isAnimationCompleted = true;
                                    animation.removeAllListeners();
                                } catch (Exception ignored) {
                                }
                            }

                            @Override
                            public void onAnimationCancel(Animator animation) {

                            }

                            @Override
                            public void onAnimationRepeat(Animator animation) {

                            }
                        });
            return animator;
        } catch (Throwable ignored) {
        }
        return super.onCreateAnimator(transit, enter, nextAnim);
    }

    @androidx.annotation.Nullable
    @Override
    public Animation onCreateAnimation(int transit, boolean enter, int nextAnim) {
        try {
            Animation animation = AnimationUtils.loadAnimation(getActivity(), nextAnim);
            if (enter)
                animation.setAnimationListener(
                        new Animation.AnimationListener() {
                            @Override
                            public void onAnimationStart(Animation animation) {

                            }

                            @Override
                            public void onAnimationEnd(Animation animation) {
                                try {
                                    if (isInitCalled && !isAfterCreatedFragmentCalled && getParentFragment() == null) {
                                        isAfterCreatedFragmentCalled = true;
                                        getBaseHelper().afterFragmentCreated(SfwFragment.this);
                                    }
                                    isAnimationCompleted = true;
                                } catch (Exception ignored) {
                                }
                            }

                            @Override
                            public void onAnimationRepeat(Animation animation) {

                            }
                        });
            return animation;
        } catch (Throwable ignored) {
        }
        return super.onCreateAnimation(transit, enter, nextAnim);
    }

    protected final void toast(@StringRes int stringId) {
        if (getActivity() instanceof SfwActivityInterface) {
            getBaseHelper().toast(stringId);
        }
    }

    protected final void toast(CharSequence msg) {
        if (getActivity() instanceof SfwActivityInterface) {
            getBaseHelper().toast(msg);
        }
    }

    protected final void showHideProgress(boolean show) {
        if (layoutProgress != null)
            layoutProgress.setVisibility(show ? View.VISIBLE : View.GONE);
        else
            Sfw.log(TAG, "Progress layout is NULL. Please add progress layout in " + this + "'s layout.", null, true);
    }


    protected final void showInternetErrMsg(boolean dismissProgressDialog) {
        showInternetErrMsg(dismissProgressDialog, ALERT_NETWORK_ERROR, false);
    }

    protected final void showInternetErrMsg(boolean dismissProgressDialog, boolean setRetry) {
        showInternetErrMsg(dismissProgressDialog, ALERT_NETWORK_ERROR, setRetry);
    }

    protected final void showInternetErrMsg(boolean dismissProgressDialog, int requestCode, boolean setRetry) {
        try {
            getBaseHelper().showInternetErrMsg(dismissProgressDialog, requestCode, setRetry, this);
        } catch (Exception ignored) {
        }
    }

    protected final String getText(@NonNull TextView textView) {
        return textView.getText().toString();
    }

    protected final String getTilText(@NonNull TextInputLayout tilLayout) {
        return tilLayout.getEditText().getText().toString();
    }


    protected View createView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        return null;
    }

    protected void onCreatedView(@androidx.annotation.Nullable View v, @androidx.annotation.Nullable Bundle savedInstanceState) {
    }

    protected abstract void init(View rootView, Object data, boolean isViewCreatedFromBackStack, @androidx.annotation.Nullable Bundle savedInstanceState);

    //It will call from onClickListener
    protected void onClick(View view, int viewId) {
    }


    /**
     * You can override this method for any custom action or hidekeyboard to false
     *
     * @param v
     */
    protected void clickOnView(View v) {
        clickOnView(v, !avoidKeyboardHideOnClick.contains(v.getId()));
    }

    protected final void clickOnView(View v, boolean hideKeyBoard) {

        if (!isViewClickActionPerforming) {
            isViewClickActionPerforming = true;
            try {
                if (hideKeyBoard)
                    Utilities.hideKeyBoard(getActivity());
                onClick(v, v == null ? -1 : v.getId());
                isViewClickActionPerforming = false;
            } catch (Throwable throwable) {
                isViewClickActionPerforming = false;
                if (!throwViewClickException)
                    Sfw.log(TAG, "clickOnView", throwable, true);
                else
                    throw new RuntimeException(throwable);
            }
        }

    }


    @Override
    public boolean onBackPressed() {
        return false;
    }

    @Override
    public void onInternetConnectionChange(boolean isConnected) {

    }

    @Override
    public void onLoginDetailsChanged(boolean forceLogout) {

    }

    @Override
    public void onDialogClosed(int dialogId) {

    }


    public final void showProgress(@NonNull CharSequence msg) {
        showProgress(msg, false);
    }


    public final void showProgress(@StringRes int msg) {
        showProgress(getStringRes(msg), false);
    }


    protected final void showProgress(@NonNull CharSequence msg, boolean isCancelable) {
        showProgress(msg, isCancelable, -1, null);
    }


    protected final void showProgress(@NonNull CharSequence msg, boolean isCancelable, int requestCode, OnAlertListener listener) {
        showProgress(new ProgressConfig(requestCode, msg, isCancelable, listener));
    }


    public final void showProgress(ProgressConfig config) {
        if (getBaseHelper() != null) {
            getBaseHelper().showProgress(config);
        }
    }


    public final void dismissProgress() {
        if (getBaseHelper() != null) {
            getBaseHelper().dismissProgress();
        }
    }

    public final void setEmptyText(String content) {
        setEmptyText(content, null);
    }

    public final void setEmptyText(String content, String buttonText) {
        if (txtEmptyContent != null && content != null) {
            txtEmptyContent.setText(content, TextView.BufferType.NORMAL);
            if (btnEmpty != null)
                btnEmpty.setVisibility(TextUtils.isEmpty(buttonText) ? View.GONE : View.VISIBLE);
//            btnEmpty.setText(buttonText == null ? "" : buttonText);
        } else
            Sfw.log(TAG, "Empty layout is NULL. Please add empty layout in " + this + "'s layout.", null, true);
    }

    public final void showHideEmptyLayout(boolean visible) {
        if (layoutEmpty != null) {
            layoutEmpty.setVisibility(visible ? View.VISIBLE : View.GONE);
        } else
            Sfw.log(TAG, "Empty layout is NULL. Please add empty layout in " + this + "'s layout.", null, true);
    }

    public final boolean isEmptyLayoutVisible() {
        return layoutEmpty != null && layoutEmpty.getVisibility() == View.VISIBLE;
    }

    public final void showHideButtonLayoutProgress(boolean show, SfwButtonLayout layout) {
        try {
            if (layout != null)
                layout.showProgress(show);
        } catch (Exception ignored) {
        }
    }


    public final void showMessage(@NonNull AlertConfig config) {
        if (getBaseHelper() != null) {
            if (config.getRequestId() > 0)
                config.setListener(this);
            getBaseHelper().showMessage(config);
        }
    }


    @Override
    public void onAlertPositiveClicked(int requestId) {

    }

    @Override
    public void onAlertNegativeClicked(int requestId) {

    }

    @Override
    public void onAlertCancelled(int requestId, boolean isProgressDialog) {

    }

    @Override
    public void onApiSuccess(int apiTypeCode, Object responseData) {

    }

    @Override
    public void onApiError(int apiTypeCode, Throwable t) {

    }

    protected boolean isFinishedLoading(int requestCode) {
        return false;
    }

    protected void loadMore(int requestCode) {
    }

    public void setRvPagination(@NonNull RecyclerView recyclerView, @NonNull RecyclerView.LayoutManager manager, int requestCode) {
        recyclerView.addOnScrollListener(new RecyclerView.OnScrollListener() {
            @Override
            public void onScrolled(@NonNull RecyclerView recyclerView, int dx, int dy) {
                try {
                    super.onScrolled(recyclerView, dx, dy);
                    if (isFinishedLoading(requestCode))
                        return;
                    int firstVisibleItemPosition = 0;
                    if (manager instanceof GridLayoutManager)
                        firstVisibleItemPosition = ((GridLayoutManager) manager).findFirstVisibleItemPosition();
                    else if (manager instanceof StaggeredGridLayoutManager)
                        firstVisibleItemPosition = ((StaggeredGridLayoutManager) manager).findFirstVisibleItemPositions(null)[0];
                    else if (manager instanceof LinearLayoutManager)
                        firstVisibleItemPosition = ((LinearLayoutManager) manager).findFirstVisibleItemPosition();
                    if ((manager.getChildCount() + firstVisibleItemPosition) >= manager.getItemCount()
                            && firstVisibleItemPosition >= 0) {
                        loadMore(requestCode);
                    }
                } catch (Exception ignored) {
                }
            }
        });
    }

    protected boolean onEditTextAction(int actionId, TextView view, KeyEvent keyEvent) {
        Utilities.hideKeyBoard(getActivity());
        return true;
    }

    public SfwCommonTextWatcher setTextWatcherForNumber(@NonNull TextInputLayout layout, final float maxValue) {
        return setTextWatcherForNumber(layout, maxValue, 0);
    }

    public SfwCommonTextWatcher setTextWatcherForNumber(@NonNull TextInputLayout layout, final float maxValue, final float minValue) {
        return setTextWatcherForNumber(layout, maxValue, minValue, 0);
    }

    protected SfwCommonTextWatcher setTextWatcherForNumber(@NonNull TextInputLayout layout, final float maxValue, @IntRange(from = 0) int maxDecimalPoint) {
        return setTextWatcherForNumber(layout, maxValue, Float.MIN_VALUE, maxDecimalPoint);
    }

    protected SfwCommonTextWatcher setTextWatcherForNumber(@NonNull TextInputLayout layout, final float maxValue, final float minValue, @IntRange(from = 0) int maxDecimalPoint) {
        if (!(layout.getEditText() instanceof SfwEditText))
            throw new RuntimeException("Please use " + SfwEditText.class.getName() + "  instead of " + (layout.getEditText() == null ? " normal EditText " : layout.getEditText().getClass().getName()));
        return setTextWatcherForNumber(layout, (SfwEditText) layout.getEditText(), maxValue, minValue, maxDecimalPoint);
    }

    protected SfwCommonTextWatcher setTextWatcherForNumber(SfwEditText editText, final float maxValue, final float minValue, @IntRange(from = 0) int maxDecimalPoint) {
        return setTextWatcherForNumber(null, editText, maxValue, minValue, maxDecimalPoint);
    }

    protected SfwCommonTextWatcher setTextWatcherForNumber(@androidx.annotation.Nullable TextInputLayout layout, SfwEditText editText, final float maxValue, final float minValue, @IntRange(from = 0) int maxDecimalPoint) {
        if (editText == null)
            return null;
        int maxCounter;
        if (maxDecimalPoint == 0 && maxValue == ((int) maxValue))
            maxCounter = ("" + ((int) maxValue)).length();
        else if (maxDecimalPoint > 0 && maxValue == ((int) maxValue))
            maxCounter = ("" + ((int) maxValue)).length() + maxDecimalPoint + 1;
        else
            maxCounter = ("" + maxValue).length();
        if (layout != null)
            layout.setCounterMaxLength(maxCounter);
        //layout.getEditText().setMaxEms(maxCounter);
        SfwCommonTextWatcher textWatcher = new SfwCommonTextWatcher(editText) {
            String beforeText;

            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {
                beforeText = s.toString();
            }


            @Override
            public void textChanged(Editable s) {
                float value = Float.parseFloat(s.toString());
                int dotIndex = s.toString().indexOf(".");
                String leadingZeroRemover = s.toString().replaceFirst("^0+(?!$)", "");
                if (!leadingZeroRemover.equals(s.toString()) || value > maxValue || value < minValue || (maxDecimalPoint > 0 && (s.toString().length() > maxCounter || (dotIndex != -1 && (dotIndex + maxDecimalPoint + 1 < s.length()))))) {
                    setTextChanged(true);
                    try {
                        int selection = editText.getSelectionStart();
                        editText.setText(beforeText);
                        editText.setSelection(selection == 0 ? selection : selection - 1);
                    } catch (Exception e) {
                        setTextChanged(false);
                    }
                }

            }
        };
        editText.addTextChangedListener(textWatcher);
        return textWatcher;
    }

    protected void setMobileWatcher(View layoutMobile, @FontRes int fontId) {

        if (layoutMobile != null) {
            CountryCodePicker countryCodePicker = layoutMobile.findViewById(R.id.countryCodePicker);
            EditText edtMobile = layoutMobile.findViewById(R.id.edtMobile);
            TextInputLayout tilMobile = layoutMobile.findViewById(R.id.tilMobile);
            try {
                Typeface typeface = ResourcesCompat.getFont(getContext(), fontId);
                countryCodePicker.setTypeFace(typeface);
                countryCodePicker.setDialogTypeFace(typeface);
                if (tilMobile != null)
                    tilMobile.setTypeface(typeface);
            } catch (Exception ignored) {
            }

//            edtMobile.setOnFocusChangeListener((v, hasFocus) -> {
//                layoutMobile.setBackgroundResource(hasFocus ? R.drawable.edt_pressed : R.drawable.edt_normal);
//            });
            countryCodePicker.setOnCountryChangeListener(() -> {
                try {
                    Phonenumber.PhoneNumber exampleNumber = phoneNumberUtil.getExampleNumberForType(phoneNumberUtil.getRegionCodeForCountryCode(countryCodePicker.getSelectedCountryCodeAsInt()), PhoneNumberUtil.PhoneNumberType.MOBILE);
                    if (exampleNumber != null) {
                        if (("" + exampleNumber.getNationalNumber()).length() < edtMobile.length()) {
                            int selection = edtMobile.getSelectionStart();
                            edtMobile.setText("" + exampleNumber.getNationalNumber());
                            if (selection < 0 || selection > edtMobile.length())
                                selection = edtMobile.length();
                            edtMobile.setSelection(selection);
                        }
                        edtMobile.setFilters(new InputFilter[]{new InputFilter.LengthFilter(("" + exampleNumber.getNationalNumber()).length())});
                        if (tilMobile != null) {
                            tilMobile.setCounterMaxLength(("" + exampleNumber.getNationalNumber()).length());
                        }
                    }
                } catch (Exception ignored) {
                }
            });
        }
    }

    public final void setOnEditorActionListener(@NonNull TextInputLayout layout) {
        setOnEditorActionListener(layout.getEditText());
    }

    protected final void setOnEditorActionListener(@NonNull EditText editText) {
        if (onEditorActionListener == null)
            onEditorActionListener = (v, actionId, event) -> (actionId == EditorInfo.IME_ACTION_DONE || actionId == EditorInfo.IME_ACTION_NEXT || actionId == EditorInfo.IME_ACTION_SEARCH || actionId == EditorInfo.IME_ACTION_DONE || actionId == EditorInfo.IME_ACTION_GO || actionId == EditorInfo.IME_ACTION_UNSPECIFIED || actionId == EditorInfo.IME_ACTION_SEND) && onEditTextAction(actionId, v, event);
        editText.setOnEditorActionListener(onEditorActionListener);
    }

    protected boolean isAValidMobile() {
        return isAValidMobile(layoutMobile);
    }

    protected boolean isAValidMobile(View layoutMobile) {
        if (layoutMobile != null) {
            if (layoutMobile.findViewById(R.id.tilMobile) != null)
                return Validation.isAValidMobile(layoutMobile.findViewById(R.id.tilMobile), layoutMobile.findViewById(R.id.countryCodePicker), true);
            return Validation.isAValidMobileTV(layoutMobile.findViewById(R.id.edtMobile), layoutMobile.findViewById(R.id.countryCodePicker), true);

        }
        return false;
    }

    public View.OnClickListener getOnClickListener() {
        return onClickListener;
    }

    protected IntentFilter setBroadcastReceiverActions() {
        return null;
    }

    protected boolean broadcastMessageReceived(Intent intent) {
        return false;
    }

    public boolean isViewCreatedFromBackStack() {
        return isViewCreatedFromBackStack;
    }

    public void changeChildFragment(Fragment fragment, @IdRes int toLayout) {
        changeChildFragment(fragment, toLayout, null);
    }

    public void changeChildFragment(Fragment fragment, @IdRes int toLayout, boolean addToBackStack) {
        changeChildFragment(fragment, toLayout, null, addToBackStack);
    }

    public void changeChildFragment(Fragment fragment, @IdRes int toLayout, @androidx.annotation.Nullable int[] animations) {
        changeChildFragment(fragment, toLayout, animations, false);
    }

    public void changeChildFragment(Fragment fragment, @IdRes int toLayout, @androidx.annotation.Nullable int[] animations, boolean addToBackStack) {
        if (fragment == null) {
            return;
        }
        FragmentTransaction transaction = getChildFragmentManager().beginTransaction();
        if (animations != null) {
            transaction.setCustomAnimations(R.animator.cube_right_in, R.animator.cube_right_out, R.animator.cube_left_in, R.animator.cube_left_out);
        }
        if (!addToBackStack) {
            transaction.disallowAddToBackStack();
        }

        transaction.replace(toLayout, fragment).commit();
        childFragmentNames.put(toLayout, fragment.getClass().getName());
    }

    public String getChildFragmentName(@IdRes int layoutId, @androidx.annotation.Nullable String defaultValue) {
        String s = childFragmentNames.get(layoutId);
        return s == null ? defaultValue : s;
    }

    private class Receiver extends BroadcastReceiver {
        @Override
        public void onReceive(Context context, Intent intent) {
            try {
                broadcastMessageReceived(intent);
            } catch (Exception ignored) {
            }
        }
    }


    private Map<Integer, String> childFragmentNames = new HashMap<>();
    private TextView.OnEditorActionListener onEditorActionListener;
    private PhoneNumberUtil phoneNumberUtil;

    @Nullable
    @BindView(idName = "layoutCommonProgress")
    protected View layoutProgress;
    @Nullable
    @BindView(idName = "txtEmptyContent")
    protected TextView txtEmptyContent;
    @Nullable
    @OnClick(idName = "btnEmpty")
    protected ImageView btnEmpty;
    @Nullable
    @BindView(idName = "layoutEmpty")
    protected View layoutEmpty;

    @Nullable
    @BindView(idName = "layoutMobile")
    protected View layoutMobile;
    @Nullable
    @BindView(idName = "tilMobile")
    protected SfwTextInputLayout tilMobile;
    @Nullable
    @BindView(idName = "edtMobile")
    protected SfwEditText edtMobile;
    @Nullable
    @BindView(idName = "countryCodePicker")
    protected CountryCodePicker countryCodePicker;


    protected D db = (D) SfwDatabase.getInstance();
    private Receiver mReceiver;
    private boolean isViewCreatedFromBackStack, isFromBackStack, keepViewInBackStackMemory = true;
    View.OnClickListener onClickListener;
    //To avoid double click or clicking a second view before completing first click action
    private boolean isViewClickActionPerforming;
    private boolean throwViewClickException;
    private boolean isInitCalled, isViewCreated, isAnimationCompleted, isAfterCreatedFragmentCalled;
    //Keyboard will not hide while clicking on these views
    private List<Integer> avoidKeyboardHideOnClick = new ArrayList<>();

    protected static final int ALERT_NETWORK_ERROR = 1;
    private static final String TAG = "SfwFragment";


}
