package jpt.sfw;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.pm.PackageManager;
import android.net.ConnectivityManager;
import android.net.wifi.WifiManager;
import android.os.Build;
import android.os.Bundle;
import android.os.SystemClock;
import android.text.TextUtils;
import android.util.Log;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.Toast;

import jpt.sfw.app.AlertConfig;
import jpt.sfw.app.Sfw;
import jpt.sfw.app.BackStackHolder;
import jpt.sfw.app.FragmentConfig;
import jpt.sfw.app.ImageUtil;
import jpt.sfw.app.ProgressConfig;
import jpt.sfw.app.ToastConfig;
import jpt.sfw.app.Utilities;
import jpt.sfw.appui.AlertDialog;
import jpt.sfw.appui.ProgressDialog;
import jpt.sfw.database.SfwDatabase;
import jpt.sfw.interfaces.SfwActivityHelperInterface;
import jpt.sfw.interfaces.SfwActivityInterface;
import jpt.sfw.annotations.SfwActivityAnnotation;
import jpt.sfw.webservice.ApiHelperListener;
import jpt.sfw.interfaces.OnActivityListener;
import jpt.sfw.interfaces.OnAlertListener;
import jpt.sfw.webservice.SfwApiHelper;

import java.lang.ref.SoftReference;
import java.util.ArrayList;
import java.util.List;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;
import androidx.core.content.ContextCompat;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentTransaction;


/**
 * Using  1-3  permission constants
 */
public class SfwActivityHelper implements SfwActivityInterface {
    private SoftReference<AppCompatActivity> activity;
    // To check is need to show msg on back press [.onBackPressed]
    private long lastBackPressed = 0;
    private int backPressInfoId;
    private boolean isNeedToShowInfoOnBackPress;
    private boolean callGConExit = true;
    private int[] fragmentAnimations;
    private final ArrayList<OnActivityListener> onActivityListeners = new ArrayList<>();
    String currentFragment = null;
    /**
     * to avoid issues when a fragment transaction is processing (ie, to avoid calling {@link #changeFragment(Fragment, FragmentConfig)} at multiple times when a fragment transaction is pending)
     * Used on {@link #afterFragmentCreated(Fragment)}  and {@link #changeFragment(Fragment, FragmentConfig)}
     * call {@link #afterFragmentCreated(Fragment)} from {@link #initListeners()} [.onBackStackChangedListener] if you are not setting any animation on fragment changing
     * call {@link #afterFragmentCreated(Fragment)} from {@link Fragment#onCreateAnimator(int, boolean, int)} OR {@link Fragment#onCreateAnimation(int, boolean, int)}
     */
    private boolean isChangingFragment, isBackPressProcessing;


    //To hold the all back stack elements (ie, fragments,config and view)
    private BackStackHolder backStackHolder = new BackStackHolder();

    //Common OnclickListener
    private View.OnClickListener onClickListener;
    //To avoid double click or clicking a second view before completing first click action
    private boolean isViewClickActionPerforming;
    private boolean throwViewClickException;
    private Intent lastActivityResultIntent;
    private int lastActivityResultRequestCode;
    private int lastActivityResultResultCode;

    //Keyboard will not hide while clicking on these views
    private List<Integer> avoidKeyboardHideOnClick = new ArrayList<>();


    private AlertDialog alertDialog;
    private ProgressDialog progressDialog;

    private static final int PERMISSION_NETWORK_STATE = 1;
    private static final int PERMISSION_WRITE_STORAGE = 2;
    private static final int PERMISSION_ACCESS_NETWORK_STATE = 3;
    private static final String TAG = "SfwActivity";


    public SfwActivityHelper(@NonNull AppCompatActivity activity) {
        if (!(activity instanceof SfwActivityInterface))
            throw new IllegalAccessError("" + activity.getClass().getName() + " should implement " + SfwActivityInterface.class.getName());
        if (!(activity instanceof SfwActivityHelperInterface))
            throw new IllegalAccessError("" + activity.getClass().getName() + " should implement " + SfwActivityHelperInterface.class.getName());
        if (Sfw.isDebuggingEnabled) {
            if (!(activity instanceof OnAlertListener))
                Sfw.log(TAG, activity.getClass().getName() + " should implement " + OnAlertListener.class.getName() + " to get AlertDialog click events.", null, true, true);
            if (!(activity instanceof ApiHelperListener))
                Sfw.log(TAG, activity.getClass().getName() + " should implement " + ApiHelperListener.class.getName() + " to get ApiHelper results.", null, true, true);
        }

        this.activity = new SoftReference<>(activity);
    }

    protected final void onCreate(@Nullable Bundle savedInstanceState) {
        SfwActivityAnnotation annotation = Sfw.getAnnotation(SfwActivityAnnotation.class, activity.get().getClass());
        if (annotation == null) {
            throw new IllegalAccessError("You should annotate this activity using " + SfwActivityAnnotation.class.getName());
        }
        try {
            activity.get().requestWindowFeature(Window.FEATURE_NO_TITLE);
        } catch (Exception ignored) {
        }

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            int statusBarColor = annotation.statusBarColor();
            if (statusBarColor == 0) {
                statusBarColor = Sfw.getIdFromString(annotation.statusBarColorAsString(), R.color.class, 0);
            }
            if (statusBarColor != 0 && statusBarColor != R.color.statusBarColor) {
                ((SfwActivityInterface) activity.get()).setStatusBarColor(ContextCompat.getColor(activity.get(), statusBarColor));
            }
        }

        isNeedToShowInfoOnBackPress = annotation.isNeedToShowInfoOnBackPress();
        throwViewClickException = annotation.throwOnClickException();
        callGConExit = annotation.callGConExit();
        fragmentAnimations = annotation.animations();
        if (fragmentAnimations.length > 0 && fragmentAnimations[0] == 0) {
            fragmentAnimations = new int[]{R.animator.slide_fragment_horizontal_right_in, R.animator.slide_fragment_horizontal_right_out, R.animator.slide_fragment_horizontal_left_in, R.animator.slide_fragment_horizontal_left_out};
        }
        if (fragmentAnimations.length != 0 && fragmentAnimations.length != 2 && fragmentAnimations.length != 4)
            throw new IllegalAccessError("Fragment animation length must be one of (0,2,4)");
        backPressInfoId = annotation.backPressInfoResId();
        if (backPressInfoId == 0) {
            backPressInfoId = Sfw.getIdFromString(annotation.backPressInfoResIdAsString(), R.string.class, 0);
        }
        if (backPressInfoId == 0) {
            backPressInfoId = R.string.info_press_back;
        }
        if (annotation.isFullScreen())
            activity.get().getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN,
                    WindowManager.LayoutParams.FLAG_FULLSCREEN);
        if (!annotation.usingCustomViewAsContent()) {
            if (annotation.contentViewLayout() == 0) {
                activity.get().setContentView(Sfw.getIdFromString(annotation.contentViewLayoutAsString(), R.layout.class, 0));
            } else
                activity.get().setContentView(annotation.contentViewLayout());
        } else
            activity.get().setContentView(((SfwActivityHelperInterface) activity.get()).getContentView());
        avoidKeyboardHideOnClick.clear();
        int[] avoidIds = annotation.avoidKeyboardHideOnClick();
        if (avoidIds.length > 0) {
            for (int id : avoidIds)
                avoidKeyboardHideOnClick.add(id);
        }

        ImageUtil.init(activity.get());
        registerReceiver();
        initListeners();
        Sfw.bindViews(activity.get(), activity.get().getWindow().getDecorView(), onClickListener);
        if (!((SfwActivityHelperInterface) activity.get()).init(savedInstanceState)) {
            ((SfwActivityHelperInterface) activity.get()).callHome();
        }
    }


    public void initListeners() {
        onClickListener = v -> {
            if (!isViewClickActionPerforming) {
                isViewClickActionPerforming = true;
                try {
                    if (!avoidKeyboardHideOnClick.contains(v.getId()))
                        Utilities.hideKeyBoard(activity.get());
                    ((SfwActivityHelperInterface) activity.get()).onClick(v, v.getId());
                    isViewClickActionPerforming = false;
                } catch (Throwable t) {
                    isViewClickActionPerforming = false;
                    if (!throwViewClickException && Sfw.isDebuggingEnabled)
                        Sfw.log(TAG, "onClick", t, true);
                    else
                        throw new RuntimeException(t);
                    throw new RuntimeException(t);
                }

            }
        };
    }

    public void registerReceiver() {
        IntentFilter filter = new IntentFilter();
        filter.addAction(ConnectivityManager.CONNECTIVITY_ACTION);
        filter.addAction(WifiManager.WIFI_STATE_CHANGED_ACTION);
        filter.addAction(activity.get().getString(R.string.intent_action_login_details_change));
        filter.addAction(activity.get().getString(R.string.intent_action_finish));
        ((SfwActivityHelperInterface) activity.get()).addBroadcastReceiverActions(filter);
        activity.get().registerReceiver(mReceiver, filter);
    }

    @Override
    public void setStatusBarColor(int statusBarColor) {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            Window window = activity.get().getWindow();
            window.addFlags(WindowManager.LayoutParams.FLAG_DRAWS_SYSTEM_BAR_BACKGROUNDS);
            window.setStatusBarColor(statusBarColor);
        }
    }

    /**
     * Should implement this method of child activities to check drawer status -> opened or not
     * eg:   [HomeActivity.onBackPressed]
     */
    @Override
    public void onBackPressed() {
        try {
            if (isChangingFragment || isBackPressProcessing || activity.get().isFinishing())
                return;
            isBackPressProcessing = true;
            Utilities.hideKeyBoard(activity.get());

            synchronized (onActivityListeners) {
                try {
                    for (int i = 0, j = onActivityListeners.size(); i < j; i++) {
                        if (onActivityListeners.get(i).onBackPressed() || ((onActivityListeners.get(i) instanceof Fragment) && ((Fragment) onActivityListeners.get(i)).getParentFragment() == null && !onActivityListeners.get(i).isAfterCreatedFragmentCalled())) {
                            isBackPressProcessing = false;
                            return;
                        }
                    }
                } catch (Exception ignored) {
                }

                boolean isHome = false;
                BackStackHolder.FragmentContentHolder previousHolder = backStackHolder.getHolder(currentFragment);
                FragmentConfig config = previousHolder == null ? null : previousHolder.getConfig();
                if (backStackHolder.getBackStackCount() > 0) {
                    Fragment fragment = activity.get().getSupportFragmentManager().findFragmentById(config != null ? config.getToId() : R.id.mainFrame);
                    isHome = ((SfwActivityHelperInterface) activity.get()).isHome(fragment);
                }//Don't add else {isHome=true;}  .. We need to call the home if isHome is false
                if (!isHome && backStackHolder.getBackStackCount() > 1) {
                    dismissProgress();
                    lastBackPressed = 0;
                    BackStackHolder.FragmentContentHolder holder = backStackHolder.removeTop();
                    if (holder != null) {
                        FragmentConfig newConfig = holder.getConfig();
                        if (config != null && config.getToId() != newConfig.getToId()) {
                            activity.get().getSupportFragmentManager().beginTransaction().remove(previousHolder.getFragment()).commitNow();
                            currentFragment = previousHolder.getFragment().getClass().getName();
                            Utilities.hideKeyBoard(activity.get());
                            return;
                        }
                        changeFragment(holder.getFragment(), newConfig);
                    }
                } else if (isHome || !((SfwActivityHelperInterface) activity.get()).callHome()) {
                    if (!isNeedToShowInfoOnBackPress || SystemClock.elapsedRealtime() - lastBackPressed < 2000) {
                        activity.get().finish();
                    } else {
                        toast(backPressInfoId);
                        lastBackPressed = SystemClock.elapsedRealtime();
                    }
                }
            }

            isBackPressProcessing = false;
        } catch (Exception e) {
            Sfw.log(TAG, "On Back Pressed", e, true);
        }
        isBackPressProcessing = false;
    }

    public void onDestroy() {

        try {
            dismissProgress();
            /*if(progressDialog!=null && progressDialog.isShowing()) {
                progressDialog.dismiss();
            }*/
            if (alertDialog != null && alertDialog.isShowing()) {
                alertDialog.dismiss();
            }
            if (activity.get() instanceof ApiHelperListener)
                SfwApiHelper.removeListener((ApiHelperListener) activity.get());
            activity.get().unregisterReceiver(mReceiver);

            onActivityListeners.clear();
        } catch (Exception ignored) {
        }
        backStackHolder.clearAll();
//        onDestroy();
//        if (callGConExit)
//            System.gc();
    }

    public boolean isCallGConExit() {
        return callGConExit;
    }

    @Override
    public void onLowMemory() {
        try {
            backStackHolder.clearAllExcept(backStackHolder.getLastHolder().getConfig().getTag());
        } catch (Exception ignored) {
        }
        System.gc();
    }

    public void onActivityResult(int requestCode, int resultCode, Intent data) {

        lastActivityResultRequestCode = requestCode;
        lastActivityResultResultCode = resultCode;
        lastActivityResultIntent = data;
        deliverActivityResult();
    }


    public void onRequestPermissionsResult(int requestCode, String[] permissions, int[] grantResults) {

        if (requestCode == PERMISSION_NETWORK_STATE && grantResults.length == 1 && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
            registerReceiver();
        } else {
            FragmentConfig config = backStackHolder.getConfig(currentFragment);
            Fragment fragment = activity.get().getSupportFragmentManager().findFragmentById(config != null ? config.getToId() : R.id.mainFrame);
            if (fragment != null)
                fragment.onRequestPermissionsResult(requestCode, permissions, grantResults);
        }
    }


    protected void onResumeFragments() {
        deliverActivityResult();
    }

    private void deliverActivityResult() {
        try {
            if (lastActivityResultIntent != null) {
                synchronized (onActivityListeners) {
                    for (int i = 0, j = onActivityListeners.size(); i < j; i++) {
                        try {
                            if (onActivityListeners.get(i) instanceof Fragment)
                                ((Fragment) onActivityListeners.get(i)).onActivityResult(lastActivityResultRequestCode, lastActivityResultResultCode, lastActivityResultIntent);
                        } catch (Exception ignored) {
                        }
                    }
                }
            }
            lastActivityResultIntent = null;
        } catch (Exception ignored) {
        }
    }

    @Override
    public void addOnActivityListener(OnActivityListener onActivityListener) {
        if (onActivityListener != null)
            synchronized (onActivityListeners) {
                if (!onActivityListeners.contains(onActivityListener)) {
                    onActivityListeners.add(onActivityListener);
                }
            }
    }

    @Override
    public void removeOnActivityListener(OnActivityListener onActivityListener) {
        synchronized (onActivityListeners) {
            onActivityListeners.remove(onActivityListener);
        }
    }

    @Override
    public Fragment getCurrentFragment() {
        FragmentConfig config = backStackHolder.getConfig(currentFragment);
        return activity.get().getSupportFragmentManager().findFragmentById(config != null ? config.getToId() : R.id.mainFrame);
    }

    @Override
    public Fragment getCurrentFragment(int containerId) {
        return activity.get().getSupportFragmentManager().findFragmentById(containerId);
    }

    @Override
    public void onLoginDetailsChanged(boolean forceLogout) {
        ((SfwActivityInterface) activity.get()).onLoginDetailsChanged(forceLogout);
    }

    @Override
    public void showProgress(ProgressConfig config) {
        showProgress(config, false);
    }

    @Override
    public void showProgress(ProgressConfig config, boolean changeMsgIfShowing) {
        if (progressDialog == null) {
            progressDialog = ((SfwActivityHelperInterface) activity.get()).createProgressDialog();
        }
        if (progressDialog.isShowing() && !changeMsgIfShowing) {
            return;
        }
        progressDialog.show(config);
    }


    @Override
    public ProgressDialog getProgressDialog() {
        return progressDialog;
    }

    @Override
    public void dismissProgress() {
        if (progressDialog != null)
            try {
                progressDialog.dismiss();
            } catch (Exception ignored) {
            }
    }

    @Override
    public void showMessage(@NonNull AlertConfig config) {
        if (alertDialog == null) {
            alertDialog = ((SfwActivityHelperInterface) activity.get()).createAlertDialog();
        }
        alertDialog.show(config);
    }

    @Override
    public AlertDialog getAlertDialog() {
        return alertDialog;
    }

    @Override
    public void showInternetErrMsg(boolean dismissProgressDialog, int requestCode, boolean setRetry, @Nullable OnAlertListener listener) {
        showInternetErrMsg(true, requestCode, setRetry, /*setRetry ?*/ R.drawable.ic_close /*: 0*/, listener);
    }

    @Override
    public void showInternetErrMsg(boolean dismissProgressDialog, int requestCode, boolean setRetry, int negativeImage, @Nullable OnAlertListener listener) {
        showInternetErrMsg(true, requestCode, setRetry ? R.drawable.ic_refresh : R.drawable.ic_accept, negativeImage, listener);
    }

    @Override
    public void showInternetErrMsg(boolean dismissProgressDialog, int requestCode, int positiveImage, int negativeImage, @Nullable OnAlertListener listener) {
        if (dismissProgressDialog)
            dismissProgress();
        AlertConfig config = new AlertConfig(listener, requestCode, activity.get().getString(R.string.tit_error), activity.get().getString(R.string.err_internet), positiveImage, negativeImage).setAsErrorWithImage().setCancelable(negativeImage == 0);
        showMessage(config);
    }

    @Override
    public void toast(int stringId) {
        toast(new ToastConfig(activity.get().getString(stringId), Toast.LENGTH_SHORT));
    }

    @Override
    public void toast(CharSequence msg) {
        toast(new ToastConfig(msg, Toast.LENGTH_SHORT));
    }


    @Override
    public void toast(ToastConfig msg) {
        Toast.makeText(activity.get(), msg.getMessage(), msg.getDuration()).show();
    }

    @Override
    public FragmentConfig createConfig(@NonNull Fragment fragment, CharSequence pageTitle, Object data) {
        return new FragmentConfig(pageTitle, fragment instanceof OnActivityListener ? ((OnActivityListener) fragment).getHolderTag() : fragment.getClass().getName(), data);
    }

    @Override
    public boolean changeFragment(@NonNull Fragment fragment, CharSequence pageTitle, Object data) {
        return changeFragment(fragment, createConfig(fragment, pageTitle, data));
    }

    @Override
    public boolean changeFragment(@NonNull Fragment fragment, CharSequence pageTitle, Object data, boolean needDrawer) {
        return changeFragment(fragment, createConfig(fragment, pageTitle, data).setNeedDrawer(needDrawer));
    }

    @Override
    public boolean addTagAndChangeFragment(@NonNull Fragment fragment, @NonNull FragmentConfig configWithoutTag) {
        return changeFragment(fragment, configWithoutTag.setTag(fragment instanceof OnActivityListener ? ((OnActivityListener) fragment).getHolderTag() : fragment.getClass().getName()));
    }

    /**
     * @param fragment new fragment
     * @Param config   Configuration
     */
    @Override
    public boolean changeFragment(@NonNull Fragment fragment, @NonNull FragmentConfig config) {
        if (isChangingFragment || fragment == null || fragment.getClass().getName().equals(currentFragment)) {
            return false;
        }

        // getSupportFragmentManager().executePendingTransactions();
        isChangingFragment = true;
        //The following variables are used to restore the back stack if the fragment transaction failed
        String previousFragment = currentFragment;
        //the holder which want to remove if the fragment transaction is success
        BackStackHolder.FragmentContentHolder removeHolder = backStackHolder.getHolder(config.getTagToRemove());
        //to hold the previous remove holder index if it contains on back stack
        int removeHolderIndex = -1;
        //to hold the previous holder index if it contains on back stack
        int index = -1;
        //to hold the previous holder if it contains on back stack
        BackStackHolder.FragmentContentHolder newHolder = null;
        try {
            currentFragment = fragment.getClass().getName();
            Utilities.hideKeyBoard(activity.get());
            if (!config.isPopupFromBackStack())
                backStackHolder.removeFromBackstack(config.getTag());
            index = backStackHolder.put(fragment, config);
            newHolder = backStackHolder.getHolder(index);
            if (removeHolder != null && !TextUtils.isEmpty(config.getTagToRemove())) {
                removeHolderIndex = backStackHolder.removeFromBackstack(config.getTagToRemove(), true);
            }
            dismissProgress();
            FragmentTransaction transaction = activity.get().getSupportFragmentManager().beginTransaction();
            if (fragmentAnimations.length == 4)
                transaction.setCustomAnimations(fragmentAnimations[0], fragmentAnimations[1], fragmentAnimations[2], fragmentAnimations[3]);
            else if (fragmentAnimations.length == 2)
                transaction.setCustomAnimations(fragmentAnimations[0], fragmentAnimations[1]);
            //                        .setCustomAnimations(R.animator.cube_right_in, R.animator.cube_right_out, R.animator.cube_left_in, R.animator.cube_left_out)
            transaction.disallowAddToBackStack().replace(config.getToId(), fragment).commit();
            return true;
        } catch (Throwable e) {
            Sfw.log(TAG, "changeFragment", e, true);
            try {
                backStackHolder.removeTop();
                if (newHolder != null) {
                    if (index >= backStackHolder.getBackStackCount()) {
                        backStackHolder.put(newHolder.getFragment(), newHolder.getConfig(), newHolder.getView());
                    } else {
                        backStackHolder.putOnPosition(index, newHolder);
                    }
                }
                if (removeHolderIndex > -1) {
                    if (removeHolderIndex >= backStackHolder.getBackStackCount()) {
                        backStackHolder.put(removeHolder.getFragment(), removeHolder.getConfig(), removeHolder.getView());
                    } else {
                        backStackHolder.putOnPosition(removeHolderIndex, removeHolder);
                    }
                }
            } catch (Throwable ignored1) {
            }
            currentFragment = previousFragment;
            isChangingFragment = false;
        }
        return false;
    }

    /**
     * * should call this from {@link Fragment#onViewCreated(View, Bundle)}, if you are not setting any animation on fragment changing
     * OR call this from {@link Fragment#onCreateAnimator(int, boolean, int)} OR {@link Fragment#onCreateAnimation(int, boolean, int)}
     */
    @Override
    public void afterFragmentCreated(@NonNull Fragment f) {
        if (f != null && f.getParentFragment() == null && f.getClass().getName().equals(currentFragment))
            isChangingFragment = false;
    }

    @Override
    public boolean changeFragmentToLastHolder() {
        try {
            BackStackHolder.FragmentContentHolder holder = backStackHolder.getLastHolder();
            if (holder != null) {
                if (!holder.getFragment().getClass().getName().equals(currentFragment))
                    currentFragment = null;
                changeFragment(holder.getFragment(), holder.getConfig());
                return true;
            } else {
                return ((SfwActivityHelperInterface) activity.get()).callHome();
            }
        } catch (Exception ignored) {
        }
        return false;
    }

    @Override
    public boolean clearBackStack(int upTo) {
        try {
            if (isChangingFragment || isBackPressProcessing)
                return false;
            if (backStackHolder.removeUpTo(upTo) != null || backStackHolder.getBackStackCount()==0) {
                return changeFragmentToLastHolder();
            }
        } catch (Exception ignored) {
        }
        return false;
    }


    /**
     * Clear all fragments from the memory except the given fragment tag
     */
    @Override
    public boolean clearBackStack(String fragmentTagToBeKeep) {
        try {
            if (isChangingFragment || isBackPressProcessing)
                return false;
            if (backStackHolder.clearAllExcept(fragmentTagToBeKeep)) {
                return changeFragmentToLastHolder();
            }
        } catch (Exception ignored) {
        }
        return false;
    }

    @Override
    public boolean clearBackStacks(int startIndex, int count, boolean topToBottom) {
        try {
            if (backStackHolder.remove(startIndex, count, topToBottom) != null || backStackHolder.getBackStackCount()==0)
                return changeFragmentToLastHolder();
        } catch (Exception ignored) {
        }
        return false;
    }

    @Override
    public boolean clearBackStackUpTo(String fragmentTagUpTo, boolean includeThis) {
        try {
            if (backStackHolder.removeUpTo(fragmentTagUpTo, includeThis) != null || backStackHolder.getBackStackCount()==0)
                return changeFragmentToLastHolder();
        } catch (Exception ignored) {
        }
        return false;
    }

    @Override
    public boolean clearBottomBackStacks(int count) {
        try {
            if (backStackHolder.removeBottom(count) != null || backStackHolder.getBackStackCount()==0)
                return changeFragmentToLastHolder();
        } catch (Exception ignored) {
        }
        return false;
    }

    @Override
    public boolean clearTopBackStacks(int count) {
        try {
            if (backStackHolder.removeTop(count) != null || backStackHolder.getBackStackCount()==0)
                return changeFragmentToLastHolder();
        } catch (Exception ignored) {
        }
        return false;
    }

    @Override
    public void onRecreateCalled() {
        if (backStackHolder != null)
            backStackHolder.clearAll();
        isBackPressProcessing = isChangingFragment = false;
        currentFragment = null;
        SfwApiHelper.clearAllListeners();
        if (SfwDatabase.getInstance() != null)
            SfwDatabase.getInstance().clearAllObservers();
    }

    @Override
    public int getBackStackIndex(String fragmentTag) {
        if (backStackHolder != null)
            return backStackHolder.getIndex(fragmentTag);
        return -1;
    }

    @Override
    @Nullable
    public BackStackHolder.FragmentContentHolder getTopBackStackHolder(String fragmentTag) {
        return backStackHolder.getHolder(getBackStackIndex(fragmentTag)+1);
    }

    @Override
    @Nullable
    public BackStackHolder.FragmentContentHolder getBottomBackStackHolder(String fragmentTag) {
        return backStackHolder.getHolder(getBackStackIndex(fragmentTag)-1);
    }

    @Override
    public boolean isTagExistsInBackStack(String fragmentTag) {
        return getBackStackIndex(fragmentTag)>-1;
    }

    @Override
    public boolean isBackStackTop(String fragmentTagToCheck, String fragmentToCompare) {
        if (backStackHolder != null)
            return backStackHolder.isTop(fragmentTagToCheck, fragmentToCompare);
        return false;
    }

    /**
     * Clearing the given fragment tag from backStack
     */
    @Override
    public boolean clearSingleFragment(String fragmentTagToBeCleared, boolean forceRemoveTop) {
        if (isChangingFragment || isBackPressProcessing)
            return false;
        boolean isTop = forceRemoveTop && backStackHolder.isTop(fragmentTagToBeCleared);
        boolean isRemoved = backStackHolder.removeFromBackstack(fragmentTagToBeCleared, forceRemoveTop) > -1;
        if (isRemoved && isTop) {
            changeFragmentToLastHolder();
        }
        return isRemoved;
    }

    @Override
    public View getFragmentViewFromBackStack(String tag) {
        return backStackHolder.getView(tag);
    }

    @Override
    public Fragment getFragment(String tag) {
        return backStackHolder.getFragment(tag);
    }

    @Override
    public FragmentConfig getFragmentConfig(String tag) {
        return backStackHolder.getConfig(tag);
    }

    @Override
    public FragmentConfig getFragmentConfig(Fragment fragment) {
        return backStackHolder.getConfig(fragment == null ? "" : fragment instanceof OnActivityListener ? ((OnActivityListener) fragment).getHolderTag() : fragment.getClass().getName());
    }

    @Override
    public void setViewToBackStack(View view, String tag) {
        backStackHolder.setView(view, tag);
    }

    @Override
    public void setDataToBackStack(Object data, String tag) {
        backStackHolder.setData(data, tag);
    }

    @Override
    public int getBackStackCount() {
        return backStackHolder.getBackStackCount();
    }

    @Override
    public boolean isFragmentAnimationUsed() {
        return fragmentAnimations.length > 0;
    }

    @Override
    public String getCurrentFragmentAsString() {
        return currentFragment;
    }

    private BroadcastReceiver mReceiver = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {
            try {
                if (((SfwActivityHelperInterface) activity.get()).broadcastMessageReceived(intent)) {
                    return;
                }
                if (intent.getAction().equals(activity.get().getString(R.string.intent_action_finish))) {
                    if (intent.hasExtra(activity.get().getString(R.string.intent_extra_activities))) {
                        ArrayList<String> activities = intent.getStringArrayListExtra(activity.get().getString(R.string.intent_extra_activities));
                        if (activities.contains(getClass().getName())) {
                            activity.get().finish();
                        }
                    }
                    return;
                }
                if (intent.getAction().equals(context.getString(R.string.intent_action_login_details_change))) {
                    boolean forceLogout = intent.getBooleanExtra(context.getString(R.string.intent_extra_logout), false);
                    onLoginDetailsChanged(forceLogout);
                    synchronized (onActivityListeners) {
                        for (int i = 0, j = onActivityListeners.size(); i < j; i++) {
                            onActivityListeners.get(i).onLoginDetailsChanged(forceLogout);
                        }
                    }
                    return;
                }
                if (intent.getAction().equals(ConnectivityManager.CONNECTIVITY_ACTION) || intent.getAction().equals(WifiManager.WIFI_STATE_CHANGED_ACTION)) {
                    boolean connected = Utilities.isNetworkAvailable(activity.get(), PERMISSION_ACCESS_NETWORK_STATE);
                    ((SfwActivityHelperInterface) activity.get()).onInternetConnectionChange(connected);
//                    if (connected) {
                    synchronized (onActivityListeners) {
                        for (int i = 0, j = onActivityListeners.size(); i < j; i++) {
                            onActivityListeners.get(i).onInternetConnectionChange(connected);
                        }
                    }
//                    }
                }

            } catch (Exception ignored) {
            }

        }
    };
}
