package jpt.sfw.appui;

import android.annotation.TargetApi;
import android.content.Context;
import android.os.Build;
import android.util.AttributeSet;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.FrameLayout;

import androidx.annotation.ColorInt;
import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.recyclerview.widget.GridLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.google.android.material.card.MaterialCardView;

import java.util.List;

import jpt.sfw.R;
import jpt.sfw.adapters.BottomMenuAdapter;
import jpt.sfw.annotations.BindView;
import jpt.sfw.app.Sfw;
import jpt.sfw.interfaces.OnAdapterListener;
import jpt.sfw.models.ValuesWithImageModel;

public class SfwBottomMenuLayout extends MaterialCardView {
    @BindView(idName = "rvBottom")
    RecyclerView rvBottom;
    private BottomMenuAdapter bottomMenuAdapter;

    public SfwBottomMenuLayout(@NonNull Context context) {
        super(context);
        initThis();
    }

    public SfwBottomMenuLayout(@NonNull Context context, @Nullable AttributeSet attrs) {
        super(context, attrs);
        initThis();
    }

    public SfwBottomMenuLayout(@NonNull Context context, @Nullable AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        initThis();
    }


    private void initThis() {
        bottomMenuAdapter=new BottomMenuAdapter();


    }

    @Override
    protected void onAttachedToWindow() {
        super.onAttachedToWindow();
        Sfw.bindViews(this,this,null);
        if(rvBottom.getAdapter()==null) {
            rvBottom.setAdapter(bottomMenuAdapter);
            if(rvBottom.getLayoutManager()==null)
            rvBottom.setLayoutManager(new GridLayoutManager(getContext().getApplicationContext(), bottomMenuAdapter.getItemCount()==0?1:bottomMenuAdapter.getItemCount()));
        }

    }

    public SfwBottomMenuLayout setList(@NonNull List<ValuesWithImageModel> list) {
        return setList(list,0);
    }

    public SfwBottomMenuLayout setList(@NonNull List<ValuesWithImageModel> list,int selectedPosition) {
        if(rvBottom!=null)
            rvBottom.setLayoutManager(new GridLayoutManager(getContext().getApplicationContext(), list.size()));
        bottomMenuAdapter.setList(list);
        bottomMenuAdapter.setSelectedPosition(selectedPosition);
        return this;
    }

    public SfwBottomMenuLayout setListener(OnAdapterListener<ValuesWithImageModel> listener) {
        bottomMenuAdapter.setListener(listener);
        return this;
    }

    public SfwBottomMenuLayout setSelectionColors(@ColorInt int selectedColor, @ColorInt int unSelectedColor) {
        bottomMenuAdapter.setSelectionColors(selectedColor,unSelectedColor);
        return this;
    }

    public SfwBottomMenuLayout setSelectedPosition(int selectedPosition) {
        bottomMenuAdapter.setSelectedPosition(selectedPosition);
        return this;
    }

    public void hideText(boolean hide) {
        bottomMenuAdapter.hideText(hide);
    }

    public BottomMenuAdapter getBottomMenuAdapter() {
        return bottomMenuAdapter;
    }

}
