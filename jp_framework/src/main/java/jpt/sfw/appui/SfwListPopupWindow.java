package jpt.sfw.appui;

import android.content.Context;
import android.os.Build;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.PopupWindow;

import jpt.sfw.R;
import jpt.sfw.adapters.ListingAdapterSfw;
import jpt.sfw.app.Sfw;
import jpt.sfw.interfaces.BaseModelInterface;
import jpt.sfw.annotations.BindView;
import jpt.sfw.interfaces.OnAdapterListener;
import jpt.sfw.annotations.OnClick;

import java.util.List;

import androidx.annotation.ColorInt;
import androidx.annotation.LayoutRes;
import androidx.annotation.NonNull;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

public class SfwListPopupWindow extends PopupWindow {
    public SfwListPopupWindow(@NonNull Context context) {
        super(LayoutInflater.from(context).inflate(R.layout.sfw_dia_popup_list, null), ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.WRAP_CONTENT);
        setAnimationStyle(R.style.SFW_PopupWindowAnimation);
        setFocusable(true);
        setOutsideTouchable(false);
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            setElevation(context.getResources().getDimensionPixelSize(R.dimen.common_margin));
        }
        oneDp = context.getResources().getDimensionPixelSize(R.dimen.one_dp);
        backgroundLayoutHeight = context.getResources().getDimensionPixelSize(R.dimen.medium_margin);

        Sfw.bindViews(this, getContentView(), view -> {
            try {
                dismiss();
            } catch (Exception ignored) {
            }
        });
        adapter = new ListingAdapterSfw((view, adapterPosition, tag) -> {
            try {
                dismiss();
                if (listener != null) {
                    listener.onViewClick(view, adapterPosition, tag);
                }
            } catch (Exception ignored) {
            }
        });
        rvList.setLayoutManager(new LinearLayoutManager(context));
        rvList.setNestedScrollingEnabled(false);
        rvList.setAdapter(adapter);
    }

   /* public SfwListPopupWindow(Context context, AttributeSet attrs) {
        this(null,0,0);
    }

    public SfwListPopupWindow(Context context, AttributeSet attrs, int defStyleAttr) {
        this(null,0,0);
    }

    public SfwListPopupWindow(Context context, AttributeSet attrs, int defStyleAttr, int defStyleRes) {
        this(null,0,0);
    }

    public SfwListPopupWindow() {
        this(null,0,0);
    }

    public SfwListPopupWindow(View contentView) {
        this(null,0,0);
    }

    public SfwListPopupWindow(int width, int height) {
        super(width, height);
    }

    public SfwListPopupWindow(View contentView, int width, int height) {
        super(contentView, width, height);
        throw new RuntimeException("Should call only SfwListPopupWindow(@NonNull Context context)");
    }

    public SfwListPopupWindow(View contentView, int width, int height, boolean focusable) {
        this(null,0,0);
    }*/

    public SfwListPopupWindow setList(List<? extends BaseModelInterface> list) {
        adapter.setList(list, "");
        return this;
    }

    public SfwListPopupWindow setList(List<? extends BaseModelInterface> list, View anchorView) {
        return setList(list, anchorView, false);
    }


    public SfwListPopupWindow setList(List<? extends BaseModelInterface> list, View anchorView, boolean setBackground) {
        return setList(list, true, anchorView, setBackground);
    }

    private SfwListPopupWindow setList(List<? extends BaseModelInterface> list, boolean setList, View anchorView, boolean setBackground) {
        if (setList)
            adapter.setList(list, "");
        if (anchorView != null) {
            setWidth(anchorView.getWidth() < minWidth ? minWidth : anchorView.getWidth());
            int height = anchorView.getHeight();
            layoutBackGround.setVisibility(height < 1 || hideTop ? View.GONE : View.VISIBLE);
            backgroundView.setVisibility(height < 1 || hideTop ? View.GONE : View.VISIBLE);
            if (backgroundView.getVisibility() != View.GONE) {
                ViewGroup.LayoutParams lp = backgroundView.getLayoutParams();
                lp.height = height + oneDp;
                lp.width = anchorView.getWidth() < minWidth ? minWidth : anchorView.getWidth();
                backgroundView.setLayoutParams(lp);
                //The anchorview position will change If keyboard is shown and then hide...
                // At that time transperant view(backgroundView) will show other views on background.
                // The below code will prevent that issue
                if (backgroundView.getVisibility() != View.INVISIBLE)
                    backgroundView.setBackground(setBackground ? anchorView.getBackground() : null);
                showAsDropDown(anchorView, 0, -lp.height - backgroundLayoutHeight, Gravity.BOTTOM);
            } else {
                showAsDropDown(anchorView, 0, 0, Gravity.BOTTOM);
            }
        }
        return this;
    }

    public void show(View anchorView, boolean setBackground) {
        setList(null, false, anchorView, setBackground);
    }

    public void setListener(OnAdapterListener<BaseModelInterface> listener) {
        this.listener = listener;
    }

    public SfwListPopupWindow setOnLongClickListener(OnAdapterListener<BaseModelInterface> onLongClickListener) {
        try {
            adapter.setOnLongClickListener(onLongClickListener);
        } catch (Exception e) {
        }
        return this;
    }

    public OnAdapterListener<? extends BaseModelInterface> getListener() {
        return adapter.getListener();
    }


    public void setFilter(String filter) {
        try {
            adapter.filter(filter);
        } catch (Exception ignored) {
        }
    }

    public void setMinWidth(int minWidth) {
        this.minWidth = minWidth;
    }

    public void setHideTop(boolean hideTop) {
        this.hideTop = hideTop;
//        if (hideTop)
//            layoutPopupOuter.setBackgroundResource(R.drawable.rectangle_popup_back);
//        else
//            layoutPopupOuter.setBackground(null);

    }
    /**
     *
     * @param layout  should contain a TextView with id "txtTitle"  and the layout outer view id should be "layoutAdaMain"
     */
    public void setCustomLayoutForList(@LayoutRes int layout) {
        adapter.setLayout(layout);
    }

    public void setSpanColor(@ColorInt int spanColor) {
        adapter.setSpanColor(spanColor);
    }

    public void setTextColor(@ColorInt int textColor) {
        adapter.setTextColor(textColor);
    }

    @ColorInt
    public int getTextColor() {
        return adapter.getTextColor();
    }

    @BindView(idName = "rvList")
    private RecyclerView rvList;
    @OnClick(idName = "layoutBackGround")
    private View layoutBackGround;
    @OnClick(idName = "backgroundView")
    private View backgroundView;
    @BindView(idName = "layoutPopupOuter")
    View layoutPopupOuter;

    private ListingAdapterSfw adapter;
    private OnAdapterListener<BaseModelInterface> listener;
    private int oneDp, backgroundLayoutHeight;
    private int minWidth;
    private boolean hideTop;


}
