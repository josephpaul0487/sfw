package jpt.sfw.appui;

import android.annotation.TargetApi;
import android.content.Context;
import android.content.res.ColorStateList;
import android.content.res.TypedArray;
import android.graphics.Color;
import android.graphics.drawable.Drawable;
import android.os.Build;
import android.text.TextUtils;
import android.util.AttributeSet;
import android.util.Log;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.FrameLayout;
import android.widget.ProgressBar;
import android.widget.TextView;

import jpt.sfw.R;
import jpt.sfw.app.Sfw;
import jpt.sfw.app.Utilities;
import jpt.sfw.ui.SfwButtonView;

import androidx.annotation.ColorInt;
import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.annotation.StringRes;

public class SfwButtonLayout extends FrameLayout {
    public SfwButtonLayout(@NonNull Context context) {
        super(context);
        init(null);
    }

    public SfwButtonLayout(@NonNull Context context, @Nullable AttributeSet attrs) {
        super(context, attrs);
        init(attrs);
    }

    public SfwButtonLayout(@NonNull Context context, @Nullable AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        init(attrs);
    }

    @TargetApi(Build.VERSION_CODES.LOLLIPOP)
    public SfwButtonLayout(@NonNull Context context, @Nullable AttributeSet attrs, int defStyleAttr, int defStyleRes) {
        super(context, attrs, defStyleAttr, defStyleRes);
        init(attrs);
    }

    private void init(AttributeSet attrs) {
        setBackgroundColor(Color.TRANSPARENT);
        String str = Sfw.getStringFromAttribute(getContext(), attrs, R.styleable.SfwButtonLayout_sfw_buttonText, R.styleable.SfwButtonLayout);
        int buttonLayout = Sfw.getIntFromAttribute(getContext(), attrs, R.styleable.SfwButtonLayout_sfw_buttonLayout, R.styleable.SfwButtonLayout, 0, false);
        int progressLayout = Sfw.getIntFromAttribute(getContext(), attrs, R.styleable.SfwButtonLayout_sfw_progressLayout, R.styleable.SfwButtonLayout, 0, false);

        button =  LayoutInflater.from(getContext()).inflate(buttonLayout==0?R.layout.button:buttonLayout, this, false);//new SfwButtonView(getContext(), attrs);
        if(button instanceof TextView) {
            Sfw.setFont((TextView) button, attrs);

            if (Sfw.isAllCaps(attrs, getContext()))
                if (!TextUtils.isEmpty(str))
                    ((TextView) button).setAllCaps(true);
            ((TextView) button).setText(str);
        }

        int layoutHeight = 0;
        try {
            int[] attrsArray = new int[]{
                    android.R.attr.layout_height, android.R.attr.minHeight
            };
            TypedArray ta = getContext().obtainStyledAttributes(attrs, attrsArray);
            try {
                layoutHeight = ta.getDimensionPixelSize(0, 0);
                if (layoutHeight < 1) {
                    layoutHeight = ta.getDimensionPixelSize(1, 0);
                }
            } finally {
                ta.recycle();
            }
        } catch (Exception ignored) {
        }
        LayoutParams lp1 = (LayoutParams) button.getLayoutParams();
        if (layoutHeight > 0) {
            lp1.height = layoutHeight;
        }
        lp1.gravity = Gravity.CENTER;
        button.setLayoutParams(lp1);

        progressView = LayoutInflater.from(getContext()).inflate(progressLayout == 0 ? R.layout.sfw_layout_common_progress : progressLayout, this, false);
        progressView.setId(R.id.buttonLayoutProgress);
        progressBar = progressView.findViewById(R.id.progressBar);
        if(progressBar!=null) {
            if(layoutHeight==0 && button.getHeight()>0) {
                layoutHeight=button.getHeight();
            }
            LayoutParams lp = (LayoutParams) progressBar.getLayoutParams();
            lp.width = layoutHeight==0?ViewGroup.LayoutParams.WRAP_CONTENT:layoutHeight;
            lp.height = layoutHeight > 0 ? layoutHeight : ViewGroup.LayoutParams.MATCH_PARENT;
            lp.gravity = Gravity.CENTER;
            progressBar.setLayoutParams(lp);
        }

        addView(button);
        addView(progressView);

        hideButtonAnimation = AnimationUtils.loadAnimation(getContext(), R.anim.hide_button);
        showButtonAnimation = AnimationUtils.loadAnimation(getContext(), R.anim.show_button);
        hideButtonAnimation.setAnimationListener(new Animation.AnimationListener() {
            @Override
            public void onAnimationStart(Animation animation) {
                isButtonHiding = true;
                changeVisibility(true);
            }

            @Override
            public void onAnimationEnd(Animation animation) {
                changeVisibility(false);
                isButtonHiding = false;
            }

            @Override
            public void onAnimationRepeat(Animation animation) {

            }
        });
        showButtonAnimation.setAnimationListener(new Animation.AnimationListener() {
            @Override
            public void onAnimationStart(Animation animation) {
                isButtonShowing = true;
                changeVisibility(true);
            }

            @Override
            public void onAnimationEnd(Animation animation) {
                isButtonShowing = false;
            }

            @Override
            public void onAnimationRepeat(Animation animation) {

            }
        });
        button.setOnClickListener(view -> {
            try {
                if (isShowingProgress())
                    return;
                showProgress(true);
                if (onClickListener != null)
                    onClickListener.onClick(this);
            } catch (Exception e) {
                Sfw.log(TAG, "OnButtonClick", e, true);
            }
        });
    }

    private void changeVisibility(boolean visible) {
        try {
            button.setVisibility(visible ? VISIBLE : GONE);
            progressView.setVisibility(visible ? GONE : VISIBLE);
        } catch (Exception ignored) {
        }
    }

    public void showProgress(boolean show) {
        try {
            if (show == isShowingProgress)
                return;
            isShowingProgress = show;
            button.clearAnimation();
            button.startAnimation(show ? hideButtonAnimation : showButtonAnimation);
        } catch (Exception ignored) {
            isShowingProgress = !show;
        }
    }

    public boolean isShowingProgress() {
        return isShowingProgress;
    }

    @Override
    public void setOnClickListener(@Nullable OnClickListener l) {
        this.onClickListener = l;
    }

    public void setText(CharSequence text) {
        if(button instanceof TextView)
            ((TextView)button).setText(text);
    }

    public void setText(@StringRes int stringId) {
        if(button instanceof TextView)
            ((TextView)button).setText(stringId);
    }

    public void setProgressColor(@ColorInt int color) {
        if(progressBar!=null && progressBar instanceof ProgressBar)
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
                ((ProgressBar)progressBar).setIndeterminateTintList(ColorStateList.valueOf(color));
            } else {
                Drawable drawable=((ProgressBar)progressBar).getIndeterminateDrawable();
                Utilities.setDrawableTint(color,drawable);
            }
    }


    public void setProgressWH(int width) {
        if(progressBar!=null) {
            ViewGroup.LayoutParams lp=progressBar.getLayoutParams();
            lp.height=width;
            lp.width=width;
            progressBar.setLayoutParams(lp);
        }
    }

    private Animation hideButtonAnimation, showButtonAnimation;
    private boolean isButtonHiding, isButtonShowing, isShowingProgress;
    private View button;
    private View progressView;
    private  View progressBar;
    private OnClickListener onClickListener;
    private static final String TAG = "SfwButtonLayout";
}
