package jpt.sfw.appui;

import android.app.Dialog;
import android.content.Context;
import android.view.View;
import android.widget.TextView;

import jpt.sfw.R;
import jpt.sfw.app.Sfw;
import jpt.sfw.app.ProgressConfig;
import jpt.sfw.annotations.BindView;
import jpt.sfw.annotations.OnClick;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;

public class ProgressDialog extends Dialog implements View.OnClickListener{
    android.app.ProgressDialog d;

    public ProgressDialog(@NonNull Context context) {
        this(context, R.style.SFW_ProgressDialog);
    }

    public ProgressDialog(@NonNull Context context, int themeResId) {
        super(context, themeResId);
        super.setContentView(R.layout.layout_progress_dialog);
        init();
    }

    protected ProgressDialog(@NonNull Context context, boolean cancelable, @Nullable OnCancelListener cancelListener) {
        this(context, R.style.SFW_ProgressDialog);
    }

    @Override
    public void setContentView(int layoutResId) {


    }

    private void init() {
        Sfw.bindViews(this, getWindow().getDecorView(), this::onClick);
    }


    @Override
    public void setContentView(View view) {

    }

    @Override
    public void show() {
        throw new RuntimeException("Should not call this.  call show(ProgressConfig config) instead");
    }

    public void show(ProgressConfig config) {

        if(config==null)
            return;
        this.lastConfig=config;
        if (config.getListener() != null) {
            setOnCancelListener(dialog -> {
                if (lastConfig!=null && lastConfig.getListener() != null)
                    lastConfig.getListener().onAlertCancelled(lastConfig.getRequestId(),false);
                clear();
            });
        }
        setCancelable(config.isCancelable());
        setCanceledOnTouchOutside(config.isCancelable());
//            ((TextView) bottomSheetDialog.findViewById(R.id.txtTitle)).setText(config.getTitle());

        updateUI(config);


        if(!isShowing())
        super.show();

    }

    public void updateUI(ProgressConfig config) {
        txtMsg.setText(config.getMsg());
    }

    public void changeText(CharSequence text) {
        if(txtMsg!=null) {
            try {
                txtMsg.setText(text);
            } catch (Exception ignored) {
            }
        }
    }

    /**
     * Clear alert listeners    called from {@link jpt.sfw.interfaces.SfwActivityInterface#showProgress(ProgressConfig)})}
     */
    public void clear() {
        lastConfig = null;
    }

    @Override
    public void onClick(View v) {
        if (lastConfig != null && lastConfig.getListener() != null && lastConfig.isCancelable()) {
            clear();
            dismiss();
        }

    }


    private ProgressConfig lastConfig;

    @BindView(idName = "txtMsg")
    private TextView txtMsg;
    @OnClick(idName = "mainLayout")
    View mainLayout;



}
