package jpt.sfw.appui;

import android.app.Dialog;
import android.content.Context;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import jpt.sfw.R;
import jpt.sfw.annotations.Nullable;
import jpt.sfw.app.AlertConfig;
import jpt.sfw.app.Sfw;
import jpt.sfw.app.Utilities;
import jpt.sfw.annotations.BindView;
import jpt.sfw.annotations.OnClick;

import androidx.annotation.LayoutRes;
import androidx.annotation.NonNull;
import androidx.annotation.StyleRes;

public class AlertDialog extends Dialog implements View.OnClickListener {
    public AlertDialog(@NonNull Context context) {
        this(context, R.style.SFW_AlertDialog);
    }

    public AlertDialog(@NonNull Context context, @StyleRes int theme) {
        this(context, theme, R.layout.sfw_layout_alert_dialog);
    }

    public AlertDialog(@NonNull Context context, @StyleRes int theme, @LayoutRes int layout) {
        super(context, theme);
        super.setContentView(layout);
        init();
    }


    @Override
    public void setContentView(int layoutResId) {

    }


    @Override
    public void setContentView(View view) {

    }

    public void setDialogId(int id) {
        this.id = id;
    }

    private void init() {
        Sfw.bindViews(this, getWindow().getDecorView(), this::onClick);
    }

    @Override
    public final void show() {
        throw new RuntimeException("Should not call this.  call show(AlertConfig config) instead");
    }

    public final void show(AlertConfig config) {

        if (config == null)
            return;
        this.lastConfig = config;
        if (config.getListener() != null) {
            setOnCancelListener(dialog -> {
                if (lastConfig != null && lastConfig.getListener() != null)
                    lastConfig.getListener().onAlertCancelled(lastConfig.getRequestId(), false, lastConfig);
                clear();
            });
        }
        setCancelable(config.isCancelable());
        setCanceledOnTouchOutside(config.isCancelable());
//            ((TextView) bottomSheetDialog.findViewById(R.id.txtTitle)).setText(config.getTitle());

        updateUI(config);
        if (!isShowing())
            super.show();

    }

    public void updateUI(AlertConfig config) {
        if (txtMsg != null)
            txtMsg.setText(config.getMsg());
        if (btnNegative != null)
            btnNegative.setVisibility(lastConfig.getNegativeImage() == 0 ? View.GONE : View.VISIBLE);
        if (lastConfig.getNegativeImage() != 0) {
            if (imgNegativeBack != null)
                Utilities.setImageViewTint(lastConfig.getNegativeBtnBackTint(), imgNegativeBack);
            if (imgNegative != null)
                imgNegative.setImageResource(lastConfig.getNegativeImage());
        }
        if (imgPositive != null)
            imgPositive.setImageResource(lastConfig.getPositiveImage());
        if (imgPositiveBack != null)
            Utilities.setImageViewTint(lastConfig.getPositiveBtnBackTint(), imgPositiveBack);
        if (imgAlert != null)
            imgAlert.setImageResource(lastConfig.getImage());
    }

    /**
     * Clear alert listeners    called from {@link jpt.sfw.interfaces.SfwActivityInterface#showMessage(AlertConfig)}
     */
    public void clear() {
        lastConfig = null;
    }


    @Override
    public void onClick(View v) {
        if (lastConfig != null && lastConfig.getListener() != null) {
            if (v.getId() == R.id.btnPositive)
                lastConfig.getListener().onAlertPositiveClicked(lastConfig.getRequestId(), lastConfig);
            else if (v.getId() == R.id.btnNegative)
                lastConfig.getListener().onAlertNegativeClicked(lastConfig.getRequestId(), lastConfig);
            else if (lastConfig.isCancelable())
                lastConfig.getListener().onAlertCancelled(lastConfig.getRequestId(), false, lastConfig);
            else
                return;
        }
        clear();
        dismiss();
    }

    protected AlertConfig lastConfig;

    @Nullable
    @BindView(idName = "txtMsg")
    protected TextView txtMsg;
    @Nullable
    @OnClick(idName = "btnPositive")
    protected View btnPositive;
    @Nullable
    @BindView(idName = "imgNegative")
    protected ImageView imgNegative;
    @Nullable
    @BindView(idName = "imgNegativeBack")
    protected ImageView imgNegativeBack;
    @Nullable
    @BindView(idName = "imgPositiveBack")
    protected ImageView imgPositiveBack;
    @Nullable
    @BindView(idName = "imgPositive")
    protected ImageView imgPositive;
    @Nullable
    @BindView(idName = "imgAlert")
    protected ImageView imgAlert;
    @Nullable
    @OnClick(idName = "btnNegative")
    protected View btnNegative;
    @Nullable
    @BindView(idName = "mainLayout")
    protected View mainLayout;
    private int id;


}
