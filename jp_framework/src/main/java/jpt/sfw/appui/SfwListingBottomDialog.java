package jpt.sfw.appui;

import android.app.Dialog;
import android.content.Context;
import android.os.Handler;
import android.text.Editable;
import android.view.View;
import android.view.ViewGroup;
import android.widget.FrameLayout;

import jpt.sfw.R;
import jpt.sfw.adapters.ListingAdapterSfw;
import jpt.sfw.annotations.Nullable;
import jpt.sfw.annotations.OnClick;
import jpt.sfw.app.Sfw;
import jpt.sfw.app.Text;
import jpt.sfw.app.Utilities;
import jpt.sfw.interfaces.BaseModelInterface;
import jpt.sfw.annotations.BindView;
import jpt.sfw.interfaces.OnAdapterListener;
import jpt.sfw.interfaces.SfwListingDialogInterfaces;
import jpt.sfw.ui.SfwEditText;
import jpt.sfw.ui.SfwCommonTextWatcher;

import java.util.ArrayList;
import java.util.List;

import androidx.annotation.ColorInt;
import androidx.annotation.LayoutRes;
import androidx.annotation.NonNull;
import androidx.annotation.StringRes;
import androidx.annotation.StyleRes;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.google.android.material.bottomsheet.BottomSheetBehavior;
import com.google.android.material.bottomsheet.BottomSheetDialog;

public class SfwListingBottomDialog extends BottomSheetDialog {
    private SfwListingDialogHelper helper;

    public SfwListingBottomDialog(@NonNull Context context) {
        this(context, R.style.ListingDialog);
    }

    public SfwListingBottomDialog(@NonNull Context context, @StyleRes int theme) {
        this(context, theme, R.layout.sfw_dia_bottom_listing);
    }

    public SfwListingBottomDialog(@NonNull Context context, boolean cancelable, OnCancelListener cancelListener) {
        this(context,R.style.ListingDialog,R.layout.sfw_dia_bottom_listing, cancelable, cancelListener);
    }

    public SfwListingBottomDialog(@NonNull Context context, @StyleRes int theme, @LayoutRes int layout) {
        this(context, theme,layout,true, null);
    }

    public SfwListingBottomDialog(@NonNull Context context, @StyleRes int theme, @LayoutRes int layout,boolean cancelable, OnCancelListener cancelListener) {
        super(context, theme);
        helper = new SfwListingDialogHelper();
        setCancelable(cancelable);
        setCanceledOnTouchOutside(cancelable);
        setOnCancelListener(cancelListener);
        setContentView(layout);
    }

    @Override
    public void setContentView(View view) {

    }

    @Override
    public void setContentView(View view, ViewGroup.LayoutParams params) {

    }

    /**
     * @param layoutResId layout should contain [ A {@link RecyclerView} with id "rvListing" ,
     *                    A {@link SfwEditText} with  id  "edtListing" ]
     *                    Optionally you can add a layout with id "layoutAlertClose" to close the dialog on click
     */
    @Override
    public void setContentView(@LayoutRes int layoutResId) {
        super.setContentView(layoutResId);
        helper.initialize(this);
    }

    public void show(boolean setExpanded) {
        helper.show(setExpanded);
    }


    @NonNull
    public SfwListingDialogHelper getHelper() {
        return helper;
    }
}
