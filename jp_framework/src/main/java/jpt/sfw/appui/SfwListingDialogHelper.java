package jpt.sfw.appui;

import android.app.Dialog;
import android.graphics.Color;
import android.os.Handler;
import android.text.Editable;
import android.text.style.ForegroundColorSpan;
import android.view.View;
import android.widget.FrameLayout;

import androidx.annotation.ColorInt;
import androidx.annotation.NonNull;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.google.android.material.bottomsheet.BottomSheetBehavior;
import com.google.android.material.bottomsheet.BottomSheetDialog;

import java.util.ArrayList;
import java.util.List;

import jpt.sfw.R;
import jpt.sfw.adapters.ListingAdapterSfw;
import jpt.sfw.annotations.BindView;
import jpt.sfw.annotations.Nullable;
import jpt.sfw.annotations.OnClick;
import jpt.sfw.app.Sfw;
import jpt.sfw.app.Text;
import jpt.sfw.app.Utilities;
import jpt.sfw.interfaces.BaseModelInterface;
import jpt.sfw.interfaces.SfwListingDialogInterfaces;
import jpt.sfw.interfaces.OnAdapterListener;
import jpt.sfw.ui.SfwCommonTextWatcher;
import jpt.sfw.ui.SfwEditText;

public class SfwListingDialogHelper implements SfwListingDialogInterfaces, View.OnClickListener {

    private String TAG = "SfwSfwListingDialogHelper";
    @BindView(idName = "rvListing")
    private RecyclerView rvLocationListing;
    //    @BindView(R.id.txtTitle)
//    private TextView txtLocationTitle;
    @BindView(idName = "edtListing")
    private SfwEditText edtListing;
    @Nullable
    @OnClick(idName = "layoutAlertClose")
    private View layoutAlertClose;

    private ListingAdapterSfw locationListingAdapter;
    private OnAdapterListener<BaseModelInterface> listener;
    private Dialog dialog;

    public void initialize(@NonNull Dialog dialog) {
        this.dialog = dialog;
        View rootView = dialog.getWindow().getDecorView();
        if (rootView == null) {
            return;
        }
        TAG = dialog.getClass().getSimpleName();
        if (TAG.length() > 23) {
            TAG = TAG.substring(0, 23);
        }
        Sfw.bindViews(this, rootView, this);
        edtListing.addTextChangedListener(new SfwCommonTextWatcher(edtListing) {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {

            }

            @Override
            public void textChanged(Editable s) {
                try {
                    locationListingAdapter.filter(s.toString());
                } catch (Exception e) {
                    Sfw.log(TAG, "", e, true);
                }
            }
        });
        rvLocationListing.setLayoutManager(new LinearLayoutManager(rootView.getContext()));
        locationListingAdapter = new ListingAdapterSfw(null);
        rvLocationListing.setAdapter(locationListingAdapter);
    }


    @Override
    public void setListener(OnAdapterListener<BaseModelInterface> listener) {
        if (locationListingAdapter != null && dialog != null) {
            this.listener = listener;
            if (locationListingAdapter.getListener() == null)
                locationListingAdapter.setListener((view, adapterPosition, tag) -> {
                    if (this.listener != null) {
                        try {
                            this.listener.onViewClick(view, adapterPosition, tag);
                        } catch (Exception e) {
                        }
                    }
                    Utilities.hideKeyBoard(dialog);
                    dialog.dismiss();
                });
        }
    }

    @Override
    public void setList(List<? extends BaseModelInterface> models) {
        if (models != null && locationListingAdapter != null) {
            locationListingAdapter.setList(models, edtListing.getText().toString());
        }
    }

    @Override
    public List<? extends BaseModelInterface> getList() {
        if (locationListingAdapter != null) {
            return locationListingAdapter.getModels();
        }
        return new ArrayList<>();
    }

    @Override
    public BaseModelInterface getItem(int position) {
        if (locationListingAdapter != null)
            return locationListingAdapter.getItem(position);
        return null;
    }

    @Override
    public void setSearchQuery(CharSequence searchQuery) {
        if (edtListing != null && searchQuery != null) {
            Text.setTextTV(searchQuery, edtListing, true);

        }
    }

    @Override
    public int getItemCount() {
        return locationListingAdapter != null ? locationListingAdapter.getItemCount() : 0;
    }

    /**
     * @param layout should contain a TextView with id "txtTitle"  and the layout outer view id should be "layoutAdaMain"
     */
    @Override
    public void setListingItemLayout(int layout) {
        if (locationListingAdapter != null)
            locationListingAdapter.setLayout(layout);
    }

    @Override
    public void showCloseButton(boolean show) {
        if (layoutAlertClose != null)
            layoutAlertClose.setVisibility(show ? View.VISIBLE : View.GONE);
    }

    @Override
    public void setEdtHint(CharSequence text) {
        if (edtListing != null) {
            edtListing.setHint(text);
        }
    }

    @Override
    public SfwEditText getEditText() {
        return edtListing;
    }

    @Override
    public RecyclerView getRecyclerView() {
        return rvLocationListing;
    }

    @Override
    public View getCloseView() {
        return layoutAlertClose;
    }

    @Override
    public ListingAdapterSfw getAdapter() {
        return locationListingAdapter;
    }

    @Override
    public void changeAdapter(@NonNull ListingAdapterSfw adapter) {
        if (adapter != null) {
            this.locationListingAdapter = adapter;
            if (rvLocationListing != null) {
                rvLocationListing.swapAdapter(locationListingAdapter, true);
            }

        }
    }

    @Override
    public OnAdapterListener<BaseModelInterface> getListener() {
        return listener;
    }

    @Override
    public void setListingItemTextColor(@ColorInt int textColor) {
        if (locationListingAdapter != null) {
            locationListingAdapter.setTextColor(textColor);
        }

    }

    @Override
    @ColorInt
    public int getListingItemTextColor() {
        if (locationListingAdapter != null) {
            locationListingAdapter.getTextColor();
        }
        return Color.TRANSPARENT;
    }

    @Override
    public void setListingItemSpanColor(@ColorInt int spanColor) {
        if (locationListingAdapter != null) {
            locationListingAdapter.setSpanColor(spanColor);
        }
    }

    @Override
    public ForegroundColorSpan getListingItemSpanColor() {
        if (locationListingAdapter != null) {
            locationListingAdapter.getSpanColor();
        }
        return null;
    }

    public void show(boolean setExpanded) {
        if (dialog != null) {
            dialog.show();
        }
        if (dialog instanceof BottomSheetDialog) {
            if (setExpanded) {
                new Handler().postDelayed(() -> {
                    try {
                        FrameLayout bottomSheet = dialog.findViewById(R.id.design_bottom_sheet);
                        BottomSheetBehavior bottomSheetBehavior = BottomSheetBehavior.from(bottomSheet);
                        bottomSheetBehavior.setState(BottomSheetBehavior.STATE_EXPANDED);
                    } catch (Exception ignored) {
                    }

                }, 0);
            }
        }

    }

    public void onClick(View v) {
        if (dialog != null && dialog.isShowing())
            dialog.dismiss();
    }


}
