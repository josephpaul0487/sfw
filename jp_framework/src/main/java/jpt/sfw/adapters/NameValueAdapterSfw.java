package jpt.sfw.adapters;

import android.text.TextUtils;
import android.util.TypedValue;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import jpt.sfw.R;
import jpt.sfw.app.Text;
import jpt.sfw.annotations.BindView;
import jpt.sfw.models.NameValueModel;

import java.util.List;

import androidx.annotation.NonNull;

public class NameValueAdapterSfw extends SfwCommonBaseAdapter<NameValueModel, NameValueAdapterSfw.ViewHolder> {

    public NameValueAdapterSfw(List<NameValueModel> models) {
        this(models, false);
    }

    public NameValueAdapterSfw(List<NameValueModel> models, boolean removeEmptyFields) {
        if (models != null) {
            if (removeEmptyFields) {
                for (int i=0;i<models.size();) {
                    if (TextUtils.isEmpty(model.getName()) || TextUtils.isEmpty(model.getValue())) {
                        models.remove(i);
                        continue;
                    }
                    i++;
                }

            }
            setList(models);
        }
    }


    @NonNull
    @Override
    public ViewHolder createHolder(@NonNull ViewGroup parent, int viewType) {
        return new ViewHolder(LayoutInflater.from(parent.getContext()).inflate(R.layout.sfw_ada_name_value, parent, false));
    }


    @Override
    public void bindHolder(@NonNull ViewHolder holder, int position) {
        try {
            Text.setHtmlString(holder.txtTitle, model.getName());
            Text.setHtmlString(holder.txtValue, model.getValue());
            holder.txtValue.setTextSize(TypedValue.COMPLEX_UNIT_PX, holder.txtValue.getResources().getDimensionPixelSize(model.getValueTextSize()));
        } catch (Exception ignored) {
        }
    }


    static class ViewHolder extends SfwCommonBaseAdapter.ViewHolderBase<NameValueModel> {
        @BindView(idName = "txtTitle")
        TextView txtTitle;
        @BindView(idName = "txtValue")
        TextView txtValue;

        public ViewHolder(View itemView) {
            super(itemView);
        }
    }

}
