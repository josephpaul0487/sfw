package jpt.sfw.adapters;

import android.text.Spannable;
import android.text.SpannableString;
import android.text.Spanned;
import android.text.TextUtils;
import android.text.style.ForegroundColorSpan;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.annotation.ColorInt;
import androidx.annotation.LayoutRes;
import androidx.annotation.NonNull;

import jpt.sfw.R;
import jpt.sfw.app.Sfw;
import jpt.sfw.interfaces.BaseModelInterface;
import jpt.sfw.annotations.BindView;
import jpt.sfw.interfaces.OnAdapterListener;
import jpt.sfw.annotations.OnClick;

import java.util.ArrayList;
import java.util.Comparator;
import java.util.List;
import java.util.Locale;

import androidx.core.content.ContextCompat;


public class ListingAdapterSfw extends SfwCommonBaseAdapter<BaseModelInterface, ListingAdapterSfw.ViewHolder> {

    public ListingAdapterSfw(OnAdapterListener<BaseModelInterface> listener) {
        this.listener = listener;
        comparator = (o1, o2) -> {
            try {
                return Integer.compare(o1.getTitle().toLowerCase(Locale.ENGLISH).indexOf(searchString), o2.getTitle().toLowerCase(Locale.ENGLISH).indexOf(searchString));
            } catch (Exception ignored) {
            }

            return 0;
        };
    }


    public ListingAdapterSfw setOnLongClickListener(OnAdapterListener<BaseModelInterface> onLongClickListener) {
        this.onLongClickListener = onLongClickListener;
        notifyDataSetChanged();
        return this;
    }

    public void setTextColor(@ColorInt int textColor) {
        this.textColor = textColor;
    }

    @ColorInt
    public int getTextColor() {
        return textColor;
    }

    public void setSpanColor(@ColorInt int spanColor) {
        colorSpan = new ForegroundColorSpan(spanColor);
        notifyDataSetChanged();
    }


    public ForegroundColorSpan getSpanColor() {
        return colorSpan;
    }

    @Override
    public ListingAdapterSfw setList(List<BaseModelInterface> list) {
        super.setList(list);
        sort(comparator);
        return this;
    }

    /**
     *
     * @param list  Same as {@link #setList(List)}  Only for casting
     * @return
     */
    public ListingAdapterSfw setModels(List<? extends BaseModelInterface> list) {
        return setList((List<BaseModelInterface>) list);
    }

    public void setList(List<? extends BaseModelInterface> list, String searchString) {
        if (list == null)
            return;
        this.fullModels.clear();
        this.fullModels.addAll(list);
        if (TextUtils.isEmpty(searchString)) {
            searchString = "";
            super.setList((List<BaseModelInterface>) list);
        } else
            filter(searchString);
    }

    public ListingAdapterSfw addList(List<BaseModelInterface> list, boolean isForFiltering) {
        if (!isForFiltering)
            super.addList(list);
        if (isForFiltering && list != null) {
            for (BaseModelInterface model : models) {
                if (!fullModels.contains(model)) {
                    fullModels.add(model);
                }
            }
            filter(searchString);
        }
        return this;
    }


    public void filter(String searchString) {
        this.searchString = searchString == null ? "" : searchString.toLowerCase(Locale.ENGLISH);
        clear();
        for (int i = 0, j = fullModels.size(); i < j; i++) {
            try {
                if (fullModels.get(i).getTitle().toLowerCase(Locale.ENGLISH).contains(this.searchString)) {
                    add(fullModels.get(i));
                }
            } catch (Exception ignored) {
            }
        }
        sort(comparator);
    }

    /**
     *
     * @param layout  should contain a TextView with id "txtTitle"  and the layout outer view id should be "layoutAdaMain"
     */
    public void setLayout(@LayoutRes int layout) {
        this.layout = layout;
        notifyDataSetChanged();
    }

    @Override
    public ViewHolder createHolder(@NonNull ViewGroup parent, int viewType) {
        if (colorSpan == null) {
            colorSpan = new ForegroundColorSpan(ContextCompat.getColor(parent.getContext(), R.color.colorAccent));
        }
        return new ViewHolder(LayoutInflater.from(parent.getContext()).inflate(layout, parent, false));
    }

    @Override
    public void bindHolder(@NonNull ViewHolder holder, int position) {
        try {
            if(textColor!=0)
                holder.txtTitle.setTextColor(textColor);
            searchIndex = searchString == null ? -1 : model.getTitle().toLowerCase(Locale.ENGLISH).indexOf(searchString);
            if (searchIndex > -1 && searchString != null && !searchString.isEmpty()) {
                try {
                    Spannable spannableString = new SpannableString(model.getTitle());
                    spannableString.setSpan(colorSpan, searchIndex, searchIndex + searchString.length(), Spanned.SPAN_INCLUSIVE_EXCLUSIVE);
                    holder.txtTitle.setText(spannableString);
                } catch (Exception e) {
                    holder.txtTitle.setText(model.getTitle());
                }
            } else {
                holder.txtTitle.setText(model.getTitle());
            }
        } catch (Exception ignored) {
            Sfw.log(TAG,"bindHolder",ignored,true);

        }
    }


    protected class ViewHolder extends SfwCommonBaseAdapter.ViewHolderBase<BaseModelInterface> {
        @BindView(idName = "txtTitle")
        TextView txtTitle;
        @OnClick(idName = "layoutAdaMain")
        View layoutAdaMain;

        private ViewHolder(View itemView) {
            super(itemView);
            if (onLongClickListener != null) {
                layoutAdaMain.setOnLongClickListener(v -> {
                    if (onLongClickListener != null) {
                        try {
                            onLongClickListener.onViewClick(v, getAdapterPosition(), getModel());
                        } catch (Exception ignored) {
                        }
                    }
                    return false;
                });
            }
        }

        @Override
        public void onClick(View v) {
            try {
                if (listener != null)
                    listener.onViewClick(v, getAdapterPosition(), getModel());
            } catch (Exception ignored) {
            }
        }
    }

    private List<BaseModelInterface> fullModels = new ArrayList<>();
    private OnAdapterListener<BaseModelInterface> onLongClickListener;
    private String searchString;
    private Comparator<BaseModelInterface> comparator;
    private int searchIndex;
    private ForegroundColorSpan colorSpan;
    private int layout = R.layout.sfw_ada_listing;
    @ColorInt private int textColor=0;
    private static final String TAG="LISTING_ADAPTER";
}
