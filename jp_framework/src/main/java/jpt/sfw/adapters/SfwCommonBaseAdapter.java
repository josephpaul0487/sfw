package jpt.sfw.adapters;

import android.content.Context;
import android.graphics.PointF;
import android.os.Handler;
import android.os.Looper;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import jpt.sfw.app.Sfw;
import jpt.sfw.interfaces.OnAdapterListener;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.LinearSmoothScroller;
import androidx.recyclerview.widget.RecyclerView;

//NOT COMPLETED
//1   add(T model,int position)
//2   getCorrectPosition()
public abstract class SfwCommonBaseAdapter<T, VH extends SfwCommonBaseAdapter.ViewHolderBase<T>> extends RecyclerView.Adapter<VH> {


    public SfwCommonBaseAdapter() {

    }

    public SfwCommonBaseAdapter(List<T> list) {
        if (list != null)
            this.models.addAll(list);
    }

    public OnAdapterListener<T> getListener() {
        return listener;
    }

    public SfwCommonBaseAdapter<T, VH> setListener(OnAdapterListener<T> listener) {
        this.listener = listener;
        return this;
    }

    public SfwCommonBaseAdapter<T, VH> setList(List<T> list) {
        if (list != null) {
            if(list.size()<20) {
                try {
                    this.models.clear();
                    this.models.addAll(list);
                    notifyThis();
                } catch (Exception ignored){}
                return this;
            }
            new Thread(() -> {
                this.models.clear();
                this.models.addAll(list);
                notifyThis();
            }
            ).start();
        }
        return this;
    }

    public SfwCommonBaseAdapter<T, VH> addList(List<T> list) {
        return addList(list, true);
    }
    public SfwCommonBaseAdapter<T, VH> addList(List<T> list, boolean removeDuplicate) {
        return  addList(list,removeDuplicate,true);
    }

    public SfwCommonBaseAdapter<T, VH> addList(List<T> list, boolean removeDuplicate,boolean notifyEachData) {
        if (list != null && !list.isEmpty()) {
            if(!removeDuplicate && list.size()<20) {
                try {
                    int currentSize = models.size();
                    SfwCommonBaseAdapter.this.models.addAll(list);
                    onItemRangeInserted(currentSize, list.size());
                } catch (Exception ignored){}
                return this;
            }
            new Thread(() -> {
                try {
                    if (removeDuplicate) {
                        for (T model : list)
                            add(model,-1,removeDuplicate,notifyEachData);
                    } else {
                        int currentSize = models.size();
                        SfwCommonBaseAdapter.this.models.addAll(list);
                        onItemRangeInserted(currentSize, list.size());
                    }
                } catch (Exception e) {
                    Sfw.log(TAG, "addList", e, true);
                }
            }).start();

        }
        return this;
    }

    public void add(T model) {
        add(model, -1);
    }

    public void add(T model, boolean replace) {
        add(model, -1, replace);
    }


    public void add(T model, int position) {
        add(model, position, true);
    }

    public void add(T model, int position, boolean replace) {
        add(model,position,replace,true);
    }

    public void add(T model, int position, boolean replace,boolean notify) {
        if (model != null) {
            try {
                int index = models.indexOf(model);
                if (index > -1) {
                    if (replace) {
                        models.set(index, model);
                        if(notify) {
                            onItemChanged(index);
                        }
                    }
                } else {
                    if (position < 0) {
                        models.add(model);
                        if(notify) {
                            onItemInserted(models.size() - 1);
                        }
                    } else {
                        models.add(position, model);
                        if(notify) {
                            onItemInserted(position);
                        }
                    }
                }
            } catch (Exception e) {
                Sfw.log(TAG, "add", e, true);
            }
        }
    }

    public void set(T model, int position) {
        if (position > -1 && position < getItemCount()) {
            models.set(position, model);
            onItemChanged(position);
        }
    }

    public void remove(int position) {
        if (position > -1 && position < models.size()) {
            models.remove(position);
            onItemRemoved(position);
        }
    }

    public void remove(T model) {
        remove(models.indexOf(model));
    }

    public void removeAll(List<T> models) {
        if (models != null)
            for (T model : models)
                remove(this.models.indexOf(model));
    }


    public void clear() {
        this.models.clear();
        notifyThis();
    }

    public int getItemIndex(T model) {
        return models.indexOf(model);
    }

    @Nullable
    public T getItem(int position) {
        if (position > -1 && position < models.size()) {
            return models.get(position);
        }
        return null;
    }

    @NonNull
    @Override
    public final VH onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        if (ctx == null) {
            ctx = parent.getContext();
            inflater = LayoutInflater.from(ctx);
        }
        return createHolder(parent, viewType);
    }

    public abstract VH createHolder(@NonNull ViewGroup parent, int viewType);

    public abstract void bindHolder(@NonNull VH holder, int position);



    @Override
    public final void onBindViewHolder(@NonNull VH holder, int position) {
        model = getItem(position);
        if (setModelToEachHolder)
            holder.setModel(model);
        bindHolder(holder, position);
    }

    @Override
    public int getItemCount() {
        return models.size();
    }

    public <E extends T> List<E> getModels() {
        return new ArrayList<>((List<E>) models);
    }


    public void sort(@NonNull Comparator<T> comparator) {
        Collections.sort(models, comparator);
        notifyThis();

    }

    public Context getContext() {
        return ctx;
    }

    public LayoutInflater getInflater() {
        return inflater;
    }

    public SfwCommonBaseAdapter<T, VH> setContext(Context ctx) {
        this.ctx = ctx;
        return this;
    }

    public boolean isSetModelToEachHolder() {
        return setModelToEachHolder;
    }

    public SfwCommonBaseAdapter<T, VH> setSetModelToEachHolder(boolean setModelToEachHolder) {
        this.setModelToEachHolder = setModelToEachHolder;
        return this;
    }

    public static class ViewHolderBase<T> extends RecyclerView.ViewHolder implements View.OnClickListener {
        T model;

        public ViewHolderBase(@NonNull View itemView) {
            super(itemView);
            Sfw.bindViews(this, itemView, this);
        }

        @Override
        public void onClick(View v) {

        }

        public T getModel() {
            return model;
        }

        public ViewHolderBase<T> setModel(T model) {
            this.model = model;
            return this;
        }
    }

    /**
     * You can override this to perform any custom action.
     */
    public void notifyThis() {
        try {
            if (Looper.getMainLooper().getThread() == Thread.currentThread())
                notifyDataSetChanged();
            else
                new Handler(Looper.getMainLooper()).post(() -> {
                    try {
                        notifyDataSetChanged();
                    } catch (Exception e) {
                        Sfw.log(TAG,"Notify All",e,true);
                    }
                });
        } catch (Exception e){
            Sfw.log(TAG,"Notify All",e,true);
        }

    }

    public void onItemChanged(int position) {
        this.onItemRangeChanged(position,1,null);
    }

    public void onItemChanged(int position,Object payload) {
        this.onItemRangeChanged(position,1,payload);
    }
    public void onItemRangeChanged(int positionStart,int count) {
        this.notifyItemRangeChanged(positionStart,count,null);
    }

    public void onItemRangeChanged(int position,int count,Object payload) {
        try {
            if (Looper.getMainLooper().getThread() == Thread.currentThread())
                notifyItemRangeChanged(position,count,payload);
            else
                new Handler(Looper.getMainLooper()).post(() -> {
                    try {
                        notifyItemRangeChanged(position,count,payload);
                    } catch (Exception e) {
                        Sfw.log(TAG,"onItemRangeChanged : position = "+position+"  count = "+count+"  payload is "+(payload==null?"NULL":"NOT NULL"),e,true);
                    }
                });
        } catch (Exception e){
            Sfw.log(TAG,"onItemRangeChanged : position = "+position+"  count = "+count+"  payload is "+(payload==null?"NULL":"NOT NULL"),e,true);
        }
    }

    public void onItemInserted(int toPosition) {
        try {
            if (Looper.getMainLooper().getThread() == Thread.currentThread())
                notifyItemInserted(toPosition);
            else
                new Handler(Looper.getMainLooper()).post(() -> {
                    try {
                        notifyItemInserted(toPosition);
                    } catch (Exception e) {
                        Sfw.log(TAG,"onItemInserted : toPosition = "+toPosition,e,true);
                    }
                });
        } catch (Exception e){
            Sfw.log(TAG,"onItemInserted : toPosition = "+toPosition,e,true);
        }
    }

    public void onItemRangeInserted(int positionStart,int count) {
        try {
            if (Looper.getMainLooper().getThread() == Thread.currentThread())
                notifyItemRangeInserted(positionStart, count);
            else
                new Handler(Looper.getMainLooper()).post(() -> {
                    try {
                        notifyItemRangeInserted(positionStart, count);
                    } catch (Exception e) {
                        Sfw.log(TAG,"onItemRangeInserted : positionStart = "+positionStart+"   count = "+count,e,true);
                    }
                });
        } catch (Exception e){
            Sfw.log(TAG,"onItemRangeInserted : positionStart = "+positionStart+"   count = "+count,e,true);
        }

    }

    public void onItemRemoved(int position) {
        onItemRemoved(position,1);
    }

    public void onItemRemoved(int positionStart,int count) {
        try {
            if (Looper.getMainLooper().getThread() == Thread.currentThread())
                notifyItemRangeRemoved(positionStart, count);
            else
                new Handler(Looper.getMainLooper()).post(() -> {
                    try {
                        notifyItemRangeRemoved(positionStart, count);
                    } catch (Exception e) {
                        Sfw.log(TAG,"onItemRemoved : positionStart = "+positionStart+"   count = "+count,e,true);
                    }
                });
        } catch (Exception e){
            Sfw.log(TAG,"onItemRemoved : positionStart = "+positionStart+"   count = "+count,e,true);
        }

    }

    public void onItemMoved(int fromPosition,int toPosition) {
        try {
            if (Looper.getMainLooper().getThread() == Thread.currentThread())
                notifyItemMoved(fromPosition, toPosition);
            else
                new Handler(Looper.getMainLooper()).post(() -> {
                    try {
                        notifyItemMoved(fromPosition, toPosition);
                    } catch (Exception e) {
                        Sfw.log(TAG,"onItemMoved : fromPosition = "+fromPosition+"   toPosition = "+toPosition,e,true);
                    }
                });
        } catch (Exception e){
            Sfw.log(TAG,"onItemMoved : fromPosition = "+fromPosition+"   toPosition = "+toPosition,e,true);
        }

    }




    protected final List<T> models = new ArrayList<>();
    protected T model;
    protected Context ctx;
    private LayoutInflater inflater;
    protected OnAdapterListener<T> listener;
    private boolean setModelToEachHolder = true;
    private static final String TAG = "SfwCommonBaseAdapter";

}
