package jpt.sfw.adapters;

import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.ColorInt;
import androidx.annotation.NonNull;
import androidx.core.content.ContextCompat;


import java.util.List;

import jpt.sfw.R;
import jpt.sfw.annotations.BindView;
import jpt.sfw.annotations.Nullable;
import jpt.sfw.annotations.OnClick;
import jpt.sfw.app.Utilities;
import jpt.sfw.models.ValuesWithImageModel;

public class BottomMenuAdapter extends SfwCommonBaseAdapter<ValuesWithImageModel, BottomMenuAdapter.ViewHolder> {

    public BottomMenuAdapter() {
    }

    public BottomMenuAdapter(List<ValuesWithImageModel> list) {
        super(list);
    }

    public void setSelectedPosition(int selectedPosition) {
        this.selectedPosition = selectedPosition;
        notifyDataSetChanged();
    }

    public int getSelectedPosition() {
        return selectedPosition;
    }

    public void setSelectionColors(@ColorInt int selectedColor, @ColorInt int unSelectedColor) {
        this.selectedColor = selectedColor;
        this.unselectedColor = unSelectedColor;
        this.isSelectedColorsSetByMethod = true;
        notifyDataSetChanged();
    }

    public void hideText(boolean hide) {
        this.hideTextView = hide;
        notifyDataSetChanged();
    }

    @NonNull
    @Override
    public ViewHolder createHolder(@NonNull ViewGroup parent, int viewType) {
        if (!isSelectedColorsSetByMethod && selectedColor == 0) {
            selectedColor = ContextCompat.getColor(getContext(), R.color.bottomMenuSelected);
            unselectedColor = ContextCompat.getColor(getContext(), R.color.bottomMenuUnSelected);
        }
        return new ViewHolder(getInflater().inflate(R.layout.sfw_ada_bottom_menu, parent, false));
    }

    @Override
    public void bindHolder(@NonNull ViewHolder holder, int position) {
        holder.txtTitle.setText(model.getTitle());
        holder.image.setImageResource(model.getImageId());
        holder.txtTitle.setTextColor(selectedPosition == position ? selectedColor : unselectedColor);
        Utilities.setImageViewTint(selectedPosition == position ? selectedColor : unselectedColor, holder.image);
        holder.txtTitle.setVisibility(hideTextView?View.GONE:View.VISIBLE);
    }


    @OnClick(idName = "layoutAdaMain")
    class ViewHolder extends SfwCommonBaseAdapter.ViewHolderBase<ValuesWithImageModel> {

        @BindView(idName = "image")
        ImageView image;
        @Nullable
        @BindView(idName = "txtTitle")
        TextView txtTitle;

        ViewHolder(@NonNull View itemView) {
            super(itemView);
        }

        @Override
        public void onClick(View v) {
            if (getListener() != null)
                getListener().onViewClick(v, getAdapterPosition(), getItem(getAdapterPosition()));
        }
    }

    private int selectedPosition = -1;
    private int selectedColor, unselectedColor;
    private boolean isSelectedColorsSetByMethod;
    boolean hideTextView;
}
