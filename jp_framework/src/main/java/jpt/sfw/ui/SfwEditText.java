package jpt.sfw.ui;

import android.content.Context;
import android.text.TextWatcher;
import android.util.AttributeSet;

import java.util.ArrayList;
import java.util.List;

import androidx.annotation.Nullable;
import androidx.appcompat.widget.AppCompatEditText;

import static jpt.sfw.app.Sfw.setDrawableTint;
import static jpt.sfw.app.Sfw.setMandatoryHint;


public class SfwEditText extends AppCompatEditText {
    public SfwEditText(Context context) {
        super(context);
        init(null);
    }

    public SfwEditText(Context context, @Nullable AttributeSet attrs) {
        super(context, attrs);
        init(attrs);
    }

    public SfwEditText(Context context, @Nullable AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        init(attrs);
    }

    private void init(AttributeSet attrs) {
//        setFont(this,attrs);
        setDrawableTint(this, attrs);
        setMandatoryHint(this, attrs);
    }

    @Override
    public void addTextChangedListener(TextWatcher watcher) {
        if (!(watcher instanceof SfwCommonTextWatcher)) {
            super.addTextChangedListener(watcher);
            normalTextWatchers.add(watcher);
//            throw new RuntimeException("Use " + SfwCommonTextWatcher.class.getName() + " instead.");
        } else
            addTextChangedListener((SfwCommonTextWatcher) watcher);
    }

    public void addTextChangedListener(SfwCommonTextWatcher watcher) {
        super.addTextChangedListener(watcher);
        textWatchers.add(watcher);
    }

    public void removeAllTextChangedListeners() {
        for (TextWatcher textWatcher : textWatchers) {
            removeTextChangedListener(textWatcher);
        }
        for (TextWatcher textWatcher : normalTextWatchers) {
            removeTextChangedListener(textWatcher);
        }
        textWatchers.clear();
        normalTextWatchers.clear();
    }

    public List<SfwCommonTextWatcher> getTextWatchers() {
        return textWatchers;
    }

    public List<TextWatcher> getNormalTextWatchers() {
        return normalTextWatchers;
    }

    private List<SfwCommonTextWatcher> textWatchers = new ArrayList<>();
    private List<TextWatcher> normalTextWatchers = new ArrayList<>();

}
