package jpt.sfw.ui;

import android.graphics.Rect;
import android.view.View;

import androidx.recyclerview.widget.RecyclerView;

public class ItemOffsetDecoration extends RecyclerView.ItemDecoration {
    private int top,bottom,left,right;

    public ItemOffsetDecoration(int margin) {
        this(margin,margin,margin,margin);
    }

    public ItemOffsetDecoration(int top, int bottom, int left, int right) {
        this.top = top;
        this.bottom = bottom;
        this.left = left;
        this.right = right;
    }

    @Override
    public void getItemOffsets(Rect outRect, View view,
                               RecyclerView parent, RecyclerView.State state) {

        // Add padding only to the zeroth item
        if (parent.getChildAdapterPosition(view) == 0) {

            outRect.right = right;
            outRect.left = left;
            outRect.top = top;
            outRect.bottom = bottom;
        }
    }
}

