package jpt.sfw.ui;

import android.content.Context;
import android.util.AttributeSet;

import jpt.sfw.app.Sfw;

import androidx.annotation.Nullable;
import androidx.appcompat.widget.AppCompatButton;


public class SfwButtonView extends AppCompatButton {
    public SfwButtonView(Context context) {
        super(context);
        init(null);
    }

    public SfwButtonView(Context context, @Nullable AttributeSet attrs) {
        super(context, attrs);
        init(attrs);
    }

    public SfwButtonView(Context context, @Nullable AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        init(attrs);
    }

    private void init(AttributeSet attrs) {
        isAllCaps = Sfw.isAllCaps(attrs, getContext());
        setAllCaps(isAllCaps);
    }

   /* @Override
    public void setText(CharSequence text, BufferType type) {
        super.setText(isAllCaps && text != null ? text.toString().toUpperCase() : text == null ? "" : text, type);
    }*/

    @Override
    public boolean isAllCaps() {
        return isAllCaps;
    }

    boolean isAllCaps;

}
