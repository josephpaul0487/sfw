package jpt.sfw.ui;

import android.content.Context;
import android.content.res.ColorStateList;
import android.graphics.drawable.Drawable;
import android.os.Build;
import android.util.AttributeSet;
import android.view.ViewGroup;
import android.widget.FrameLayout;
import android.widget.ProgressBar;

import androidx.annotation.ColorInt;
import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.annotation.RequiresApi;

import jpt.sfw.R;
import jpt.sfw.annotations.BindView;
import jpt.sfw.app.Sfw;
import jpt.sfw.app.Utilities;

public class ProgressLayout extends FrameLayout {
    @jpt.sfw.annotations.Nullable
    @BindView(idName = "progressBar")
    ProgressBar progressBar;

    public ProgressLayout(@NonNull Context context) {
        super(context);
        init();
    }

    public ProgressLayout(@NonNull Context context, @Nullable AttributeSet attrs) {
        super(context, attrs);
        init();
    }

    public ProgressLayout(@NonNull Context context, @Nullable AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        init();
    }

    @RequiresApi(api = Build.VERSION_CODES.LOLLIPOP)
    public ProgressLayout(@NonNull Context context, @Nullable AttributeSet attrs, int defStyleAttr, int defStyleRes) {
        super(context, attrs, defStyleAttr, defStyleRes);
        init();
    }

    private void init() {
        Sfw.bindViews(this,this,null);
    }

    public void setProgressColor(@ColorInt int color) {
        if(progressBar!=null)
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
                progressBar.setIndeterminateTintList(ColorStateList.valueOf(color));
            } else {
                Drawable drawable=progressBar.getIndeterminateDrawable();
                Utilities.setDrawableTint(color,drawable);
            }
    }

    public void setProgressWH(int width) {
        if(progressBar!=null) {
            ViewGroup.LayoutParams lp=progressBar.getLayoutParams();
            lp.height=width;
            lp.width=width;
            progressBar.setLayoutParams(lp);
        }
    }
}
