package jpt.sfw.ui;

import android.content.Context;
import android.util.AttributeSet;

import jpt.sfw.app.Sfw;

import androidx.annotation.Nullable;
import androidx.appcompat.widget.AppCompatTextView;

import static jpt.sfw.app.Sfw.setDrawableTint;
import static jpt.sfw.app.Sfw.setMandatoryHint;

public class SfwTextView extends AppCompatTextView {
    public SfwTextView(Context context) {
        super(context);
        init(null);
    }

    public SfwTextView(Context context, @Nullable AttributeSet attrs) {
        super(context, attrs);
        init(attrs);
    }

    public SfwTextView(Context context, @Nullable AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        init(attrs);
    }


    private void init(AttributeSet attrs) {
        try {
            isCallCaps= Sfw.isAllCaps(attrs,getContext());
//            if(getId()== R.id.txtTitle)
//            setFont(this,attrs);
            setDrawableTint(this,attrs);
            setMandatoryHint(this,attrs);
            this.attrs=attrs;
        } catch (Exception e) {
        }
    }


    @Override
    public void setCompoundDrawablesRelativeWithIntrinsicBounds(int start, int top, int end, int bottom) {
        super.setCompoundDrawablesRelativeWithIntrinsicBounds(start, top, end, bottom);

        setDrawableTint(this,attrs);
    }



    AttributeSet attrs;
    private boolean isCallCaps;
}

