package jpt.sfw.ui;

import android.text.Editable;
import android.text.TextWatcher;

import java.lang.ref.SoftReference;

import androidx.annotation.NonNull;

public class SfwCommonTextWatcher implements TextWatcher {
    boolean textChanged;
    private SoftReference<SfwEditText> editText;

    public SfwCommonTextWatcher(@NonNull SfwEditText editText) {
        this.editText=new SoftReference<>(editText);
    }


    @Override
    public void beforeTextChanged(CharSequence s, int start, int count, int after) {

    }

    @Override
    public void onTextChanged(CharSequence s, int start, int before, int count) {

    }

    @Override
    public final void afterTextChanged(Editable s) {
        if(!textChanged) {
            try {
                textChanged(s);
            } catch (Exception ignored) {
            }
        } else {
            textChanged=false;
        }
    }

    public void textChanged(Editable s) {

    }

    public boolean isTextChanged() {
        return textChanged;
    }

    public void setTextChanged(boolean textChanged) {
        this.textChanged = textChanged;
        try {
            for (SfwCommonTextWatcher watcher:editText.get().getTextWatchers()) {
                if(watcher!=this) {
                    watcher.notifyTextChanged(textChanged);
                }
            }
        } catch (Exception ignored){}
    }

    private void notifyTextChanged(boolean textChanged) {
        this.textChanged=textChanged;
    }
}
