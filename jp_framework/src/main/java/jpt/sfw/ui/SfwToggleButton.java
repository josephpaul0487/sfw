package jpt.sfw.ui;

import android.content.Context;
import android.util.AttributeSet;

import jpt.sfw.app.Sfw;

import androidx.appcompat.widget.SwitchCompat;


public class SfwToggleButton extends SwitchCompat {



    public SfwToggleButton(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        init(attrs);
    }

    public SfwToggleButton(Context context, AttributeSet attrs) {
        super(context, attrs);
        init(attrs);
    }

    public SfwToggleButton(Context context) {
        super(context);
        init(null);
    }

    private void init(AttributeSet attrs) {
        Sfw.setFont(this,attrs);
    }
}
