package jpt.sfw.ui;

import android.content.Context;
import android.text.TextUtils;
import android.text.TextWatcher;
import android.util.AttributeSet;
import android.widget.EditText;

import com.google.android.material.textfield.TextInputLayout;
import jpt.sfw.app.Sfw;
import jpt.sfw.app.Utilities;

import java.util.ArrayList;
import java.util.List;

import androidx.annotation.Nullable;

public class SfwTextInputLayout extends TextInputLayout {
    public SfwTextInputLayout(Context context) {
        super(context);
        init(null);
    }

    public SfwTextInputLayout(Context context, AttributeSet attrs) {
        super(context, attrs);
        init(attrs);
    }

    public SfwTextInputLayout(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        init(attrs);
    }

    private void init(AttributeSet attrs) {
        isMandatory = Sfw.isMandatoryHint(attrs, getContext());
        setHint(getHint());
    }

    public void setMandatory(boolean isMandatory) {
        if (this.isMandatory != isMandatory) {
            String hint = getHint() != null ? String.valueOf(getHint()) : "";
            if (!isMandatory && !TextUtils.isEmpty(hint) && hint.endsWith("*"))
                hint = hint.substring(0, hint.length() - 1);
            this.isMandatory = isMandatory;
            setHint(hint);
        }
    }

    @Override
    public void setHint(@Nullable CharSequence hint) {
        if (isMandatory) {
            if (!TextUtils.isEmpty(hint)) {
                super.setHint(Utilities.getMandatoryString((String) hint));
            }
        } else
            super.setHint(hint);
    }




    public void addTextChangedListener(TextWatcher watcher) {
        if(getEditText()!=null) {
            getEditText().addTextChangedListener(watcher);
            if(!(getEditText() instanceof SfwEditText))
            textWatchers.add(watcher);
        }
    }

    public void removeAllTextChangedListeners() {
        EditText editText=getEditText();
        if(editText instanceof SfwEditText) {
            ((SfwEditText) editText).removeAllTextChangedListeners();
            return;
        }
        for (TextWatcher textWatcher:textWatchers) {
            editText.removeTextChangedListener(textWatcher);
        }
        textWatchers.clear();
    }

    public int length() {
        return getEditText()==null?0:getEditText().length();
    }

    public String getText() {
        return getEditText()==null?"":getEditText().getText().toString();
    }

    public void setText(CharSequence text) {
        if(getEditText()!=null) {
            getEditText().setText(text);
            getEditText().setSelection(getEditText().length());
        }
    }




    private List<TextWatcher> textWatchers=new ArrayList<>();
    boolean isMandatory;


}
