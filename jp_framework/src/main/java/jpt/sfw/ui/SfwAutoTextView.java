package jpt.sfw.ui;

import android.content.Context;
import android.graphics.Rect;
import android.util.AttributeSet;

import androidx.annotation.Nullable;
import androidx.appcompat.widget.AppCompatAutoCompleteTextView;

import static jpt.sfw.app.Sfw.setDrawableTint;
import static jpt.sfw.app.Sfw.setMandatoryHint;


public class SfwAutoTextView extends AppCompatAutoCompleteTextView {
    public SfwAutoTextView(Context context) {
        super(context);
        init(null);
    }

    public SfwAutoTextView(Context context, @Nullable AttributeSet attrs) {
        super(context, attrs);
        init(attrs);
    }

    public SfwAutoTextView(Context context, @Nullable AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        init(attrs);
    }

    private void init(AttributeSet attrs) {
        try {
//            setFont(this,attrs);
            setDrawableTint(this,attrs);
            setMandatoryHint(this,attrs);
        } catch (Exception e) {
        }
    }

    @Override
    public boolean enoughToFilter() {
        return true;
    }

    @Override
    protected void onFocusChanged(boolean focused, int direction,
                                  Rect previouslyFocusedRect) {
        super.onFocusChanged(focused, direction, previouslyFocusedRect);
        if (focused) {
            try {
                performFiltering(getText(), 0);
            } catch (Exception e) {
            }
        }
    }



}
