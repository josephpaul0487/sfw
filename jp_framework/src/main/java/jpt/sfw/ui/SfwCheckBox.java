package jpt.sfw.ui;

import android.content.Context;
import android.util.AttributeSet;

import androidx.annotation.Nullable;
import androidx.appcompat.widget.AppCompatCheckBox;

import static jpt.sfw.app.Sfw.setFont;


public class SfwCheckBox extends AppCompatCheckBox {
    public SfwCheckBox(Context context) {
        super(context);
        init(null);
    }

    public SfwCheckBox(Context context, @Nullable AttributeSet attrs) {
        super(context, attrs);
        init(attrs);
    }

    public SfwCheckBox(Context context, @Nullable AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        init(attrs);
    }

    private void init(AttributeSet attrs) {
        setFont(this,attrs);
    }
}
