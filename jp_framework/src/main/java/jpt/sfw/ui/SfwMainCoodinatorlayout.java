package jpt.sfw.ui;

import android.content.Context;
import android.util.AttributeSet;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.coordinatorlayout.widget.CoordinatorLayout;

public class SfwMainCoodinatorlayout extends CoordinatorLayout {
    public SfwMainCoodinatorlayout(@NonNull Context context) {
        super(context);
    }

    public SfwMainCoodinatorlayout(@NonNull Context context, @Nullable AttributeSet attrs) {
        super(context, attrs);
    }

    public SfwMainCoodinatorlayout(@NonNull Context context, @Nullable AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
    }



    public float getXFraction() {
        return transactions.getXFraction(this);
    }


    public void setYFraction(float fraction) {
        transactions.setYFraction(fraction, this);
    }

    public void setXFraction(float fraction) {
        transactions.setXFraction(fraction, this);
    }

    public void setGlide(float fraction) {
        transactions.setGlide(fraction, this);
    }

    public void setGlideBack(float fraction) {
        transactions.setGlideBack(fraction, this);
    }

    public void setCube(float fraction) {
        transactions.setCube(fraction, this);
    }

    public void setCubeBack(float fraction) {
        transactions.setCubeBack(fraction, this);
    }

    public void setRotateDown(float fraction) {
        transactions.setRotateDown(fraction, this);
    }

    public void setRotateUp(float fraction) {
        transactions.setRotateUp(fraction, this);
    }

    public void setAccordionPivotZero(float fraction) {
        transactions.setAccordionPivotZero(fraction, this);
    }

    public void setAccordionPivotWidth(float fraction) {
        transactions.setAccordionPivotWidth(fraction, this);
    }

    public void setTableHorizontalPivotZero(float fraction) {
        transactions.setTableHorizontalPivotZero(fraction, this);
    }

    public void setTableHorizontalPivotWidth(float fraction) {
        transactions.setTableHorizontalPivotWidth(fraction, this);
    }

    public void setTableVerticalPivotZero(float fraction) {
        transactions.setTableVerticalPivotZero(fraction, this);
    }

    public void setTableVerticalPivotHeight(float fraction) {
        transactions.setTableVerticalPivotHeight(fraction, this);
    }

    public void setZoomFromCornerPivotHG(float fraction) {
        transactions.setZoomFromCornerPivotHG(fraction, this);
    }

    public void setZoomFromCornerPivotZero(float fraction) {
        transactions.setZoomFromCornerPivotZero(fraction, this);
    }

    public void setZoomFromCornerPivotWidth(float fraction) {
        transactions.setZoomFromCornerPivotWidth(fraction, this);
    }

    public void setZoomFromCornerPivotHeight(float fraction) {
        transactions.setZoomFromCornerPivotHeight(fraction, this);
    }

    public void setZoomSlideHorizontal(float fraction) {
        transactions.setZoomSlideHorizontal(fraction, this);
    }

    public void setZoomSlideVertical(float fraction) {
        transactions.setZoomSlideVertical(fraction, this);
    }

    private FragmentTransactions transactions = new FragmentTransactions();
}
