package jpt.sfw.ui;

import android.view.View;
import android.view.ViewTreeObserver;

public class FragmentTransactions {

    private float yFraction = 0;
    private float xFraction = 0;
    private ViewTreeObserver.OnPreDrawListener preDrawListener = null;

    public float getXFraction(View v) {
        try {
            return v.getX() / v.getWidth(); // TODO: guard divide-by-zero
        } catch (Exception e) {
        }
        return v.getX();
    }


    public void setYFraction(float fraction,View v) {
        this.yFraction = fraction;
        if (v.getHeight() == 0) {
            if (preDrawListener == null) {
                preDrawListener = () -> {
                    v.getViewTreeObserver().removeOnPreDrawListener(
                            preDrawListener);
                    setYFraction(yFraction,v);
                    return true;
                };
                v.getViewTreeObserver().addOnPreDrawListener(preDrawListener);
            }
            return;
        }
        float translationY = v.getHeight() * fraction;
        v.setTranslationY(translationY);
    }

    public void setXFraction(float fraction,View v) {
        this.xFraction = fraction;
        if (v.getWidth() == 0) {
            if (preDrawListener == null) {
                preDrawListener = new ViewTreeObserver.OnPreDrawListener() {
                    @Override
                    public boolean onPreDraw() {
                        v.getViewTreeObserver().removeOnPreDrawListener(
                                preDrawListener);
                        setXFraction(xFraction,v);
                        return true;
                    }
                };
                v.getViewTreeObserver().addOnPreDrawListener(preDrawListener);
            }
            return;
        }
        float translationX = v.getWidth() * fraction;
        v.setTranslationX(translationX);
    }

    public void setGlide(float fraction,View v) {
        float translationX = v.getWidth() * fraction;
        v.setTranslationX(translationX);
        v.setRotationY(90 * fraction);
        v.setPivotX(0);
    }

    public void setGlideBack(float fraction,View v) {
        float translationX = v.getWidth() * fraction;
        v.setTranslationX(translationX);
        v.setRotationY(90 * fraction);
        v.setPivotX(0);
        v.setPivotY(v.getHeight() / 2);
    }

    public void setCube(float fraction,View v) {
        float translationX = v.getWidth() * fraction;
        v.setTranslationX(translationX);
        v.setRotationY(90 * fraction);
        v.setPivotX(0);
        v.setPivotY(v.getHeight() / 2);
    }

    public void setCubeBack(float fraction,View v) {
        float translationX = v.getWidth() * fraction;
        v.setTranslationX(translationX);
        v.setRotationY(90 * fraction);
        v.setPivotY(v.getHeight() / 2);
        v.setPivotX(v.getWidth());
    }

    public void setRotateDown(float fraction,View v) {
        float translationX = v.getWidth() * fraction;
        v.setTranslationX(translationX);
        v.setRotation(20 * fraction);
        v.setPivotY(v.getHeight());
        v.setPivotX(v.getWidth() / 2);
    }

    public void setRotateUp(float fraction,View v) {
        float translationX = v.getWidth() * fraction;
        v.setTranslationX(translationX);
        v.setRotation(-20 * fraction);
        v.setPivotY(0);
        v.setPivotX(v.getWidth() / 2);
    }

    public void setAccordionPivotZero(float fraction,View v) {
        v.setScaleX(fraction);
        v.setPivotX(0);
    }

    public void setAccordionPivotWidth(float fraction,View v) {
        v.setScaleX(fraction);
        v.setPivotX(v.getWidth());
    }

    public void setTableHorizontalPivotZero(float fraction,View v) {
        v.setRotationY(90 * fraction);
        v.setPivotX(0);
        v.setPivotY(v.getHeight() / 2);
    }

    public void setTableHorizontalPivotWidth(float fraction,View v) {
        v.setRotationY(-90 * fraction);
        v.setPivotX(v.getWidth());
        v.setPivotY(v.getHeight() / 2);
    }

    public void setTableVerticalPivotZero(float fraction,View v) {
        v.setRotationX(-90 * fraction);
        v.setPivotX(v.getWidth() / 2);
        v.setPivotY(0);
    }

    public void setTableVerticalPivotHeight(float fraction,View v) {
        v.setRotationX(90 * fraction);
        v.setPivotX(v.getWidth() / 2);
        v.setPivotY(v.getHeight());
    }

    public void setZoomFromCornerPivotHG(float fraction,View v) {
        v.setScaleX(fraction);
        v.setScaleY(fraction);
        v.setPivotX(v.getWidth());
        v.setPivotY(v.getHeight());
    }

    public void setZoomFromCornerPivotZero(float fraction,View v) {
        v.setScaleX(fraction);
        v.setScaleY(fraction);
        v.setPivotX(0);
        v.setPivotY(0);
    }

    public void setZoomFromCornerPivotWidth(float fraction,View v) {
        v.setScaleX(fraction);
        v.setScaleY(fraction);
        v.setPivotX(v.getWidth());
        v.setPivotY(0);
    }

    public void setZoomFromCornerPivotHeight(float fraction,View v) {
        v.setScaleX(fraction);
        v.setScaleY(fraction);
        v.setPivotX(0);
        v.setPivotY(v.getHeight());
    }

    public void setZoomSlideHorizontal(float fraction,View v) {
        v.setTranslationX(v.getWidth() * fraction);
        v.setPivotX(v.getWidth() / 2);
        v.setPivotY(v.getHeight() / 2);
    }

    public void setZoomSlideVertical(float fraction,View v) {
        v.setTranslationY(v.getHeight() * fraction);
        v.setPivotX(v.getWidth() / 2);
        v.setPivotY(v.getHeight() / 2);
    }
}
