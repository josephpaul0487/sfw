package jpt.sfw.models;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import jpt.sfw.interfaces.SingleModelInterface;

public class CommonModel<T> extends StatusModel implements SingleModelInterface<T> {
    @SerializedName("data")
    @Expose
    T data;

    public T getData() {
        return data;
    }


}
