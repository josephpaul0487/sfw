package jpt.sfw.models;

import com.google.gson.annotations.SerializedName;

import java.util.ArrayList;
import java.util.List;

import jpt.sfw.interfaces.ListModelInterface;


public class CommonListModel<T> extends StatusModel implements ListModelInterface<T> {
    @SerializedName("data")
    List<T> data;

    public List<T> getData() {
        return data==null?new ArrayList<>():data;
    }


}
