package jpt.sfw.models;

import com.google.gson.annotations.SerializedName;
import jpt.sfw.annotations.DbPrimary;
import jpt.sfw.interfaces.BaseModelInterface;
import jpt.sfw.interfaces.HelperMethods;

import java.io.Serializable;
import java.util.Objects;

public class BaseModel implements Serializable,Cloneable, BaseModelInterface, HelperMethods {


    @DbPrimary
    @SerializedName("id")
    protected long id=0;
    @SerializedName("title")
    String mainTitle;
    @SerializedName("image")
    String mainImage;


    public BaseModel(long id, String title) {
        this.id = id;
        this.mainTitle = title;
    }

    public BaseModel() {
    }

    public long getId() {
        return id;
    }

    public String getMainImage() {
        return avoidNull(mainImage);
    }

    public String getTitle() {
        return avoidNull(mainTitle);
    }



    public BaseModel setTitle(String title) {
        this.mainTitle = title;
        return this;
    }


    public BaseModel setId(long id) {
        this.id =  (int) id;
        return this;
    }


    public BaseModel setMainImage(String image) {
        this.mainImage = image;
        return this;
    }

    public String getMainTitle() {
        return getTitle();
    }

    public BaseModel setMainTitle(String mainTitle) {
        this.mainTitle = mainTitle;
        return this;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        BaseModel baseModel = (BaseModel) o;
        return id == baseModel.id;
    }

    @Override
    public int hashCode() {
        return Objects.hash(id);
    }
}
