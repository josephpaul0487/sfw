package jpt.sfw.models;

import com.google.gson.annotations.SerializedName;

import java.io.Serializable;
import java.util.Objects;

import androidx.annotation.DrawableRes;
import androidx.annotation.Nullable;

import jpt.sfw.interfaces.HelperMethods;


public class ImageModel implements Serializable, HelperMethods {

    @SerializedName("image_path")
    String path;
    @SerializedName("id")
    long id;
    @SerializedName("title")
    String title;

    @DrawableRes int imageId;

    Object tag;

    public ImageModel(long id, String title, String path) {
        this.path = path;
        this.id = id;
        this.title = title;
    }

    public ImageModel(long id, String title,@DrawableRes int imageId) {
        this.imageId = imageId;
        this.id = id;
        this.title = title;
    }

    public String getPath() {
        return avoidNull(path);
    }


    public ImageModel setPath(String path) {
        this.path = path;
        return this;
    }

    public long getId() {
        return id;
    }

    public ImageModel setId(long id) {
        this.id = id;
        return this;
    }

    public String getTitle() {
        return avoidNull(title);
    }

    public ImageModel setTitle(String title) {
        this.title = title;
        return this;
    }

    public int getImageId() {
        return imageId;
    }

    public ImageModel setImageId(int imageId) {
        this.imageId = imageId;
        return this;
    }

    @Nullable
    public Object getTag() {
        return tag;
    }

    public ImageModel setTag(@Nullable Object tag) {
        this.tag = tag;
        return this;
    }


    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        ImageModel that = (ImageModel) o;
        return id == that.id &&
                imageId == that.imageId &&
                Objects.equals(path, that.path) &&
                Objects.equals(title, that.title);
    }

    @Override
    public int hashCode() {
        return Objects.hash(path, id, title, imageId);
    }
}
