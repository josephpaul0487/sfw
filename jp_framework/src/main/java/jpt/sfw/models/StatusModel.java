package jpt.sfw.models;

import android.text.TextUtils;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.List;

import androidx.annotation.Nullable;

import jpt.sfw.interfaces.HelperMethods;
import jpt.sfw.interfaces.StatusInterface;

public class StatusModel implements StatusInterface, HelperMethods {
    @SerializedName("status")
    @Expose
    int status;
    @SerializedName("msg")
    @Expose
    String msg;
    @SerializedName("image_url")
    @Expose
    String image;
    @SerializedName("errors")
    @Expose
    List<String> errors;
    @SerializedName("tag_code")
    int tagCode;


    public boolean isSuccess() {
        return status==1;
    }

    public int getStatus() {
        return status;
    }

    public String getMsg() {
        return msg==null ?errors==null?"": TextUtils.join("\n",errors):msg;
    }

    public String getImagePath() {
        return avoidNull(image);
    }

    public StatusModel setMsg(String msg) {
        this.msg = msg;
        return this;
    }

    @Nullable
    public List<String> getErrors() {
        return errors;
    }

    public StatusModel setErrors(List<String> errors) {
        this.errors = errors;
        return this;
    }

    public StatusModel setStatus(int status) {
        this.status = status;
        return this;
    }

    public String getImage() {
        return getImagePath();
    }

    public StatusModel setImage(String image) {
        this.image = image;
        return this;
    }

    public int getTagCode() {
        return tagCode;
    }

    public StatusModel setTagCode(int tagCode) {
        this.tagCode = tagCode;
        return this;
    }
}
