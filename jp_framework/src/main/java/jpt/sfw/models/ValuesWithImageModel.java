package jpt.sfw.models;

import androidx.annotation.DrawableRes;

import java.util.Objects;

public class ValuesWithImageModel {
    final int id;
    final String title;
    @DrawableRes final int imageId;

    public ValuesWithImageModel(int id, String title,@DrawableRes int imageId) {
        this.id = id;
        this.title = title;
        this.imageId = imageId;
    }

    public ValuesWithImageModel(int id,@DrawableRes int imageId) {
        this(id,"",imageId);
    }

    public String getTitle() {
        return title==null?"":title;
    }

    public int getImageId() {
        return imageId;
    }

    public int getId() {
        return id;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        ValuesWithImageModel that = (ValuesWithImageModel) o;
        if(id>0 || that.id>0)
            return id==that.id;
        return id == that.id &&
                imageId == that.imageId &&
                Objects.equals(title, that.title);
    }

    @Override
    public int hashCode() {
        if(id>0)
            return Objects.hash(id);
        return Objects.hash(id, title, imageId);
    }
}
