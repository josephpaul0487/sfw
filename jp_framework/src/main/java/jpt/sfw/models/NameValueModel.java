package jpt.sfw.models;

import jpt.sfw.R;
import jpt.sfw.interfaces.HelperMethods;

import java.util.Objects;

import androidx.annotation.DimenRes;
import androidx.annotation.NonNull;

public class NameValueModel implements HelperMethods {
    final String name,value;
    @DimenRes final int valueTextSize;

    public NameValueModel(@NonNull String name, @NonNull String value, @DimenRes int valueTextSize) {
        this.name = name;
        this.value = value;
        this.valueTextSize = valueTextSize;
    }

    public NameValueModel(@NonNull String name, @NonNull String value) {
        this.name = name;
        this.value = value;
        this.valueTextSize = R.dimen.ts_common;
    }

    public String getName() {
        return avoidNull(name);
    }

    public String getValue() {
        return avoidNull(value);
    }

    @DimenRes
    public int getValueTextSize() {
        return valueTextSize;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        NameValueModel that = (NameValueModel) o;
        return Objects.equals(name, that.name);
    }

    @Override
    public int hashCode() {

        return Objects.hash(name);
    }
}
