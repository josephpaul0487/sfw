package jpt.sfw.app;

import androidx.annotation.NonNull;

public class ToastConfig {
    private CharSequence message;
    private int duration;

    public ToastConfig() {
    }

    public ToastConfig(@NonNull CharSequence message, int duration) {
        this.message = message;
        this.duration = duration;
    }

    @NonNull
    public CharSequence getMessage() {
        return message==null?"":message;
    }

    public int getDuration() {
        return duration;
    }

}
