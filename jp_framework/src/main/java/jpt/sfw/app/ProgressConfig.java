package jpt.sfw.app;

import jpt.sfw.interfaces.OnAlertListener;

public class ProgressConfig {
    private CharSequence msg;
    private boolean cancelable;
    private int progressStyle;
    private OnAlertListener listener;
    int requestId;

    public ProgressConfig(CharSequence msg) {
        this(-1,msg,false,null);
    }

    public ProgressConfig(int requestId, CharSequence msg, boolean cancelable, OnAlertListener listener) {
        this(requestId,msg,cancelable,-1,listener);
    }

    public ProgressConfig(int requestId,CharSequence msg, boolean cancelable, int progressStyle,OnAlertListener listener) {
        this.msg = msg;
        this.cancelable = cancelable;
        this.progressStyle = progressStyle;
        this.listener=listener;
        this.requestId=requestId;
    }

    public CharSequence getMsg() {
        return msg==null?"":msg;
    }

    public boolean isCancelable() {
        return cancelable;
    }

    public int getProgressStyle() {
        return progressStyle;
    }

    public OnAlertListener getListener() {
        return listener;
    }

    public int getRequestId() {
        return requestId;
    }
}
