package jpt.sfw.app;

import android.content.ClipData;
import android.content.ClipboardManager;
import android.content.Context;
import android.os.Build;
import android.text.Html;
import android.text.SpannableString;
import android.text.Spanned;
import android.text.TextUtils;
import android.widget.EditText;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.TextView;

import com.google.android.material.textfield.TextInputLayout;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;

import static android.content.Context.CLIPBOARD_SERVICE;


public class Text {
    public static String getText(TextInputLayout layout){
        try {
            return layout.getEditText().getText().toString();
        } catch (Exception e) {
        }
        return "";
    }

    public static CharSequence checkIsNull(CharSequence str) {
        return str==null?"":str;
    }


    public static void setText(@Nullable CharSequence str, @NonNull TextInputLayout e) {
        setText(str,e,true);
    }

    public static void setText(@Nullable CharSequence str, @NonNull TextInputLayout e, boolean replaceText) {
        setTextTV(str,e.getEditText(),replaceText);
    }

    public static void setTextTV(@Nullable CharSequence str, @NonNull EditText e) {
        setTextTV(str,e,true);
    }
    public static void setTextTV(@Nullable CharSequence str, @NonNull EditText e,boolean replaceText) {
        if (replaceText || e.length()==0) {
            e.setText(checkIsNull(str));
            e.setSelection(e.length());
        }
    }

    public static String getText(RadioGroup group){
        try {
            return ((RadioButton)group.findViewById(group.getCheckedRadioButtonId())).getText().toString();
        } catch (Exception e) {
        }
        return "";
    }

    public static void setHtmlString(@Nullable TextView textView,@Nullable String content) {
        if(textView!=null) {
            textView.setText(getHtmlString(content));
            if(textView instanceof EditText)
                ((EditText) textView).setSelection(textView.length());
        }
    }

    public static Spanned getHtmlString(@Nullable String content) {
        try {
            if(!TextUtils.isEmpty(content)) {
                if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.N) {
                    return Html.fromHtml(content, Html.FROM_HTML_MODE_LEGACY);
                } else {
                    return Html.fromHtml(content);
                }
            }
        } catch (Exception ignored) {
        }
        return new SpannableString("");
    }

    public static void copyToClipboard(Context ctx, String label, String text) {
        try {
            ClipboardManager clipboard = (ClipboardManager) ctx.getSystemService(CLIPBOARD_SERVICE);
            ClipData clip = ClipData.newPlainText(label, text);
            clipboard.setPrimaryClip(clip);
        } catch (Exception e) {
        }
    }
}
