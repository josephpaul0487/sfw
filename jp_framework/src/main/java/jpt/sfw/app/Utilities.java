package jpt.sfw.app;

import android.Manifest;
import android.annotation.SuppressLint;
import android.app.Activity;
import android.app.ActivityManager;
import android.app.Dialog;
import android.app.DownloadManager;
import android.content.ClipData;
import android.content.ClipboardManager;
import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.content.pm.ResolveInfo;
import android.content.res.ColorStateList;
import android.content.res.Resources;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.PorterDuff;
import android.graphics.PorterDuffColorFilter;
import android.graphics.drawable.Drawable;
import android.graphics.drawable.LayerDrawable;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.net.Uri;
import android.os.Build;
import android.os.Environment;
import android.preference.PreferenceManager;
import android.provider.MediaStore;
import android.provider.Settings;
import android.text.Html;
import android.text.SpannableString;
import android.text.Spanned;
import android.text.TextUtils;
import android.util.DisplayMetrics;
import android.util.TypedValue;
import android.view.View;
import android.view.inputmethod.InputMethodManager;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import jpt.sfw.BuildConfig;
import jpt.sfw.R;
import jpt.sfw.appui.CountDrawable;

import org.apache.commons.io.FileUtils;

import java.io.BufferedInputStream;
import java.io.BufferedReader;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import java.util.Locale;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import java.util.zip.ZipEntry;
import java.util.zip.ZipInputStream;

import androidx.annotation.ColorInt;
import androidx.annotation.DimenRes;
import androidx.annotation.DrawableRes;
import androidx.annotation.IdRes;
import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.core.app.ActivityCompat;
import androidx.core.content.ContextCompat;
import androidx.core.graphics.drawable.DrawableCompat;

public class Utilities {
    public static int[] getScreenResolution(@NonNull Activity act) {
        DisplayMetrics displayMetrics = new DisplayMetrics();
        act.getWindowManager().getDefaultDisplay().getMetrics(displayMetrics);
        return new int[]{displayMetrics.widthPixels, displayMetrics.heightPixels};

    }

    public static void copyToClipBoard(String title, String content, Context ctx) {
        try {
            ClipboardManager clipboard = (ClipboardManager) ctx.getSystemService(Context.CLIPBOARD_SERVICE);
            ClipData clip = ClipData.newPlainText(title, content);
            if (clipboard != null) {
                clipboard.setPrimaryClip(clip);
            }
        } catch (Exception ignored) {
        }

    }

    public static String getDeviceDetails() {
        return "Device=" + Build.DEVICE + "<br>Manufacturer=" + Build.MANUFACTURER + "<br>model=" + Build.MODEL + "<br>brand=" + Build.BRAND;
    }

    public static String getDeviceId(Context ctx) {
        return Settings.Secure.getString(ctx.getContentResolver(),
                Settings.Secure.ANDROID_ID);
    }

    public static String loadFromAsset(@NonNull String path, @NonNull Context ctx) {
        try {
            return readFromStream(ctx.getAssets().open(path));
        } catch (Exception ignored) {
        }
        return "";
    }


    public static byte[] readFile(@NonNull File file, int length) {
        FileInputStream in = null;
        try {
            if (length > 0) {
                in = new FileInputStream(file);
                byte[] by = new byte[length];
                in.read(by);
                return by;
            } else {
                return FileUtils.readFileToByteArray(file);
            }
        } catch (Exception ignored) {
        } finally {
            if (in != null) {
                try {
                    in.close();
                } catch (Exception ignored) {
                }
            }
        }
        return new byte[0];
    }


    public static String readFromStream(InputStream stream) {
        try (BufferedReader reader = new BufferedReader(
                new InputStreamReader(stream, "UTF-8"))) {

            // do reading, usually loop until end of file reading
            StringBuilder builder = new StringBuilder();
            String mLine = reader.readLine();
            while (mLine != null) {
                //process line
                builder.append(mLine);
                mLine = reader.readLine();
            }
            return builder.toString();
        } catch (Exception ignored) {
        } finally {
            if (stream != null) {
                try {
                    stream.close();
                } catch (Exception ignored) {
                }

            }
        }
        return "";
    }

    public static void saveFromAssets(String assetPath, String toPath, Context ctx) {
        InputStream input = null;
        FileOutputStream output = null;
        try {
            input = ctx.getAssets().open(assetPath);
            byte[] content = new byte[input.available()];
            input.read(content);
            new File(toPath).getParentFile().mkdirs();
            output = new FileOutputStream(toPath);
            output.write(content);
            output.flush();
        } catch (Exception ignored) {
        } finally {
            try {
                if (input != null) {
                    input.close();
                }
            } catch (Exception ignored) {
            }
            try {
                if (output != null) {
                    output.close();
                }
            } catch (Exception ignored) {
            }


        }
    }


    public static void scanMedia(Context ctx, String path) {
        try {
            File file = new File(path);
            Uri uri = Uri.fromFile(file);
            Intent scanFileIntent = new Intent(
                    Intent.ACTION_MEDIA_SCANNER_SCAN_FILE, uri);
            ctx.sendBroadcast(scanFileIntent);
        } catch (Exception ignored) {
        }

    }

    public static Spanned getMandatoryString(String str) {
        if (TextUtils.isEmpty(str))
            return new SpannableString("");
        if (str.endsWith("*"))
            return new SpannableString(str);
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.N) {
            return Html.fromHtml(str + "<font color='#A1050D'>&nbsp*</font>", Html.FROM_HTML_MODE_LEGACY);
        } else {
            return Html.fromHtml(str + "<font color=#A1050D>&nbsp*</font>");
        }
    }

    public static boolean isPackageInstalled(Context context, String packageName) {

        try {
            PackageManager packageManager = context.getPackageManager();
            Intent intent = packageManager.getLaunchIntentForPackage(packageName);
            if (intent != null)
                return packageManager.queryIntentActivities(intent, PackageManager.MATCH_DEFAULT_ONLY).size() > 0;
        } catch (Exception ignored) {
        }
        return false;
    }


    public static void hideKeyBoard(Dialog dialog) {
        try {
            hideKeyBoard(dialog.getCurrentFocus());
        } catch (Exception ignored) {
        }

    }

    public static void hideKeyBoard(Activity activity) {
        try {
            hideKeyBoard(activity.getCurrentFocus() != null ? activity.findViewById(android.R.id.content) : activity.getCurrentFocus());
        } catch (Exception ignored) {
        }

    }

    public static void hideKeyBoard(View view) {
        try {
            InputMethodManager imm = (InputMethodManager) view.getContext().getSystemService(Context.INPUT_METHOD_SERVICE);
            view.clearFocus();
            if (imm != null) {
                imm.hideSoftInputFromWindow(view.getApplicationWindowToken(), InputMethodManager.HIDE_NOT_ALWAYS);
            }
            //}
        } catch (Exception ignored) {
        }

    }


    public static int dpToPx(float dp, @NonNull Resources resources) {
        //        return (int) (dp * resources.getDimensionPixelSize(R.dimen.one_dp));
        return Math.round(TypedValue.applyDimension(TypedValue.COMPLEX_UNIT_DIP, dp, resources.getDisplayMetrics()));
    }

    public static float dpToPxFloat(float dp, @NonNull Resources resources) {
        //        return (int) (dp * resources.getDimensionPixelSize(R.dimen.one_dp));
        return TypedValue.applyDimension(TypedValue.COMPLEX_UNIT_DIP, dp, resources.getDisplayMetrics());
    }

    public static float pxToDp(float px, @NonNull Resources resources) {
        //        return (int) (dp * resources.getDimensionPixelSize(R.dimen.one_dp));
        return px / ((float) resources.getDisplayMetrics().densityDpi / DisplayMetrics.DENSITY_DEFAULT);
    }

    public static float getExactDimen(@DimenRes int dimenKey, Resources resources) {
        return resources.getDimension(dimenKey) / resources.getDisplayMetrics().density;
    }

    public static String checkUrlHavePrefix(String url) {
        return (url == null || url.toLowerCase().startsWith("http://") || url.toLowerCase().startsWith("https://")) ? url : "http://" + url;
    }


    public static void setTextViewDrawableColor(TextView textView, int color) {
        try {
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
                textView.setCompoundDrawableTintList(ColorStateList.valueOf(color));
            }
            Drawable [] drawables=textView.getCompoundDrawablesRelative();
            if(drawables[0]==null && drawables[1]==null && drawables[2]==null && drawables[3]==null) {
                drawables=textView.getCompoundDrawables();
            }
            for (Drawable drawable :drawables) {
                if (drawable != null) {
                    setDrawableTint(color, drawable);
                    drawable.setColorFilter(new PorterDuffColorFilter(color, PorterDuff.Mode.SRC_IN));
                }
            }
        } catch (Exception ignored) {
        }

    }


    /**
     * Create a File for saving an image or video
     */


    public static String getDateFromDatePicker(Date date, boolean needTime) {
        try {
            SimpleDateFormat outFormat;
            if (!needTime) {
//                outFormat = new SimpleDateFormat("EEE dd MMM yyyy", Locale.ENGLISH);
                outFormat = new SimpleDateFormat("dd-MM-yyyy", Locale.ENGLISH);
            } else
                outFormat = new SimpleDateFormat("EEE dd MMM yyyy hh:mm aa", Locale.ENGLISH);
            return outFormat.format(date);
        } catch (Exception ignored) {
        }
        return "";
    }

    public static String setMinLength(int number, int minLength, char toAdd) {
        return String.format(Locale.ENGLISH, "%" + toAdd + minLength + "d", number);
    }

    public static String setMinLengthTime(int number) {
        return setMinLength(number, 2, '0');
    }

    public static void setDrawableTint(Context ctx, int color, @DrawableRes int drawableId) {
        try {
            setDrawableTint(color, ContextCompat.getDrawable(ctx, drawableId));
        } catch (Exception ignored) {
        }

    }

    public static void setDrawableTint(int color, Drawable drawable) {
        try {
            drawable = DrawableCompat.wrap(drawable);
            DrawableCompat.setTint(drawable, color);
            DrawableCompat.setTintMode(drawable, PorterDuff.Mode.SRC_IN);
        } catch (Exception ignored) {
        }

    }

    public static void setImageViewTint(@ColorInt int color,@NonNull ImageView imageView) {
        try {
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
                imageView.setImageTintList(ColorStateList.valueOf(color));
            } else {
                imageView.setColorFilter(color);
            }
        } catch (Exception ignored) {
        }

    }

    public static void setImageViewTint(@ColorInt int color,@NonNull ImageView imageView,@Nullable PorterDuff.Mode tintMode) {
        try {

            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
                imageView.setImageTintMode(tintMode);
                imageView.setImageTintList(ColorStateList.valueOf(color));
            } else {
                imageView.setColorFilter(color,tintMode);
            }
        } catch (Exception ignored) {
        }

    }

    public static void setCountOnDrawable(Context context, String count, LayerDrawable icon,@IdRes int layerId) {
        try {
            CountDrawable badge;

            // Reuse drawable if possible
            Drawable reuse = icon.findDrawableByLayerId(layerId);
            if (reuse != null && reuse instanceof CountDrawable) {
                badge = (CountDrawable) reuse;
            } else {
                badge = new CountDrawable(context);
            }

            badge.setCount(count);
            icon.mutate();
            icon.setDrawableByLayerId(layerId, badge);
        } catch (Exception ignored) {
        }
    }



    public static boolean checkStringContainsId(String deliminator, String toCheck, String id) {
        return (!TextUtils.isEmpty(toCheck) && !TextUtils.isEmpty(deliminator) && !TextUtils.isEmpty(id)) && (toCheck.equals(id) || toCheck.startsWith(id + deliminator) || toCheck.endsWith(deliminator + id) || toCheck.contains(deliminator + id + deliminator));
    }


    public static String getRealPathFromURI(Context context, Uri contentUri, boolean isImage, boolean isAudio, boolean isVideo) {
        Cursor cursor = null;
        try {
            if (new File(contentUri.getPath()).exists()) {
                return contentUri.getPath();
            } else if (contentUri.getPath().contains("/document/primary:Download/")) {
                try {
                    String path = Environment.getExternalStoragePublicDirectory(Environment.DIRECTORY_DOWNLOADS).toString() + "/" + contentUri.getPath().substring("/document/primary:Download/".length());
                    if (new File(path).exists()) {
                        return path;
                    }
                } catch (Exception ignored) {
                }

            }

            String[] proj = {isAudio ? MediaStore.Audio.Media.DATA : isVideo ? MediaStore.Video.Media.DATA : MediaStore.Images.Media.DATA};
            cursor = context.getContentResolver().query(contentUri, proj, null, null, null);
            if (cursor != null) {
                int column_index = cursor.getColumnIndexOrThrow(MediaStore.Images.Media.DATA);
                if (cursor.moveToFirst())
                    return cursor.getString(column_index);
            }
        } finally {
            if (cursor != null) {
                cursor.close();
            }
        }
        return null;
    }

    public static Bitmap getBackground(View mainView) {
        try {
            mainView.setDrawingCacheEnabled(true);
            mainView.setDrawingCacheQuality(View.DRAWING_CACHE_QUALITY_LOW);
            mainView.buildDrawingCache();
            Bitmap b = Bitmap.createBitmap(mainView.getDrawingCache());
            mainView.setDrawingCacheEnabled(false);
            return b;
        } catch (Exception ignored) {
        }

        return null;
    }


    public static void unzip(InputStream inputStream, File targetDirectory) throws IOException {
        ZipInputStream zis = new ZipInputStream(
                new BufferedInputStream(inputStream));
        try {
            ZipEntry ze;
            int count;
            byte[] buffer = new byte[8192];
            while ((ze = zis.getNextEntry()) != null) {
                File file = new File(targetDirectory, ze.getName());
                File dir = ze.isDirectory() ? file : file.getParentFile();
                if (!dir.isDirectory() && !dir.mkdirs())
                    throw new FileNotFoundException("Failed to ensure directory: " +
                            dir.getAbsolutePath());
                if (ze.isDirectory())
                    continue;
                FileOutputStream fout = new FileOutputStream(file);
                try {
                    while ((count = zis.read(buffer)) != -1)
                        fout.write(buffer, 0, count);
                } finally {
                    fout.close();
                }
            /* if time should be restored as well
            long time = ze.getTime();
            if (time > 0)
                file.setLastModified(time);
            */
            }
        } finally {
            zis.close();
        }
    }

    private void openAppSettings(@NonNull Context ctx) {

        try {
            Intent intent = new Intent();
            intent.setAction(
                    Settings.ACTION_APPLICATION_DETAILS_SETTINGS);
            Uri uri = Uri.fromParts("package",
                    BuildConfig.LIBRARY_PACKAGE_NAME, null);//BuildConfig.APPLICATION_ID is deprecated
            intent.setData(uri);
            intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
            ctx.startActivity(intent);
        } catch (Exception e) {
        }
    }

    public static boolean isPermissionBlockedFromAsking(Activity activity, String permission) {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            return ContextCompat.checkSelfPermission(activity, permission) != PackageManager.PERMISSION_GRANTED
                    && !activity.shouldShowRequestPermissionRationale(permission)
                    && PreferenceManager.getDefaultSharedPreferences(activity).getBoolean(permission, false);
        }
        return false;
    }

    public static boolean hasPermission(Context ctx, int requestCode, String[] permissions, String toastMessage) {
        return hasPermission(ctx, requestCode, permissions, toastMessage, true);
    }

    public static boolean hasPermission(Context ctx, int requestCode, String[] permissions, String toastMessage, boolean showDialog) {
        int permission = 0;
        for (String permission1 : permissions) {
            permission += ContextCompat.checkSelfPermission(ctx,
                    permission1);
            if (permission != PackageManager.PERMISSION_GRANTED && ctx instanceof Activity) {
                ActivityCompat.shouldShowRequestPermissionRationale((Activity) ctx, permission1);
            }
        }
        if (permission != PackageManager.PERMISSION_GRANTED) {
            if (showDialog && ctx instanceof Activity) {
                ActivityCompat.requestPermissions((Activity) ctx,
                        permissions,
                        requestCode);
            } else if (toastMessage != null) {
                Toast.makeText(ctx, toastMessage, Toast.LENGTH_SHORT).show();
            }
            return false;
        }
        return true;
    }

    public static boolean hasPermission(Context ctx, int requestCode, String[] permissions, boolean toast) {
        return hasPermission(ctx, requestCode, permissions, toast ? ctx.getString(R.string.msg_need_permission) : null);
    }

    public static boolean hasLocationPermission(Context ctx, int requestCode, boolean toast) {
        //        return hasPermission(ctx, requestCode, new String[]{"android.permission.ACCESS_MOCK_LOCATION", Manifest.permission.ACCESS_COARSE_LOCATION, Manifest.permission.ACCESS_FINE_LOCATION}, toast);
        return hasPermission(ctx, requestCode, new String[]{Manifest.permission.ACCESS_COARSE_LOCATION, Manifest.permission.ACCESS_FINE_LOCATION}, toast);
    }

    public static boolean isPermissionsGranted(@NonNull int[] grantResults, int permissionLength) {
        boolean isPermissionAccepted = grantResults.length == permissionLength;
        for (int permission : grantResults) {
            isPermissionAccepted = isPermissionAccepted && permission == PackageManager.PERMISSION_GRANTED;
        }
        return isPermissionAccepted;
    }

    public static boolean isNeedAutoStartPermission() {
        switch (Build.MANUFACTURER.toLowerCase()) {
            case "xiaomi":
            case "oppo":
            case "vivo":
            case "letv":
            case "honor":
                return true;
            default:
                return false;
        }
    }

    /**
     * @param requestCode should greater than -1
     **/
    public static void callAutoStartupSettings(@NonNull Activity act,int requestCode) throws Exception {
        if(requestCode<0) {
            throw new Exception("Request code should be a positive integer");
        }
        callAutoStartup(act,requestCode);

    }
    public static void addAutoStartupSettings(@NonNull Context ctx) {
        callAutoStartup(ctx,0);
    }
    private static void callAutoStartup(@NonNull Context ctx,int requestCode) {
        try {
            Intent intent = new Intent();
            String manufacturer = Build.MANUFACTURER.toLowerCase();
            if ("xiaomi".equalsIgnoreCase(manufacturer)) {
                intent.setComponent(new ComponentName("com.miui.securitycenter", "com.miui.permcenter.autostart.AutoStartManagementActivity"));
            } else if ("oppo".equalsIgnoreCase(manufacturer)) {
                intent.setComponent(new ComponentName("com.coloros.safecenter", "com.coloros.safecenter.permission.startup.StartupAppListActivity"));
            } else if ("vivo".equalsIgnoreCase(manufacturer)) {
                intent.setComponent(new ComponentName("com.vivo.permissionmanager", "com.vivo.permissionmanager.activity.BgStartUpManagerActivity"));
            } else if ("letv".equalsIgnoreCase(manufacturer)) {
                intent.setComponent(new ComponentName("com.letv.android.letvsafe", "com.letv.android.letvsafe.AutobootManageActivity"));
            } else if ("honor".equalsIgnoreCase(manufacturer)) {
                intent.setComponent(new ComponentName("com.huawei.systemmanager", "com.huawei.systemmanager.optimize.process.ProtectActivity"));
            }
            List<ResolveInfo> list = ctx.getPackageManager().queryIntentActivities(intent, PackageManager.MATCH_DEFAULT_ONLY);
            if (list.size() > 0) {
                if(ctx instanceof Activity && requestCode>-1) {
                    ((Activity)ctx).startActivityForResult(intent,requestCode);
                } else {
                    ctx.startActivity(intent);
                }

            }
        } catch (Exception ignored) {
        }
    }

    public static void callUrl(String url, Context ctx) {
        try {
            callUrl(url, true, ctx);
        } catch (Exception e) {
            try {
                callUrl(url, false, ctx);
            } catch (Exception ignored) {
            }
        }
    }

    public static void callUrl(String url, boolean filterToApp, Context ctx) throws PackageManager.NameNotFoundException {
        Uri uri = Uri.parse(url);
        Intent browserIntent = null;

        if (filterToApp) {
            if (url.toLowerCase().contains("facebook.com")) {
                PackageManager packageManager = ctx.getPackageManager();
                if (isPackageInstalled(ctx, "com.facebook.lite")) {
//                    browserIntent = ctx.getPackageManager().getLaunchIntentForPackage("com.facebook.lite");
                    uri = Uri.parse("fb://facewebmodal/f?href=" + url);
                } else if (isPackageInstalled(ctx, "com.facebook.katana")) {
                    int versionCode = packageManager.getPackageInfo("com.facebook.katana", 0).versionCode;
                    if (versionCode >= 3002850) { //newer versions of fb app
                        uri = Uri.parse("fb://facewebmodal/f?href=" + url);
                    } else { //older versions of fb app
                        String page = url.substring(url.indexOf("facebook.com") + "facebook.com/".length() + 1);
                        uri = Uri.parse("fb://page/" + page);
                    }
                }
            } else if (url.toLowerCase().contains("plus.google.com") && isPackageInstalled(ctx, "com.google.android.apps.plus")) {
                browserIntent = ctx.getPackageManager().getLaunchIntentForPackage("com.google.android.apps.plus");
            } else if (url.toLowerCase().contains("twitter.com") && isPackageInstalled(ctx, "com.twitter.android")) {
                browserIntent = ctx.getPackageManager().getLaunchIntentForPackage("com.twitter.android");
            } else if (url.toLowerCase().contains("youtube.com") && isPackageInstalled(ctx, "com.google.android.youtube")) {
                String youtubeId = parseYoutubeVideoId(url);
                if (TextUtils.isEmpty(youtubeId))
                    browserIntent = ctx.getPackageManager().getLaunchIntentForPackage("com.google.android.youtube");
                else {
                    browserIntent = new Intent(Intent.ACTION_VIEW, Uri.parse("vnd.youtube:" + youtubeId));
                }
            } else if (url.toLowerCase().contains("hotstar.com") && isPackageInstalled(ctx, "in.startv.hotstar")) {
                browserIntent = ctx.getPackageManager().getLaunchIntentForPackage("in.startv.hotstar");
            }
        }
        if (browserIntent == null)
            browserIntent = new Intent(Intent.ACTION_VIEW, uri);
        browserIntent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
        browserIntent.setData(uri);
        ctx.startActivity(browserIntent);
    }

    public static String parseYoutubeVideoId(String youtubeUrl) {
        String video_id = null;
        try {
            if (youtubeUrl != null && youtubeUrl.trim().length() > 0 &&
                    youtubeUrl.startsWith("http")) {
                // ^.*((youtu.be\/)|(v\/)|(\/u\/\w\/)|(embed\/)|(watch\?))\??v?=?([^#\&\?]*).*/
                String expression = "^.*((youtu.be" + "\\/)"
                        + "|(v\\/)|(\\/u\\/w\\/)|(embed\\/)|(watch\\?))\\??v?=?([^#\\&\\?]*).*";
                CharSequence input = youtubeUrl;
                Pattern pattern = Pattern.compile(expression, Pattern.CASE_INSENSITIVE);
                Matcher matcher = pattern.matcher(input);
                if (matcher.matches()) {
                    // Regular expression some how doesn't work with id with "v" at
                    // prefix
                    String groupIndex1 = matcher.group(7);
                    if (groupIndex1 != null && groupIndex1.length() == 11)
                        video_id = groupIndex1;
                    else if (groupIndex1 != null && groupIndex1.length() == 10)
                        video_id = "v" + groupIndex1;
                }
            }
        } catch (Exception ignored) {
        }

        return video_id;
    }

    public static Uri getOutputMediaFileUri(@NonNull Context ctx) {
        return Uri.fromFile(getOutputMediaFile(ctx));
    }

    /**
     * Create a File for saving an image or video
     */
    public static File getOutputMediaFile(@NonNull Context ctx) {
        // To be safe, you should check that the SDCard is mounted
        // using Environment.getExternalStorageState() before doing this.
        File mediaStorageDir = new File(Environment.getExternalStoragePublicDirectory(
                Environment.DIRECTORY_PICTURES), ctx.getString(R.string.app_name));
        // This location works best if you want the created images to be shared
        // between applications and persist after your app has been uninstalled.

        // Create the storage directory if it does not exist
        if (!mediaStorageDir.exists()) {
            if (!mediaStorageDir.mkdirs()) {
                return null;
            }
        }

        // Create a media file name
        String timeStamp = new SimpleDateFormat("yyyyMMdd_HHmmss", Locale.ENGLISH).format(new Date());
        File mediaFile = new File(mediaStorageDir.getPath() + File.separator +
                "IMG_" + timeStamp + ".jpg");

        return mediaFile;
    }

    public static boolean isNetworkAvailable(Context ctx, int requestCode) {
        try {
            if (hasPermission(ctx, requestCode, new String[]{Manifest.permission.ACCESS_NETWORK_STATE}, false)) {
                ConnectivityManager connectivityManager
                        = (ConnectivityManager) ctx.getSystemService(Context.CONNECTIVITY_SERVICE);
                @SuppressLint("MissingPermission") NetworkInfo activeNetworkInfo = connectivityManager.getActiveNetworkInfo();
                return activeNetworkInfo != null && activeNetworkInfo.isConnected();
            }
        } catch (Exception ignored) {
        }
        return false;
    }

    public static int calculateInSampleSize(
            BitmapFactory.Options options, int reqWidth, int reqHeight) {
        // Raw height and width of image
        final int height = options.outHeight;
        final int width = options.outWidth;
        int inSampleSize = 1;

        if (height > reqHeight || width > reqWidth) {

            final int halfHeight = height / 2;
            final int halfWidth = width / 2;

            // Calculate the largest inSampleSize value that is a power of 2 and keeps both
            // height and width larger than the requested height and width.
            while ((halfHeight / inSampleSize) >= reqHeight
                    && (halfWidth / inSampleSize) >= reqWidth) {
                inSampleSize *= 2;
            }
        }

        return inSampleSize;
    }

    public static Bitmap decodeBitmap(Resources res, @DrawableRes int resId, @Nullable String path,
                                      int reqWidth, int reqHeight) {

        // First decode with inJustDecodeBounds=true to check dimensions
        final BitmapFactory.Options options = new BitmapFactory.Options();
        options.inJustDecodeBounds = true;
        if (path == null)
            BitmapFactory.decodeResource(res, resId, options);
        else
            BitmapFactory.decodeFile(path, options);

        // Calculate inSampleSize
        options.inSampleSize = calculateInSampleSize(options, reqWidth, reqHeight);

        // Decode bitmap with inSampleSize set
        options.inJustDecodeBounds = false;
        if (path == null)
            return BitmapFactory.decodeResource(res, resId, options);
        return BitmapFactory.decodeFile(path, options);
    }

    public static void viewAFileInOtherApp(Context ctx, @NonNull String mimeType, String file) {
        try {
            if (ctx == null)
                return;
            Intent intent = new Intent();
            intent.setAction(Intent.ACTION_VIEW);
            intent.setDataAndType(Uri.fromFile(new File(file)), mimeType);
            intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
            ctx.startActivity(Intent.createChooser(intent, "Select Application"));
        } catch (Exception ignored) {
            Toast.makeText(ctx, R.string.info_no_apps_handle, Toast.LENGTH_SHORT).show();
        }
    }

    /**
     *
     * @param ctx               required
     * @param urlPath           The server file url to download
     * @param extension         Document type  such as pdf, docx....
     * @param fileName          Local device filename to save
     * @param requestCode       External storage permission check
     * @return
     */
    public static long downloadAfile(@NonNull Context ctx,@NonNull String urlPath,@Nullable String extension,@Nullable String fileName, int requestCode) {
        try {
            if (Utilities.hasPermission(ctx, requestCode, new String[]{Manifest.permission.WRITE_EXTERNAL_STORAGE}, true)) {
                DownloadManager.Request request = new DownloadManager.Request(
                        Uri.parse(urlPath));
                try {
                    if (TextUtils.isEmpty(extension))
                        extension = urlPath.substring(urlPath.lastIndexOf(".") + 1);
                } catch (Exception ignored) {
                }

                Calendar c = Calendar.getInstance();

                File f = Environment.getExternalStoragePublicDirectory(Environment.DIRECTORY_DOWNLOADS);
                new File(f.getAbsolutePath() + "/" + ctx.getString(R.string.app_name)).mkdirs();
                request.setDestinationInExternalPublicDir(Environment.DIRECTORY_DOWNLOADS, ctx.getString(R.string.app_name) + "/" + fileName + "_" + c.get(Calendar.DATE) + "_" + c.getDisplayName(Calendar.MONTH, Calendar.SHORT, Locale.UK) + "_" + c.get(Calendar.YEAR) + "_" + c.get(Calendar.HOUR_OF_DAY) + "_" + c.get(Calendar.MINUTE) + "_" + c.get(Calendar.SECOND) + (TextUtils.isEmpty(extension) ? "" : "."+extension));
                request.allowScanningByMediaScanner();
                request.setVisibleInDownloadsUi(true).setNotificationVisibility(DownloadManager.Request.VISIBILITY_VISIBLE);
                request.setNotificationVisibility(DownloadManager.Request.VISIBILITY_VISIBLE_NOTIFY_COMPLETED);
                request.setDescription("Downloading  "+fileName+" ...");
                DownloadManager dm = (DownloadManager) ctx.getSystemService(Context.DOWNLOAD_SERVICE);

//            request.setDestinationInExternalFilesDir(ctx,
//                    Environment.DIRECTORY_DOWNLOADS,"AndroidTutorialPoint.jpg");
                return dm.enqueue(request);
            }
        } catch (Exception ignored) {
        }
        return 0;
    }

    public static boolean isMyServiceRunning(Class<?> serviceClass,Context ctx) {
        ActivityManager manager = (ActivityManager) ctx.getSystemService(Context.ACTIVITY_SERVICE);
        for (ActivityManager.RunningServiceInfo service : manager.getRunningServices(Integer.MAX_VALUE)) {
            if (serviceClass.getName().equals(service.service.getClassName())) {
                return true;
            }
        }
        return false;
    }
}
