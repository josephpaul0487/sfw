package jpt.sfw.app;

import android.content.Context;
import android.content.SharedPreferences;

import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import jpt.sfw.R;
import jpt.sfw.annotations.AppConfig;

import java.lang.reflect.Constructor;
import java.lang.reflect.Type;
import java.util.ArrayList;
import java.util.List;


public class Preferences {
    private static Preferences instance;


    private SharedPreferences preferences;
    private Gson gson = new Gson();
    private boolean isDebuggingEnabled;

    private static final String TAG = "Preferences";


    public static Preferences init(@NonNull Context ctx, @NonNull AppConfig config) {
        Class<? extends Preferences> cls = config.preferenceClassType();
        if (instance == null) {
            try {
                Constructor<? extends Preferences> constructor = cls.getConstructor(Context.class);
                constructor.setAccessible(true);
                instance = constructor.newInstance(ctx);
                instance.isDebuggingEnabled = config.debuggingEnabled();
            } catch (Throwable e) {
                throw new IllegalAccessError("Can't initialize this class : " + cls.getName() + ".  Class should contain a public  constructor with one parameter (Context) . Should not be an abstract class. ");
            }
        }
        return instance;
    }

    public static Preferences getInstance() {
        return instance;
    }

    public void destroy() {
        try {
            preferences=null;
            instance=null;
            gson=null;
        } catch (Exception ignored){}
    }

    public Preferences(Context ctx) {
        preferences = ctx.getSharedPreferences(ctx.getPackageName() + ctx.getString(R.string.app_name), Context.MODE_PRIVATE);
    }

    public void clear(String key) {
        preferences.edit().remove(key).commit();
    }

    public <T> boolean putList(String key, List<T> models) {
        try {
            if (models != null) {
                putString(key, gson.toJson(models));
            }
        } catch (Exception e) {
            log("putList",e);
        }
        return false;
    }

    public <T> List<T> getList(String key, Class<T> elementClass) {
        try {
            final Type type = new TypeToken<List<T>>() {
            }.getType();
            return gson.fromJson(preferences.getString(key, gson.toJson(new ArrayList<T>())), TypeToken.getParameterized(List.class, type).getType());
        } catch (Exception e) {
            log("getList",e);
        }
        return new ArrayList<>();
    }

    public void putBoolean(String key, boolean value) {
        preferences.edit().putBoolean(key, value).commit();
    }

    public int getInt(String key, int defValue) {
        return preferences.getInt(key, defValue);
    }

    public void putInt(String key, int value) {
        preferences.edit().putInt(key, value).commit();
    }

    public float getFloat(String key, float defValue) {
        return preferences.getFloat(key, defValue);
    }

    public void putFloat(String key, float value) {
        preferences.edit().putFloat(key, value).commit();
    }

    public boolean getBoolean(String key, boolean defValue) {
        return preferences.getBoolean(key, defValue);
    }

    public void putString(String key, String value) {
        preferences.edit().putString(key, value).commit();
    }

    public String getString(String key, String defValue) {
        return preferences.getString(key, defValue);
    }

    public <T> boolean putObject(T model, String key) {
        try {
            if (model != null) {
                putString(key, gson.toJson(model));
            }
        } catch (Exception e) {
            log("putObject",e);
        }
        return false;
    }

    public <T> T getObject(Class<T> elementClass, String key) {
        try {
            return gson.fromJson(getString(key, ""), elementClass);
        } catch (Exception e) {
            log("getObject",e);
        }
        return null;
    }

    public boolean isDebuggingEnabled() {
        return isDebuggingEnabled;
    }

    public Preferences setDebuggingEnabled(boolean debuggingEnabled) {
        isDebuggingEnabled = debuggingEnabled;
        return this;
    }

    public void log(@NonNull String value, @Nullable Throwable t) {
        log(value,t,t!=null);
    }

    public void log(@NonNull String value, @Nullable Throwable t, boolean isError) {
        Sfw.log(TAG,value,t,isError,isDebuggingEnabled);
    }
}
