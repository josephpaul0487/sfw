package jpt.sfw.app;

import android.app.Application;
import android.content.Context;
import android.content.res.TypedArray;
import android.graphics.drawable.Drawable;
import android.os.StrictMode;
import android.text.TextUtils;
import android.util.AttributeSet;
import android.util.Log;
import android.view.View;
import android.widget.TextView;

import io.michaelrocks.libphonenumber.android.PhoneNumberUtil;
import jpt.sfw.R;
import jpt.sfw.database.SfwDatabase;
import jpt.sfw.annotations.AppConfig;
import jpt.sfw.annotations.BindView;
import jpt.sfw.annotations.OnClick;

import java.lang.reflect.Field;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Locale;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.annotation.StyleableRes;
import androidx.core.content.res.ResourcesCompat;
import androidx.multidex.MultiDex;

import com.google.android.material.card.MaterialCardView;

import jpt.sfw.webservice.SfwApiHelper;
import jpt.sfw.webservice.SfwClient;

public class Sfw {

    public static PhoneNumberUtil getPhoneNumberUtil(@NonNull Context ctx) {
        if (phoneNumberUtil == null)
            phoneNumberUtil = PhoneNumberUtil.createInstance(ctx);
        return phoneNumberUtil;
    }

    public static void setFont(TextView view, AttributeSet attrs) {
        try {
            int[] attrsArray = new int[]{
                    android.R.attr.fontFamily, // 0
            };

            TypedArray ta = view.getContext().obtainStyledAttributes(attrs, attrsArray);
            int typefaceId = 0;
            try {
                typefaceId = ta.getResourceId(0, 0);
            } finally {
                ta.recycle();
            }
            if (typefaceId != 0)
                view.setTypeface(ResourcesCompat.getFont(view.getContext(), typefaceId));
        } catch (Exception ignored) {
        }
    }

    public static int getIntFromAttribute(@NonNull Context ctx, @NonNull AttributeSet attrs, int attribute, @StyleableRes int[] styleable, int defaultValue, boolean isForDimen) {
        TypedArray a = ctx.getTheme().obtainStyledAttributes(
                attrs,
                styleable,
                0, 0);

        try {
            if (isForDimen)
                return a.getDimensionPixelSize(attribute, defaultValue);
            return a.getInt(attribute, defaultValue);
        } catch (Exception ignored) {
        } finally {
            a.recycle();
        }
        return 0;
    }

    public static String getStringFromAttribute(@NonNull Context ctx, @NonNull AttributeSet attrs, int attribute, @StyleableRes int[] styleable) {
        TypedArray a = ctx.getTheme().obtainStyledAttributes(
                attrs,
                styleable,
                0, 0);

        try {
            String str = a.getString(attribute);
            if (str != null)
                return str;
        } catch (Exception ignored) {
        } finally {
            a.recycle();
        }
        return "";
    }

    public static void setDrawableTint(TextView textView, AttributeSet attrs) {
        try {

            int drawableTintColor = getIntFromAttribute(textView.getContext(), attrs, R.styleable.AppAttrs_sfw_drawableTint, R.styleable.AppAttrs, 0, false);
            if (drawableTintColor != 0) {
                Utilities.setTextViewDrawableColor(textView, drawableTintColor);
            }
            int drawableSize = getIntFromAttribute(textView.getContext(), attrs, R.styleable.AppAttrs_sfw_drawableSize, R.styleable.AppAttrs, 0, true);
            if (drawableSize > 0) {
                Drawable[] drawables = textView.getCompoundDrawablesRelative();
                if (drawables[0] == null && drawables[1] == null && drawables[2] == null && drawables[3] == null) {
                    drawables = textView.getCompoundDrawables();
                }
                for (Drawable d : drawables) {
                    if (d != null) {
                        d.setBounds(0, 0, drawableSize, drawableSize);
                    }
                }
                textView.setCompoundDrawablesRelative(drawables[0], drawables[1], drawables[2], drawables[3]);
            }

//            }
        } catch (Exception ignored) {
        }
    }

    public static void setMandatoryText(TextView textView, AttributeSet attrs) {
        try {
            boolean isMandatory = isMandatoryHint(attrs, textView.getContext());
            if (isMandatory) {
                boolean isAllCaps = isAllCaps(attrs, textView.getContext());
                //This font is not working on all caps.. So we should set by code
                if (isAllCaps) {
                    textView.setAllCaps(false);
                }
                textView.setText(Utilities.getMandatoryString(isAllCaps ? textView.getText().toString().toUpperCase(Locale.getDefault()) : textView.getText().toString()));
            }
        } catch (Exception ignored) {
        }
    }

    public static void setMandatoryHint(TextView textView, AttributeSet attrs) {
        try {
            boolean isMandatory = isMandatoryHint(attrs, textView.getContext());
            if (isMandatory) {
                boolean isAllCaps = isAllCaps(attrs, textView.getContext());
                //This font is not working on all caps.. So we should set by code
                if (isAllCaps) {
                    textView.setAllCaps(false);
                }
                textView.setHint(Utilities.getMandatoryString(isAllCaps ? textView.getHint().toString().toUpperCase(Locale.getDefault()) : textView.getHint().toString()));
            }
        } catch (Exception ignored) {
        }
    }

    public static boolean isMandatoryHint(AttributeSet attrs, Context ctx) {
        try {
//            if (Build.VERSION.SDK_INT < Build.VERSION_CODES.M) {
            TypedArray a = ctx.getTheme().obtainStyledAttributes(
                    attrs,
                    R.styleable.AppAttrs,
                    0, 0);
            boolean isMandatory;
            try {
                isMandatory = a.getBoolean(R.styleable.AppAttrs_sfw_isMandatoryHint, false);
            } finally {
                a.recycle();
            }

            return isMandatory;
        } catch (Exception ignored) {
        }
        return false;
    }

    public static boolean isAllCaps(AttributeSet attrs, Context ctx) {
        try {
            int[] attrsArray = new int[]{
                    android.R.attr.textAllCaps, // 0
            };
            TypedArray ta = ctx.obtainStyledAttributes(attrs, attrsArray);
            try {
                return ta.getBoolean(0, false);
            } finally {
                ta.recycle();
            }
        } catch (Exception ignored) {
        }
        return false;
    }

    private static boolean isPkgIncludedToBindView(String classNameInLowercase) {
        for (String s : bindViewIncludedPackages) {
            if (classNameInLowercase.startsWith(s)) {
                return true;
            }
        }
        return false;
    }

    //this will not bind superclass fields
    public static void bindViews(@NonNull Object cls, @NonNull View view, @Nullable View.OnClickListener onClickListener) {
        bindViews(cls, cls.getClass(), view, onClickListener);
    }

    private static void bindViews(@NonNull Object object, @NonNull Class<?> cls, @NonNull View view, @Nullable View.OnClickListener onClickListener) {
//        if (cls.getSuperclass() != null && (cls.getSuperclass().getName().startsWith(view.getContext().getPackageName()) || cls.getSuperclass().getName().startsWith("jpt.sfw")))
        if (cls.getSuperclass() != null && isPkgIncludedToBindView(cls.getSuperclass().getName()))
            bindViews(object, cls.getSuperclass(), view, onClickListener);
        List<OnClick> classesOnClick = getAnnotations(OnClick.class, cls);
        for (OnClick classOnClick : classesOnClick) {
            int[] ids = classOnClick.values();
            int id = classOnClick.value();
            if (id == View.NO_ID) {
                String idStr = classOnClick.idName();
                id = getIdFromString(idStr, R.id.class, View.NO_ID);
            }
            int[] idss;
            if (id != View.NO_ID) {
                idss = new int[ids.length + 1];
                System.arraycopy(ids, 0, idss, 0, ids.length);
                idss[ids.length] = id;
            } else {
                idss = ids;
            }
            for (int i : idss) {
                View v = view.findViewById(i);
                if (v != null) {
                    v.setOnClickListener(onClickListener);
                } else {
                    throw new RuntimeException("No view found for id " + i + " (" + cls.getName() + ") .");
                }
            }
        }
        for (Field f : cls.getDeclaredFields()) {
            f.setAccessible(true);
            if (f.isAnnotationPresent(BindView.class) || f.isAnnotationPresent(OnClick.class)) {
                int id = f.isAnnotationPresent(BindView.class) ? f.getAnnotation(BindView.class).value() : f.getAnnotation(OnClick.class).value();
                try {
                    if (id == View.NO_ID) {
                        String idStr = f.isAnnotationPresent(BindView.class) ? f.getAnnotation(BindView.class).idName() : f.getAnnotation(OnClick.class).idName();
                        id = getIdFromString(idStr, R.id.class, View.NO_ID);
                    }
                    View v = view.findViewById(id);
                    if(v==null && !f.isAnnotationPresent(jpt.sfw.annotations.Nullable.class)) {
                        throw new RuntimeException("No view found for id " + id + " -- " + f.getName() + " -- (" + cls.getName() + ") .  Use @jpt.sfw.annotations.Nullable annotation if the view is optional");
                    }
                    if (f.isAnnotationPresent(OnClick.class)) {
                        v.setOnClickListener(onClickListener);
                    }
                    f.set(object, v);
                } catch (Exception e) {
                    if (e instanceof ClassCastException || e instanceof IllegalArgumentException) {
                        throw new RuntimeException(e);
                    }
                    if (!f.isAnnotationPresent(jpt.sfw.annotations.Nullable.class)) {
                        throw new RuntimeException("No view found for id " + id + " -- " + f.getName() + " -- (" + cls.getName() + ") .  Use @jpt.sfw.annotations.Nullable annotation if the view is optional");
                    }
                }
            }
        }
    }

    public static <T> T getAnnotation(@NonNull Class<T> annotationType, @Nullable Class targetObject) {
        if (targetObject == null)
            return null;
        try {
            T annotation = (T) targetObject.getAnnotation(annotationType);
            if (annotation != null)
                return annotation;
            if (targetObject.getSuperclass() == null || targetObject.getSuperclass().getName().startsWith("android") || targetObject.getSuperclass().getName().startsWith("java"))
                return null;
            return getAnnotation(annotationType, targetObject.getSuperclass());
        } catch (Exception ignored) {
        }
        return null;
    }

    public static <T> List<T> getAnnotations(@NonNull Class<T> annotationType, @NonNull Class targetObject) {
        List<T> list = new ArrayList<>();

        T annotation = (T) targetObject.getAnnotation(annotationType);
        if (annotation != null)
            list.add(annotation);
        if (targetObject.getSuperclass() != null && !targetObject.getSuperclass().getName().startsWith("android") && !targetObject.getSuperclass().getName().startsWith("java"))
            list.addAll(getAnnotations(annotationType, targetObject.getSuperclass()));
        return list;
    }

    public static int getIdFromString(String id, @NonNull Class cls, int defaultValue) {
        try {
            if (!TextUtils.isEmpty(id)) {
                Field field = cls.getDeclaredField(id);
                return field.getInt(null);
            }
        } catch (Exception ignored) {
        }
        return defaultValue;
    }


    public static void setup(@NonNull Application application) {
        AppConfig appConfig = getAnnotation(AppConfig.class, application.getClass());
        if (appConfig == null)
            throw new IllegalAccessError("You should annotate your application class using jpt.sfw.annotations.AppConfig");
        isDebuggingEnabled = appConfig.debuggingEnabled();
        Validation.init(application);
        MultiDex.install(application);
        if (appConfig.initializeDatabase())
            SfwDatabase.init(application, appConfig);
        Preferences.init(application, appConfig);
        if (appConfig.initializeApiHelper())
            SfwApiHelper.init(application, appConfig);
        SfwClient.init(appConfig);
        ImageUtil.setNeedLogging(appConfig.debuggingEnabled());

        bindViewIncludedPackages.clear();
        bindViewIncludedPackages.addAll(Arrays.asList("jpt.sfw", application.getPackageName().toLowerCase()));
        for (String s : appConfig.bindViewsIncludedPkgsStartsWith())
            bindViewIncludedPackages.add(s.toLowerCase());

        if (appConfig.setVmPolicy()) {
            //To avoid errors like FileExposedException
            StrictMode.VmPolicy.Builder builder = new StrictMode.VmPolicy.Builder();
            StrictMode.setVmPolicy(builder.build());
        }


//        replaceFont("SERIF", Typeface.createFromAsset(getAssets(), getCommonFont(this)));
//        replaceFont("DEFAULT", Typeface.createFromAsset(getAssets(), getCommonFont(this)));
//        replaceFont("MONOSPACE", Typeface.createFromAsset(getAssets(), getCommonFont(this)));
//        replaceFont("SANS_SERIF", Typeface.createFromAsset(getAssets(), getCommonFont(this)));
    }

    public static void log(@NonNull String tag, @NonNull String value, @Nullable Throwable t, boolean isError) {
        log(tag, value, t, isError, isDebuggingEnabled);
    }

    public static void log(@NonNull String tag, @NonNull String value, @Nullable Throwable t, boolean isError, boolean isDebuggingEnabled) {
        if (!isDebuggingEnabled)
            return;
        if (isError)
            Log.e(tag, value, t);
        else
            Log.d(tag, value, t);
    }

    private static PhoneNumberUtil phoneNumberUtil;
    public static boolean isDebuggingEnabled;
    private static final List<String> bindViewIncludedPackages = new ArrayList<>();
}
