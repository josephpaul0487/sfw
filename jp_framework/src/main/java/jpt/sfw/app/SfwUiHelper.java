package jpt.sfw.app;

import android.content.Context;

import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.LinearSmoothScroller;
import androidx.recyclerview.widget.RecyclerView;

public class SfwUiHelper {
    private static final String TAG ="SfwUiHelper";
    public static void scrollToBottom(RecyclerView recyclerView) {
        if(recyclerView!=null) {
            if(recyclerView.getLayoutManager() instanceof LinearLayoutManager) {
                int checkPosition =((LinearLayoutManager)recyclerView.getLayoutManager()).getReverseLayout()?0:recyclerView.getLayoutManager().getItemCount()-1;
                if( ((LinearLayoutManager)recyclerView.getLayoutManager()).findLastCompletelyVisibleItemPosition()!=checkPosition) {
                    scrollToPosition(recyclerView,checkPosition);
                }
            }

        }
    }

    public static void scrollToTop(RecyclerView recyclerView) {
        if(recyclerView!=null) {
            if(recyclerView.getLayoutManager() instanceof LinearLayoutManager) {
                int checkPosition =((LinearLayoutManager)recyclerView.getLayoutManager()).getReverseLayout()?recyclerView.getLayoutManager().getItemCount()-1:0;
                if( ((LinearLayoutManager)recyclerView.getLayoutManager()).findFirstCompletelyVisibleItemPosition()!=checkPosition) {
                    scrollToPosition(recyclerView,checkPosition);
                }
            }

        }
    }

    public static void scrollToPosition(RecyclerView recyclerView,int position) {
        if(recyclerView!=null ) {
            try {
                if(recyclerView.getLayoutManager()!=null) {
                    RecyclerView.SmoothScroller scroller = createSmoothScroller(recyclerView.getContext());
                    scroller.setTargetPosition(position);
                    recyclerView.getLayoutManager().startSmoothScroll(scroller);
                } else {
                    Sfw.log(TAG,"scrollToPosition : layout manager is null -----  position = "+position,null,false);
                }
            } catch (Exception e){
                Sfw.log(TAG,"scrollToPosition : position = "+position,e,true);
            }


        }
    }

    public static LinearSmoothScroller createSmoothScroller(Context ctx) {
        return  new LinearSmoothScroller(ctx) {
            @Override
            protected int getHorizontalSnapPreference() {
                return SNAP_TO_START;
            }

            @Override
            protected int getVerticalSnapPreference() {
                return SNAP_TO_START;
            }
        };
    }
}
