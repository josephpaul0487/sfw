package jpt.sfw.app;

import android.Manifest;
import android.annotation.SuppressLint;
import android.app.Activity;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.Color;
import android.net.Uri;
import android.os.Build;
import android.os.StrictMode;
import android.provider.MediaStore;
import android.text.TextUtils;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.google.android.material.bottomsheet.BottomSheetDialog;

import jpt.sfw.R;
import com.yalantis.ucrop.UCrop;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileOutputStream;
import java.lang.reflect.Method;
import java.util.Arrays;
import java.util.List;
import java.util.Locale;

import androidx.annotation.DrawableRes;
import androidx.annotation.FloatRange;
import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;
import androidx.core.content.ContextCompat;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentActivity;
import jpt.sfw.interfaces.SfwActivityInterface;

/**
 * You should check {@link #isFilePicked()} before use the last picked file
 * Using 69 As Ucrop Constant for startActivityForResult  ->  You can change it by using {@link ImageUploadConfig#setRequestUcrop(int)}
 */
public class ImageUploadHelper {
    private TextView txtGallery, txtCamera, txtFile;
    private View layoutGallery, layoutCamera, layoutFile;
    private ImageView imgGallery, imgCamera, imgFile;
    private LinearLayout layoutPickerBtnMain;
    private ImageUploadConfig config;
    private BottomSheetDialog bottomSheetDialog;

    private Uri fileUri;
    private String uploadImagePath = null;
    private String lastSelectedImagePath;
    private int onPressedColor;
    private int onTouchUpColor;
    private int tag;
    private boolean isDebuggingEnabled;
    private int onPressedBack=R.drawable.circle ;
    private int onTouchUpBack=R.drawable.circle_stroke;

    private static final String TAG="ImageUploadHelper";


    public ImageUploadHelper(@Nullable ImageView imageView, FragmentActivity activity, Fragment fragment, int permissionStorage, int permissionCamera, int requestGallery, int requestCamera) {
        this(imageView, activity, fragment, permissionStorage, permissionCamera, requestGallery, requestCamera, 0, null, 0, 0);
    }

    /**
     * @param activity          if calling from activity
     * @param fragment          if calling from fragment
     * @param permissionStorage permission contant for storage.                     should call {@link #onRequestPermissionsResult(int, String[], int[])} from your activity or fragment
     * @param permissionCamera  permission contant for camera.                      should call {@link #onRequestPermissionsResult(int, String[], int[])} from your activity or fragment
     * @param requestGallery    activity result contant fro select a file.          should call {@link #onActivityResult(int, int, Intent)} from your activity or fragment
     * @param requestCamera     activity result contant fro capture from camera.    should call {@link #onActivityResult(int, int, Intent)} from your activity or fragment
     * @param ratioWidth        the width ratio
     * @param ratioHeight       the heigh ratio
     */
    public ImageUploadHelper(@Nullable ImageView imageView, FragmentActivity activity, Fragment fragment, int permissionStorage, int permissionCamera, int requestGallery, int requestCamera, int requestUcrop, String temporaryImagePath, @FloatRange(from = 0,to = 16) float ratioWidth, @FloatRange(from = 0,to = 16) float ratioHeight) {
        this(new ImageUploadConfig(imageView,activity,fragment,permissionStorage,permissionCamera,requestGallery,requestCamera,requestUcrop,temporaryImagePath,ratioWidth,ratioHeight));
    }

    public ImageUploadHelper(@NonNull ImageUploadConfig config) {
        this.config=config;
        setupProfileImageDialog();
    }

    public ImageUploadConfig getConfig() {
        return config;
    }

    public boolean isDebuggingEnabled() {
        return isDebuggingEnabled;
    }

    public ImageUploadHelper setDebuggingEnabled(boolean debuggingEnabled) {
        isDebuggingEnabled = debuggingEnabled;
        return this;
    }

    public ImageUploadHelper enableFilePicker(boolean enable, int requestCode, int permissionCode) {
        layoutFile.setVisibility(enable ? View.VISIBLE : View.GONE);
        txtFile.setVisibility(enable ? View.VISIBLE : View.GONE);
        layoutPickerBtnMain.setWeightSum(enable ? 3 : 2);
       config.setRequestFile(requestCode);
        config.setPermissionFile(permissionCode);
        return this;
    }

    public void deleteLastPath() {
        if (lastSelectedImagePath != null) {
            try {
                if (new File(lastSelectedImagePath).exists()) {
                    new File(lastSelectedImagePath).delete();
                    Utilities.scanMedia(config.getActivity(), lastSelectedImagePath);
                }
            } catch (Throwable ignored) {
            }
        }
        lastSelectedImagePath = null;
    }

    public void clear() {
        config=null;
        dismissDialog();
        bottomSheetDialog = null;
    }


    public void showDialog() {
        bottomSheetDialog.show();
    }

    public void dismissDialog() {
        if(bottomSheetDialog!=null && bottomSheetDialog.isShowing())
            bottomSheetDialog.dismiss();
    }

    private void setupProfileImageDialog() {
        bottomSheetDialog = new BottomSheetDialog(config.getActivity());
        View v = LayoutInflater.from(config.getActivity()).inflate(R.layout.sfw_image_upload_helper_layout, null, false);

        txtCamera = v.findViewById(R.id.txtCamera);
        txtGallery = v.findViewById(R.id.txtGallery);
        txtFile = v.findViewById(R.id.txtFile);

        layoutCamera = v.findViewById(R.id.layoutCamera);
        layoutGallery = v.findViewById(R.id.layoutGallery);
        layoutFile = v.findViewById(R.id.layoutFile);
        layoutPickerBtnMain = v.findViewById(R.id.layoutPickerBtnMain);

        imgCamera = v.findViewById(R.id.imgCamera);
        imgGallery = v.findViewById(R.id.imgGallery);
        imgFile = v.findViewById(R.id.imgFile);


        layoutGallery.setOnClickListener(v12 -> galleryIntent());
        layoutCamera.setOnClickListener(v1 -> cameraIntent());
        layoutFile.setOnClickListener(v1 -> fileIntent(config.getFileMimeType()));

        onPressedColor=config.getOnPressedColor()==0?ContextCompat.getColor(config.getActivity(), R.color.colorPrimary):config.getOnPressedColor();
        onTouchUpColor=config.getOnTouchUpColor()==0?ContextCompat.getColor(config.getActivity(), R.color.colorAccent):config.getOnTouchUpColor();

        @SuppressLint("ClickableViewAccessibility") View.OnTouchListener onTouchListener = (v14, event) -> {
            switch (event.getAction() & MotionEvent.ACTION_MASK) {
                case MotionEvent.ACTION_DOWN:
//                        imgGallery.setColorFilter(onPressedColor, android.graphics.PorterDuff.Mode.SRC_IN);
//                        txtGallery.setTextColor(onPressedColor);
//                        Utilities.setDrawableTint(onPressedColor,layoutGallery.getBackground());
                    if (v14 == layoutGallery)
                        setColor(imgGallery, txtGallery, layoutGallery, true);
                    else if (v14 == layoutCamera)
                        setColor(imgCamera, txtCamera, layoutCamera, true);
                    else if (v14 == layoutFile)
                        setColor(imgFile, txtFile, layoutFile, true);

                    break;
                case MotionEvent.ACTION_UP:
//                        if (v == layoutGallery)
                    setColor(imgGallery, txtGallery, layoutGallery, false);
//                        else if (v == layoutCamera)
                    setColor(imgCamera, txtCamera, layoutCamera, false);
                    setColor(imgFile, txtFile, layoutFile, false);
//                        else
                    break;
            }
            return false;
        };
        layoutGallery.setOnTouchListener(onTouchListener);
        layoutCamera.setOnTouchListener(onTouchListener);
        layoutFile.setOnTouchListener(onTouchListener);

        v.findViewById(R.id.btnCancel).setOnClickListener(v13 -> dismissDialog());
        bottomSheetDialog.setContentView(v);
    }



    private void setColor(ImageView imageView, TextView textView, View layout, boolean isSelected) {
        imageView.setColorFilter(isSelected ? Color.WHITE : onTouchUpColor, android.graphics.PorterDuff.Mode.SRC_IN);
        textView.setTextColor(isSelected ? onPressedColor : onTouchUpColor);
        layout.setBackgroundResource(isSelected ? onPressedBack:onTouchUpBack);
        if (isSelected)
            jpt.sfw.app.Utilities.setDrawableTint(onPressedColor, layout.getBackground());
    }


    public String getSelectedPath() {
        return uploadImagePath;
    }

    public boolean isFilePicked() {
        return !TextUtils.isEmpty(uploadImagePath);
    }

    public String getPickedFileExtension() {
        if (uploadImagePath != null && uploadImagePath.length() > 2) {
            int dotIndex = uploadImagePath.lastIndexOf(".");
            if (dotIndex > -1) {
                return uploadImagePath.substring(dotIndex + 1);
            }
        }
        return "";
    }


    public void galleryIntent() {
        try {
            if (Utilities.hasPermission(config.getActivity(), config.getPermissionStorage(), new String[]{Manifest.permission.WRITE_EXTERNAL_STORAGE}, true)) {
                if(onIntentCalledForGalleryOrCamera(true))
                    return;
                Intent intent = new Intent();
                intent.setType("image/*");
                intent.setAction(Intent.ACTION_GET_CONTENT);
                if (config.getFragment() != null )
                    config.getActivity().startActivityFromFragment(config.getFragment(), Intent.createChooser(intent, config.getActivity().getString(R.string.info_choose_file_upload)), config.getRequestGallery());
                else
                    config.getActivity().startActivityForResult(Intent.createChooser(intent, config.getActivity().getString(R.string.info_choose_file_upload)), config.getRequestGallery());
            } else {
                log("galleryIntent : Permission denied",null);
            }
        } catch (Throwable e) {
                log("galleryIntent",e);
            try {
                if (config.getActivity() instanceof SfwActivityInterface)
                    ((SfwActivityInterface) config.getActivity()).toast(R.string.info_no_apps_handle);
                else
                    Toast.makeText(config.getActivity(), R.string.info_no_apps_handle, Toast.LENGTH_SHORT).show();
            } catch (Exception ignored) {
            }

        }
        dismissDialog();

    }



    public void cameraIntent() {
        try {
            if (Utilities.hasPermission(config.getActivity(), config.getPermissionCamera(), new String[]{Manifest.permission.CAMERA, Manifest.permission.WRITE_EXTERNAL_STORAGE}, true)) {

                if (Build.VERSION.SDK_INT > Build.VERSION_CODES.M) {
                    try {
                        Method m = StrictMode.class.getMethod("disableDeathOnFileUriExposure");
                        m.setAccessible(true);
                        m.invoke(null);
                    } catch (Exception e) {
                    }
                }
                if(onIntentCalledForGalleryOrCamera(false))
                    return;
                Intent intent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
                fileUri = Utilities.getOutputMediaFileUri(config.getActivity());
                intent.putExtra(MediaStore.EXTRA_OUTPUT, fileUri);
                if (config.getFragment()!=null)
                    config.getActivity().startActivityFromFragment(config.getFragment(), intent, config.getRequestCamera());
                else
                    config.getActivity().startActivityForResult(intent, config.getRequestCamera());
            } else {
                log("cameraIntent : Permission Denied",null);
            }
        } catch (Throwable e) {
            log("cameraIntent",e);
            try {
                Toast.makeText(config.getActivity(), R.string.info_no_apps_handle, Toast.LENGTH_SHORT).show();
            } catch (Exception ignored) {
            }

        }
        dismissDialog();
    }

    /*private void fileIntent() {
        fileIntent("application/*");
    }*/

    public void fileIntent(String mimeType) {
        try {
            if (Utilities.hasPermission(config.getActivity(), config.getPermissionFile(), new String[]{Manifest.permission.WRITE_EXTERNAL_STORAGE}, true)) {
                if(onIntentCalledForFile(mimeType))
                    return;
                Intent i = new Intent();
                i.setType(mimeType);
                i.setAction(Intent.ACTION_GET_CONTENT);
                i.addCategory(Intent.CATEGORY_OPENABLE);
                if (config.getFragment()!=null)
                    config.getActivity().startActivityFromFragment(config.getFragment(), Intent.createChooser(i, config.getActivity().getString(R.string.info_choose_document_upload)), config.getRequestFile());
                else
                    config.getActivity().startActivityForResult(Intent.createChooser(i, config.getActivity().getString(R.string.info_choose_document_upload)), config.getRequestFile());
            } else {
                log("fileIntent : Permission Denied",null);
            }
        } catch (Exception e) {
           log("fileIntent",e);
            try {
                Toast.makeText(config.getActivity(), R.string.info_no_apps_handle, Toast.LENGTH_SHORT).show();
            } catch (Exception ignored1) {
            }
        }
        dismissDialog();
    }

    public boolean onIntentCalledForGalleryOrCamera(boolean isForGallery) {
        return false;
    }

    public boolean onIntentCalledForFile(String mimeType) {
        return false;
    }


    public boolean onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        if (Utilities.isPermissionsGranted(grantResults, permissions.length)) {
            if (requestCode == config.getPermissionCamera()) {
                cameraIntent();
            } else if (requestCode == config.getPermissionStorage()) {
                galleryIntent();
            } else if (requestCode == config.getPermissionFile()) {
                fileIntent(config.getFileMimeType());
            }
        }
        return requestCode == config.getPermissionCamera() || requestCode == config.getPermissionStorage() || requestCode == config.getPermissionFile();
    }


    public final boolean onActivityResult(int requestCode, int resultCode, Intent data) {
        return onActivityResult(requestCode,resultCode,data,true);
    }
    /**
     * You should check {@link #isFilePicked()} before use the last picked file
     */
    public final boolean onActivityResult(int requestCode, int resultCode, Intent data,boolean deleteLastCroppedImagePath) {

        try {
            if (!isRequestCodeValid(requestCode)) {
                return false;
            }
            if (requestCode == config.getRequestUcrop() && deleteLastCroppedImagePath) {
                deleteLastPath();
            }
            if (resultCode == Activity.RESULT_OK) {
                uploadImagePath = null;
                if(onResult(requestCode,data))
                    return true;
                if (requestCode == config.getRequestUcrop()) {
                    final Uri resultUri = UCrop.getOutput(data);
                    uploadImagePath = resultUri.getPath();
                    setImageOnImageView(uploadImagePath);
                    if(config.getActivity()!=null)
                        Utilities.scanMedia(config.getActivity(),uploadImagePath);
                } else if (requestCode == config.getRequestGallery()) {
                    onSelectFromGalleryResult(data);
                } else if (requestCode == config.getRequestCamera()) {
                    onCaptureImageResult(data);
                } else if (requestCode == config.getRequestFile()) {
                    onSelectFile(data);
                }
            }
        } catch (Exception e) {
            log("onActivityResult",e);
        }
        return true;
    }

    public boolean isRequestCodeValid(int requestCode) {
        return requestCode == config.getRequestFile() || requestCode == config.getRequestCamera() || requestCode == config.getRequestGallery() || requestCode == config.getRequestUcrop();
    }

    public boolean onResult(int requestCode,Intent data) {
        return false;
    }

    public void setImageOnImageView(String filePath) {
        if (config.getImageView() != null && !TextUtils.isEmpty(filePath)) {
            ImageUtil.cancelLoading(config.getImageView());
            config.getImageView().setImageURI(null);
            config.getImageView().setImageURI(Uri.fromFile(new File(filePath)));
        }
    }

    private void onCaptureImageResult(Intent data) {
        Uri tempUri = fileUri;
        lastSelectedImagePath = tempUri.getPath();
        cropImage(tempUri);
    }

    private void onSelectFromGalleryResult(Intent data) {

        Bitmap bm;
        if (data != null) {
            try {
                bm = MediaStore.Images.Media.getBitmap(config.getActivity().getContentResolver(), data.getData());
                if(bm!=null) {
                    Uri tempUri = getImageUri(bm, 90);
                    cropImage(tempUri);
                    bm.recycle();
                } else {
                    Toast.makeText(config.getActivity(), "File Not Found", Toast.LENGTH_SHORT).show();
                }
            } catch (Throwable e) {
                log("onSelectFromGalleryResult",e);
                if(e instanceof OutOfMemoryError)
                    try {
                        System.gc();
                        Toast.makeText(config.getActivity(), "File size is too large", Toast.LENGTH_SHORT).show();
                    } catch (Exception ignored) {
                    }
            }
        } else {
            log("onSelectFromGalleryResult : Intent data is null",null);
        }

    }

    private void onSelectFile(Intent data) {

        String path = FileUtils.getPath(config.getActivity(), data.getData());
        List<String> images=Arrays.asList(".jpg",".jpeg",".png");
        if(images.contains(FileUtils.getExtension(path))) {
            cropImage(Uri.fromFile(new File(path)));
            return;
        }

        if (path != null && config.getAllowedFileTypes() != null && config.getAllowedFileTypes().size() > 0) {
            int dotLastIndex = path.lastIndexOf(".");
            String ext = null;
            if (dotLastIndex > 0) {
                ext = path.substring(dotLastIndex + 1).toLowerCase(Locale.ENGLISH);
            }
            if (ext == null || !config.getAllowedFileTypes().contains(ext)) {
                if(config.getActivity()!=null) {
                    Toast.makeText(config.getActivity(), "The selected file type is not supported\nChoose only "+config.getAllowedFileTypes().toString(), Toast.LENGTH_SHORT).show();
                }
                return;
            }
        }
        uploadImagePath = path;
        if(uploadImagePath!=null && config.getFilePickerFolder() !=null) {
//            new Thread(() -> {
                try {
                    List<String> docs=Arrays.asList("xls","pdf","doc","txt","docx","xlsx","ppt", "pptx");
                    String ext=FileUtils.getExtension(path).toLowerCase(Locale.ENGLISH);
                    String prefix=docs.contains(ext)?"DOC_":"FILE_";

                    String fileName=config.getFilePickerFolder()+prefix+AppHelper.createAfileNameUsingTime()+"."+ext;
                    while (new File(fileName).exists())
                        fileName=config.getFilePickerFolder()+prefix+AppHelper.createAfileNameUsingTime()+"."+ext;

                    org.apache.commons.io.FileUtils.copyFile(new File(uploadImagePath), new File(fileName));
                    if(new File(fileName).exists()) {
                        uploadImagePath= fileName;
                        Utilities.scanMedia(config.getActivity(), fileName);
                    } else
                        uploadImagePath=null;
                } catch (Exception ignored) {
                    uploadImagePath=null;
                }
//            }).start();

        }

    }

    @Nullable
    public Uri getImageUri(Bitmap inImage, int quality) {


        try {
            ByteArrayOutputStream bytes = new ByteArrayOutputStream();
            inImage.compress(Bitmap.CompressFormat.JPEG, quality, bytes);
            byte[] by = bytes.toByteArray();
            String imageFilePath=config.getImageFilePath();
            if(!new File(imageFilePath).getParentFile().exists())
                new File(imageFilePath).getParentFile().mkdirs();
            FileOutputStream out = new FileOutputStream(imageFilePath);
            out.write(by);
            out.close();
            return Uri.fromFile(new File(imageFilePath));
        } catch (Throwable e) {
            System.gc();
            log("getImageUri",e);
            if (e instanceof OutOfMemoryError && quality == 100) {
                getImageUri(inImage, 75);
            }
        }
        return null;
    }

    private void cropImage(Uri tempUri) {
        try {

//            showMessage(R.string.tit_error,"Uri="+String.valueOf(tempUri),R.string.tit_ok);
            UCrop crop = UCrop.of(tempUri, Uri.fromFile(new File(config.getImageFilePath())));
            if (config.getRatioWidth() > 0 && config.getRatioHeight() > 0)
                crop.withAspectRatio(config.getRatioWidth(), config.getRatioHeight());
            if (config.getMaxHeight() > 0 && config.getMaxWidth() > 0)
                crop = crop.withMaxResultSize(config.getMaxWidth(), config.getMaxHeight());
            if (config.getFragment()!=null)
                crop.start(config.getActivity(), config.getFragment(), config.getRequestUcrop());
            else if(config.getActivity() instanceof AppCompatActivity)
                crop.start((AppCompatActivity) config.getActivity(), config.getRequestUcrop());
            else
                Toast.makeText(config.getActivity(), "Activity not an instance of AppCompactActivity", Toast.LENGTH_SHORT).show();
        } catch (Exception e) {
            log("cropImage",e);
        }
    }

    public ImageUploadHelper setTag(int tag) {
        this.tag = tag;
        return this;
    }

    public int getTag() {
        return tag;
    }


    public boolean isAUCropRequestCode(int requestCode) {
        return requestCode==config.getRequestUcrop();
    }

    public ImageUploadHelper setConfig(@NonNull ImageUploadConfig config) {
        this.config = config;
        return this;
    }

    public Uri getFileUri() {
        return fileUri;
    }

    public ImageUploadHelper setFileUri(Uri fileUri) {
        this.fileUri = fileUri;
        return this;
    }

    public String getUploadImagePath() {
        return uploadImagePath;
    }

    public ImageUploadHelper setUploadImagePath(String uploadImagePath) {
        this.uploadImagePath = uploadImagePath;
        return this;
    }

    public String getLastSelectedImagePath() {
        return lastSelectedImagePath;
    }

    public ImageUploadHelper setLastSelectedImagePath(String lastSelectedImagePath) {
        this.lastSelectedImagePath = lastSelectedImagePath;
        return this;
    }

    public int getOnPressedColor() {
        return onPressedColor;
    }

    public ImageUploadHelper setOnPressedColor(int onPressedColor) {
        this.onPressedColor = onPressedColor;
        return this;
    }

    public int getOnTouchUpColor() {
        return onTouchUpColor;
    }

    public ImageUploadHelper setOnTouchUpColor(int onTouchUpColor) {
        this.onTouchUpColor = onTouchUpColor;
        return this;
    }

    public ImageUploadHelper setPressedBackground(@DrawableRes int drawableId) {
        if(drawableId!=0) {
            this.onPressedBack = drawableId;
        }
        return this;
    }

    public ImageUploadHelper setNormalBackground(@DrawableRes int drawableId) {
        if(drawableId!=0) {
            this.onTouchUpBack = drawableId;
        }
        return this;
    }

    private void invalidateBackground() {
        if(imgGallery!=null) {
            setColor(imgGallery, txtGallery, layoutGallery, false);
            setColor(imgFile, txtFile, layoutFile, false);
            setColor(imgCamera, txtCamera, layoutCamera, false);
        }
    }

    public void log(@NonNull String content, @Nullable  Throwable t) {
        if(isDebuggingEnabled)
            Log.e(TAG,content,t);
    }




}
