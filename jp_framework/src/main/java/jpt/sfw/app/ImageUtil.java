package jpt.sfw.app;

import android.app.Activity;
import android.content.Context;
import android.graphics.Bitmap;
import android.text.TextUtils;
import android.util.Log;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;

import com.bumptech.glide.Glide;
import com.bumptech.glide.RequestManager;
import com.bumptech.glide.load.DataSource;
import com.bumptech.glide.load.Transformation;
import com.bumptech.glide.load.engine.GlideException;
import com.bumptech.glide.request.RequestListener;
import com.bumptech.glide.request.RequestOptions;
import com.bumptech.glide.request.target.Target;

import jpt.sfw.R;
import jpt.sfw.interfaces.ImageLoadingListener;

import java.lang.ref.PhantomReference;
import java.lang.ref.ReferenceQueue;
import java.lang.ref.SoftReference;

import androidx.annotation.DrawableRes;
import androidx.annotation.NonNull;
import androidx.annotation.Nullable;

import jp.wasabeef.glide.transformations.RoundedCornersTransformation;


public class ImageUtil {
    public static final int SCALE_FIT = 1;
    public static final int SCALE_NONE = 0;
    public static final int SCALE_CENTER_CROP = 2;
    public static final int SCALE_CENTER_INSIDE = 3;
    private static final String TAG = "ImageUtil";

    private static int screenWidth = 0;
    private static int screenHeight = 0;
    private static boolean needLogging;

    private static RequestManager requestManager;


    public static void init(Activity activity) {
        try {
            if (screenWidth == 0) {
                int[] wh = Utilities.getScreenResolution(activity);
                screenHeight = wh[1];
                screenWidth = wh[0];
                requestManager = Glide.with(activity.getApplicationContext());
            }
        } catch (Exception ignored) {
        }
    }

    public static void setNeedLogging(boolean needLogging) {
        ImageUtil.needLogging = needLogging;
    }

    public static void loadImage(Config config) {
        try {
            loadImage(new SoftReference<>(config));
        } catch (Exception e) {
            if (needLogging)
                Log.e(TAG, "loadImage", e);
        }
    }

    private static void loadImage(@NonNull final SoftReference<Config> configuration) {
        try {
            Config config = configuration.get();
            if (needLogging) {
                if (config == null)
                    Log.e(TAG, "loadImage : Configuration should not be null.");
                else if (TextUtils.isEmpty(config.getUrl()))
                    Log.e(TAG, "loadImage : Image url should not be empty.");
//                else if (config.getImageView() == null)
//                    Log.e(TAG, "loadImage : ImageView  should not be null. (Url -> " + config.getUrl() + ")");
            }
//            if (config == null || TextUtils.isEmpty(config.getUrl()) || config.getImageView() == null) {
            if (config == null || TextUtils.isEmpty(config.getUrl())) {
                if (config != null && (config.getPlaceHolder() != 0 || config.getErrorHolder() != 0) && config.getImageView() != null)
                    config.getImageView().setImageResource(config.getErrorHolder() != 0 ? config.getErrorHolder() : config.getPlaceHolder());
                return;
            }
            RequestOptions options = new RequestOptions();

            if (config.getErrorHolder() != 0)
                options.error(config.getErrorHolder());
            if (config.getPlaceHolder() != 0)
                options.placeholder(config.getPlaceHolder());
            if (config.getImageView() != null)
                switch (config.getScaleType()) {
                    case SCALE_FIT:
                        config.getImageView().setScaleType(ImageView.ScaleType.FIT_XY);//options.imageScaleType(ImageScaleType.EXACTLY);
                        break;
                    case SCALE_CENTER_CROP:
                        options.centerCrop();//requestCreator.centerCrop();
                        break;
                    case SCALE_CENTER_INSIDE:
                        options.centerInside();//requestCreator.centerInside();
                        break;
                }
            if (!config.isEnableAnimation())
                options = options.dontAnimate();
            if (config.getTransformation() != null)
                options = options.transform(config.getTransformation());
            if (config.isUseRoundTransform())
                options.apply(RequestOptions.circleCropTransform());

            try {
                if (config.getListener() != null)
                    config.getListener().onLoadingStarted(config.getUrl(), config.getImageView());
                showHideView(config.getLoader(), true);
                if (config.getImageView() != null && (config.getViewWidth() > 0 || config.getViewHeight() > 0)) {
                    ViewGroup.LayoutParams lp = config.getImageView().getLayoutParams();
                    if (config.getViewWidth() > 0)
                        lp.width = config.getViewWidth();
                    if (config.getViewHeight() > 0)
                        lp.height = config.getViewHeight();
                    config.getImageView().setLayoutParams(lp);
                    if (config.isSetParentViewBoundsAsSame()) {
                        View parent = ((View) config.getImageView().getParent());
                        ViewGroup.LayoutParams lp1 = parent.getLayoutParams();
                        int paddingBottom = parent.getPaddingBottom();
                        int topPadding = parent.getPaddingTop();
                        lp1.width = config.getViewWidth();
                        if (config.getViewHeight() > 0)
                            lp1.height = config.getViewHeight() + paddingBottom + topPadding;
                        parent.setLayoutParams(lp1);
                    }
                }
            } catch (Throwable ignored) {
            }
            RequestListener<Bitmap> listener;

            ImageLoadingListener imageLoadingListener;
            //SoftReference<Config> configuration does not hold the reference if the imageview is null.
            // But we need to hold the reference to the listener to notify
            // So imageLoadingListener variable will hold a strong reference to the listener .
            if (config.getImageView() == null && config.getListener() != null)
                imageLoadingListener = config.getListener();
            else
                imageLoadingListener = null;

//            if ((config.loader != null || config.viewWidth > 0 || config.listener!=null)) {
            listener = new RequestListener<Bitmap>() {
                @Override
                public boolean onLoadFailed(@Nullable GlideException e, Object model, Target<Bitmap> target, boolean isFirstResource) {
                    boolean isTrue = configuration.get() == null || configuration.get().getImageView() == null;
                    try {
                        if (needLogging)
                            Log.e(TAG, "loadImage Failed", e);
                        if (configuration.get() == null && imageLoadingListener == null) {
                            if (needLogging)
                                Log.e(TAG, "loadImage Failed : configuration destroyed. Cannot notify listener", e);
                            return false;
                        }
                        if (configuration.get() != null && configuration.get().getListener() != null)
                            configuration.get().getListener().onLoadingFailed(configuration.get().getUrl(), configuration.get().getImageView(), e);
                        else if (imageLoadingListener != null)
                            imageLoadingListener.onLoadingFailed(model.toString(), null, e);
                        if (configuration.get() != null)
                            showHideView(configuration.get().getLoader(), false);
                    } catch (Throwable e1) {
                        if (needLogging)
                            Log.e(TAG, "loadImage  Failed", e1);
                    }
                    configuration.clear();

                    return isTrue;
                }

                @Override
                public boolean onResourceReady(Bitmap resource, Object model, Target<Bitmap> target, DataSource dataSource, boolean isFirstResource) {
                    boolean isTrue = configuration.get() == null || configuration.get().getImageView() == null;
                    try {
                        if (needLogging)
                            Log.e(TAG, "loadImage : onResourceReady : " + model);
                        if (configuration.get() == null && imageLoadingListener == null) {
                            if (needLogging)
                                Log.e(TAG, "loadImage : onResourceReady : configuration destroyed. Cannot notify listener.  Url : " + model);
                            return false;
                        }
                        if (configuration.get() != null && configuration.get().getListener() != null)
                            configuration.get().getListener().onLoadingComplete(configuration.get().getUrl(), configuration.get().getImageView(), resource);
                        else if (imageLoadingListener != null)
                            imageLoadingListener.onLoadingComplete(model.toString(), null, resource);
                        if (configuration.get() != null)
                            showHideView(configuration.get().getLoader(), false);

                    } catch (Throwable e) {
                        if (needLogging)
                            Log.e(TAG, "loadImage : onResourceReady : " + model, e);
                    }
                    configuration.clear();

                    return isTrue;
                }
            };
            if (requestManager == null) {
                Context ctx = config.getContext() != null ? config.getContext() : config.getImageView() == null ? null : config.getImageView().getContext().getApplicationContext();
                if (ctx == null) {
                    Log.e(TAG, "loadImage : Either Context or ImageView must not be null : url => " + config.getUrl());
                    return;
                }
                requestManager = Glide.with(ctx.getApplicationContext());
            }
            if (config.getImageView() != null)
                requestManager.asBitmap().apply(options).listener(listener).load(config.getUrl()).into(config.getImageView());
            else
                requestManager.asBitmap().apply(options).listener(listener).load(config.getUrl()).preload();
        } catch (Throwable e) {
            if (needLogging)
                Log.e(TAG, "loadImage", e);
        }
    }


    public static void loadResourceForBlur(@DrawableRes int drawableId, @NonNull ImageView view) {
        loadResourceWithOptions(drawableId, view, new RequestOptions().transform(new jp.wasabeef.glide.transformations.BlurTransformation(5, 1)));
    }

    public static void loadResourceImage(@DrawableRes int drawableId, @NonNull ImageView view) {
        loadResourceWithOptions(drawableId, view, null);
    }

    public static void loadResourceWithOptions(@DrawableRes int drawableId, @NonNull ImageView view, RequestOptions options) {

        if (drawableId != 0 && view != null) {

            try {
                if (options == null)
                    Glide.with(view.getContext()).asBitmap().load(drawableId).into(view);
                else
                    Glide.with(view.getContext()).asBitmap().apply(options).load(drawableId).into(view);
            } catch (Exception e) {
                if (needLogging)
                    Log.e(TAG, "loadResource", e);
            }
        } else if (needLogging)
            Log.e(TAG, "loadImage : drawableId != 0 && ImageView != null");
    }


    private static void showHideView(View v, boolean show) {
        try {
            if (v != null)
                v.setVisibility(show ? View.VISIBLE : View.GONE);
        } catch (Exception e) {
        }
    }

    public static void cancelLoading(@NonNull ImageView imageView) {
        try {
            Glide.with(imageView.getContext()).clear(imageView);
        } catch (Exception ignored) {
        }
    }


    public static class Config {
        private String url;
        private int placeHolder = 0;
        private int errorHolder = 0;
        private int scaleType = SCALE_NONE;
        private int viewHeight = 0, viewWidth = 0, marginLeft = 0, marginRight = 0;
        private int resizeWidth, resizeHeight;
        private boolean setParentViewBoundsAsSame = false;
        private ImageView imageView;
        private View loader;
        private ImageLoadingListener listener;
        private com.bumptech.glide.load.Transformation<Bitmap> transformation;
        private boolean enableAnimation;
        private Context context;
        boolean useRoundTransform;

        public Config(@NonNull String url, @NonNull ImageView imageView) {
            this.url = url;
            this.imageView = imageView;
        }

        public Config(@NonNull String url, @NonNull Context context) {
            this.url = url;
            this.context = context.getApplicationContext();
        }

        public String getUrl() {
            return url;
        }

        public int getPlaceHolder() {
            return placeHolder;
        }

        public int getErrorHolder() {
            return errorHolder;
        }

        public int getScaleType() {
            return scaleType;
        }

        public ImageView getImageView() {
            return imageView;
        }

        public View getLoader() {
            return loader;
        }


        public Config setContext(@Nullable Context context) {
            this.context = context == null ? null : context.getApplicationContext();
            return this;
        }

        protected Context getContext() {
            return context;
        }

        public Config setUrl(String url) {
            this.url = url;
            return this;
        }

        public Config setPlaceHolder(int placeHolder) {
            this.placeHolder = placeHolder;
            return this;
        }

        public Config setErrorHolder(int errorHolder) {
            this.errorHolder = errorHolder;
            return this;
        }

        public Config setScaleType(int scaleType) {
            this.scaleType = scaleType;
            return this;
        }

        public Config setImageView(ImageView imageView) {
            this.imageView = imageView;
            return this;
        }

        public Config setLoader(View loader) {
            this.loader = loader;
            return this;
        }


        /**
         * Better use {@link #setUseRoundTransform(boolean)}
         */
        public Config useRoundTransform(Context ctx) {
            return useRoundTransform(ctx.getResources().getDimensionPixelSize(R.dimen.common_margin));
        }

        /**
         * Better use {@link #setUseRoundTransform(boolean)}
         */
        public Config useRoundTransform(int radius) {
            return setTransformation(new RoundedCornersTransformation(radius, 0));
        }

        public boolean isUseRoundTransform() {
            return useRoundTransform;
        }

        public Config setUseRoundTransform(boolean useRoundTransform) {
            this.useRoundTransform = useRoundTransform;
            return this;
        }

        public int getViewHeight() {
            return viewHeight;
        }

        public Config setViewHeight(int viewHeight) {
            this.viewHeight = viewHeight;
            return this;
        }

        public Config resize(int width, int height) {
            this.resizeWidth = width;
            this.resizeHeight = height;
            return this;
        }

        public int getViewWidth() {
            return viewWidth;
        }

        public Config setViewWidth(int viewWidth) {
            this.viewWidth = viewWidth;
            return this;
        }

        public Config setViewWidth(boolean useScreenWidth, int leftmargin, int rightMargin) {
            if (useScreenWidth) {
                this.viewWidth = screenWidth;
            }
            if (leftmargin > -1)
                setMarginLeft(leftmargin);
            else if (imageView != null) {
                this.viewWidth = this.viewWidth - imageView.getLeft();
            }
            if (rightMargin > -1)
                setMarginRight(rightMargin);
            else if (imageView != null) {
                this.viewWidth = this.viewWidth - imageView.getRight();
            }
            return this;
        }

        public int getMarginLeft() {
            return marginLeft;
        }

        public Config setMarginLeft(int marginLeft) {
            this.marginLeft = marginLeft;
            if (this.viewWidth > 0)
                this.viewWidth -= marginLeft;
            return this;
        }

        public int getMarginRight() {
            return marginRight;
        }

        public Config setMarginRight(int marginRight) {
            this.marginRight = marginRight;
            if (this.viewWidth > 0)
                this.viewWidth -= marginRight;
            return this;
        }

        public ImageLoadingListener getListener() {
            return listener;
        }

        public Config setListener(ImageLoadingListener listener) {
            this.listener = listener;
            return this;
        }

        public boolean isSetParentViewBoundsAsSame() {
            return setParentViewBoundsAsSame;
        }

        public Config setSetParentViewBoundsAsSame(boolean setParentViewBoundsAsSame) {
            this.setParentViewBoundsAsSame = setParentViewBoundsAsSame;
            return this;
        }

        public int getResizeWidth() {
            return resizeWidth;
        }

        public Config setResizeWidth(int resizeWidth) {
            this.resizeWidth = resizeWidth;
            return this;
        }

        public int getResizeHeight() {
            return resizeHeight;
        }

        public Config setResizeHeight(int resizeHeight) {
            this.resizeHeight = resizeHeight;
            return this;
        }

        public Transformation<Bitmap> getTransformation() {
            return transformation;
        }

        public Config setTransformation(Transformation<Bitmap> transformation) {
            this.transformation = transformation;
            return this;
        }

        public boolean isEnableAnimation() {
            return enableAnimation;
        }

        public Config setEnableAnimation(boolean enableAnimation) {
            this.enableAnimation = enableAnimation;
            return this;
        }
    }
}
