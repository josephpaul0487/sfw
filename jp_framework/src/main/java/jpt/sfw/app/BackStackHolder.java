package jpt.sfw.app;

import android.util.Log;
import android.view.View;

import java.util.ArrayList;
import java.util.List;

import androidx.annotation.NonNull;
import androidx.fragment.app.Fragment;

import jpt.sfw.SfwActivity;

public class BackStackHolder {
    private final ArrayList<FragmentContentHolder> holder = new ArrayList<>();
    private final ArrayList<String> tags = new ArrayList<>();
    private final String TAG = "BackStack";


    public int put(Fragment fragment, FragmentConfig config) {
        return put(fragment, config, null);
    }

    /**
     * @param fragment the fragment to be change
     * @param config   fragment configuration
     *                 #@return Index of the fragment if already placed in the backstack else -1 .
     *                 It will help to restore the fragment position if the fragment transaction failed on {@link jpt.sfw.interfaces.SfwActivityInterface#changeFragment(Fragment, FragmentConfig)}
     */
    public int put(Fragment fragment, FragmentConfig config, View view) {
        return put(-1, config.getTag(), new FragmentContentHolder(fragment, config, view));
        /*//Check the fragment already placed in the backstack
        int index = tags.indexOf(config.tag);
        if (index > -1) {
            //Popup from the backstack
            FragmentContentHolder temp = holder.get(index);
            remove(index);
            tags.add(config.tag);
            holder.add(new FragmentContentHolder(fragment, config, view==null?temp.view:view));
        } else {
            //Add to backstack
            holder.add(new FragmentContentHolder(fragment, config, view));
            tags.add(config.tag);
        }
        return index;*/
    }

    /**
     * Used on {@link jpt.sfw.interfaces.SfwActivityInterface#changeFragment(Fragment, FragmentConfig)} to restore previous holder wheb the fragment transaction failed
     *
     * @param index          to which index
     * @param fragmentHolder previous holder
     * @return true if the holder replaced successfully
     */
    public boolean putOnPosition(int index, FragmentContentHolder fragmentHolder) {
        return put(index, fragmentHolder.config.getTag(), fragmentHolder) > -1;
        /*if(fragmentHolder!=null && index>-1 && holder.size()>index) {
            int previousIndex=tags.indexOf(fragmentHolder.config.tag);
            if(previousIndex>-1)  {
                tags.remove(previousIndex);
                holder.remove(previousIndex);
            }
            tags.add(fragmentHolder.config.tag);
            holder.add(index, fragmentHolder);

            return true;
        }
        return false;*/
    }

    /**
     * will not remove from backstack if the tag is on top
     *
     * @param tagToBeRemoved the fragment that need to remove from the backstack
     * @return true->If removed else false
     */
    public boolean removeFromBackstack(@NonNull String tagToBeRemoved) {
        return removeFromBackstack(tagToBeRemoved, false) > -1;
    }

    /**
     * will not remove from backstack if the tag is on top and forceRemove is false
     *
     * @param tagToBeRemoved the fragment that need to remove from the backstack
     * @param forceRemove    will remove the top one also
     * @return true->If removed else false
     */
    public int removeFromBackstack(@NonNull String tagToBeRemoved, boolean forceRemove) {
        if (!forceRemove && isTop(tagToBeRemoved)) {
            return -1;
        }
        int index = tags.indexOf(tagToBeRemoved);
        remove(index);
        return index;
    }

    public FragmentContentHolder getHolder(int index) {
        if (index > -1 && index < holder.size()) {
            return holder.get(index);
        }
        return null;
    }

    public boolean isTop(String tag) {
        return tags.indexOf(tag) == (tags.size() - 1);
    }

    /**
     * @param tagToBeRetained clear all fragment except this
     * @return true->If this retained  else false
     */
    public boolean clearAllExcept(String tagToBeRetained) {
        int index = tags.indexOf(tagToBeRetained);
        if (index > -1) {
            //Popup from the backstack
            FragmentContentHolder temp = holder.get(index);
            holder.clear();
            tags.clear();
            put(-1, tagToBeRetained, temp);
            /*tags.add(tagToBeRetained);
            holder.add(temp);*/
        } else {
            holder.clear();
            tags.clear();
        }
        return index > -1;
    }

    //to get the old view of the fragment from the backstack
    public View getView(String tag) {
        int index = tags.indexOf(tag);
        return index > -1 ? holder.get(index).view : null;
    }

    //to get the old view of the fragment from the backstack
    public FragmentConfig getConfig(String tag) {
        int index = tags.indexOf(tag);
        return index > -1 ? holder.get(index).config : null;
    }

    public void setView(View v, String tag) {
        FragmentContentHolder fragmentContentHolder = getHolder(tag);
        if (fragmentContentHolder != null) {
            fragmentContentHolder.setView(v);
        }
    }

    public void setData(Object data, String tag) {
        FragmentContentHolder fragmentContentHolder = getHolder(tag);
        if (fragmentContentHolder != null) {
            fragmentContentHolder.setData(data);
        }
    }

    //clear all from the backstack : Please call it on Activity#onDestroy()
    public void clearAll() {
        holder.clear();
        tags.clear();
    }

    public int getBackStackCount() {
        return tags.size();
    }

    //called from the Activity.onBackPressed()
    public FragmentContentHolder removeTop() {
        return removeTop(1);
    }

    //called from the Activity.onBackPressed()
    public FragmentContentHolder removeUpTo(int upTo) {
        return remove(tags.size() - 1, tags.size() - upTo, true);
    }

    public FragmentContentHolder removeUpTo(String fragmentTagUpTo, boolean includeThis) {
        return removeUpTo(tags.indexOf(fragmentTagUpTo) + (includeThis ? 0 : 1));
    }

    //You cannot remove bottom fragment.. At least one required
    public FragmentContentHolder remove(int startIndex, int count, boolean topToBottom) {
        if (count < tags.size() && ((topToBottom && startIndex <= tags.size() && startIndex > -1) || (!topToBottom && startIndex + count <= tags.size()))) {
            int index = topToBottom ? startIndex - count + 1 : startIndex;
            List<String> t = new ArrayList<>(tags.subList(0, index));
            List<FragmentContentHolder> fragmentContentHolders = new ArrayList<>(holder.subList(0, index));
            if (tags.size() > index + count) {
                t.addAll(tags.subList(index + count, tags.size()));
                fragmentContentHolders.addAll(holder.subList(index + count, holder.size()));
            }

            this.tags.clear();
            this.tags.addAll(t);
            this.holder.clear();
            this.holder.addAll(fragmentContentHolders);
            if (holder.size() > 0)
                return holder.get(holder.size() - 1);
        } else if (topToBottom) {
            Sfw.log(TAG, "Conditions not met.     count < backStackSize && startIndex <= backStackSize && startIndex > -1 :  startIndex = " + startIndex + "  count = " + count, null, true);
        } else {
            Sfw.log(TAG, "Conditions not met.     count < backStackSize && startIndex + count <= backStackSize :  startIndex = " + startIndex + "  count = " + count, null, true);
        }
        return null;
    }

    public FragmentContentHolder removeTop(int count) {
        return removeUpTo(tags.size() - count);
    }

    public FragmentContentHolder removeBottom(int count) {
        return remove(0, count, false);
    }

    public FragmentContentHolder getLastHolder() {
        return holder.size() > 0 ? holder.get(holder.size() - 1) : null;
    }

    public FragmentContentHolder getHolder(String tag) {
        if (tag == null)
            return null;
        int index = tags.indexOf(tag);
        if (index > -1) {
            return holder.get(index);
        }
        return null;
    }

    public Fragment getFragment(String tag) {
        if (tag == null)
            return null;
        int index = tags.indexOf(tag);
        if (index > -1) {
            return holder.get(index).getFragment();
        }
        return null;
    }

    //return the index of the tag in the backstack
    private int put(int index, String tag, FragmentContentHolder fragmentContentHolder) {
        int tagIndex = tags.indexOf(tag);
        FragmentContentHolder temp = null;
        if (tagIndex > -1) {
            temp = holder.get(tagIndex);
        }
        if (index > -1 && index == tagIndex) {
            holder.set(index, fragmentContentHolder.setView(fragmentContentHolder.view != null || temp == null ? fragmentContentHolder.view : temp.view));
            return -1;
        }

        remove(tagIndex);
        if (index > -1) {
            int i = tagIndex < index ? index - 1 : index;
            tags.add(i, tag);
            holder.add(i, fragmentContentHolder.setView(fragmentContentHolder.view != null || temp == null ? fragmentContentHolder.view : temp.view));
        } else {
            tags.add(tag);
            holder.add(fragmentContentHolder.setView(fragmentContentHolder.view != null || temp == null ? fragmentContentHolder.view : temp.view));
        }
        return tagIndex;
    }

    public boolean remove(int index) {
        if (index > -1 && index < tags.size()) {
            tags.remove(index);
            return holder.remove(index) != null;
        }
        return false;
    }

    public int getIndex(String fragmentTag) {
        return tags.indexOf(fragmentTag);
    }

    public boolean isTop(String fragmentTagToCheck, String fragmentToCompare) {
        return tags.indexOf(fragmentTagToCheck) > tags.indexOf(fragmentToCompare);
    }

    public class FragmentContentHolder {
        Fragment fragment;
        FragmentConfig config;
        View view;

        FragmentContentHolder(Fragment fragment, FragmentConfig config, View view) {
            this.fragment = fragment;
            this.config = config;
            this.view = view;
        }

        public Fragment getFragment() {
            return fragment;
        }

        public FragmentConfig getConfig() {
            return config;
        }

        public View getView() {
            return view;
        }

        public FragmentContentHolder setView(View view) {
            this.view = view;
            return this;
        }

        public FragmentContentHolder setData(Object data) {
            if (this.config != null) {
                this.config.setData(data);
            }
            return this;
        }
    }
}
