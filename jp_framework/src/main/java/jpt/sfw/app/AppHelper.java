package jpt.sfw.app;

import android.Manifest;
import android.app.Activity;
import android.app.DatePickerDialog;
import android.content.Context;
import android.content.Intent;
import android.content.res.ColorStateList;
import android.content.res.XmlResourceParser;
import android.graphics.Bitmap;
import android.graphics.drawable.BitmapDrawable;
import android.net.Uri;
import android.os.Environment;
import android.text.InputType;
import android.text.TextUtils;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;

import jpt.sfw.R;
import jpt.sfw.interfaces.DatePickerListener;
import jpt.sfw.models.NameValueModel;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileOutputStream;
import java.text.DecimalFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import java.util.Locale;
import java.util.TimeZone;

import androidx.annotation.DimenRes;
import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.annotation.XmlRes;


public class AppHelper {


    public static Calendar getCalendar() {
//        Calendar c=Calendar.getInstance();
//        c.setTimeZone(TimeZone.);
        return Calendar.getInstance();
    }

    public static DatePickerDialog pickDate(@Nullable DatePickerDialog datePickerDialog, @NonNull Activity activity, @Nullable String currentDate, String toDateFormat, @Nullable TextView txtDate) {
        return pickDate(datePickerDialog,activity,currentDate,toDateFormat,txtDate);
    }

    public static DatePickerDialog pickDate(@Nullable DatePickerDialog datePickerDialog, @NonNull Activity activity, @Nullable String currentDate, String toDateFormat, @Nullable TextView txtDate, @Nullable DatePickerListener listener) {
        Calendar c = AppHelper.getCalendar();
        if (!TextUtils.isEmpty(currentDate)) {
            try {
                Date start = AppHelper.parseStringToDate(currentDate, toDateFormat);
                c.setTime(start);
            } catch (Exception ignored) {
            }

        }
        if (datePickerDialog == null) {
            DatePickerDialog pickerDialog = new DatePickerDialog(activity, R.style.SFW_datePicker, (view, year, month, dayOfMonth) -> {
                if (txtDate != null || listener != null) {
                    String parseDate = formatDateToOtherFormat("dd-MM-yyyy", toDateFormat, dayOfMonth + "-" + (month + 1) + "-" + year);
                    if (txtDate != null)
                        txtDate.setText(parseDate);
                    if (listener != null)
                        listener.afterPickDate(toDateFormat, parseDate, year, month + 1, dayOfMonth);
                }
            }, c.get(Calendar.YEAR), c.get(Calendar.MONTH), c.get(Calendar.DATE));
            pickerDialog.show();
            return pickerDialog;
        } else {
            try {
                if (!TextUtils.isEmpty(currentDate)) {
                    datePickerDialog.updateDate(c.get(Calendar.YEAR), c.get(Calendar.MONTH), c.get(Calendar.DATE));
                }
            } catch (Exception ignored) {
            }
        }

        datePickerDialog.show();
        return datePickerDialog;
    }


    public static SimpleDateFormat getDateFormat() {
        return getUtcDateFormat(getWebDateTimeFormat());
    }

    public static SimpleDateFormat getUtcDateFormat(String dateFormat) {
        SimpleDateFormat simpleDateFormat = new SimpleDateFormat(dateFormat, Locale.ENGLISH);
        simpleDateFormat.setTimeZone(TimeZone.getTimeZone("UTC"));
        return simpleDateFormat;
    }

    public static String getTime(int seconds) {
        if (seconds > 2599)
            return String.format(Locale.ENGLISH, "%02d:%02d:%02d", seconds / 3600, (seconds % 3600) / 60, seconds % 60);
        return String.format(Locale.ENGLISH, "%02d:%02d", seconds / 60, seconds % 60);
    }

    public static String getWebDateTimeFormat() {
        return "yyyy-MM-dd H:m:s";
    }

    public static String getWebDateFormat() {
        return "yyyy-MM-dd";
    }

    public static String parseDob(@NonNull String dob) {
        return formatDateToOtherFormat("yyyy-MM-dd", "dd-MMM-yyyy", dob);
    }

    public static String formatDateToOtherFormat(@NonNull String fromFormat, @NonNull String toFormat, @NonNull String date) {
        return formatDateToOtherFormat(fromFormat, toFormat, date, false);
    }

    public static String formatDateToOtherFormat(@NonNull String fromFormat, @NonNull String toFormat, @NonNull String date, boolean webToLocal) {
        try {
            if (TextUtils.isEmpty(date))
                return "";
            SimpleDateFormat dobInFormat = new SimpleDateFormat(fromFormat, Locale.ENGLISH);
            SimpleDateFormat dobOutFormat = new SimpleDateFormat(toFormat, Locale.ENGLISH);
            if (webToLocal)
                dobInFormat.setTimeZone(TimeZone.getTimeZone("UTC"));
            dobOutFormat.setTimeZone(TimeZone.getDefault());
            return dobOutFormat.format(dobInFormat.parse(date));
        } catch (Exception ignored) {
        }
        return date;
    }

    public static String formatLocalDateToUtcTimezone(@NonNull String fromFormat, @NonNull String toFormat, @NonNull String date) {
        try {
            SimpleDateFormat dobInFormat = new SimpleDateFormat(fromFormat, Locale.ENGLISH);
            dobInFormat.setTimeZone(TimeZone.getDefault());
            SimpleDateFormat dobOutFormat = new SimpleDateFormat(toFormat, Locale.ENGLISH);
            dobOutFormat.setTimeZone(TimeZone.getTimeZone("UTC"));
            return dobOutFormat.format(dobInFormat.parse(date));
        } catch (Exception ignored) {
        }
        return date;
    }

    public static String formatLocalDateToUtcTimezone(@NonNull String toFormat, @NonNull Date date) {
        try {
            SimpleDateFormat dobOutFormat = new SimpleDateFormat(toFormat, Locale.ENGLISH);
            dobOutFormat.setTimeZone(TimeZone.getTimeZone("UTC"));
            return dobOutFormat.format(date);
        } catch (Exception ignored) {
        }
        return "";
    }

    public static String formatDateToString(@NonNull String toFormat, @NonNull Date date) {
        try {
            SimpleDateFormat dobOutFormat = new SimpleDateFormat(toFormat, Locale.ENGLISH);
            dobOutFormat.setTimeZone(TimeZone.getDefault());
            return dobOutFormat.format(date);
        } catch (Exception ignored) {
        }
        return "";
    }

    public static String formatWebToOtherFormat(@NonNull String toFormat, @NonNull String date) {
        return formatDateToOtherFormat(getWebDateTimeFormat(), toFormat, date, true);
    }

    public static String formatWebDateToOtherFormat(@NonNull String toFormat, @NonNull String date) {
        return formatDateToOtherFormat(getWebDateFormat(), toFormat, date, true);
    }

    public static Date parseStringToDate(@NonNull String date, @NonNull String format) {
        try {
            SimpleDateFormat simpleDateFormat = new SimpleDateFormat(format, Locale.ENGLISH);
            simpleDateFormat.setTimeZone(TimeZone.getDefault());
            return simpleDateFormat.parse(date);
        } catch (Exception ignored) {
        }
        return null;
    }

    public static String createAfileNameUsingTime() {
        try {
            return new SimpleDateFormat("yyyyMMddHHmmssSSS", Locale.ENGLISH).format(Calendar.getInstance().getTime());
        } catch (Exception ignored) {
        }
        return "";
    }

    public static int getInt(String number, int defaultNumber) {
        if (!TextUtils.isEmpty(number)) {
            try {
                return Integer.parseInt(number);
            } catch (Exception ignored) {
            }
        }
        return defaultNumber;
    }

    public static void nameValueList(@NonNull List<NameValueModel> list, @NonNull String key, @Nullable String value, @DimenRes int textSize) {
        if (!TextUtils.isEmpty(value)) {
            list.add(new NameValueModel(key, value, textSize));
        }
    }

    public static void nameValueList(@NonNull List<NameValueModel> list, @NonNull String key, @Nullable String value) {
        nameValueList(list, key, value, R.dimen.ts_small);
    }

    public static List<NameValueModel> nameValueList(@NonNull List<NameValueModel> list, @NonNull String[] keys, @NonNull String[] values, @DimenRes int textSize) {
        for (int i = 0, j = keys.length <= values.length ? keys.length : values.length; i < j; i++) {
            if (!TextUtils.isEmpty(values[i])) {
                list.add(new NameValueModel(keys[i], values[i], textSize));
            }
        }
        return list;
    }

    public static List<NameValueModel> nameValueList(@NonNull Context ctx, @NonNull List<NameValueModel> list, @NonNull int[] keys, @NonNull String[] values, @DimenRes int textSize) {
        for (int i = 0, j = keys.length <= values.length ? keys.length : values.length; i < j; i++) {
            if (!TextUtils.isEmpty(values[i])) {
                list.add(new NameValueModel(ctx.getString(keys[i]), values[i], textSize));
            }
        }
        return list;
    }

    public static void togglePassword(@NonNull ImageView v, @NonNull EditText edtPassword) {
        int visibility = 0;
        try {
            visibility = (int) v.getTag();
        } catch (Exception e) {
        }
        togglePassword(v, edtPassword, visibility == 0);

    }

    public static void togglePassword(@NonNull ImageView v, @NonNull EditText edtPassword, boolean setVisible) {
        int selection = edtPassword.getSelectionStart();
        edtPassword.setInputType(setVisible ? InputType.TYPE_TEXT_VARIATION_VISIBLE_PASSWORD : InputType.TYPE_CLASS_TEXT |
                InputType.TYPE_TEXT_VARIATION_PASSWORD);
//        v.setImageResource(setVisible ? R.drawable.ic_password_open : R.drawable.ic_password_close);
        v.setTag(setVisible ? 1 : 0);
        edtPassword.setSelection(selection);
        Sfw.setFont(edtPassword, null);
    }

    public static void setTextColor(TextView textView, @XmlRes int color) {
        try {
            textView.setTextColor(getColorStateList(color, textView.getContext()));
        } catch (Exception e) {
        }
    }

    private static ColorStateList getColorStateList(@XmlRes int color, Context ctx) {
        try {
            XmlResourceParser xrp = ctx.getResources().getXml(color);
            return ColorStateList.createFromXml(ctx.getResources(), xrp);
        } catch (Exception e) {
        }
        return null;
    }

    public static String formatToDecimal(double value) {
        try {
            DecimalFormat format = new DecimalFormat();
            format.setDecimalSeparatorAlwaysShown(false);
            String s = format.format(value);
            int index = s.indexOf(".");
            if (index > -1 && index >= s.length() - 2)
                return s + "0";
            return s;
        } catch (Exception e) {
            return "" + value;
        }
    }


    public static void share(Activity act, int permissionCode, ImageView imageView, String title, String shareDetails) {
        try {
            if (Utilities.hasPermission(act, permissionCode, new String[]{Manifest.permission.WRITE_EXTERNAL_STORAGE}, true)) {
                File mediaStorageDir = new File(Environment.getExternalStoragePublicDirectory(
                        Environment.DIRECTORY_PICTURES), act.getString(R.string.app_name) + "/.Share");
                mediaStorageDir.mkdirs();
                ArrayList<Uri> list = new ArrayList<>();
                Intent intent = new Intent(Intent.ACTION_SEND);

                Bitmap bitmap = ((BitmapDrawable) imageView.getDrawable()).getBitmap();
                ByteArrayOutputStream out = new ByteArrayOutputStream();
                bitmap.compress(Bitmap.CompressFormat.JPEG, 100, out);
                String timeStamp = new SimpleDateFormat("yyyyMMdd_HHmmss", Locale.ENGLISH).format(new Date());
                File mediaFile = new File(mediaStorageDir.getPath() + File.separator +
                        "IMG_" + timeStamp + ".jpg");
                FileOutputStream outputStream = new FileOutputStream(mediaFile);
                outputStream.write(out.toByteArray());
                outputStream.close();
                out.close();
                Uri screenshotUri = Uri.fromFile(mediaFile);
                intent.putExtra(Intent.EXTRA_STREAM, screenshotUri);

                intent.putExtra(Intent.EXTRA_TITLE, title);
                intent.putExtra(Intent.EXTRA_TEXT, shareDetails);
                intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                intent.setType("image/jpeg");
                act.startActivity(Intent.createChooser(intent, act.getString(R.string.app_name)));
            }
        } catch (Exception e) {
        }
    }

    /*public static void startFilePicker(int requestCode, Fragment fragment) {
        try {
            Intent intent = new Intent(fragment.getActivity(), FilePickerActivity.class);
            intent.putExtra(FilePickerConstants.CHOOSER_TEXT, "Please select image");
            intent.putExtra(FilePickerConstants.FILE, true);
//            intent.putExtra(FilePickerConstants.MULTIPLE_TYPES, new String[]{FilePickerConstants.MIME_IMAGE});
//            intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK|Intent.FLAG_ACTIVITY_CLEAR_TOP);
            fragment.getActivity().startActivityFromFragment(fragment,intent, requestCode);
        } catch (Exception ignored){}
    }*/


}
