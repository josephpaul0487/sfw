package jpt.sfw.app;

import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.text.TextUtils;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.google.android.material.bottomsheet.BottomSheetDialog;
import com.yalantis.ucrop.UCrop;

import java.io.File;
import java.lang.ref.SoftReference;
import java.util.List;

import androidx.annotation.FloatRange;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentActivity;
import jpt.sfw.R;

public class ImageUploadConfig {
    private SoftReference<FragmentActivity> activity;
    private SoftReference<Fragment> fragment;
    private SoftReference<ImageView> imageView;





    //the image will save to this path after crop
    private String imageFilePath;
    //the other files will save to this folder
    private String filePickerFolder;
    private float ratioWidth;
    private float ratioHeight;
    private int maxWidth;
    private int maxHeight;
    private int onPressedColor;
    private int onTouchUpColor;

    private final int permissionStorage;
    private final int permissionCamera;
    private int permissionFile;
    private final int requestCamera, requestGallery;
    private int requestUcrop;
    private int requestFile;

    private String fileMimeType = "application/*";
    private List<String> allowedFileTypes;

    public ImageUploadConfig(int permissionStorage, int permissionCamera, int requestCamera, int requestGallery) {
        this(null,null,null,permissionStorage,permissionCamera,requestGallery,requestCamera);
    }

    public ImageUploadConfig(@Nullable ImageView imageView, FragmentActivity activity, Fragment fragment, int permissionStorage, int permissionCamera, int requestGallery, int requestCamera) {
        this(imageView, activity, fragment, permissionStorage, permissionCamera, requestGallery, requestCamera, 0, null, 0, 0);
    }

    /**
     * @param activity          if calling from activity
     * @param fragment          if calling from fragment
     * @param permissionStorage permission contant for storage.                     should call {@link ImageUploadHelper#onRequestPermissionsResult(int, String[], int[])} from your activity or fragment
     * @param permissionCamera  permission contant for camera.                      should call {@link ImageUploadHelper#onRequestPermissionsResult(int, String[], int[])} from your activity or fragment
     * @param requestGallery    activity result contant fro select a file.          should call {@link ImageUploadHelper#onActivityResult(int, int, Intent)} from your activity or fragment
     * @param requestCamera     activity result contant fro capture from camera.    should call {@link ImageUploadHelper#onActivityResult(int, int, Intent)} from your activity or fragment
     * @param ratioWidth        the width ratio
     * @param ratioHeight       the heigh ratio
     */
    public ImageUploadConfig(@Nullable ImageView imageView, FragmentActivity activity, Fragment fragment, int permissionStorage, int permissionCamera, int requestGallery, int requestCamera, int requestUcrop, String temporaryImagePath, @FloatRange(from = 0,to = 16) float ratioWidth, @FloatRange(from = 0,to = 16) float ratioHeight) {
        this.imageView = new SoftReference<>(imageView);
        this.activity = new SoftReference<>(activity);
        this.fragment = new SoftReference<>(fragment);
        if (activity == null && fragment != null) {
            this.activity = new SoftReference<>(fragment.getActivity());
        }

        this.permissionStorage = permissionStorage;
        this.permissionCamera = permissionCamera;
        this.requestCamera = requestCamera;
        this.requestGallery = requestGallery;
        this.imageFilePath = temporaryImagePath == null ? this.activity.get().getExternalCacheDir() + "/file_temp.jpg" : temporaryImagePath;
        this.ratioWidth = ratioWidth == 0 ? this.activity.get().getResources().getInteger(R.integer.crop_w_pro_ratio) : ratioWidth;
        this.ratioHeight = ratioHeight == 0 ? this.activity.get().getResources().getInteger(R.integer.crop_h_pro_ratio) : ratioHeight;
        if (ratioHeight == ratioWidth) {
            this.maxWidth = this.activity.get().getResources().getInteger(R.integer.crop_w_pro_max);
            this.maxHeight = this.activity.get().getResources().getInteger(R.integer.crop_h_pro_max);
        }
        this.requestUcrop = requestUcrop == 0 ? UCrop.REQUEST_CROP : requestUcrop;

    }

    public Context getContext() {
        return activity.get()!=null?activity.get():fragment.get()!=null?fragment.get().getContext():imageView.get()!=null?imageView.get().getContext():null;
    }

    public FragmentActivity getActivity() {
        return activity.get();
    }

    public ImageUploadConfig setActivity(FragmentActivity activity) {
        this.activity = new SoftReference<>(activity);
        return this;
    }

    public Fragment getFragment() {
        return fragment.get();
    }

    public ImageUploadConfig setFragment(Fragment fragment) {
        this.fragment = new SoftReference<>(fragment);
        return this;
    }

    public ImageView getImageView() {
        return imageView.get();
    }

    public ImageUploadConfig setImageView(ImageView imageView) {
        this.imageView = new SoftReference<>(imageView);
        return this;
    }


    public String getImageFilePath() {
        return imageFilePath;
    }

    public ImageUploadConfig setImageFilePath(String imageFilePath) {
        this.imageFilePath = imageFilePath == null ? activity.get().getExternalCacheDir() + "/file_temp.jpg" : imageFilePath;
        return this;
    }

    public String getFilePickerFolder() {
        return filePickerFolder;
    }

    /**
     *
     * @param filePickerFolder   Should be a directory name
     * @return
     */
    public ImageUploadConfig setFilePickerFolder(String filePickerFolder) {
        if(new File(filePickerFolder).isFile())
            throw new RuntimeException("Should provide a valid folder name");
        this.filePickerFolder = TextUtils.isEmpty(filePickerFolder)?null:filePickerFolder.endsWith("/")?filePickerFolder:filePickerFolder+"/";
        return this;
    }

    public float getRatioWidth() {
        return ratioWidth;
    }

    public ImageUploadConfig setRatioWidth(float ratioWidth) {
        this.ratioWidth = ratioWidth;
        return this;
    }

    public float getRatioHeight() {
        return ratioHeight;
    }

    public ImageUploadConfig setRatioHeight(float ratioHeight) {
        this.ratioHeight = ratioHeight;
        return this;
    }

    public int getMaxWidth() {
        return maxWidth;
    }

    public ImageUploadConfig setMaxWidth(int maxWidth) {
        this.maxWidth = maxWidth;
        return this;
    }

    public int getMaxHeight() {
        return maxHeight;
    }

    public ImageUploadConfig setMaxHeight(int maxHeight) {
        this.maxHeight = maxHeight;
        return this;
    }

    public int getOnPressedColor() {
        return onPressedColor;
    }

    public ImageUploadConfig setOnPressedColor(int onPressedColor) {
        this.onPressedColor = onPressedColor;
        return this;
    }

    public int getOnTouchUpColor() {
        return onTouchUpColor;
    }

    public ImageUploadConfig setOnTouchUpColor(int onTouchUpColor) {
        this.onTouchUpColor = onTouchUpColor;
        return this;
    }

    public int getPermissionStorage() {
        return permissionStorage;
    }

    public int getPermissionCamera() {
        return permissionCamera;
    }

    public int getPermissionFile() {
        return permissionFile;
    }

    public ImageUploadConfig setPermissionFile(int permissionFile) {
        this.permissionFile = permissionFile;
        return this;
    }

    public int getRequestCamera() {
        return requestCamera;
    }

    public int getRequestGallery() {
        return requestGallery;
    }

    public int getRequestUcrop() {
        return requestUcrop;
    }

    public ImageUploadConfig setRequestUcrop(int requestUcrop) {
        this.requestUcrop = requestUcrop;
        return this;
    }

    public int getRequestFile() {
        return requestFile;
    }

    public ImageUploadConfig setRequestFile(int requestFile) {
        this.requestFile = requestFile;
        return this;
    }



    public String getFileMimeType() {
        return fileMimeType;
    }

    public ImageUploadConfig setFileMimeType(String fileMimeType) {
        this.fileMimeType = fileMimeType;
        return this;
    }

    public List<String> getAllowedFileTypes() {
        return allowedFileTypes;
    }

    public ImageUploadConfig setAllowedFileTypes(List<String> allowedFileTypes) {
        this.allowedFileTypes = allowedFileTypes;
        return this;
    }
}
