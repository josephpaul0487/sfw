package jpt.sfw.app;

import android.content.Context;
import android.graphics.Color;

import jpt.sfw.R;
import jpt.sfw.interfaces.OnAlertListener;

import androidx.annotation.ColorInt;
import androidx.annotation.DrawableRes;
import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.annotation.StringRes;

public class AlertConfig {

    public AlertConfig() {
    }

    public AlertConfig(int requestId, @NonNull CharSequence title, @NonNull CharSequence msg, @DrawableRes int positiveImage) {
        this(null, requestId, title, msg, positiveImage, 0);
    }

    public AlertConfig(int requestId, @NonNull CharSequence title, @NonNull CharSequence msg, @DrawableRes int positiveImage, @DrawableRes int negativeImage) {
        this(null, requestId, title, msg, positiveImage, negativeImage);
    }

    public AlertConfig(@NonNull Context ctx, @Nullable OnAlertListener listener, int requestId, @StringRes int title, @NonNull CharSequence msg, @DrawableRes int positiveImage, @DrawableRes int negativeImage) {
        this(listener, requestId, ctx == null ? "" : ctx.getString(title), msg, positiveImage, negativeImage);
    }

    public AlertConfig(@NonNull Context ctx, @Nullable OnAlertListener listener, int requestId, @StringRes int title, @StringRes int msg, @DrawableRes int positiveImage, @DrawableRes int negativeImage) {
        this(listener, requestId, ctx == null ? "" : ctx.getString(title), ctx == null ? "" : ctx.getString(msg), positiveImage, negativeImage);
    }

    public AlertConfig(@Nullable OnAlertListener listener, int requestId, @NonNull CharSequence title, @NonNull CharSequence msg, @DrawableRes int positiveImage, @DrawableRes int negativeImage) {
        this(listener, requestId, title, msg, positiveImage, negativeImage, 0);
    }

    /**
     * @param alertType 0 for error   1 for success  2 for warning
     */
    public AlertConfig(@Nullable OnAlertListener listener, int requestId, @NonNull CharSequence title, @NonNull CharSequence msg, @DrawableRes int positiveImage, @DrawableRes int negativeImage, int alertType) {
        this.listener = listener;
        this.requestId = requestId;
        this.title = title;
        this.msg = msg;
        this.positiveImage = positiveImage;
        this.negativeImage = negativeImage;
        if (alertType > -1 && alertType < 3) {
            this.alertType = alertType;
        }
    }

    @Nullable
    public OnAlertListener getListener() {
        return listener;
    }

    public AlertConfig setListener(@Nullable OnAlertListener listener) {
        this.listener = listener;
        return this;
    }

    public boolean hasListener() {
        return listener != null;
    }

    public int getRequestId() {
        return requestId;
    }

    public AlertConfig setRequestId(int requestId) {
        this.requestId = requestId;
        return this;
    }

    @NonNull
    public CharSequence getTitle() {
        return title;
    }

    public AlertConfig setTitle(@NonNull CharSequence title) {
        this.title = title;
        return this;
    }

    @DrawableRes
    public int getImage() {
        if (image != 0)
            return image;
        switch (alertType) {
            case 1:
                return R.drawable.ic_success;
            case 2:
                return R.drawable.ic_warning;
            default:
                return R.drawable.ic_error_image;
        }
    }

    public AlertConfig setImage(int image) {
        this.image = image;
        return this;
    }

    @NonNull
    public CharSequence getMsg() {
        return msg;
    }

    public AlertConfig setMsg(@NonNull CharSequence msg) {
        this.msg = msg;
        return this;
    }

    @NonNull
    public CharSequence getPositiveText() {
        return positiveText;
    }

    public AlertConfig setPositiveText(@NonNull CharSequence positiveText) {
        this.positiveText = positiveText;
        return this;
    }

    @Nullable
    public CharSequence getNegativeText() {
        return negativeText;
    }

    public AlertConfig setNegativeText(@Nullable CharSequence negativeText) {
        this.negativeText = negativeText;
        return this;
    }

    public boolean isCancelable() {
        return cancelable;
    }

    public AlertConfig setCancelable(boolean cancelable) {
        this.cancelable = cancelable;
        return this;
    }

    public AlertConfig setAsWarning() {
        alertType = 2;
        return this;
    }

    public AlertConfig setAsSuccess() {
        alertType = 1;
        return this;
    }

    public AlertConfig setAsSuccessWithImage() {
        alertType = 1;
        image = 0;
        return this;
    }

    public AlertConfig setAsWarningWithImage() {
        alertType = 2;
        image = 0;
        return this;
    }

    public AlertConfig setAsErrorWithImage() {
        alertType = 0;
        image = 0;
        return this;
    }


    @ColorInt
    public int getPositiveBtnBackTint() {
        switch (alertType) {
            case 1:
                return successTint;
            case 2:
                return errorTint;
            default:
                return negativeImage != 0 ? successTint : errorTint;
        }
    }

    @ColorInt
    public int getNegativeBtnBackTint() {
        switch (alertType) {
            case 2:
                return successTint;
            default:
                return errorTint;
        }//notification_type=1, member_id=31, end_date=2018-08-03, note=vxvxvxvx, widget_member_rx_reminder_id=0, repeatation_type=2,3,4,5,6,7,8,9, email_noification=1, timings=07:00, medication=vvv, label=xvxvxcv, start_date=2018-08-02, sms_notification=0
    }

    public AlertConfig setPositiveImage(int positiveImage) {
        this.positiveImage = positiveImage;
        return this;
    }

    public AlertConfig setNegativeImage(int negativeImage) {
        this.negativeImage = negativeImage;
        return this;
    }

    public int getAlertType() {
        return alertType;
    }

    public AlertConfig setAlertType(int alertType) {
        this.alertType = alertType;
        return this;
    }

    public int getErrorTint() {
        return errorTint;
    }

    public AlertConfig setErrorTint(int errorTint) {
        this.errorTint = errorTint;
        return this;
    }

    public int getSuccessTint() {
        return successTint;
    }

    public AlertConfig setSuccessTint(int successTint) {
        this.successTint = successTint;
        return this;
    }

    public int getPositiveImage() {
        return positiveImage;
    }

    public int getNegativeImage() {
        return negativeImage;
    }

    //To listen positiveButtonClick , negativeButtonClick and onDismiss
    @Nullable
    private OnAlertListener listener;
    //An integer id
    protected int requestId;
    //Title for the alert     ->  Should not be null
    @NonNull
    protected CharSequence title = "";
    //Alert message           ->  Should not be null
    @NonNull
    protected CharSequence msg = "";
    //Positive button text    ->  Should not be null
    @NonNull
    protected CharSequence positiveText = "Yes";
    //Negative Button text
    @Nullable
    protected CharSequence negativeText;
    @DrawableRes
    protected int positiveImage;
    @DrawableRes
    protected int negativeImage;
    //Set is alert cancelable
    protected boolean cancelable = true;
    //0 for error   1 for success   2 for warning
    protected int alertType = 0;

    @DrawableRes
    protected int image = 0;
    @ColorInt
    protected int errorTint = Color.parseColor("#d2232a");
    @ColorInt
    protected int successTint = Color.parseColor("#02aa5c");
}
