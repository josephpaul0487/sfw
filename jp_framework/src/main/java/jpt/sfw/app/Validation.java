package jpt.sfw.app;

import android.content.Context;
import android.content.res.Resources;
import android.util.Patterns;
import android.widget.TextView;

import com.google.android.material.textfield.TextInputLayout;
import com.hbb20.CountryCodePicker;

import androidx.annotation.Nullable;
import jpt.sfw.R;

import java.util.regex.Pattern;

import androidx.annotation.NonNull;
import io.michaelrocks.libphonenumber.android.PhoneNumberUtil;
import io.michaelrocks.libphonenumber.android.Phonenumber;

public class Validation {
    private static Pattern specialChars = Pattern.compile("[$=\\\\|<>^*%]");
    private static Pattern nameValidator = Pattern.compile("[a-zA-Z0-9 ]*");
    private static Pattern passwordValidator = Pattern.compile("((?=.*\\d)(?=.*[a-z])(?=.*[A-Z])(?=.*[@#$%]).{6,10})");
    private static String passwordError;

    public static void init(@NonNull Context ctx) {
        passwordValidator=Pattern.compile("((?=.*\\d)(?=.*[a-z])(?=.*[A-Z])(?=.*[@#$%]).{"+ctx.getResources().getInteger(R.integer.min_password_length)+","+ctx.getResources().getInteger(R.integer.max_password_length)+"})");
        passwordError=ctx.getResources().getString(R.string.err_pass_validator)+" ("+ctx.getResources().getInteger(R.integer.min_password_length)+","+ctx.getResources().getInteger(R.integer.max_password_length)+")";
    }

    @NonNull
    public static Pattern getSpecialChars() {
        return specialChars;
    }

    public static void setSpecialChars(@NonNull Pattern specialChars) {
        Validation.specialChars = specialChars;
    }

    @NonNull
    public static Pattern getNameValidator() {
        return nameValidator;
    }

    public static void setNameValidator(@NonNull Pattern nameValidator) {
        Validation.nameValidator = nameValidator;
    }

    @NonNull
    public static Pattern getPasswordValidator() {
        return passwordValidator;
    }

    public static void setPasswordValidator(@NonNull Pattern passwordValidator) {
        Validation.passwordValidator = passwordValidator;
    }

    public static void setPasswordValidator(@NonNull Pattern passwordValidator,String passwordError) {
        Validation.passwordValidator = passwordValidator;
        Validation.passwordError=passwordError;
    }

    public static void setPasswordValidator(@NonNull Pattern passwordValidator,@NonNull Context ctx) {
        Validation.passwordValidator = passwordValidator;
        Validation.passwordError=ctx.getResources().getString(R.string.err_pass_validator)+" ("+ctx.getResources().getInteger(R.integer.min_password_length)+","+ctx.getResources().getInteger(R.integer.max_password_length)+")";
    }

    public static String getPasswordError() {
        return passwordError;
    }

    public static void setPasswordError(String passwordError) {
        Validation.passwordError = passwordError;
    }

    public static boolean checkIsEmptyTV(@NonNull TextView... editTexts) {
        for (TextView e : editTexts) {
            if (e.getText().toString().trim().isEmpty()) {
                e.setError(e.getResources().getString(R.string.err_empty));
//                new ShakeAnimation(e).animate();
                e.requestFocus();
                return true;
            } else {
                e.setError(null);
            }
        }
        return false;
    }

    public static boolean checkIsAnEmailTV(@NonNull TextView editText) {
        if (Patterns.EMAIL_ADDRESS.matcher(editText.getText().toString()).matches()) {
            editText.setError(null);
            return true;
        }
        editText.requestFocus();
//        new ShakeAnimation(editText).animate();
        editText.setError(editText.getResources().getString(R.string.err_email_valid));
        return false;
    }

    public static boolean isAValidMobileTV(@NonNull  TextView editText, @Nullable CountryCodePicker countryCodePicker, boolean isMobile) {
        Phonenumber.PhoneNumber phoneNumber = null;
        if (countryCodePicker != null)
            try {
                phoneNumber = Sfw.getPhoneNumberUtil(editText.getContext()).getExampleNumberForType(Sfw.getPhoneNumberUtil(editText.getContext()).getRegionCodeForCountryCode(countryCodePicker.getSelectedCountryCodeAsInt()), PhoneNumberUtil.PhoneNumberType.MOBILE);
                phoneNumber.setNationalNumber(Long.parseLong(editText.getText().toString()));
            } catch (Exception e) {
                phoneNumber = null;
            }
        if (Patterns.PHONE.matcher(editText.getText().toString()).matches() && (!isMobile || (phoneNumber != null && Sfw.getPhoneNumberUtil(editText.getContext()).isValidNumber(phoneNumber)))) {
            editText.setError(null);
            return true;
        }
        editText.requestFocus();
//        new ShakeAnimation(editText).animate();
        editText.setError(editText.getResources().getString(R.string.err_mobile_valid));
        return false;
    }

    public static boolean isAValidMobileTV(@Nullable CountryCodePicker countryCodePicker,@NonNull  TextView editText) {
        return isAValidMobileTV( editText,countryCodePicker, true);
    }

    public static boolean checkIsHavingSpecialCharsTV(@NonNull TextView... editTexts) {
        for (TextView e : editTexts) {
            if (specialChars.matcher(e.getText().toString()).find()) {
                e.setError(e.getResources().getString(R.string.err_special_characters));
//                new ShakeAnimation(e).animate();
                e.requestFocus();
                return true;
            }
        }
        return false;
    }

   

    public static boolean isAValidPasswordTV(@NonNull TextView... editTexts) {
        if (!checkIsEmptyTV(editTexts)) {
            Resources resources=null;
            for (TextView ed : editTexts) {
                if(resources==null)
                    resources=ed.getResources();
                String text = ed.getText().toString();
                if (!passwordValidator.matcher(text).matches()) {
                    ed.setError(passwordError);
//                    new ShakeAnimation(ed).animate();
                    ed.requestFocus();
                    return false;
                } else if (text.contains(" ") || text.contains("\t")) {
                    ed.setError(resources.getString(R.string.err_pass_space));
//                    new ShakeAnimation(ed).animate();
                    ed.requestFocus();
                    return false;
                }
            }
            return true;
        }
        return false;
    }

    public static boolean isSamePasswordTV(@NonNull TextView one,@NonNull TextView two) {
        if (!one.getText().toString().equals(two.getText().toString())) {
            two.setError(two.getResources().getString(R.string.err_pass_not_match));
            return false;
        }
        two.setError(null);
        return true;
    }

    public static boolean isAvalidWebLinkTV(@NonNull TextView... editTexts) {
        for (TextView ed : editTexts) {
            if (ed.length() != 0 && !Patterns.WEB_URL.matcher(ed.getText().toString()).matches()) {
                ed.setError(ed.getResources().getString(R.string.err_web_url));
//                new ShakeAnimation(ed).animate();
                ed.requestFocus();
                return false;
            }
        }
        return true;
    }

    public static boolean isAvalidNameTV(@NonNull TextView editText) {
        if (checkIsEmptyTV(editText)) {
            return false;
        }
        if (!nameValidator.matcher(editText.getText().toString()).find()) {
            editText.setError(editText.getResources().getString(R.string.err_special_characters));
//            new ShakeAnimation(editText).animate();
            editText.requestFocus();
            return false;
        } else {
            editText.setError(null);
        }
        return true;
    }


    public static boolean checkIsEmpty(@NonNull TextInputLayout... layouts) {
        for (TextInputLayout e : layouts) {
            if (e.getEditText().getText().toString().trim().isEmpty()) {
                e.setErrorEnabled(true);
                e.setError(e.getResources().getString(R.string.err_empty));
                //new ShakeAnimation(e).animate();
                e.requestFocus();
                return true;
            } else {
                e.setErrorEnabled(false);
                e.setError(null);
            }
        }
        return false;
    }



    public static boolean checkIsAnEmail(@NonNull TextInputLayout layout) {
        if(Patterns.EMAIL_ADDRESS.matcher(layout.getEditText().getText().toString()).matches()) {
            layout.setErrorEnabled(false);
            layout.setError(null);
            return true;
        }
        layout.requestFocus();
        layout.setErrorEnabled(true);
      //  new ShakeAnimation(layout).animate();
        layout.setError(layout.getResources().getString(R.string.err_email_valid));
        return false;
    }

    public static boolean isAValidMobile(@NonNull TextInputLayout layout, @androidx.annotation.Nullable CountryCodePicker countryCodePicker, boolean isMobile) {
        Phonenumber.PhoneNumber phoneNumber = null;
        Context ctx=layout.getContext();
        if (countryCodePicker != null)
            try {
                phoneNumber = Sfw.getPhoneNumberUtil(ctx).getExampleNumberForType(Sfw.getPhoneNumberUtil(ctx).getRegionCodeForCountryCode(countryCodePicker.getSelectedCountryCodeAsInt()), PhoneNumberUtil.PhoneNumberType.MOBILE);
                phoneNumber.setNationalNumber(Long.parseLong(layout.getEditText().getText().toString()));
            } catch (Exception e) {
                phoneNumber = null;
            }
        if (Patterns.PHONE.matcher(layout.getEditText().getText().toString()).matches() && (!isMobile || (phoneNumber != null && Sfw.getPhoneNumberUtil(ctx).isValidNumber(phoneNumber)))) {
            layout.setError(null);
            return true;
        }
        layout.getEditText().requestFocus();
//        new ShakeAnimation(editText).animate();
        layout.setError(ctx.getResources().getString(R.string.err_mobile_valid));
        return false;


    }

    public static boolean checkIsHavingSpecialChars(@NonNull TextInputLayout... layouts) {
        for (TextInputLayout e : layouts) {
            if(specialChars.matcher(e.getEditText().getText().toString()).find()) {
                e.setErrorEnabled(true);
                e.setError(e.getResources().getString(R.string.err_special_characters));
              //  new ShakeAnimation(e).animate();
                e.requestFocus();
                return true;
            }
        }
        return false;
    }

    public static boolean isAValidPassword(@NonNull TextInputLayout... layouts) {
        if (!checkIsEmpty(layouts) && !checkIsHavingSpecialChars(layouts)) {
            Resources resources=null;
            for (TextInputLayout ed : layouts) {
                if(resources==null)
                    resources=ed.getResources();
                String text = ed.getEditText().getText().toString();
                if (!passwordValidator.matcher(text).matches()) {
                    ed.setErrorEnabled(true);
                    ed.setError(resources.getString(R.string.err_pass_validator)+" ("+resources.getInteger(R.integer.min_password_length)+","+resources.getInteger(R.integer.max_password_length)+")");
//                    new ShakeAnimation(ed).animate();
                    ed.requestFocus();
                    return false;
                } else if (text.contains(" ") || text.contains("\t")) {
                    ed.setErrorEnabled(true);
                    ed.setError(resources.getString(R.string.err_pass_space));
//                    new ShakeAnimation(ed).animate();
                    ed.requestFocus();
                    return false;
                }
            }
            return true;
        }
        return false;
    }



    public static boolean isAvalidName(@NonNull TextInputLayout layout) {
        if(checkIsEmpty(layout)) {
            return false;
        }
        if(!nameValidator.matcher(layout.getEditText().getText().toString()).find()) {
            layout.setErrorEnabled(true);
            layout.setError(layout.getResources().getString(R.string.err_special_characters));
            //new ShakeAnimation(layout).animate();
            layout.requestFocus();
            return false;
        } else {
            layout.setErrorEnabled(false);
            layout.setError(null);
        }
        return true;
    }

    public static boolean isSamePassword(@NonNull TextInputLayout one,@NonNull TextInputLayout two) {
        if (!one.getEditText().getText().toString().equals(two.getEditText().getText().toString())) {
            two.setError(two.getResources().getString(R.string.err_pass_not_match));
            return false;
        }
        two.setError(null);
        return true;
    }

    public static boolean isAvalidWebLink(@NonNull TextInputLayout... editTexts) {
        for (TextInputLayout layout : editTexts) {
            if (layout.getEditText().length() != 0 && !Patterns.WEB_URL.matcher(layout.getEditText().getText().toString()).matches()) {
                layout.setErrorEnabled(true);
                layout.setError(layout.getResources().getString(R.string.err_web_url));
                //new ShakeAnimation(layout).animate();
                layout.requestFocus();
                return false;
            }
        }
        return true;
    }


    public static boolean isImageListValidation(int size) {
        if (size > 1) {
            return true;
        }
        return false;
    }
}
