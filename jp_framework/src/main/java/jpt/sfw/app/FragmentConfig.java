package jpt.sfw.app;

import jpt.sfw.R;

import androidx.annotation.IdRes;

public class FragmentConfig {
    //Use fragment instance from memory
    private boolean popupFromBackStack;
    // set false if fragment has custom toolbar or no need of toolbar
    private boolean needToolbar = true;
    // set false if fragment does not need bottombar
    private boolean needBottomBar = true;
    //set true if need drawerlayout functionality  or set false if only need back icon
    private  boolean needDrawer=true;
    //Clear all fragments from memory except home
    private boolean clearBackStack;
    //to which layout
    @IdRes
    private int toId = R.id.mainFrame;
    //Title Of The Activity
    private CharSequence pageTitle;
    //Backstack tag
    private String tag;
    //Provide this tag if want to remove the backstack (Eg: ForgotFragment)
    private String tagToRemove;
    //Any data
    private Object data;

    public FragmentConfig() {
    }

    public FragmentConfig(CharSequence pageTitle, String tag, Object data) {
        this.pageTitle=pageTitle==null?"":pageTitle;
        this.tag=tag;
        this.data=data;
    }

    public boolean isPopupFromBackStack() {
        return popupFromBackStack;
    }

    public FragmentConfig setPopupFromBackStack(boolean popupFromBackStack) {
        this.popupFromBackStack = popupFromBackStack;
        return this;
    }

    public boolean isNeedToolbar() {
        return needToolbar;
    }

    public FragmentConfig setNeedToolbar(boolean needToolbar) {
        this.needToolbar = needToolbar;
        return this;
    }

    public boolean isNeedBottomBar() {
        return needBottomBar;
    }

    public FragmentConfig setNeedBottomBar(boolean needBottomBar) {
        this.needBottomBar = needBottomBar;
        return this;
    }

    public boolean isNeedDrawer() {
        return needDrawer;
    }

    public FragmentConfig setNeedDrawer(boolean needDrawer) {
        this.needDrawer = needDrawer;
        return this;
    }

    public boolean isClearBackStack() {
        return clearBackStack;
    }

    public FragmentConfig setClearBackStack(boolean clearBackStack) {
        this.clearBackStack = clearBackStack;
        return this;
    }

    public int getToId() {
        return toId;
    }

    public FragmentConfig setToId(int toId) {
        this.toId = toId;
        return this;
    }

    public CharSequence getPageTitle() {
        return pageTitle;
    }

    public FragmentConfig setPageTitle(CharSequence pageTitle) {
        this.pageTitle = pageTitle;
        return this;
    }

    public String getTag() {
        return tag;
    }

    public FragmentConfig setTag(String tag) {
        this.tag = tag;
        return this;
    }

    public Object getData() {
        return data;
    }

    public FragmentConfig setData(Object data) {
        this.data = data;
        return this;
    }

    public String getTagToRemove() {
        return tagToRemove;
    }

    public FragmentConfig setTagToRemove(String tagToRemove) {
        this.tagToRemove = tagToRemove;
        return this;
    }

    @Override
    public String toString() {
        return "FragmentConfig{" +
                "popupFromBackStack=" + popupFromBackStack +
                ", needToolbar=" + needToolbar +
                ", needBottomBar=" + needBottomBar +
                ", needDrawer=" + needDrawer +
                ", clearBackStack=" + clearBackStack +
                ", toId=" + toId +
                ", pageTitle=" + pageTitle +
                ", tag='" + tag + '\'' +
                ", tagToRemove='" + tagToRemove + '\'' +
                ", data=" + (data==null?"NULL":data.toString()) +
                '}';
    }
}
