package jpt.sfw.webservice;


import org.json.JSONObject;

import java.util.HashMap;
import java.util.List;

import okhttp3.MultipartBody;
import okhttp3.RequestBody;
import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.http.Body;
import retrofit2.http.DELETE;
import retrofit2.http.Field;
import retrofit2.http.FieldMap;
import retrofit2.http.FormUrlEncoded;
import retrofit2.http.GET;
import retrofit2.http.HeaderMap;
import retrofit2.http.Headers;
import retrofit2.http.Multipart;
import retrofit2.http.POST;
import retrofit2.http.PUT;
import retrofit2.http.Part;
import retrofit2.http.PartMap;
import retrofit2.http.Streaming;
import retrofit2.http.Url;

public interface SfwApiInterface {


    @FormUrlEncoded
    @PUT
    Call<String> callWebFormUrl(@HeaderMap HashMap<String, String> headers, @FieldMap HashMap<String, String> params, @Url String url);

    @POST
    Call<String> callPostUrlParams(@HeaderMap HashMap<String, String> headers, @Url String url);

    @Multipart
    @POST
    Call<String> callWebMultiPart(@HeaderMap HashMap<String, String> headers, @PartMap HashMap<String, RequestBody> params, @Part List<MultipartBody.Part> files, @Url String url);

    @Multipart
    @POST
    Call<String> callWebMultiPart(@HeaderMap HashMap<String, String> headers, @PartMap HashMap<String, RequestBody> params, @Part MultipartBody.Part fileOne, @Part MultipartBody.Part fileTwo, @Url String url);

    @Multipart
    @POST
    Call<String> callWebMultiPart(@HeaderMap HashMap<String, String> headers, @PartMap HashMap<String, RequestBody> params, @Url String url);

    @Headers("Content-Type: application/json")
    @POST
    Call<String> callJsonInput(@HeaderMap HashMap<String, String> headers, @retrofit2.http.Body String params, @Url String url);

    @GET
    Call<String> fetchData(@HeaderMap HashMap<String, String> headers, @Url String url);

    @DELETE
    Call<String> deleteData(@HeaderMap HashMap<String, String> headers, @Url String url);

    @Streaming
    @GET
    Call<ResponseBody> downloadFileByUrl(@Url String fileUrl);




}
