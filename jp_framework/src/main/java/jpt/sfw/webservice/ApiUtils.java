package jpt.sfw.webservice;


import org.json.JSONObject;

import java.io.File;
import java.util.HashMap;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import okhttp3.MediaType;
import okhttp3.MultipartBody;
import okhttp3.RequestBody;


public class ApiUtils {


    public static JSONObject getJsonObject(String[] keys, String[] values) {
        JSONObject object = new JSONObject();
        for (int i = 0; i < keys.length && i < values.length; i++) {
            try {
                object.put(keys[i], values[i]);
            } catch (Exception e) {
            }
        }
        return object;
    }

    public static HashMap<String, String> getApiParams(String[] keys, String[] values) {
        if (keys == null || values == null)
            throw new IllegalAccessError("Keys and Values should not be null");
        if (keys.length != values.length)
            throw new IllegalAccessError("Keys and Values length should be equal");
        HashMap<String, String> map = new HashMap<>();
        for (int i = 0; i < keys.length ; i++) {
            try {
                if (keys[i] != null)
                    map.put(keys[i], values[i] == null ? "" : values[i]);
            } catch (Exception e) {
            }
        }
        return map;
    }



    public static HashMap<String, RequestBody> getApiPartParams(String[] keys, String[] values) {
        if (keys == null || values == null)
            throw new IllegalAccessError("Keys and Values should not be null");
        if (keys.length != values.length)
            throw new IllegalAccessError("Keys and Values length should be equal");
        HashMap<String, RequestBody> map = new HashMap<>();
        for (int i = 0; i < keys.length ; i++) {
            try {
                if (keys[i] != null)
                map.put(keys[i], RequestBody.create(MediaType.parse("text/plain"), values[i]));
            } catch (Exception e) {
            }
        }
        return map;
    }

    public static String paramsAsString(String[] keys, String[] values) {
        StringBuilder builder=new StringBuilder();
        if (keys == null || values == null)
            return "";
            //throw new IllegalAccessError("Keys and Values should not be null");
        if (keys.length != values.length)
            throw new IllegalAccessError("Keys and Values length should be equal");
        if(keys.length>0)
            builder.append("?");
        for (int i = 0; i < keys.length ; i++) {
            try {
                if (keys[i] != null) {
                    if(builder.length()>1)
                        builder.append("&");
                    builder.append(keys[i]).append("=").append(values[i]);
                }
            } catch (Exception e) {
            }
        }
        return builder.toString();
    }

    public static MultipartBody.Part getImageMultiPart(@Nullable String filePath, @NonNull String key) {
        return getFileMultiPart(filePath,key);
    }

    public static MultipartBody.Part getFileMultiPart(@Nullable String filePath, @NonNull String key) {
        if (filePath == null)
            return null;
        File file = new File(filePath);
        String mediaType="application/*";
        int lastIndex=filePath.lastIndexOf(".");
        if(lastIndex>-1) {
            String ext=filePath.substring(lastIndex+1).toLowerCase();
            switch (ext) {
                case "jpg":
                case "jpeg":
                case "bmp":
                case "png":
                case "gif":mediaType="image/"+ext;
                    break;
                case "pdf":
                case "doc":
                case "docx":
                case "xls":
                case "xlsx":
                case "ppt":
                case "pptx": mediaType="application/"+ext;
                    break;

            }
        }
        RequestBody reqFile = RequestBody.create(MediaType.parse(mediaType), file);
        return MultipartBody.Part.createFormData(key, file.getName(), reqFile);
    }


   /* public static MultipartBody.Part prepareFilePart(String partName, Uri fileUri, Context ctx) {
        // use the FileUtils to get the actual file by uri
        File file = jpt.sfw.app.FileUtils.getFile(ctx, fileUri);
        // create RequestBody instance from file
        try {
            RequestBody requestFile = RequestBody.create(
                    MediaType.parse(ctx.getContentResolver().getType(fileUri)),
                    file
            );
            // MultipartBody.Part is used to send also the actual file name
            return MultipartBody.Part.createFormData(partName, file.getName(), requestFile);
        } catch (Exception ignored) {
        }
        return null;

    }*/


}
