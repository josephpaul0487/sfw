package jpt.sfw.webservice;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;

public interface ApiHelperListener {
    /**
     *
     * @param apiTypeCode  @see {@link SfwApiHelper}
     */
    default void onApiSuccess(int apiTypeCode, Object responseData){}
    default void onApiError(int apiTypeCode, Throwable t){}
    default void onApiSuccess(int apiTypeCode, Object responseData,SfwApiCallModel callModel){
        onApiSuccess(apiTypeCode,responseData);
    }
    default void onApiError(int apiTypeCode, Throwable t, @Nullable Object responseData,@NonNull SfwApiCallModel callModel){
        onApiError(apiTypeCode,t);
    }
}
