package jpt.sfw.webservice;

import java.io.IOException;
import java.util.Objects;

import androidx.annotation.NonNull;
import okhttp3.Interceptor;
import okhttp3.Request;
import okhttp3.Response;

public class UploadProgressInterceptor implements Interceptor,CountingRequestBody.Listener {
    private int index;
    private long id;

    public UploadProgressInterceptor(int index,long id) {
        this.index=index;
        this.id=id;
    }

    @Override public Response intercept(@NonNull Chain chain) throws IOException {
        Request originalRequest = chain.request();

        if (originalRequest.body() == null) {
            return chain.proceed(originalRequest);
        }

        Request progressRequest = originalRequest.newBuilder()
                .method(originalRequest.method(),
                        new CountingRequestBody(originalRequest.body(), this))
                .build();

        return chain.proceed(progressRequest);
    }

    public void addToIndex(int add) {
        this.index+=add;
    }

    public int getIndex() {
        return index;
    }

    public long getId() {
        return id;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        UploadProgressInterceptor that = (UploadProgressInterceptor) o;
        return id == that.id;
    }

    @Override
    public int hashCode() {

        return Objects.hash(id);
    }

    @Override
    public void onRequestProgress(long bytesWritten, long contentLength) {

    }
}
