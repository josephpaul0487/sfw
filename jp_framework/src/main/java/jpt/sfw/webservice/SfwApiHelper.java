package jpt.sfw.webservice;

import android.content.Context;
import android.text.TextUtils;

import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;

import jpt.sfw.app.Sfw;
import jpt.sfw.database.SfwDatabase;
import jpt.sfw.annotations.AppConfig;
import jpt.sfw.interfaces.ListModelInterface;
import jpt.sfw.interfaces.SfwActivityInterface;
import jpt.sfw.R;
import jpt.sfw.app.AlertConfig;
import jpt.sfw.app.ProgressConfig;
import jpt.sfw.interfaces.OnAlertListener;
import jpt.sfw.interfaces.SingleModelInterface;
import jpt.sfw.interfaces.StatusInterface;
import jpt.sfw.models.CommonListModel;
import jpt.sfw.models.CommonModel;
import jpt.sfw.models.StatusModel;

import java.io.IOException;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.lang.reflect.Type;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;

import okhttp3.MultipartBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.converter.gson.GsonConverterFactory;

public class SfwApiHelper {

    public enum WebServiceType {
        GET, PUT, POST, DELETE, JSON_POST, SIMPLE_POST
    }

    private List<Integer> fetchingApiTypes = new ArrayList<>();
    private Context ctx;
    private boolean isDebuggingEnabled;
    private static boolean initializeApiHelper;


    private final static List<ApiHelperListener> listeners = new ArrayList<>();
    private static SfwApiHelper instance;


    private static final String TAG = "SfwApiHelper";
    public static final int API_COMMON_ADD_EDIT = 1;
    public static final int API_COMMON_DELETE = 2;
    public static final int API_COMMON_SUBMIT = 3;


    public static void init(@NonNull Context ctx, @NonNull AppConfig config) {
        Class<? extends SfwApiHelper> cls = config.apiHelperClassType();
        try {
            if (instance == null) {
                initializeApiHelper = config.initializeApiHelper();
                instance = cls.newInstance();
                instance.ctx = ctx.getApplicationContext();
                instance.isDebuggingEnabled = config.debuggingEnabled();
            }
        } catch (Exception e) {
            throw new IllegalAccessError("Can't initialize this class : " + cls.getName() + ".  Class should contain a public empty constructor. Should not be an abstract class.");
        }
    }

    public boolean isDebuggingEnabled() {
        return isDebuggingEnabled;
    }

    public SfwApiHelper setDebuggingEnabled(boolean debuggingEnabled) {
        isDebuggingEnabled = debuggingEnabled;
        return this;
    }

    public Context getContext() {
        return ctx;
    }

    public static SfwApiHelper getInstance() {
        if (initializeApiHelper && instance == null)
            throw new IllegalAccessError("You should initialize this class using init(Context ctx,Class<? extends SfwDatabase> cls) method from your application class");
        return instance;
    }

    public static void clearAllListeners() {
        synchronized (listeners) {
            listeners.clear();
        }
    }


    public static void addListener(@Nullable ApiHelperListener listener) {
        synchronized (listeners) {
            if (listener != null && !listeners.contains(listener)) {
                listeners.add(listener);
            }
        }
    }

    public static void removeListener(@Nullable ApiHelperListener listener) {
        synchronized (listeners) {
            listeners.remove(listener);
        }
    }

    protected  void callSuccess(int apiCode, Object object,SfwApiCallModel callModel) {
        try {
            //  new Handler(Looper.getMainLooper()).post(() -> {
            try {
                fetchingApiTypes.remove(Integer.valueOf(apiCode));
                synchronized (listeners) {
                    for (int i = 0; i < listeners.size(); i++) {
                        try {
                            listeners.get(i).onApiSuccess(apiCode, object,callModel);
                        } catch (Exception ignored) {
                        }
                    }
                }
            } catch (Exception ignored) {
            }

            //  });

        } catch (Exception ignored) {
        }
    }

    protected  void callError(int apiCode, Throwable t, Object object,SfwApiCallModel callModel) {
        try {
            // new Handler(Looper.getMainLooper()).post(() -> {
            try {
                fetchingApiTypes.remove(Integer.valueOf(apiCode));
                synchronized (listeners) {
                    for (int i = 0; i < listeners.size(); i++) {
                        listeners.get(i).onApiError(apiCode, t,object,callModel);
                    }
                }
            } catch (Exception ignored) {
            }
            // });
        } catch (Exception ignored) {
        }
    }


    public final Call<String> addOrUpdate(int alertCode, String databaseKey, @NonNull String url, boolean isMultipart, @Nullable SfwActivityInterface act, @NonNull OnAlertListener alertListener, @NonNull ApiHelperListener apiHelperListener, @NonNull String[] key, @NonNull String[] value) {
        return addOrUpdate(alertCode, databaseKey, url, true, isMultipart, act, alertListener, apiHelperListener, null, key, value);
    }

    public final Call<String> addOrUpdate(int alertCode, String databaseKey, @NonNull String url, boolean needProgressDialog, boolean isMultipart, @Nullable SfwActivityInterface act, @NonNull OnAlertListener alertListener, @NonNull ApiHelperListener apiHelperListener, @NonNull String[] key, @NonNull String[] value) {
        return addOrUpdate(alertCode, databaseKey, url, needProgressDialog, isMultipart, act, alertListener, apiHelperListener, null, key, value);
    }

    public final Call<String> addOrUpdate(int alertCode, String databaseKey, @NonNull String url, boolean needProgressDialog, boolean isMultipart, @Nullable SfwActivityInterface act, @NonNull OnAlertListener alertListener, @NonNull ApiHelperListener apiHelperListener, @Nullable List<MultipartBody.Part> files, @NonNull String[] key, @NonNull String[] value) {
        return addOrUpdate(alertCode, databaseKey, url, needProgressDialog, isMultipart, act, alertListener, apiHelperListener, files, null, null, key, value);
    }


    public final Call<String> addOrUpdate(int alertCode, String databaseKey, @NonNull String url, boolean needProgressDialog, boolean isMultipart, @Nullable SfwActivityInterface act, @NonNull OnAlertListener alertListener, @NonNull ApiHelperListener apiHelperListener, @Nullable MultipartBody.Part fileOne, @Nullable MultipartBody.Part fileTwo, @NonNull String[] key, @NonNull String[] value) {
        return addOrUpdate(alertCode, databaseKey, url, needProgressDialog, isMultipart, act, alertListener, apiHelperListener, null, fileOne, fileTwo, key, value);
    }

    /**
     * @param alertCode          show message on failure {@link SfwActivityInterface#showInternetErrMsg(boolean, int, boolean, OnAlertListener)}
     * @param databaseKey        if not null  and api success-> will clear the contents from the {@link SfwDatabase#clear(Class, String)} associated with the key
     * @param url                url of the api
     * @param needProgressDialog true -> will show a progressDialog
     * @param isMultipart        use this for add or if having files to upload
     * @param act                to show progress msg and error message
     * @param alertListener      to catch positive button click event from the {@link jpt.sfw.appui.AlertDialog} shows on failure
     * @param apiHelperListener  to get the response to the caller class {@link ApiHelperListener#onApiSuccess(int, Object)}
     * @param files              if you want upload a files
     * @param fileOne            some apis want to upload as single file
     * @param fileTwo            some apis want to upload as single file
     * @param key                For creating key value params {@link ApiUtils#getApiParams(String[], String[])} {@link ApiUtils#getApiPartParams(String[], String[])}
     * @param value              For creating key value params {@link ApiUtils#getApiParams(String[], String[])} {@link ApiUtils#getApiPartParams(String[], String[])}
     */
    public final Call<String> addOrUpdate(int alertCode, String databaseKey, @NonNull String url, boolean needProgressDialog, boolean isMultipart, @Nullable SfwActivityInterface act, @NonNull OnAlertListener alertListener, @NonNull ApiHelperListener apiHelperListener, @Nullable List<MultipartBody.Part> files, @Nullable MultipartBody.Part fileOne, @Nullable MultipartBody.Part fileTwo, @NonNull String[] key, @NonNull String[] value) {
        return callWeb(new SfwApiCallModel<>(isMultipart ? SfwApiHelper.WebServiceType.POST : SfwApiHelper.WebServiceType.PUT, true, alertCode, SfwApiHelper.API_COMMON_ADD_EDIT, needProgressDialog ? ctx.getString(R.string.info_submitting) : null, false, true, true, StatusModel.class, null, null, false, databaseKey, url, isMultipart, act, alertListener, apiHelperListener, files, fileOne, fileTwo, key, value, null, null));
    }

    public final <T> Call<String> submitData(int alertCode, boolean shouldLoggedIn, @NonNull Class<T> responseClassType, String databaseKey, @NonNull String url, @Nullable SfwActivityInterface act, @NonNull OnAlertListener alertListener, @NonNull ApiHelperListener apiHelperListener, @Nullable MultipartBody.Part fileOne, @NonNull String[] key, @NonNull String[] value) {
        return callWeb(new SfwApiCallModel<>(WebServiceType.POST, shouldLoggedIn, alertCode, SfwApiHelper.API_COMMON_SUBMIT, ctx.getString(R.string.info_submitting), false, true, true, responseClassType, null, null, false, databaseKey, url, true, act, alertListener, apiHelperListener, null, fileOne, null, key, value, null, null));
    }

    public final <T> Call<String> submitData(int alertCode, boolean shouldLoggedIn, boolean isMultipart, @NonNull Class<T> responseClassType, String databaseKey, @NonNull String url, @Nullable SfwActivityInterface act, @NonNull OnAlertListener alertListener, @NonNull ApiHelperListener apiHelperListener, @Nullable MultipartBody.Part fileOne, @NonNull String[] key, @NonNull String[] value) {
        return callWeb(new SfwApiCallModel<>(isMultipart ? WebServiceType.POST : WebServiceType.PUT, shouldLoggedIn, alertCode, SfwApiHelper.API_COMMON_SUBMIT, ctx.getString(R.string.info_submitting), false, true, true, responseClassType, null, CommonModel.class, false, databaseKey, url, isMultipart, act, alertListener, apiHelperListener, null, fileOne, null, key, value, null, null));
    }

    public final <T> Call<String> submitData(int alertCode, boolean shouldLoggedIn, boolean showProgressDialog, @NonNull Class<T> responseClassType, String databaseKey, @NonNull String url, @Nullable SfwActivityInterface act, @NonNull OnAlertListener alertListener, @NonNull ApiHelperListener apiHelperListener, @NonNull String[] key, @NonNull String[] value) {
        return callWeb(new SfwApiCallModel<>(WebServiceType.POST, shouldLoggedIn, alertCode, SfwApiHelper.API_COMMON_SUBMIT, showProgressDialog ? ctx.getString(R.string.info_submitting) : null, false, true, true, responseClassType, null, CommonModel.class, false, databaseKey, url, true, act, alertListener, apiHelperListener, null, null, null, key, value, null, null));
    }

    public final <T> Call<String> fetchDataListWithoutProgress(int apiCode, boolean shouldLoggedIn, boolean shareResultGlobally, boolean clearPreviousData, @NonNull Class<T> responseClassType, String databaseKey, @NonNull String url, @Nullable ApiHelperListener apiHelperListener) {
        return callWeb(new SfwApiCallModel<>(WebServiceType.GET, shouldLoggedIn, -1, apiCode, null, shareResultGlobally, clearPreviousData, false, responseClassType, CommonListModel.class, null, true, databaseKey, url, false, null, null, apiHelperListener, null, null, null, null, null, null, null).setShouldPassValueToDb(true));
    }

    public final <T> Call<String> fetchDataListWithoutProgress(int apiCode, boolean shouldLoggedIn, boolean shareResultGlobally, boolean clearPreviousData, @NonNull Class<T> responseClassType, String databaseKey, @NonNull String url, @Nullable ApiHelperListener apiHelperListener, @Nullable String methodToInvokeOnSuccess) {
        return callWeb(new SfwApiCallModel<>(WebServiceType.GET, shouldLoggedIn, -1, apiCode, null, shareResultGlobally, clearPreviousData, false, responseClassType, CommonListModel.class, null, true, databaseKey, url, false, null, null, apiHelperListener, null, null, null, null, null, null, methodToInvokeOnSuccess).setShouldPassValueToDb(methodToInvokeOnSuccess == null));
    }

    public final <T> Call<String> fetchSingleDataWithoutProgress(int apiCode, boolean shouldLoggedIn, boolean shareResultGlobally, @NonNull Class<T> responseClassType, String databaseKey, @NonNull String url, @Nullable ApiHelperListener apiHelperListener) {
        return callWeb(new SfwApiCallModel<>(WebServiceType.GET, shouldLoggedIn, -1, apiCode, null, shareResultGlobally, true, false, responseClassType, null, CommonModel.class, false, databaseKey, url, false, null, null, apiHelperListener, null, null, null, null, null, null, null));
    }

    public final <T> void fetchSingleDataWithoutProgress(int apiCode, boolean shouldLoggedIn, boolean shareResultGlobally, @NonNull Class<T> responseClassType, String databaseKey, @NonNull String url, @Nullable ApiHelperListener apiHelperListener, @Nullable String methodToInvokeOnSuccess) {
        callWeb(new SfwApiCallModel<>(WebServiceType.GET, shouldLoggedIn, -1, apiCode, null, shareResultGlobally, true, false, responseClassType, null, CommonModel.class, false, databaseKey, url, false, null, null, apiHelperListener, null, null, null, null, null, null, methodToInvokeOnSuccess));
    }

    public final <T> Call<String> deleteData(boolean shareResultGlobally, @NonNull String url, @Nullable ApiHelperListener apiHelperListener) {
        return deleteData(null, -1, shareResultGlobally, url, null, null, apiHelperListener);
    }


    public final <T> Call<String> deleteData(@Nullable String progressMsg, int alertCode, boolean shareResultGlobally, @NonNull String url, @Nullable SfwActivityInterface act, @Nullable OnAlertListener alertListener, @Nullable ApiHelperListener apiHelperListener) {
        return callWeb(new SfwApiCallModel<>(WebServiceType.DELETE, true, alertCode, API_COMMON_DELETE, progressMsg, shareResultGlobally, false, true, StatusModel.class, null, null, false, null, url, false, act, alertListener, apiHelperListener, null, null, null, null, null, null, null));
    }


    public final <T> Call<String> callWeb(@NonNull WebServiceType webServiceType, boolean shouldLoggedIn, int alertCode, int apiCode, @Nullable CharSequence progressMsg, boolean shareResultGlobally, boolean clearPreviousData, boolean showMessageOnError, @NonNull Class<T> responseClassType, boolean responseIsAList, String databaseKey, @NonNull String url, boolean isMultipart, @Nullable SfwActivityInterface act, @Nullable OnAlertListener alertListener, @Nullable ApiHelperListener apiHelperListener, @Nullable List<MultipartBody.Part> files, @Nullable MultipartBody.Part fileOne, @Nullable MultipartBody.Part fileTwo, @Nullable String[] key, @Nullable String[] value) {
        return callWeb(new SfwApiCallModel<>(webServiceType, shouldLoggedIn, alertCode, apiCode, progressMsg, shareResultGlobally, clearPreviousData, showMessageOnError, responseClassType, null, null, responseIsAList, databaseKey, url, isMultipart, act, alertListener, apiHelperListener, files, fileOne, fileTwo, key, value, null, null));
    }


    public final <T> Call<String> callWeb(@NonNull SfwApiCallModel<T> callModel) {
        if ((callModel.key == null && callModel.value != null) || (callModel.key != null && callModel.value == null) || (callModel.key != null && callModel.key.length != callModel.value.length)) {
            throw new IllegalAccessError("Keys and Values length should be equal. URL = " + callModel.getUrl());
        }
        if (TextUtils.isEmpty(callModel.getUrl())) {
            throw new IllegalAccessError("URL should not be empty. apicode= " + callModel.getApiCode());
        }
        Type type;
        if (callModel.responseIsAList)
            type = TypeToken.getParameterized(callModel.mainClassListType, callModel.responseClassType).getType();
        else if (callModel.responseClassType.equals(StatusModel.class))
            type = StatusModel.class;
        else
            type = TypeToken.getParameterized(callModel.mainClassSingleType, callModel.responseClassType).getType();
        addListener(callModel.getApiHelperListener());
        if (fetchingApiTypes.contains(callModel.apiCode) || (callModel.shouldLoggedIn && !SfwDatabase.getInstance().isLoggedIn())) {
            return null;
        }
        fetchingApiTypes.add(callModel.apiCode);

//        SoftReference<SfwActivityInterface> activity = callModel.act;
//        SoftReference<OnAlertListener> listener = callModel.alertListener;
//        SoftReference<ApiHelperListener> apiListener = callModel.apiHelperListener;
        try {

            if (callModel.getAct() != null && callModel.progressMsg != null)
                callModel.getAct().showProgress(new ProgressConfig(callModel.progressMsg));
            addMoreKeyValueForParams(callModel);

            Call<String> api;
            if (callModel.headers == null)
                callModel.headers = new HashMap<>();
            addCustomHeaders(callModel.headers, callModel.apiCode);
            if (callModel.webServiceType == WebServiceType.SIMPLE_POST) {
                api = SfwClient.getInstance().getApiInterfaceForString().callPostUrlParams(callModel.headers, callModel.url + ApiUtils.paramsAsString(callModel.key, callModel.value));
            } else if (callModel.isMultipart && callModel.webServiceType == WebServiceType.POST) {
                if (callModel.files != null && callModel.files.size() > 0)
                    api = SfwClient.getInstance().getApiInterfaceForString().callWebMultiPart(callModel.headers, ApiUtils.getApiPartParams(callModel.key, callModel.value), callModel.files, callModel.url);
                else if (callModel.fileOne != null || callModel.fileTwo != null)
                    api = SfwClient.getInstance().getApiInterfaceForString().callWebMultiPart(callModel.headers, ApiUtils.getApiPartParams(callModel.key, callModel.value), callModel.fileOne, callModel.fileTwo, callModel.url);
                else
                    api = SfwClient.getInstance().getApiInterfaceForString().callWebMultiPart(callModel.headers, ApiUtils.getApiPartParams(callModel.key, callModel.value), callModel.url);
            } else if (callModel.webServiceType == WebServiceType.PUT) {
                api = SfwClient.getInstance().getApiInterfaceForString().callWebFormUrl(callModel.headers, ApiUtils.getApiParams(callModel.key, callModel.value), callModel.url);
            } else if (callModel.webServiceType == WebServiceType.JSON_POST) {
                String params="";
                if(callModel.key!=null && callModel.key.length>0) {
                    params=ApiUtils.getJsonObject(callModel.key, callModel.value).toString();
                } else if(callModel.dataForJsonInput instanceof String) {
                    params=(String) callModel.dataForJsonInput;
                } else if(callModel.dataForJsonInput!=null) {
                    params=new Gson().toJson(callModel.dataForJsonInput);
                }
                api = SfwClient.getInstance().getApiInterfaceForString().callJsonInput(callModel.headers, params, callModel.url);
            } else if (callModel.webServiceType == WebServiceType.DELETE) {
                api = SfwClient.getInstance().getApiInterfaceForString().deleteData(callModel.headers, callModel.url);
            } else {
                api = SfwClient.getInstance().getApiInterfaceForString().fetchData(callModel.headers, callModel.url);
            }

            api.enqueue(new Callback<String>() {
                @Override
                public void onResponse(Call<String> call, Response<String> response) {
                    try {
                        fetchingApiTypes.remove(Integer.valueOf(callModel.apiCode));
                        if (callModel.getAct() != null && callModel.progressMsg != null)
                            callModel.getAct().dismissProgress();
                        handleResponse(call, response, type, callModel);
                    } catch (Exception e) {
                        log("response :   api code = " + callModel.apiCode + " URL = " + callModel.getUrl(), e, true);
                        onApiError(callModel, null, null, false, false, response==null?null:response.body());
                    }
                    onApiCompleted(callModel, false);
                }

                @Override
                public void onFailure(@NonNull Call<String> call, @NonNull Throwable t) {
                    fetchingApiTypes.remove(Integer.valueOf(callModel.apiCode));
                    onApiError(callModel, null, t, true, callModel.progressMsg != null, null);
                    onApiCompleted(callModel, true);
                }
            });
            return api;
        } catch (Exception e) {
            log("callweb :  URL = " + callModel.getUrl(), e, true);
            fetchingApiTypes.remove(Integer.valueOf(callModel.apiCode));
            onApiError(callModel, null, e, false, callModel.progressMsg != null, null);
        }
        return null;
    }

    protected final <T> Object parseResponse(Response<String> response, @NonNull Type type, SfwApiCallModel<T> callModel) {
        String message = response.body();
        if (message == null && response.errorBody() != null) {
            try {
                message = response.errorBody().string();
            } catch (Exception ignored) {
            }
        }
        if (message == null && isDebuggingEnabled()) {
            StringBuilder debug = new StringBuilder().append("Response is NULL.  apiCode=").append(callModel.getApiCode()).append("   url=").append(callModel.getUrl());
            debug.append(" Response Code = ").append(response.code()).append("  Response Message = ").append(response.message());
            log(debug.toString(), null, true);
        }

        if (response.body() == null && message != null) {
            log("Response status code is " + response.code() + "  Response Message = " + response.message() + ".  apiCode=" + callModel.getApiCode() + "   url=" + callModel.getUrl(), null, true);
        }

        return new Gson().fromJson(message, type);
    }


    protected <T> void handleResponse(Call<String> call, Response<String> response, @NonNull Type type, @NonNull SfwApiCallModel<T> callModel) {
        handleResponse(parseResponse(response, type, callModel), call, response, type, callModel);
    }

    /**
     * Override this method to handle the response string
     */
    protected <T> void handleResponse(Object parsedObject, Call<String> call, Response<String> response, @NonNull Type type, @NonNull SfwApiCallModel<T> callModel) {
        StatusInterface model = (StatusInterface) parsedObject;
        if (model.isSuccess()) {
            boolean isResultPassed = false;
            if (!TextUtils.isEmpty(callModel.methodToInvokeOnSuccess)) {
                isResultPassed = true;
                try {
                    Method method = getClass().getDeclaredMethod(callModel.methodToInvokeOnSuccess, StatusModel.class.equals(callModel.responseClassType) ? StatusModel.class : callModel.responseIsAList ? callModel.mainClassListType : callModel.mainClassSingleType, SfwApiCallModel.class);
                    method.setAccessible(true);
                    method.invoke(SfwApiHelper.this, parsedObject, callModel);
                } catch (Throwable e) {
                    log("handleResponse : invoking method = " + callModel.methodToInvokeOnSuccess, e, true);
                    if (e instanceof SecurityException || e instanceof NoSuchMethodError || e instanceof NoSuchMethodException || e instanceof InvocationTargetException || e instanceof ExceptionInInitializerError)
                        throw new IllegalAccessError(e.getLocalizedMessage());
                }
            } else if (!TextUtils.isEmpty(callModel.databaseKey) || callModel.shouldPassValueToDb) {
                isResultPassed = true;
                if (callModel.responseClassType.equals(StatusModel.class))
                    setDbValuesOnSuccess(callModel, null, null);
                else
                    setDbValuesOnSuccess(callModel, parsedObject instanceof ListModelInterface ? (ListModelInterface<T>) parsedObject : null, parsedObject instanceof SingleModelInterface ? (SingleModelInterface<T>) parsedObject : null);
            }
            if (TextUtils.isEmpty(callModel.methodToInvokeOnSuccess)) {
                if (callModel.shareResultGlobally)
                    callSuccess(callModel.apiCode, parsedObject,callModel);
                else if (callModel.getApiHelperListener() != null)
                    callModel.getApiHelperListener().onApiSuccess(callModel.apiCode, parsedObject,callModel);
                isResultPassed = isResultPassed || callModel.shareResultGlobally || callModel.getApiHelperListener() != null;
            }
            if (!isResultPassed) {
                String conditions = " SfwApiCallModel. [ methodToInvokeOnSuccess != null  OR databaseKey !=null OR shouldPassValueToDb == true OR shareResultGlobally == true || alertListener != null]";
                log("ApiSuccess but did nothing with result.  apicode=" + callModel.getApiCode() + " url = " + callModel.getUrl() + "  Should pass some conditions to perform any action. Conditions are " + conditions, null, true);
            }

        } else {
            onApiError(callModel, model.getMsg(), null, false, false, parsedObject);
        }
    }

    protected <T> void onApiError(@NonNull SfwApiCallModel<T> callModel, String message, Throwable error, boolean showInternetError, boolean dismissProgress, Object response) {
        try {
            SfwActivityInterface activity = callModel.getAct();
            if (dismissProgress && activity != null)
                activity.dismissProgress();
            if (callModel.getApiHelperListener() != null)
                callModel.getApiHelperListener().onApiError(callModel.apiCode, error == null ? new Exception(message == null ? "" : message) : error, response, callModel);
            if (activity != null && callModel.showMessageOnError) {
                if (showInternetError)
                    activity.showMessage(new AlertConfig(callModel.alertCode,((Context) activity).getString(R.string.tit_error),((Context) activity).getString(R.string.err_internet),R.drawable.ic_refresh).setPositiveText(((Context) activity).getString(R.string.retry)).setNegativeText(((Context) activity).getString(R.string.cancel)).setListener(callModel.getAlertListener()).setAsErrorWithImage());
//                    activity.showInternetErrMsg(false, callModel.alertCode, true, callModel.getAlertListener());
                else if (activity instanceof Context)
                    activity.showMessage(new AlertConfig(-1, ((Context) activity).getString(R.string.tit_error), message == null ? ((Context) activity).getString(R.string.msg_error) : message, R.drawable.ic_accept).setPositiveText(((Context) activity).getString(R.string.tit_ok)).setAsErrorWithImage());
            }
            if (callModel.getApiHelperListener() == null && (activity == null || !callModel.showMessageOnError))
                log("onApiError  : apiCode=" + callModel.apiCode + "  message=" + message, null, true);
        } catch (Exception e) {
            log("onApiError : apiCode=" + callModel.apiCode, e, true);
        }
    }


    protected <T> void setDbValuesOnSuccess(@NonNull SfwApiCallModel callModel, @Nullable ListModelInterface<T> listData, @Nullable SingleModelInterface<T> singleData) {
        if (callModel.clearPreviousData)
            SfwDatabase.getInstance().clear(callModel.responseClassType, callModel.databaseKey);
        if (callModel.responseIsAList) {
            if (callModel.clearPreviousData)
                SfwDatabase.getInstance().setData(callModel.databaseKey, listData == null ? null : listData.getData());
            else
                SfwDatabase.getInstance().addData(callModel.databaseKey, listData == null ? null : listData.getData());
        } else if (!StatusModel.class.equals(callModel.responseClassType)) {
            SfwDatabase.getInstance().setSingleData(singleData == null ? null : singleData.getData(), callModel.databaseKey);
        } else {
            log("setDbValuesOnSuccess : response is either a list (implements ListModelInterface) or a single ( implements SingleModelInterface ) object. Found " + StatusModel.class.getName() + "   api code = " + callModel.apiCode + " URL = " + callModel.getUrl(), null, true);
        }
    }

    public final void addFetchingApiType(Integer apiCode) {
        if (!fetchingApiTypes.contains(apiCode))
            fetchingApiTypes.add(apiCode);
    }

    public final boolean removeFetchingApiType(Integer apiCode) {
        return fetchingApiTypes.remove(apiCode);
    }

    public final boolean isFetchingApi(Integer apiCode) {
        return fetchingApiTypes.contains(apiCode);
    }

    protected void addCustomHeaders(@NonNull HashMap<String, String> headers, int apiCode) {
    }

    protected void addMoreKeyValueForParams(@NonNull SfwApiCallModel callModel) {
    }

    protected void onApiCompleted(@NonNull SfwApiCallModel callModel, boolean isError) {
    }

    protected void log(@NonNull String value, @Nullable Throwable t, boolean isError) {
        Sfw.log(TAG, value, t, isError, isDebuggingEnabled);
    }


}
