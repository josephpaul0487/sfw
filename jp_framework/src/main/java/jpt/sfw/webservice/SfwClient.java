package jpt.sfw.webservice;

import android.util.Log;

import java.lang.reflect.Constructor;
import java.lang.reflect.ParameterizedType;

import androidx.annotation.NonNull;
import jpt.sfw.annotations.AppConfig;
import okhttp3.OkHttpClient;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;
import retrofit2.converter.scalars.ScalarsConverterFactory;

public class SfwClient<T> {

    private static SfwClient ourInstance;

    private Retrofit retrofit = null;
    private SfwApiInterface sfwApiInterface, apiWithoutConverter;
    private T apiCustomInterface, apiCustomWithoutConverter;
    private Class<T> typeClass;
    private final String baseUrl;
    private boolean isDebuggingEnabled;

    private  long readTimeout;
    private  long connectionTimeout;
    private  long writeTimeout;


    public static SfwClient init(@NonNull AppConfig config) {
        Class<? extends SfwClient> cls=config.retrofitClientClassType();
        String baseUrl=config.retrofitBaseUrl();
        try {
            if(ourInstance==null) {

                if(cls.equals(SfwClient.class))
                    ourInstance=new SfwClient<SfwApiInterface>(baseUrl, config.retrofitConnectionTimeout(),config.retrofitReadTimeout(),config.retrofitWriteTimeout());
                else {
                    Constructor<? extends SfwClient> constructor = cls.getConstructor(String.class, long.class, long.class, long.class);
                    constructor.setAccessible(true);
                    ourInstance = constructor.newInstance(baseUrl,config.retrofitConnectionTimeout(),config.retrofitReadTimeout(),config.retrofitWriteTimeout());
                }
                ourInstance.isDebuggingEnabled=config.debuggingEnabled();

            }
        } catch (Exception e) {
            throw new IllegalAccessError("Can't initialize this class : "+cls.getName()+".  Class should contain a public empty constructor. Should not be an abstract class.");
        }
        return ourInstance;
    }

    public static SfwClient getInstance() {
        if(ourInstance==null)
            throw new IllegalAccessError("You should initialize this class using init(SfwClient client) method from your application class");
        return ourInstance;
    }

    public SfwClient(@NonNull String baseUrl) {
        this(baseUrl,0,0,0);

    }

    public SfwClient(@NonNull String baseUrl,long connectionTimeout,long readTimeout,long writeTimeout) {
        this.baseUrl = baseUrl;
        this.connectionTimeout=connectionTimeout;
        this.readTimeout=readTimeout;
        this.writeTimeout=writeTimeout;
        try {
            this.typeClass = (Class<T>) ((ParameterizedType) getClass()
                    .getGenericSuperclass()).getActualTypeArguments()[0];
        } catch (Exception ignored){
            try {
                this.typeClass= (Class<T>) SfwApiInterface.class;
            } catch (Exception ignored1) {
            }
        }

    }

    private OkHttpClient getHttpClient() {
        return UnsafeOkHttpClient.getUnsafeOkHttpClient(isDebuggingEnabled,connectionTimeout,readTimeout,writeTimeout,null,null);
    }



    private Retrofit returnRetrofit(String url) {
        return new Retrofit.Builder().client(getHttpClient()).baseUrl(url).addConverterFactory(GsonConverterFactory.create()).build();
    }

    private Retrofit retrofitForString(String url) {
        return new Retrofit.Builder().client(getHttpClient()).baseUrl(url).addConverterFactory(ScalarsConverterFactory.create()).build();
    }

    public Retrofit getApiClient() {
        if (retrofit == null)
            retrofit = returnRetrofit(baseUrl);
        return retrofit;
    }

    public Retrofit getApiClient(String baseUrl) {
        return returnRetrofit(baseUrl);
    }



    public SfwApiInterface getSfwApiInterface() {
        if (sfwApiInterface == null)
            sfwApiInterface =  getApiClient().create(SfwApiInterface.class);
        return sfwApiInterface;
    }

    public SfwApiInterface getApiInterfaceForString() {
        if (apiWithoutConverter == null)
            apiWithoutConverter =  retrofitForString(baseUrl).create(SfwApiInterface.class);
        return apiWithoutConverter;
    }

    public <E> E getApiInterfaceForString(@NonNull Class<E> type) {
        return retrofitForString(baseUrl).create(type);
    }

    public <E> E getApiInterfaceForString(@NonNull Class<E> type,@NonNull String baseUrl) {
        return retrofitForString(baseUrl).create(type);
    }

    public <E> E getApiInterface(@NonNull Class<E> type) {
        return returnRetrofit(baseUrl).create(type);
    }
    public <E> E getApiInterface(@NonNull Class<E> type,@NonNull String baseUrl) {
        return returnRetrofit(baseUrl).create(type);
    }

    public T getApiCustomInterface() {
        if (apiCustomInterface == null)
            apiCustomInterface =  getApiClient().create(typeClass);
        return apiCustomInterface;
    }

    public T getApiCustomInterfaceForString() {
        if (apiCustomWithoutConverter == null)
            apiCustomWithoutConverter =  retrofitForString(baseUrl).create(typeClass);
        return apiCustomWithoutConverter;
    }




}
