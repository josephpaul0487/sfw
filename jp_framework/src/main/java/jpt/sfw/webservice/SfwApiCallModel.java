package jpt.sfw.webservice;

import jpt.sfw.SfwActivity;
import jpt.sfw.database.SfwDatabase;
import jpt.sfw.interfaces.ListModelInterface;
import jpt.sfw.interfaces.SfwActivityInterface;
import jpt.sfw.app.AlertConfig;
import jpt.sfw.interfaces.OnAlertListener;
import jpt.sfw.interfaces.SingleModelInterface;
import jpt.sfw.models.CommonListModel;
import jpt.sfw.models.CommonModel;
import jpt.sfw.models.StatusModel;

import java.lang.ref.SoftReference;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import okhttp3.MultipartBody;

public class SfwApiCallModel<T> {
    @NonNull
    SfwApiHelper.WebServiceType webServiceType= SfwApiHelper.WebServiceType.POST;
    boolean shouldLoggedIn=false;
    int alertCode;
    int apiCode;
    @Nullable
    CharSequence progressMsg;
    boolean shareResultGlobally=false;
    boolean clearPreviousData=false;
    boolean showMessageOnError=true;

    @NonNull Class<T> responseClassType;
    @Nullable Class<? extends ListModelInterface> mainClassListType= CommonListModel.class;
    @Nullable Class<? extends SingleModelInterface> mainClassSingleType= CommonModel.class;
    boolean responseIsAList;
    String databaseKey;
    @NonNull String url;
    boolean isMultipart;
    @Nullable
    private SoftReference<SfwActivityInterface> act;
    @Nullable
    private SoftReference<OnAlertListener> alertListener;
    @Nullable
    private  SoftReference<ApiHelperListener> apiHelperListener;
    @Nullable
    List<MultipartBody.Part> files;
    @Nullable MultipartBody.Part fileOne;
    @Nullable MultipartBody.Part fileTwo;
    @Nullable String[] key;
    @Nullable String[] value;
    @NonNull HashMap<String,String> headers;
    @Nullable String methodToInvokeOnSuccess;

    Object tag;
    Object dataForJsonInput;

    /**
     * this value will check only    1. {@link #databaseKey} is empty
     *                               2. {@link #methodToInvokeOnSuccess} is empty
     */
    boolean shouldPassValueToDb;


    /**
     * Response should be an instance of {@link CommonModel<T>} || {@link CommonListModel <T>} || {@link StatusModel}
     *
     * @param alertCode               show message on failure {@link SfwActivityInterface#showInternetErrMsg(boolean, int, boolean, OnAlertListener)}
     * @param apiCode                 unique api code to identify result
     * @param progressMsg             progress dialog will not show if progressMsg is null
     * @param shareResultGlobally     true && result.isSuccess() -> Will share the result with the all registered {@link SfwApiHelper#listeners}     * @param showMessageOnError  result.IsError() or any exception occurred  -> Will show error message @see {@link SfwActivity#showInternetErrMsg(boolean, int, boolean, OnAlertListener)}  && {@link SfwActivityInterface#showMessage(AlertConfig)}
     * @param clearPreviousData       true -> {@link SfwDatabase#setData(String, List)}  will call
     *                                false -> {@link SfwDatabase#addData(String, List)} will call
     *                                it will ignored if methodToInvokeOnSuccess is not null
     * @param responseClassType       to which class the result should parse -> the type class should be an instance of {@link StatusModel}
     *                                Use UserModel if responseData is CommonModel<UserModel> Or CommonListModel<UserModel>
     * @param databaseKey             if not null  and api success-> will clear the contents from the {@link SfwDatabase#clear(Class, String)} associated with the key
     * @param url                     url of the api
     * @param isMultipart             use this for add or if having files to upload
     * @param act                     to show progress msg and error message
     * @param alertListener           to catch positive button click event from the {@link jpt.sfw.appui.AlertDialog} shows on failure
     * @param apiHelperListener       to get the response to the caller class {@link ApiHelperListener#onApiSuccess(int, Object)}
     * @param files                   if you want upload a files
     * @param fileOne                 some apis want to upload as single file
     * @param fileTwo                 some apis want to upload as single file
     * @param key                     For creating key value params {@link ApiUtils#getApiParams(String[], String[])} {@link ApiUtils#getApiPartParams(String[], String[])}
     * @param value                   For creating key value params {@link ApiUtils#getApiParams(String[], String[])} {@link ApiUtils#getApiPartParams(String[], String[])}
     * @param methodToInvokeOnSuccess the method to be called after a success api call..
     *                                you can use this function to perform any custom action
     *                                the method class should extends {@link SfwApiHelper}
     *                                the method must  contain 2 parameters
     *                                FIRST PARAM  :  {@link CommonListModel <T>} OR {@link CommonModel<T>} OR {@link StatusModel}
     *                                SECOND PARAM : {@link SfwApiCallModel<T>}
     *
     * @param apiCode
     * @param databaseKey
     * @param clearPreviousData
     * @param apiHelperListener
     *                                if methodToInvokeOnSuccess is null the {@link SfwDatabase#setData(String, List)} or {@link SfwDatabase#addData(String, List)} will call
     */
    public SfwApiCallModel(@NonNull SfwApiHelper.WebServiceType webServiceType, boolean shouldLoggedIn, int alertCode, int apiCode, @Nullable CharSequence progressMsg, boolean shareResultGlobally, boolean clearPreviousData,
                           boolean showMessageOnError, @NonNull Class<T> responseClassType,@Nullable Class<CommonListModel> mainClassListType, @Nullable Class<CommonModel> mainClassSingleType,
                           boolean responseIsAList, String databaseKey, @NonNull String url, boolean isMultipart, @Nullable SfwActivityInterface act, @Nullable OnAlertListener alertListener, @Nullable ApiHelperListener apiHelperListener, @Nullable List<MultipartBody.Part> files, @Nullable MultipartBody.Part fileOne, @Nullable MultipartBody.Part fileTwo, @Nullable String[] key, @Nullable String[] value, @Nullable HashMap<String,String> headers, @Nullable String methodToInvokeOnSuccess) {
        this.webServiceType=webServiceType;
        this.shouldLoggedIn=shouldLoggedIn;
        this.alertCode=alertCode;
        this.apiCode=apiCode;
        this.progressMsg=progressMsg;
        this.shareResultGlobally=shareResultGlobally;
        this.clearPreviousData=clearPreviousData;
        this.showMessageOnError=showMessageOnError;
        this.responseClassType=responseClassType;
        this.mainClassListType=mainClassListType;
        this.mainClassSingleType=mainClassSingleType;
        this.responseIsAList=responseIsAList;
        this.databaseKey=databaseKey;
        this.url=url;
        this.isMultipart=isMultipart;
        this.act=new SoftReference<>(act);
        this.alertListener=new SoftReference<>(alertListener);
        this.apiHelperListener=new SoftReference<>(apiHelperListener);
        this.files=files;
        this.fileOne=fileOne;
        this.fileTwo=fileTwo;
        this.key=key;
        this.value=value;
        this.headers=headers==null?new HashMap<>():headers;
        this.methodToInvokeOnSuccess=methodToInvokeOnSuccess;
    }


    public SfwApiCallModel(@NonNull SfwApiHelper.WebServiceType webServiceType, int alertCode, int apiCode, @Nullable CharSequence progressMsg, boolean showMessageOnError, @NonNull Class<T> responseClassType, @NonNull String url, boolean isMultipart, @Nullable SfwActivityInterface act, @Nullable OnAlertListener alertListener, @Nullable ApiHelperListener apiHelperListener, @Nullable String[] key, @Nullable String[] value, @NonNull HashMap<String, String> headers) {
        this.webServiceType = webServiceType;
        this.alertCode = alertCode;
        this.apiCode = apiCode;
        this.progressMsg = progressMsg;
        this.showMessageOnError = showMessageOnError;
        this.responseClassType = responseClassType;
        this.url = url;
        this.isMultipart = isMultipart;
        this.act = new SoftReference<>(act);
        this.alertListener = new SoftReference<>(alertListener);
        this.apiHelperListener = new SoftReference<>(apiHelperListener);
        this.key = key;
        this.value = value;
        this.headers = headers==null?new HashMap<>():headers;;
    }



    public SfwApiCallModel(@NonNull SfwApiHelper.WebServiceType webServiceType, int alertCode, int apiCode, @Nullable CharSequence progressMsg, @NonNull Class<T> responseClassType, String databaseKey, @NonNull String url, boolean isMultipart, @Nullable SfwActivityInterface act, @Nullable OnAlertListener alertListener, @Nullable ApiHelperListener apiHelperListener) {
        this(webServiceType,alertCode,apiCode,progressMsg,true,responseClassType,url,isMultipart,act,alertListener,apiHelperListener,null,null,null);
        this.databaseKey=databaseKey;
    }

    public SfwApiCallModel() {
    }

    public SfwApiCallModel(@NonNull SfwApiHelper.WebServiceType webServiceType, @NonNull Class<T> responseClassType, String databaseKey, @NonNull String url) {
        this.webServiceType = webServiceType;
        this.responseClassType = responseClassType;
        this.databaseKey = databaseKey;
        this.url = url;
    }

    public SfwApiCallModel<T> singleFileUpload(@Nullable MultipartBody.Part file,@NonNull String [] keys,@NonNull String [] values,@Nullable HashMap<String,String> headers) {
        setKeyValues(keys,values,headers==null?new HashMap<>():headers);
        this.fileOne=file;
        this.webServiceType= SfwApiHelper.WebServiceType.POST;
        this.isMultipart=isMultipart;
        return this;
    }

    public SfwApiCallModel<T> setListeners(@Nullable SfwActivityInterface act, @Nullable OnAlertListener alertListener, @Nullable ApiHelperListener apiHelperListener) {
            this.act=new SoftReference<>(act);
            this.alertListener=new SoftReference<>(alertListener);
            this.apiHelperListener=new SoftReference<>(apiHelperListener);
            return this;
    }

    public SfwApiCallModel<T> setCodes(int alertCode, int apiCode,@Nullable CharSequence progressMsg,boolean isMultipart) {
        this.alertCode=alertCode;
        this.apiCode=apiCode;
        this.progressMsg=progressMsg;
        this.isMultipart=isMultipart;
        return this;
    }

    public SfwApiCallModel<T> setWebdata(@NonNull SfwApiHelper.WebServiceType webServiceType,@Nullable String databaseKey, @NonNull String url) {
        this.databaseKey=databaseKey;
        this.url=url;
        this.webServiceType=webServiceType;
        return this;
    }

    public SfwApiCallModel<T> setKeyValues(@Nullable String[] keys, @Nullable String[] values, @NonNull HashMap<String, String> headers) {
        this.key=keys;
        this.value=values;
        this.headers=headers==null?new HashMap<>():headers;
        return this;
    }

    public SfwApiCallModel<T> setOthers(boolean showMessageOnError, boolean shareResultGlobally, boolean clearPreviousData,boolean responseIsAList) {
        this.showMessageOnError=showMessageOnError;
        this.shareResultGlobally=shareResultGlobally;
        this.clearPreviousData=clearPreviousData;
        this.responseIsAList=responseIsAList;
        return this;
    }

    public SfwApiCallModel<T> setClasses(@NonNull Class<T> responseClassType,@Nullable Class<CommonListModel> mainClassListType, @Nullable Class<CommonModel> mainClassSingleType) {
        this.responseClassType=responseClassType;
        this.mainClassListType=mainClassListType;
        this.mainClassSingleType=mainClassSingleType;
        return this;
    }

    public SfwApiCallModel<T> setFiles(@Nullable List<MultipartBody.Part> files, @Nullable MultipartBody.Part fileOne, @Nullable MultipartBody.Part fileTwo) {
        this.files=files;
        this.fileOne=fileOne;
        this.fileTwo=fileTwo;
        return this;
    }

    @NonNull
    public SfwApiHelper.WebServiceType getWebServiceType() {
        return webServiceType;
    }

    public SfwApiCallModel<T> setWebServiceType(@NonNull SfwApiHelper.WebServiceType webServiceType) {
        this.webServiceType = webServiceType;
        return this;
    }

    public boolean isShouldLoggedIn() {
        return shouldLoggedIn;
    }

    public SfwApiCallModel<T> setShouldLoggedIn(boolean shouldLoggedIn) {
        this.shouldLoggedIn = shouldLoggedIn;
        return this;
    }

    public int getAlertCode() {
        return alertCode;
    }

    public SfwApiCallModel<T> setAlertCode(int alertCode) {
        this.alertCode = alertCode;
        return this;
    }

    public int getApiCode() {
        return apiCode;
    }

    public SfwApiCallModel<T> setApiCode(int apiCode) {
        this.apiCode = apiCode;
        return this;
    }

    @Nullable
    public CharSequence getProgressMsg() {
        return progressMsg;
    }

    public SfwApiCallModel<T> setProgressMsg(@Nullable CharSequence progressMsg) {
        this.progressMsg = progressMsg;
        return this;
    }

    public boolean isShareResultGlobally() {
        return shareResultGlobally;
    }

    public SfwApiCallModel<T> setShareResultGlobally(boolean shareResultGlobally) {
        this.shareResultGlobally = shareResultGlobally;
        return this;
    }

    public boolean isClearPreviousData() {
        return clearPreviousData;
    }

    public SfwApiCallModel<T> setClearPreviousData(boolean clearPreviousData) {
        this.clearPreviousData = clearPreviousData;
        return this;
    }

    public boolean isShowMessageOnError() {
        return showMessageOnError;
    }

    public SfwApiCallModel<T> setShowMessageOnError(boolean showMessageOnError) {
        this.showMessageOnError = showMessageOnError;
        return this;
    }

    @NonNull
    public Class<T> getResponseClassType() {
        return responseClassType;
    }

    public SfwApiCallModel<T> setResponseClassType(@NonNull Class<T> responseClassType) {
        this.responseClassType = responseClassType;
        return this;
    }

    @Nullable
    public Class<? extends ListModelInterface> getMainClassListType() {
        return mainClassListType;
    }



    @Nullable
    public Class<? extends SingleModelInterface> getMainClassSingleType() {
        return mainClassSingleType;
    }



    public boolean isResponseIsAList() {
        return responseIsAList;
    }

    public SfwApiCallModel<T> setResponseIsAList(boolean responseIsAList) {
        this.responseIsAList = responseIsAList;
        return this;
    }

    public String getDatabaseKey() {
        return databaseKey;
    }

    public SfwApiCallModel<T> setDatabaseKey(String databaseKey) {
        this.databaseKey = databaseKey;
        return this;
    }

    @NonNull
    public String getUrl() {
        return url;
    }

    public SfwApiCallModel<T> setUrl(@NonNull String url) {
        this.url = url;
        return this;
    }

    public boolean isMultipart() {
        return isMultipart;
    }

    public SfwApiCallModel<T> setMultipart(boolean multipart) {
        isMultipart = multipart;
        return this;
    }

    @Nullable
    public SfwActivityInterface getAct() {
        return act.get();
    }

    public SfwApiCallModel<T> setAct(@Nullable SfwActivityInterface act) {
        this.act = new SoftReference<>(act);
        return this;
    }

    @Nullable
    public OnAlertListener getAlertListener() {
        return alertListener.get();
    }

    public SfwApiCallModel<T> setAlertListener(@Nullable OnAlertListener alertListener) {
        this.alertListener = new SoftReference<>(alertListener);
        return this;
    }

    @Nullable
    public ApiHelperListener getApiHelperListener() {
        return apiHelperListener.get();
    }

    public SfwApiCallModel<T> setApiHelperListener(@Nullable ApiHelperListener apiHelperListener) {
        this.apiHelperListener = new SoftReference<>(apiHelperListener);
        return this;
    }

    @Nullable
    public List<MultipartBody.Part> getFiles() {
        return files;
    }

    public SfwApiCallModel<T> setFiles(@Nullable List<MultipartBody.Part> files) {
        this.files = files;
        return this;
    }

    @Nullable
    public MultipartBody.Part getFileOne() {
        return fileOne;
    }

    public SfwApiCallModel<T> setFileOne(@Nullable MultipartBody.Part fileOne) {
        this.fileOne = fileOne;
        return this;
    }

    @Nullable
    public MultipartBody.Part getFileTwo() {
        return fileTwo;
    }

    public SfwApiCallModel<T> setFileTwo(@Nullable MultipartBody.Part fileTwo) {
        this.fileTwo = fileTwo;
        return this;
    }

    @Nullable
    public String[] getKey() {
        return key;
    }

    public SfwApiCallModel<T> setKey(@Nullable String[] key) {
        this.key = key;
        return this;
    }

    @Nullable
    public String[] getValue() {
        return value;
    }

    public SfwApiCallModel<T> setValue(@Nullable String[] value) {
        this.value = value;
        return this;
    }

    @NonNull
    public HashMap<String, String> getHeaders() {
        return headers;
    }

    public SfwApiCallModel<T> setHeaders(@NonNull HashMap<String, String> headers) {
        this.headers = headers==null?new HashMap<>():headers;;
        return this;
    }

    @Nullable
    public String getMethodToInvokeOnSuccess() {
        return methodToInvokeOnSuccess;
    }

    public SfwApiCallModel<T> setMethodToInvokeOnSuccess(@Nullable String methodToInvokeOnSuccess) {
        this.methodToInvokeOnSuccess = methodToInvokeOnSuccess;
        return this;
    }

    public boolean isShouldPassValueToDb() {
        return shouldPassValueToDb;
    }

    public SfwApiCallModel<T> setShouldPassValueToDb(boolean shouldPassValueToDb) {
        this.shouldPassValueToDb = shouldPassValueToDb;
        return this;
    }

    public SfwApiCallModel<T> setMainClassListType(@Nullable Class<? extends ListModelInterface> mainClassListType) {
        this.mainClassListType = mainClassListType;
        return this;
    }

    public SfwApiCallModel<T> setMainClassSingleType(@Nullable Class<? extends SingleModelInterface> mainClassSingleType) {
        this.mainClassSingleType = mainClassSingleType;
        return this;
    }

    public SfwApiCallModel<T> setAct(@Nullable SoftReference<SfwActivityInterface> act) {
        this.act = act;
        return this;
    }

    public SfwApiCallModel<T> setAlertListener(@Nullable SoftReference<OnAlertListener> alertListener) {
        this.alertListener = alertListener;
        return this;
    }

    public SfwApiCallModel<T> setApiHelperListener(@Nullable SoftReference<ApiHelperListener> apiHelperListener) {
        this.apiHelperListener = apiHelperListener;
        return this;
    }

    public Object getTag() {
        return tag;
    }

    public SfwApiCallModel<T> setTag(Object tag) {
        this.tag = tag;
        return this;
    }

    public Object getDataForJsonInput() {
        return dataForJsonInput;
    }

    public SfwApiCallModel<T> setDataForJsonInput(Object dataForJsonInput) {
        this.dataForJsonInput = dataForJsonInput;
        return this;
    }

    @Override
    public String toString() {
        return "SfwApiCallModel{" +
                "webServiceType=" + webServiceType +
                ", shouldLoggedIn=" + shouldLoggedIn +
                ", alertCode=" + alertCode +
                ", apiCode=" + apiCode +
                ", progressMsg=" + progressMsg +
                ", shareResultGlobally=" + shareResultGlobally +
                ", clearPreviousData=" + clearPreviousData +
                ", showMessageOnError=" + showMessageOnError +
                ", responseClassType=" + responseClassType +
                ", mainClassListType=" + mainClassListType +
                ", mainClassSingleType=" + mainClassSingleType +
                ", responseIsAList=" + responseIsAList +
                ", databaseKey='" + databaseKey + '\'' +
                ", url='" + url + '\'' +
                ", isMultipart=" + isMultipart +
                ", act=" + act +
                ", alertListener=" + alertListener +
                ", apiHelperListener=" + apiHelperListener +
                ", files=" + files +
                ", fileOne=" + fileOne +
                ", fileTwo=" + fileTwo +
                ", key=" + Arrays.toString(key) +
                ", value=" + Arrays.toString(value) +
                ", headers=" + headers +
                ", methodToInvokeOnSuccess='" + methodToInvokeOnSuccess + '\'' +
                ", shouldPassValueToDb=" + shouldPassValueToDb +
                '}';
    }
}
