package jpt.sfw.interfaces;

import java.util.List;

public interface ListModelInterface<T>  extends StatusInterface {
    List<T> getData();
}
