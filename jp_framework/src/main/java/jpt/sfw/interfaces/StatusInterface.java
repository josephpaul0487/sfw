package jpt.sfw.interfaces;

import java.util.List;

public interface StatusInterface {
    boolean isSuccess();
    String getMsg();
    String getImagePath();
    List<String> getErrors();
    int getTagCode();
}
