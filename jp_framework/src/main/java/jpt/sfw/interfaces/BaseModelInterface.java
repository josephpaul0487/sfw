package jpt.sfw.interfaces;

public interface BaseModelInterface {
    public long getId();

    public String getMainImage();

    public String getTitle();

}
