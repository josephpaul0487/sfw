package jpt.sfw.interfaces;

import android.graphics.Bitmap;
import android.view.View;

/**
 * Created by C9 on 12/12/2017.
 */

public interface ImageLoadingListener {
    void onLoadingFailed(String url, View view, Exception ignored);
    void onLoadingStarted(String url, View view);
    void onLoadingComplete(String url, View view, Bitmap image);
}
