package jpt.sfw.interfaces;

import android.view.View;

import jpt.sfw.app.AlertConfig;
import jpt.sfw.app.BackStackHolder;
import jpt.sfw.app.FragmentConfig;
import jpt.sfw.app.ProgressConfig;
import jpt.sfw.app.ToastConfig;
import jpt.sfw.appui.AlertDialog;
import jpt.sfw.appui.ProgressDialog;
import jpt.sfw.interfaces.OnActivityListener;
import jpt.sfw.interfaces.OnAlertListener;

import androidx.annotation.ColorInt;
import androidx.annotation.DrawableRes;
import androidx.annotation.IdRes;
import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.annotation.StringRes;
import androidx.fragment.app.Fragment;

public interface SfwActivityInterface {

    void setStatusBarColor(@ColorInt int statusBarColor);
    void onBackPressed();
    void onLowMemory();

    void addOnActivityListener(OnActivityListener onActivityListener);
    void removeOnActivityListener(OnActivityListener onActivityListener);
    Fragment getCurrentFragment();
    Fragment getCurrentFragment(@IdRes int containerId);
    String getCurrentFragmentAsString();
    void onLoginDetailsChanged(boolean forceLogout);

    void showProgress(ProgressConfig config);
    void showProgress(ProgressConfig config, boolean changeMsgIfShowing);
    ProgressDialog getProgressDialog();
    void dismissProgress();

    void showMessage(@NonNull AlertConfig config);
    AlertDialog getAlertDialog();
    void showInternetErrMsg(boolean dismissProgressDialog, int requestCode, boolean setRetry, @Nullable OnAlertListener listener);
    void showInternetErrMsg(boolean dismissProgressDialog, int requestCode, boolean setRetry, @DrawableRes int negativeImage, @Nullable OnAlertListener listener);
    void showInternetErrMsg(boolean dismissProgressDialog, int requestCode, @DrawableRes int positiveImage, @DrawableRes int negativeImage, @Nullable OnAlertListener listener);

    void toast(@StringRes int stringId);
    void toast(CharSequence msg);
    void toast(ToastConfig msg);

    FragmentConfig createConfig(@NonNull Fragment fragment, CharSequence pageTitle, Object data);
    boolean changeFragment(@NonNull Fragment fragment, CharSequence pageTitle, Object data);
    boolean changeFragment(@NonNull Fragment fragment, CharSequence pageTitle, Object data, boolean needDrawer);
    boolean addTagAndChangeFragment(@NonNull Fragment fragment, @NonNull FragmentConfig configWithoutTag);
    boolean changeFragment(@NonNull Fragment fragment, @NonNull FragmentConfig config);


    void afterFragmentCreated(@NonNull Fragment f);
    boolean clearBackStack(String fragmentTagToBeKeep);
    boolean clearBackStack(int upTo);
    boolean clearBackStackUpTo(String fragmentTagUpTo,boolean includeThis);
    boolean clearBackStacks(int startIndex,int count,boolean topToBottom);
    boolean clearTopBackStacks(int count);
    boolean clearBottomBackStacks(int count);
    boolean clearSingleFragment(String fragmentTagToBeCleared, boolean forceRemoveTop);
    View getFragmentViewFromBackStack(String tag);
    Fragment getFragment(String tag);
    FragmentConfig getFragmentConfig(String tag);
    FragmentConfig getFragmentConfig(Fragment fragment);
    void setViewToBackStack(View view, String tag);
    void setDataToBackStack(Object data, String tag);
    int getBackStackCount();
    boolean isFragmentAnimationUsed();

    BackStackHolder.FragmentContentHolder getTopBackStackHolder(String fragmentTag);
    BackStackHolder.FragmentContentHolder getBottomBackStackHolder(String fragmentTag);
    boolean isTagExistsInBackStack(String fragmentTag);
    boolean isBackStackTop(String fragmentTagToCheck, String fragmentToCompare);
    int getBackStackIndex(String fragmentTag);
    void onRecreateCalled();
    boolean changeFragmentToLastHolder();


}
