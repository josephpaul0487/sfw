package jpt.sfw.interfaces;

public interface OnActivityListener {

    boolean onBackPressed();
    void onInternetConnectionChange(boolean isConnected);
    void onLoginDetailsChanged(boolean forceLogout);
    void onDialogClosed(int dialogId);
    boolean isAfterCreatedFragmentCalled();
    String getHolderTag();
}
