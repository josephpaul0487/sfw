package jpt.sfw.interfaces;

public interface HelperMethods {
    default String avoidNull(String str) {
        return str==null?"":str;
    }

    default String trim(String str) {
        return str==null?"":str.trim();
    }

    default boolean isEmpty(String str) {
        return avoidNull(str).isEmpty();
    }

    default boolean isEmpty(String str,boolean trim) {
        return trim?trim(str).isEmpty():avoidNull(str).isEmpty();
    }
}
