package jpt.sfw.interfaces;

public interface DatePickerListener {
    void afterPickDate(String dateFormat,String parsedDate,int year,int month,int date);
}
