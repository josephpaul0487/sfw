package jpt.sfw.interfaces;

public interface SingleModelInterface<T> extends StatusInterface {
    T getData();
}
