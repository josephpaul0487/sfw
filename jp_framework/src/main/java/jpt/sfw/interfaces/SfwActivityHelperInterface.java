package jpt.sfw.interfaces;

import android.content.Intent;
import android.content.IntentFilter;
import android.os.Bundle;
import android.view.View;

import jpt.sfw.appui.AlertDialog;
import jpt.sfw.appui.ProgressDialog;

import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;

public interface SfwActivityHelperInterface {
    void onCreate(@Nullable Bundle savedInstanceState);
    View getContentView();
    boolean init(@Nullable Bundle savedInstanceState);
    void initListeners();
    void registerReceiver();
    boolean callHome();
    boolean isHome(Fragment fragment);
    void onClick(View v,int viewId);
    void addBroadcastReceiverActions(IntentFilter filter);
    void onInternetConnectionChange(boolean isNetworkAvailable);
    boolean broadcastMessageReceived(Intent intent);
    ProgressDialog createProgressDialog();
    AlertDialog createAlertDialog();
}
