package jpt.sfw.interfaces;

import android.text.style.ForegroundColorSpan;
import android.view.View;

import androidx.annotation.ColorInt;
import androidx.annotation.LayoutRes;
import androidx.annotation.NonNull;
import androidx.annotation.StringRes;
import androidx.recyclerview.widget.RecyclerView;

import java.util.List;

import jpt.sfw.adapters.ListingAdapterSfw;
import jpt.sfw.ui.SfwEditText;

 public interface SfwListingDialogInterfaces {
    void setListener(OnAdapterListener<BaseModelInterface> listener);
    void setList(List<? extends BaseModelInterface> models);
    List<? extends BaseModelInterface> getList();
    BaseModelInterface getItem(int position);
    void setSearchQuery(CharSequence searchQuery);
    int getItemCount();
    void setListingItemLayout(@LayoutRes int layout);
    void showCloseButton(boolean show);
     void setEdtHint(CharSequence text);

     SfwEditText getEditText();

     RecyclerView getRecyclerView();

     View getCloseView();

     ListingAdapterSfw getAdapter();

     void changeAdapter(@NonNull ListingAdapterSfw adapter) ;

     OnAdapterListener<BaseModelInterface> getListener();

     void setListingItemTextColor(@ColorInt int textColor);

    @ColorInt
     int getListingItemTextColor();

     void setListingItemSpanColor(@ColorInt int spanColor) ;
     ForegroundColorSpan getListingItemSpanColor() ;
}
