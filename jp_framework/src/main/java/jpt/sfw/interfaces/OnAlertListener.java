package jpt.sfw.interfaces;

import jpt.sfw.app.AlertConfig;

/**
 * Created by C9 on 9/25/2017.
 */

public interface OnAlertListener {
    default void onAlertPositiveClicked(int requestId){}
    default  void onAlertNegativeClicked(int requestId){}
    default  void onAlertCancelled(int requestId, boolean isProgressDialog){}
    default void onAlertPositiveClicked(int requestId, AlertConfig config){
        onAlertPositiveClicked(requestId);
    }
    default  void onAlertNegativeClicked(int requestId, AlertConfig config){
        onAlertNegativeClicked(requestId);
    }
    default  void onAlertCancelled(int requestId, boolean isProgressDialog, AlertConfig config){
        onAlertCancelled(requestId,isProgressDialog);
    }
}
