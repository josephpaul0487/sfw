package jpt.sfw.interfaces;

import android.view.View;

public interface OnAdapterListener<T> {
    void onViewClick(View view, int adapterPosition, T tag);
}
