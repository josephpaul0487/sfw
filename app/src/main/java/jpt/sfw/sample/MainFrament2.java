package jpt.sfw.sample;

import android.os.Bundle;
import android.view.View;
import android.widget.Button;

import androidx.annotation.Nullable;

import jpt.sfw.annotations.OnClick;
import jpt.sfw.annotations.SfwFragmentAnnotation;
import jpt.sfw.fragments.SfwFragment;

@OnClick(R.id.btn2)
@SfwFragmentAnnotation(contentViewLayout = R.layout.frag_main2)
public class MainFrament2 extends SfwFragment {
    @Override
    protected void init(View rootView, Object data, boolean isViewCreatedFromBackStack, @Nullable Bundle savedInstanceState) {

    }

    @Override
    protected void onClick(View view, int viewId) {
        toast(((Button)view).getText());
    }
}
