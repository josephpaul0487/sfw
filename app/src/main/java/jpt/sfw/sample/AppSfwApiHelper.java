package jpt.sfw.sample;

import android.util.Log;

import androidx.annotation.NonNull;

import java.lang.reflect.Type;

import jpt.sfw.models.CommonModel;
import jpt.sfw.webservice.ApiHelperListener;
import jpt.sfw.webservice.SfwApiCallModel;
import jpt.sfw.webservice.SfwApiHelper;

public class AppSfwApiHelper extends SfwApiHelper {

    public AppSfwApiHelper() {
    }

    public static AppSfwApiHelper getInstance() {
        return (AppSfwApiHelper) SfwApiHelper.getInstance();
    }

    @Override
    protected <T> void handleResponse(retrofit2.Call<String> call, retrofit2.Response<String> response, @NonNull Type type, @NonNull SfwApiCallModel<T> callModel) {
        Log.e("handleResponse","="+response.body());
        try {
            super.handleResponse(call, response, type, callModel);
        } catch (Exception e) {
            Log.e("handleResponse","=",e);
        }
    }

    private void afterFetchLocation(CommonModel locations, int apiCode, String dbKey, boolean clearPreviousData, ApiHelperListener listener) {
        Log.e("afterFetchLocation","=enter");
    }
}
