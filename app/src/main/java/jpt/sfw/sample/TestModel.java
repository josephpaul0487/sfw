package jpt.sfw.sample;

import com.google.gson.annotations.SerializedName;

import jpt.sfw.annotations.DbAnnotation;
import jpt.sfw.annotations.DbEntity;
import jpt.sfw.annotations.DbExclude;
import jpt.sfw.annotations.DbPrimary;
import jpt.sfw.enums.ColumnTypes;
import jpt.sfw.interfaces.BaseModelInterface;
import jpt.sfw.models.BaseModel;

//@Entity
@DbEntity(tables = {"test","other_test", "next"}, staticClassName = "Testing3")
public class TestModel extends BaseModel implements BaseModelInterface {


    @SerializedName("primary_id")
    long id;
    @DbPrimary(tables = "other_test")
    long otherPrimary;
    @DbPrimary(tables ="next")
    long thirdPrimary;
    @DbAnnotation(columnName = "test")
    @DbExclude
    String tesrr;

    @DbPrimary(tables = "test")
    @DbAnnotation(columnName = "name", columnType = ColumnTypes.VARCHAR, length = 100)
    String fullName;

    final int finalField;

    @DbExclude(tables = {"other_test", "next"})
    String newTest;
    @DbExclude(tables = {"other_test"})
    boolean booleanValue;

    public TestModel() {
        this(0);
    }

    public TestModel(int finalField) {
        this.finalField = finalField;
    }

    public long getId() {
        return id;
    }

    public TestModel setId(long id) {
        this.id = id;
        return this;
    }

    public String getFullName() {
        return avoidNull(fullName);
    }

    public TestModel setFullName(String fullName) {
        this.fullName = fullName;
        return this;
    }

    public String getTest() {
        return tesrr;
    }

    public TestModel setTest(String test) {
        this.tesrr = test;
        return this;
    }

    public String getNewTest() {
        return newTest;
    }

    public TestModel setNewTest(String newTest) {
        this.newTest = newTest;
        return this;
    }

    public boolean isBooleanValue() {
        return booleanValue;
    }

    public TestModel setBooleanValue(boolean booleanValue) {
        this.booleanValue = booleanValue;
        return this;
    }

    @Override
    public String toString() {
        return "TestModel{" +
                "id=" + id +
                ", otherPrimary=" + otherPrimary +
                ", thirdPrimary=" + thirdPrimary +
                ", tesrr='" + tesrr + '\'' +
                ", fullName='" + fullName + '\'' +
                ", finalField=" + finalField +
                ", newTest='" + newTest + '\'' +
                ", booleanValue=" + booleanValue +
                '}';
    }
}
