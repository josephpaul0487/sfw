package jpt.sfw.sample;

import android.app.Application;

import jpt.sfw.annotations.AppConfig;
import jpt.sfw.app.Sfw;

@AppConfig(debuggingEnabled = true,retrofitBaseUrl = "http://pms.sbkrealestate.com/fsuite/api/fmapps/",apiHelperClassType = AppSfwApiHelper.class,databaseClassType = AppDb.class,retrofitClientClassType = AppSfwClient.class)
public class App extends Application {

    @Override
    public void onCreate() {
        super.onCreate();
        Sfw.setup(this);
    }




}
