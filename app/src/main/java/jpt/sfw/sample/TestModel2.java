package jpt.sfw.sample;

import jpt.sfw.annotations.DbEntity;
import jpt.sfw.interfaces.BaseModelInterface;

//@Entity
@DbEntity(tables = {"test2"}, staticClassName = "TestingTwo")
public class TestModel2 extends TestModel implements BaseModelInterface {



    final int finalField;


    public TestModel2() {
        this(0);
    }

    public TestModel2(int finalField) {
        this.finalField = finalField;
    }

    public long getId() {
        return id;
    }

    public TestModel2 setId(long id) {
        this.id = id;
        return this;
    }


}
