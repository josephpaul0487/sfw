package jpt.sfw.sample;

import androidx.annotation.NonNull;

import jpt.sfw.webservice.SfwClient;

public class AppSfwClient extends SfwClient<AppApiInterface> {
    public AppSfwClient(@NonNull String baseUrl,long connectionTimeOut,long readTimeOut,long writeTimeOut) {
        super(baseUrl,connectionTimeOut,readTimeOut,writeTimeOut);
    }



    public static AppSfwClient getInstance() {
        return (AppSfwClient) SfwClient.getInstance();
    }
}
