package jpt.sfw.sample;

import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.ImageView;

import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;

import org.json.JSONObject;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;

import jpt.sfw.SfwActivity;
import jpt.sfw.adapters.ListingAdapterSfw;
import jpt.sfw.annotations.BindView;
import jpt.sfw.annotations.OnClick;
import jpt.sfw.annotations.SfwActivityAnnotation;
import jpt.sfw.app.Sfw;
import jpt.sfw.appui.SfwBottomMenuLayout;
import jpt.sfw.appui.SfwListPopupWindow;
import jpt.sfw.interfaces.BaseModelInterface;
import jpt.sfw.models.BaseModel;
import jpt.sfw.webservice.ApiUtils;
import jpt.sfw.webservice.SfwClient;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

@SfwActivityAnnotation(contentViewLayout = R.layout.activity_main,isNeedToShowInfoOnBackPress = true)
public class MainActivity extends SfwActivity<AppDb> {

    @BindView(R.id.layoutBottom)
    SfwBottomMenuLayout layoutBottom;
    @OnClick(R.id.image)
    ImageView image;
    SfwListPopupWindow popupWindow;

    @Override
    public boolean init(@Nullable Bundle savedInstanceState) {

        List<BaseModelInterface> test=new ArrayList<>();
        ListingAdapterSfw adapterSfw=new ListingAdapterSfw(null);
        adapterSfw.setList(test);

        popupWindow=new SfwListPopupWindow(this);
        popupWindow.setList(Arrays.asList(new BaseModel(1,"1"),new BaseModel(2,"1=gjgjg"),new BaseModel(1,"3hgjgyjg")));
        popupWindow.setHideTop(true);
        popupWindow.setMinWidth(700);



        Log.e("URL","="+"user/login"+ApiUtils.paramsAsString(new String[]{"userId","password"},new String[]{"Techs","Techs"}));
//        SfwClient.getInstance().getApiInterfaceForString().callPostParams(new HashMap<>(), ApiUtils.getApiParams(new String[]{"userId","password"},new String[]{"Techs","Techs"}),"user/login").enqueue(new Callback<String>() {
        SfwClient.getInstance().getApiInterfaceForString().callJsonInput(new HashMap<>(), ApiUtils.getJsonObject(new String[]{"userId","password"},new String[]{"Techs","Techs"}).toString(),"WorkOrder/Add").enqueue(new Callback<String>() {
            @Override
            public void onResponse(Call<String> call, Response<String> response) {
                Log.e("Rees","="+response.body());
                try {
                    JSONObject object=new JSONObject(response.body());
                    SfwClient.getInstance().getApiInterfaceForString().callPostUrlParams(ApiUtils.getApiParams(new String[]{"token"},new String[]{object.getJSONObject("data").getString("token")}), "WorkOrder/GetWorkOrders"+ApiUtils.paramsAsString(new String[]{"status"},new String[]{"O"})).enqueue(new Callback<String>() {
                        @Override
                        public void onResponse(Call<String> call, Response<String> response) {
                            Log.e("Rees 2","="+response.body());

                        }

                        @Override
                        public void onFailure(Call<String> call, Throwable t) {
                            Log.e("FAILE  2","",t);
                        }
                    });
                } catch (Exception e) {
                    Log.e("Exxx","",e);
                }
            }

            @Override
            public void onFailure(Call<String> call, Throwable t) {
Log.e("FAILE","",t);
            }
        });


        test.add(new TestModel(10).setFullName("ER ").setNewTest("hello"));
//        db.addData(Testing.TEST,test);
        db.setSingleData(test.get(0),null);
//        db.putAll(new DbPutterModel<>(TestModel_tbl_SFW.TEST,false,test));

//        db.putAll(new DbPutterModel<>(Testing.OTHER_TEST,false, Collections.singletonList(new TestModel(11).setFullName("other test "+System.currentTimeMillis()).setNewTest("ssss").setBooleanValue(true))));
//
//        db.putAll(new DbPutterModel<>(Testing.NEXT,false, Collections.singletonList(new TestModel(12).setFullName("next "+System.currentTimeMillis()).setNewTest("ddddd"))));
//        try {
//            Log.e("Get test",db.getAll(new DbGetterModel<>(TestModel.class,Testing.TEST)).toString());
//        } catch (Throwable e) {
////            Sfw.log("Get test","",e,true);
//        }
//        try {
//            Log.e("Get other",db.getAll(new DbGetterModel<>(TestModel.class,Testing.NEXT)).toString());
//        } catch (Throwable e) {
////            Sfw.log("Get other","",e,true);
//        }
//        try {
//            Log.e("Get next",db.getAll(new DbGetterModel<>(TestModel.class,Testing.OTHER_TEST)).toString());
//        } catch (Throwable e) {
////            Sfw.log("Get next","",e,true);
//        }

        Sfw.log("hai","----------------------------------------------------------------------",null,false,true);
        Sfw.log("hai","----------------------------------------------------------------------",null,false,true);
        Sfw.log("hai","----------------------------------------------------------------------",null,false,true);


        return false;
    }

    @Override
    protected void onResume() {
        super.onResume();
        //layoutBottom.setList(Arrays.asList(new ValuesWithImageModel(1,"ddd",R.drawable.ic_attachment)));
    }

    @Override
    public void onClick(View v, int viewId) {
        switch (getBackStackCount()) {
            case 1:changeFragment(new MainFrament2(),"",null);
            break;
            case 2:changeFragment(new MainFrament3(),"",null);
                break;
            case 3:changeFragment(new MainFrament4(),"",null);
                break;
            case 4:changeFragment(new MainFrament5(),"",null);
                break;
        }

//        popupWindow.show(image,false);
    }

    @Override
    public boolean isHome(Fragment fragment) {
//        return true;
        return fragment instanceof MainFrament;
    }

    @Override
    public boolean callHome() {
        changeFragment(new MainFrament(),"",null);
        return true;
    }






}
