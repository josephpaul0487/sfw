package jpt.sfw.sample;

import android.content.Context;
import android.util.Log;

import jpt.sfw.annotations.DbConfig;
import jpt.sfw.database.SfwDatabase;

@DbConfig(entities = {TestModel.class},dbName = "Test",debuggingEnabled = true,version = 65)
public class AppDb extends SfwDatabase {
    protected AppDb(Context ctx) {
        super(ctx);
        Log.e("CAA","CALLLALA");
    }

    public static AppDb getInstance() {
        return (AppDb) SfwDatabase.getInstance();
    }

    public void set() {
        Log.e("Esssss","set called");
    }
}
