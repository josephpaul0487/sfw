package jpt.sfw.sample;

import android.view.View;
import android.widget.Button;

import jpt.sfw.annotations.OnClick;

@OnClick(R.id.btn1)
public class ChildFragment extends MainFrament2 {

    @Override
    protected void onClick(View view, int viewId) {
        toast(((Button)view).getText()+"  child");
    }
}
