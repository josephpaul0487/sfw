package jpt.sfw.sample;

import android.os.Bundle;
import android.view.View;
import android.widget.EditText;

import androidx.annotation.Nullable;

import jpt.sfw.annotations.BindView;
import jpt.sfw.annotations.SfwFragmentAnnotation;
import jpt.sfw.fragments.SfwFragment;

@SfwFragmentAnnotation(contentViewLayout = R.layout.frag_main,onClickViewIds = R.id.btnCommon)
public class MainFrament extends SfwFragment {

    @Override
    protected void init(View rootView, Object data, boolean isViewCreatedFromBackStack, @Nullable Bundle savedInstanceState) {
//getChildFragmentManager().beginTransaction().replace(R.id.childFrame,new ChildFragment()).commit();
    }

    @Override
    protected void onClick(View view, int viewId) {
       // if(Validation.isAValidPasswordTV(editText))
        changeFragment(new MainFrament2(),"",null);
    }


    @BindView(R.id.edt)
    EditText editText;
}
