package jpt.sfw.sample;

import android.os.Bundle;

import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;

import jpt.sfw.SfwActivity;
import jpt.sfw.annotations.SfwActivityAnnotation;

@SfwActivityAnnotation(contentViewLayout = R.layout.activity_main2)
public class Main2Activity extends SfwActivity {


    @Override
    public boolean init(@Nullable Bundle savedInstanceState) {


        return false;
    }

    @Override
    public boolean isHome(Fragment fragment) {
        return false;
    }

    @Override
    public boolean callHome() {
        return false;
    }

}
