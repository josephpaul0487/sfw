package jpt.sfw.annotations;

import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.Target;

import static java.lang.annotation.RetentionPolicy.RUNTIME;
import static java.lang.annotation.RetentionPolicy.SOURCE;

@Target(ElementType.METHOD)
@Retention(SOURCE)
public @interface SfwApiAnnotation {
    Class webServiceType();
    Class responseClassType();
    Class mainClassListType();
    Class mainClassSingleType();


}
