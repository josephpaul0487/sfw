package jpt.sfw.annotations;

import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.Target;

import static java.lang.annotation.RetentionPolicy.RUNTIME;


@Target({ElementType.FIELD,ElementType.TYPE})
@Retention(RUNTIME)
public @interface OnClick  {
    /** View ID to which the filed to be bound. */
    int value() default -1;

    /** Use this argument in library projects to bind the views
     * eg :  Use   @BindView(idName = "mainLayout")   for R.id.mainLayout
     * Inconstant values not supporting in annotations while using in android library project
     */
    String idName() default "";

    /** View IDs to which the filed to be bound. */

    int [] values() default {};
}
