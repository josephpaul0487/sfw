package jpt.sfw.annotations;

import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

@Target(ElementType.METHOD)
@Retention(RetentionPolicy.CLASS)
public @interface SfwQuery {
    /**
     * The SQLite query to be run.
     * @return The query to be run.
     */
    String value();
}
