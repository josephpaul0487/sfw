package jpt.sfw.annotations;

public @interface SfwUpdate {
    String table() default "";
}
