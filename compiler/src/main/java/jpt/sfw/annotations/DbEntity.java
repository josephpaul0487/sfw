package jpt.sfw.annotations;

import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

@Retention(RetentionPolicy.RUNTIME)
@Target(ElementType.TYPE)
public @interface DbEntity {
    /**
     *
     * @return  Table names  :: The class name will set as table name if it returns empty array
     */
    String[] tables() default {};
    boolean useRetrofitAnnotationForColumnName() default true;
    /**
     * @return  Table names  :: these tables data will clear while the database version is upgrading
     */
    String [] clearTablesOnUpgrade() default {};

    /**
     * @return  This classname used to generate the Original entity class's table names and column names to static field
     * Class name should not be the same as Entity class name
     * The Entity class's package name should not have another file named as this
     *
     * Eg:  Entity Class name = MyFirstDbEntityModel.java     annotated with DbEntity(tables="tbl_user" , staticClassName="MFDEM")  and have field id
     *      Then at compile time we will create another java file named as MFDEM.java
     *      with fields public static final String TBL_USER="tbl_user";
     *                  public static final String CLM_ID="id";
     *
     */
    String staticClassName() default "";
}
