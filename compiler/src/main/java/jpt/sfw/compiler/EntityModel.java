package jpt.sfw.compiler;

import java.util.Arrays;

public class EntityModel {
    private String[] entities;
    private String className;
    private String packageName;
    private String fullName;
    private String newClassName;
    private boolean useRetrofitAnnotationForColumnName;

    EntityModel(String[] entities, String className, String packageName, String fullName, String newClassName,boolean useRetrofitAnnotationForColumnName) {
        this.entities = entities==null || entities.length==0?new String[]{className}:entities;
        this.className = className==null?"":className;
        this.packageName = packageName==null?"":packageName;
        this.fullName = fullName==null?"":fullName;
        this.newClassName = newClassName==null || newClassName.replaceAll("\\s+","").trim().isEmpty()?this.className + "_tbl_SFW":newClassName;
//        this.newClassName = this.className + "_tbl_SFW";
        this.useRetrofitAnnotationForColumnName=useRetrofitAnnotationForColumnName;
    }

    String[] getEntities() {
        return entities;
    }

    String getClassName() {
        return className;
    }

    String getPackageName() {
        return packageName;
    }

    String getFullName() {
        return fullName;
    }

    String getNewClassName() {
        return newClassName;
    }

    @Override
    public String toString() {
        return "EntityModel{" +
                "entities=" + Arrays.toString(entities) +
                ", className='" + className + '\'' +
                ", packageName='" + packageName + '\'' +
                ", fullName='" + fullName + '\'' +
                ", newClassName='" + newClassName + '\'' +
                ", useRetrofitAnnotationForColumnName=" + useRetrofitAnnotationForColumnName +
                '}';
    }
}
